<?php

$_['text_title_filter']         = 'Фильтр';

$_['column_filter_field']       = 'Поле';
$_['column_filter_condition']   = 'Сравнение';
$_['column_filter_value']       = 'Значение';

$_['button_filter']             = 'Фильтровать';
$_['button_close']              = 'Закрыть';
$_['button_reset']              = 'Сбросить';

$_['entry_filter_name']         = 'Название фильтра';

$_['text_total_documents']      = 'Всего: ';
$_['text_show_documents']       = 'по: ';
