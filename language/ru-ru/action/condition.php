<?php

$_['heading_title']                         = 'Условие';


#
$_['entry_first_value']                    = 'Что сравниваем';
$_['help_first_value']                    = 'Выберите поле и его метод (при необходимости) в качестве первого аргумента сравнения';
$_['entry_first_value_field']                    = 'Поле текущего документа';
$_['entry_second_value']                    = 'С чем сравниваем';
$_['help_second_value']                    = 'Выберите поле и его метод (при необходимости) или введите значение вручную в качестве второго аргумента сравнения';
$_['entry_second_value_field']                    = 'Поле текущего документа';


$_['text_comparison_method']                = 'Метод сравнения';
$_['text_tab_condition']                    = 'Условие';
$_['text_tab_actions_true']                 = 'Условие верно';
$_['text_tab_actions_false']                = 'Условие ложно';
$_['text_add_action']                       = 'Добавить действие';
$_['text_field']                            = 'с содержимым поля';
$_['text_manual_input']                     = 'со значением';
$_['text_variable']                         = 'c переменной';
$_['text_log']                              = 'Поле %s %s %s';

$_['help_second_value_type']               = 'Тип второго значения условия';

$_['text_condition']                       = 'ЕСЛИ %s %s %s, ТО:';
$_['text_condition_else']                  = 'ИНАЧЕ:';

#

