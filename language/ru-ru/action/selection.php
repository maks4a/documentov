<?php

$_['entry_field_document']                      = 'Поле с документами';
$_['entry_condition_selection']                 = 'Условия выборки';
$_['entry_field_result']                        = 'Поля для записи результата';
$_['entry_document_source']                     = 'Источник документов';
$_['entry_doctype_list']                        = 'выберите тип документов';
$_['entry_document_field']                      = 'выберите поле с документами';
$_['entry_doctype_field']                       = 'выберите поле с типом документа';

$_['help_condition_selection']                  = 'Введите условия, на основании которых будет производится выборка документов';
$_['help_field_result']                         = 'Поле, в которое будут записаны идентификаторы документов, соответствующих условиям выборки';
$_['help_document_source']                      = 'Укажите источник документов, для которых будут применены условия выборки';
$_['help_doctype_list']                         = 'Условия выборки будут применены ко всем документам выбранного типа';
$_['help_document_field']                       = 'Условия выборки будут применены ко всем документам из выбранного поля';
$_['help_doctype_field']                        = 'Условия выборки будут применены ко всем документам имеюющих тип, находящийся в выбранном поле (типов может быть несколько)';

$_['column_field_1']                            = 'Поле';
$_['column_comparison']                         = 'Сравнение';
$_['column_field_2']                            = 'Значение';
$_['column_field_target']                       = 'В поле';
$_['column_field_source']                       = 'Из поля';


$_['text_and']                                  = 'И';
$_['text_or']                                   = 'ИЛИ';
$_['text_description_document_field']           = 'Выбираются документы из поля %s';
$_['text_description_doctype_field']            = 'Выбираются все документы типа из поля %s';
$_['text_description_doctype_list']             = 'Выбираются все документы типа %s';
$_['text_description_field_result']             = 'Результат записывается в поле %s';
$_['text_description_without_field_document']   = 'Нет документов для выборки';
$_['text_document_source_document_field']       = 'Документы из поля';
$_['text_document_source_doctype_field_list']   = 'Все документы заданного типа, выбранного из списка';
$_['text_document_source_doctype_field']        = 'Все документы заданного типа, полученного из поля';