<?php
$_['heading_title']               = 'Таблица';
$_['text_add_button']             = 'Добавить столбец';
$_['text_add_field ']                = 'Добавить столбец';
$_['entry_column_title']           = 'Заголовок столбца';
$_['entry_inner_field_type']       = 'Поле';
$_['text_add_field']               = 'Добавить столбец';
$_['text_title_edit_row']           = 'Редактирование строки таблицы';
