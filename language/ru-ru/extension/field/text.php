<?php

$_['heading_title']                     = 'Текст';

$_['text_success']                      = 'Настройки поля сохранены';
$_['text_extension']                    = 'Поля';
$_['text_field_description_default']    = 'Задано значение по умолчанию';
$_['text_description_editor_enabled']   = 'Отображать редактор';
$_['text_method_get_first_chars']       =  'получить первые n симоволов';
$_['text_method_append_text']           =  'добавить текст в конец поля';
$_['text_method_append_text_new_line']  =  'добавить текст в конец поля на новую строку';
$_['text_method_insert_text']           =  'вставить текст в начало поля';
$_['text_method_insert_text_new_line']  =  'вставить текст в начало поля на новую строку';
$_['text_method_append_text_separator'] =  'добавить текст в конец поля через разделитель';
$_['text_method_param_source']          =  'Добавляемый текст';
$_['text_method_param_char_count']      = 'количество символов';



$_['error_permission']                  = 'У вас нет прав на редактирование поля';
$_['entry_editor_enabled']              = 'Отображать редактор';
$_['entry_default']                     = 'Значение по умолчанию';
$_['entry_char_count']                  = 'Количество символов';
$_['entry_method_insert_text_separator']    = 'Введите разделитель';

$_['help_editor_enabled']               = 'Отображать редактор для текста';
$_['help_default']                      = 'Значение, которое будет автоматически записано в поле при создании документа';
$_['help_char_count']                   = 'Количество символов';


$_['action_setting']                    = 'Нет настроек';