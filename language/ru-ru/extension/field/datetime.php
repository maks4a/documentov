<?php

$_['heading_title']                         = 'Время';

$_['text_success']                          = 'Настройки поля сохранены';
$_['text_extension']                        = 'Поля';
$_['text_description_format']               = 'Формат: %s';
$_['text_description_now_by_default']       = 'Текущая дата по умолчанию';

$_['error_permission']                      = 'У вас нет прав на редактирование поля';
$_['entry_format']                          = 'Формат';
$_['entry_default']                         = 'По умолчанию отображать текущую дату';
$_['entry_method_get_difference_datefld']   = 'другое поле документа-источника:';

$_['help_default']                          = 'Устанавливать ли текущую дату по умолчанию при создании документа польователем';
$_['help_method_get_difference_datefld']    = 'другое поле документа-источника, содержащее дату';

$_['text_method_get_display_value']         = 'получить отображаемое значение';
$_['text_method_get_year']                  = 'получить год';
$_['text_method_get_month']                 = 'получить месяц';
$_['text_method_get_day']                   = 'получить день';
$_['text_method_get_hour']                  = 'получить час';
$_['text_method_get_min']                   = 'получить минуту';
$_['text_method_get_sec']                   = 'получить секунду';
$_['text_method_get_number_day']            = 'получить порядковый номер дня в году';
$_['text_method_get_number_week']           = 'получить порядковый номер недели в году';
$_['text_method_get_day']                   = 'получить день';
$_['text_method_get_difference']            = 'разница в днях с другим временем';
$_['text_method_get_difference_h']          = 'разница в часах с другим временем';
$_['text_method_get_difference_m']          = 'разница в минутах с другим временем';
$_['text_method_adjust_date_plus']          = 'увеличить дату на количество дней';
$_['text_method_adjust_month_plus']         = 'увеличить дату на количество месяцев';
$_['text_method_adjust_year_plus']          = 'увеличить дату на количество лет';
$_['text_method_adjust_minute_plus']        = 'увеличить время на количество минут';
$_['text_method_adjust_hour_plus']          = 'увеличить время на количество часов';
$_['text_method_adjust_date_minus']         = 'уменьшить дату на количество дней';
$_['text_method_adjust_month_minus']        = 'уменьшить дату на количество месяцев';
$_['text_method_adjust_year_minus']         = 'уменьшить дату на количество лет';
$_['text_method_adjust_minute_minus']       = 'уменьшить время на количество минут';
$_['text_method_adjust_hour_minus']         = 'уменьшить время на количество часов';

$_['text_format_dqmqY_Heies']               = 'ДД.ММ.ГГГГ ЧЧ:ММ:СС';
$_['text_format_dqmqY_Hei']                 = 'ДД.ММ.ГГГГ ЧЧ:ММ';
$_['text_format_dvmvY_Heies']               = 'ДД/ММ/ГГГГ ЧЧ:ММ:СС';
$_['text_format_dvmvY_Hei']                 = 'ДД/ММ/ГГГГ ЧЧ:ММ';
$_['text_format_mxdxY_Heies']               = 'ММ-ДД-ГГГГ ЧЧ:ММ:СС';
$_['text_format_mxdxY_Hei']                 = 'ММ-ДД-ГГГГ ЧЧ:ММ';
$_['text_format_Yxmxd_Heies']               = 'ГГГГ-ММ-ДД ЧЧ:ММ:СС';
$_['text_format_Yxmxd_Hei']                 = 'ГГГГ-ММ-ДД ЧЧ:ММ';
$_['text_format_Heies']                     = 'ЧЧ:ММ:СС';
$_['text_format_Hei']                       = 'ЧЧ:ММ';
$_['text_format_dqmqY']                     = 'ДД.ММ.ГГГГ';
$_['text_format_dvmvY']                     = 'ДД/ММ/ГГГГ';
$_['text_format_mxdxY']                     = 'ММ-ДД-ГГГГ';
$_['text_format_Yxmxd']                     = 'ГГГГ-ММ-ДД';
$_['text_format_Yxm']                       = 'ГГГГ-ММ';
$_['text_format_yxm']                       = 'ГГ-ММ';
$_['text_format_mxd']                       = 'ММ-ДД';

$_['action_setting']                        = 'Нет настроек';








