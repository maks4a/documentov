<?php
// Heading
$_['heading_title'] = 'Services';

// Text
$_['text_success'] = 'Settings successfully changed';
$_['text_confirm'] = 'Deinstralating the service will delete all its data and settings \r\n\r\nAre you sure you want to delete this service?';
// Column
$_['column_name'] = 'Name';
$_['column_status'] = 'Status';
$_['column_action'] = 'Service';