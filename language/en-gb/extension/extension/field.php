<?php
// Heading
$_['heading_title'] = 'Fields';

// Text
$_['text_success'] = 'Settings successfully changed!';
$_['text_list'] = 'Module List';
$_['text_confirm'] = 'Deleting a field will delete all its tables. If the field was used and data was entered in it, this data will be lost! \r\n\r\nAre you sure you want to delete the field? ';

// Column
$_['column_name'] = 'Name';
$_['column_status'] = 'Status';
$_['column_action'] = 'Action';

// Error
$_['error_permission'] = 'You do not have permission to edit the Fields';
$_['error_is_in_use_doctypes'] =' The field %s is used in the following document types: %s. Removal is prohibited ';

