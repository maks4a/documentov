<?php
// Heading
$_['heading_title'] = 'Actions';

// Text
$_['text_success'] = 'Settings successfully changed!';
$_['text_list'] = 'Module List';
$_['text_confirm'] = 'Are you sure you want to delete this action?';

// Column
$_['column_name'] = 'Name';
$_['column_status'] = 'Status';
$_['column_action'] = 'Action';

// Error
$_['error_permission'] = 'You do not have permission to edit Actions';
$_['error_is_in_use_doctypes_folders'] =' Action %s is used in the following document types:%s. The same action is used in logs:%s. You can not delete. ';
$_['error_is_in_use_doctypes'] =' Action %s is used in the following document types:%s. You can not delete. ';
$_['error_is_in_use_folders'] =' Action %s is used by logs:%s. You can not delete. ';
$_['error_remove_unavaliable'] = "Unable to delete";

