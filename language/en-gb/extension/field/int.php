<?php

$_['heading_title'] = 'Number (integer)';

$_['text_success'] = 'Field settings saved';
$_['text_extension'] = 'Fields';
$_['text_description_delimiter'] = 'Delimiter: %s';
$_['text_description_default'] = 'Default value: %s';
$_['text_description_min'] = 'Minimum value: %s';
$_['text_description_max'] = 'Maximum value: %s';

$_['help_delimiter'] = 'Defines how to separate triads when displaying a number (space, period, comma and without delimiter)';

$_['error_permission'] = 'You do not have edit access to the field';
$_['entry_delimiter'] = 'Delimiter';
$_['entry_default'] = 'Default value';
$_['entry_min'] = 'Minimum value of the field';
$_['entry_max'] = 'The maximum value of the field';
$_['entry_char_count'] = 'Number of characters';
        
$_['help_default'] = 'The value that this field accepts by default when creating the document by the user';
$_['help_char_count'] = 'The method will return a number with an unsigned number, adding it with leading zeros if necessary';

$_['text_without_delimiter'] = 'No separator';
$_['text_space'] = 'Space';
$_['text_dot'] = 'Point';
$_['text_comma'] = 'Comma';

$_['text_method_add'] = 'add';
$_['text_method_subtract'] = 'take away';
$_['text_method_multiply'] = 'multiply';
$_['text_method_divide'] = 'split';
$_['text_method_remainder'] = 'remainder of division';
$_['text_method_add_leading_zeros'] = 'supplement the number with leading zeros';

$_['action_setting'] = 'No settings';