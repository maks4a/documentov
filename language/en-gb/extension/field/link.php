<?php

$_['heading_title'] = 'Link';

$_['text_success'] = 'Field settings saved';
$_['text_extension'] = 'Fields';
$_['text_select_doctype'] = '--- First select the document type ---';
$_['text_description_doctype'] = 'Link to document type %s';
$_['text_description_field'] = 'Display field %s';

$_['text_method_get_first_link'] = 'get the first field value identifier';
$_['text_method_get_first_link_url'] = 'get the url of the first link in the field';
$_['text_method_get_first_link_text'] = 'get the text of the first link from the field';
$_['text_method_get_display_value_not_link'] = 'get the display value of the field without hyperlinks';
$_['text_method_get_display_value_link'] = 'get the displayed field value with hyperlinks';
$_['text_method_get_another_field_value']        = 'получить значение поля документа по ссылке';
$_['text_method_get_another_field_display']     = 'получить отображаемое значение заданного поля документа по ссылке';
$_['text_method_append_link'] = 'add the contents of another field';
$_['text_method_remove_link'] = 'delete the contents of another field';


$_['error_permission'] = 'You do not have edit access to the field';

$_['entry_doctype'] = 'Document Type';
$_['entry_field'] = 'Field';
$_['entry_multi_select'] = 'Multiple choice';
$_['entry_list'] = 'Show list';
$_['entry_href'] = 'Remove hyperlink';
$_['entry_delimiter'] = 'Delimiter';
$_['entry_method_get_another_field'] = 'Выберите поле, значение которого будет получено';

$_['help_doctype'] = 'Select the type of documents that can be in the link';
$_['help_field'] = 'Select the field of the document type whose value will be displayed in the link';
$_['help_multi_select'] = 'With multiple selections, the user can select multiple values ​​in the field';
$_['help_list'] = 'Show in form as a drop-down list is not recommended for a large number of entries (by default, it is displayed as a field with autocomplete)';
$_['help_href'] = 'Show the value of the field in the view mode without hyperlink to the selected document';
$_['help_delimiter'] = 'In the view mode, separate the field values ​​with this delimiter for multiple selections';


$_['action_setting'] = 'No settings';