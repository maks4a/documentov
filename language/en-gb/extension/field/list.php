<?php

$_['heading_title'] = 'List';

$_['text_success'] = 'List field settings have been saved';
$_['text_value_list_value'] = 'Value';
$_['text_value_list_title'] = 'Header';
$_['text_button_add_value'] = 'Add option';
$_['text_button_del_value'] = 'Delete option';
$_['text_vertical'] = 'Vertical';
$_['text_horizontal'] = 'Horizontal';
$_['text_list'] = 'Drop-down list';
$_['text_action'] = 'Actions';
$_['text_method_get_display_value'] = 'get the displayed text value of the selected option';
$_['text_method_set_by_title'] = 'find and write the value by title';
$_['text_value_default'] = 'By default';
$_['text_title_multiple_list'] = 'Hold Ctrl or Shift (or move the mouse with the left button pressed) to select multiple lines';

$_['action_setting'] = 'No settings';

$_['entry_table_list_values'] = 'List values';
$_['entry_multi_select'] = 'Multiple choice';
$_['entry_visualization'] = 'Display';

$_['help_table_list_values'] = 'The value of the variant will be returned by default; The headings are displayed for the user, depending on the language selected; Order determines the sorting of variants (in ascending order)';
$_['help_multi_select'] = 'If set to Yes, the user can select several options from the list; if not, then only one';
$_['help_visualization'] = 'How to display a list - horizontally or vertically';

$_['description_list'] = 'Options';
$_['description_list_empty'] = 'The list is empty';