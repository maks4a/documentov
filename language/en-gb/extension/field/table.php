<?php

$_['heading_title'] = 'Table';
$_['text_add_button'] = 'Add a column';
$_['text_add_field'] = 'Add a column';
$_['entry_column_title'] = 'Column Header';
$_['entry_inner_field_type'] = 'Field';
$_['text_add_field'] = 'Add a column';
$_['text_title_edit_row'] = 'Editing a table row';
