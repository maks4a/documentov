<?php

$_['heading_title'] = 'Time';

$_['text_success'] = 'Field settings saved';
$_['text_extension'] = 'Fields';
$_['text_description_format'] = 'Format:%s';
$_['text_description_now_by_default'] = 'The current default date';

$_['error_permission'] = 'You do not have edit access to the field';
$_['entry_format'] = 'Format';
$_['entry_default'] = 'By default, display the current date';
$_['entry_method_get_difference_datefld'] = 'other field of the source document:';

$_['help_default'] = 'Do you want to set the current date by default when creating a document by the user';
$_['help_method_get_difference_datefld'] = 'other field of the source document containing the date';

$_['text_method_get_display_value']         = 'получить отображаемое значение';
$_['text_method_get_year'] = 'get the year';
$_['text_method_get_month'] = 'get the month';
$_['text_method_get_day'] = 'get the day';
$_['text_method_get_hour'] = 'get the hour';
$_['text_method_get_min'] = 'get a minute';
$_['text_method_get_sec'] = 'get second';
$_['text_method_get_number_day'] = 'get the ordinal number of the day in the year';
$_['text_method_get_number_week'] = 'get the week ordinal of the year';
$_['text_method_get_day'] = 'get the day';
$_['text_method_get_difference'] = 'difference in days with another source document field';
$_['text_method_adjust_date_plus'] = 'increase the date by the number of days';
$_['text_method_adjust_date_minus'] = 'decrease the date by the number of days';

$_['text_format_dqmqY_Heies'] = 'DD.MM.YYYY HH: MM: SS';
$_['text_format_dvmvY_Heies'] = 'DD / MM / YYYY HH: MM: SS';
$_['text_format_mxdxY_Heies'] = 'MM-DD-YYYY HH: MM: SS';
$_['text_format_Yxmxd_Heies'] = 'YYYY-MM-DD HH: MM: SS';
$_['text_format_dqmqY'] = 'DD.MM.YYYY';
$_['text_format_dvmvY'] = 'DD / MM / YYYY';
$_['text_format_mxdxY'] = 'MM-DD-YYYY';
$_['text_format_Yxmxd'] = 'YYYY-MM-DD';
$_['text_format_Yxm'] = 'YYYY-MM';
$_['text_format_yxm'] = 'YY-MM';
$_['text_format_mxd'] = 'MM-DD';

$_['action_setting'] = 'No settings';








