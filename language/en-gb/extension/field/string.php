<?php

$_['heading_title'] = 'Line';

$_['text_success'] = 'Field settings saved';
$_['text_extension'] = 'Fields';
$_['text_description_mask'] = 'Input mask: %s';
$_['text_description_default'] = 'Default value: %s';
$_['text_method_get_first_chars'] = 'get the first n characters';
$_['text_method_append_text'] = 'add text to the end of the field';
$_['text_method_insert_text'] = 'insert text at the beginning of the field';


$_['error_permission'] = 'You do not have edit access to the field';

$_['entry_mask'] = 'Input mask';
$_['entry_default'] = 'Default value';

$_['help_mask'] = 'You can use the input mask (9 is any number, a is any letter, * is any character (for example, the mask for phone number is +9 (999) 999-99-99';
$_['help_default'] = 'The value that this field accepts by default when creating the document by the user';


$_['action_setting'] = 'No settings';