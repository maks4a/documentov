<?php

$_['heading_title'] = 'Text';

$_['text_success'] = 'Field settings saved';
$_['text_extension'] = 'Fields';
$_['text_field_description_default'] = 'The default value is set to';
$_['text_description_editor_enabled'] = 'Show editor';
$_['text_method_get_first_chars'] = 'get the first n characters';
$_['text_method_append_text'] = 'add text to the end of the field';
$_['text_method_append_text_new_line'] = 'add text to the end of the field to a new line';
$_['text_method_insert_text'] = 'insert text at the beginning of the field';
$_['text_method_insert_text_new_line'] = 'insert text at the beginning of the field to a new line';

$_['error_permission'] = 'You do not have edit access to the field';
$_['entry_editor_enabled'] = 'Show editor';
$_['entry_default'] = 'Default value';
$_['entry_char_count'] = 'Number of characters';

$_['text_method_append_text_separator'] =  'добавить текст в конец поля через разделитель';

$_['help_editor_enabled'] = 'Show editor for text';
$_['help_default'] = 'The value that will be automatically written in the field when creating the document';
$_['help_char_count'] = 'Number of characters';


$_['button_select_file']                = 'Выбрать файл';


$_['action_setting'] = 'No settings';