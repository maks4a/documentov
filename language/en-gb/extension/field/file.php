<?php

$_['heading_title']                 = 'File';
$_['locale_widget_messages']        = 'upload: \'Загрузить\',abort: \'Остановить\',remove: \'Удалить\',complete: \'Готово\',error: \'Ошибка\',errorFilesLimit: \'Количество выбранных файлов превышает лимит (%filesLimit%)\',errorSizeLimit: \'Файл "%fileName%" превышает предельный размер (%sizeLimit%)\',errorFileType: \'Файл "%fileName%" является некорректным\',errorOldBrowser: \'Ваш браузер не может загружать файлы. Обновите его до последней версии\'';

$_['entry_file_ext_allowed']        = 'Разрешенные для загрузки расширения файлов';
$_['entry_file_mime_allowed']       = 'Разрешенные для загрузки форматы файлов (MIME)';
$_['entry_preview']                 = 'Предварительный просмотр';
$_['entry_preview_width']           = 'Ширина, пикс';
$_['entry_preview_height']          = 'Высота, пикс';
$_['entry_preview_link']            = 'Скрыть ссылку';
$_['entry_size_file']               = 'Максимальный размер загружаемого файла';
$_['entry_limit_files']             = 'Максимальное количество загружаемых файлов';
$_['entry_delimiter']               = 'Разделитель между файлами';

$_['help_file_ext_allowed']         = 'Каждое создаваемое поле файлового типа будет по умолчанию содержать перечисленные расширения в качестве разращенных. Лишние расширения можно убрать в настройках каждого конкретного поля';
$_['help_file_mime_allowed']         = 'Каждое создаваемое поле файлового типа будет по умолчанию содержать перечисленные MIME-форматыы в качестве разращенных. Лишние форматы можно убрать в настройках каждого конкретного поля';
$_['help_preview']                  = 'При установленном предварительном просмотре в документе будет показываться изображение файла соответствующего размера. Актуально для форматов JPG, PNG, GIF, PDF';
$_['help_preview_width']            = 'Максимальная ширина изображения предварительного просмотра. Если значение пусто или равно 0, то ширина изменяться не будет';
$_['help_preview_height']           = 'Максимальная высота изображения предварительного просмотра. Если значение пусто или равно 0, то высота изменяться не будет';
$_['help_preview_link']             = 'Не показывать гиперссылку на исходный файл';
$_['help_size_file']                = 'Ограничение на размер загружаемого файла (в килобайтах). Если значение пусто или равно 0, то размер файла будет ограничен только настройками в php.ini';
$_['help_limit_files']              = 'Максимальное количество файлов, которое может быть загружено. Если значение пусто или равно 0, то ограничения нет';
$_['help_delimiter']                = 'Если будет загружено несколько файлов, то в режиме просмотра между ними будет использоваться этот разделитель';

$_['text_success']                  = 'Настройки файлового поля изменены';

$_['error_filetype']                = 'Данный тип файла не разрешен для загрузки';
$_['error_filename']                = 'Неверное имя файла. Оно должно быть не менее 3 и не более 250 символов';
$_['error_upload']                  = 'Ошибка при загрузке';
$_['error_filesize']                = 'Размер файла превышает разрешенный. Обратите к администратору.';

$_['description_preview']           = 'Предварительный просмотр';
$_['description_size_file']         = 'Ограничен максимальный размер файла';
$_['description_limit_files']       = 'Ограничено количество загружаемых файлов до %s';

$_['button_select_files']           = 'Select files';
$_['button_select_file']            = 'Select file';


$_['text_method_get_files_with_links']  = 'получить файлы с гиперссылками';
$_['text_method_get_number_files']  = 'получить количество файлов в поле';
$_['text_method_append_file']           =  'добавить к содержимому поля';
$_['text_method_remove_file']           =  'удалить из содержимого поля';
$_['text_no_files']                     = 'Нет загруженных файлов';