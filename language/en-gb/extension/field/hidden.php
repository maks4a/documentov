<?php

$_['heading_title'] = 'Password';

$_['text_success'] = 'Field settings saved';
$_['text_extension'] = 'Fields';
$_['text_1_hash'] = 'Unidirectional';
$_['text_2_hash'] = 'Bidirectional';
$_['text_1_hash_description'] = 'Unidirectional hashing';
$_['text_2_hash_description'] = 'Bidirectional hashing';
$_['text_method_get_decrypt_value'] = 'Get the decrypted value';
        
$_['error_permission'] = 'You do not have edit access to the field';

$_['help_type_hash'] = 'Unidirectional (secure) or bi-directional (less secure) hashing. With twelve-level encryption, the field will have a method that returns the value entered by the user, in unidirectional - the initial value can not be obtained ';
$_['entry_type_hash'] = 'Hash Type';


$_['action_setting'] = 'No settings';