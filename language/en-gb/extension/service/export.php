<?php

$_['heading_title'] = 'Export / import configuration';

$_['service_setting'] = 'The service does not have global settings';

$_['tab_export'] = 'Export';
$_['tab_import'] = 'Import';

$_['column_doctype'] = 'Document Type';
$_['column_folder'] = 'Journal';

$_['entry_select_doctype'] = 'Exported document types';
$_['entry_select_folder'] = 'Exported Document Journals';

$_['help_select_doctype'] = 'Select the types of documents to be exported';
$_['help_select_folder'] = 'Select the document logs to be exported';
$_['help_column_document'] = 'Export not only the document type, but all documents of this type';

$_['button_upload'] = 'Unload';
$_['button_download'] = 'Download';

$_['error_filetype'] = 'Invalid file type';
$_['error_content'] = 'The file does not contain data to import';
$_['error_doctype_exists'] =' An imported document type or journal already exists. If you want to still replace this document type or journal, select the Overwrite existing document types and logs radio button';

$_['text_import_success'] = 'Import completed';
$_['text_force'] = 'Overwrite existing document types (logs) if they exist';