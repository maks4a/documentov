<?php

$_['heading_title'] = 'Demon';

$_['service_setting'] = 'The service does not have global settings';

$_['button_demon_start'] = 'Launch daemon';
$_['button_demon_restart'] = 'Restart the daemon';
$_['button_demon_stop'] = 'Stop Demon';
$_['text_daemon_task_log'] = 'Log:';
$_['button_refresh'] = 'Refresh';
$_['text_demon_started'] = 'Demon is running';
$_['text_demon_stopped'] = 'The daemon is stopped';
$_['text_win_instruction'] = 'To open the daemon, open the Windows command line on the server and enter the <code> php %s / daemon.php start </ code> command. Do not close the window. ';