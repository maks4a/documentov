<?php

$_['heading_title'] = 'Usage statistics';

$_['service_setting'] = 'The service does not have global settings';

$_['text_type_login'] = 'Login to the system';
$_['text_params_type_login'] = 'Username: %s';
$_['text_type_login_fail'] = 'Error logging in';
$_['text_params_type_login_fail'] = 'User name %s' ;;
$_['text_type_open_folder'] = 'Open log';
$_['text_params_type_open_folder'] = '<a href="%s"> %s </a>';
$_['text_type_open_document'] = 'Opening a document';
$_['text_type_create_document0'] = 'Create a document';
$_['text_type_create_document'] = 'Created document';
$_['text_params_type_open_document'] = '<a href="%s"> %s </a>';
$_['text_params_type_create_document0'] = 'Create document %s';
$_['text_params_type_create_document_1'] = 'Created a <a href="%s"> document %s </a>';
$_['text_params_type_create_document_2'] = 'Created document %s';
$_['text_type_execute_folder_button'] = 'Clicking on the log button';
$_['text_params_type_execute_folder_button'] = ' %s in the <a href="%s"> journal %s </a>';
$_['text_type_execute_document_button'] = 'Clicking on the document button';
$_['text_params_type_execute_document_button'] = ' %s in the <a href="%s"> document %s </a>' ;;
$_['text_link'] = 'Link';
$_['text_customers_online'] = 'Users in the system:';
$_['text_customers_last_hour'] = 'Users in the last hour:';
$_['text_customers_today'] = 'Users today:';
$_['text_customers_yesterday'] = 'Users yesterday:';
$_['text_total_events'] = 'Total events:';
$_['text_show_events'] = 'show by';
$_['text_filter'] = 'Filter';
$_['text_empty_button'] = '[Button without name and action]';
$_['text_document_remove'] = 'The document was later deleted';
$_['text_folder_remove'] = 'The log was later deleted';
$_['text_doctype_remove'] = 'The type of this document was later deleted';

$_['column_date'] = 'Time';
$_['column_date1'] = 'Time after';
$_['column_date2'] = 'Time to';
$_['column_customer'] = 'User';
$_['column_event'] = 'Event';
$_['column_description'] = 'Description';
$_['column_ip'] = 'IP address';
