<?php

$_['text_access_set'] = 'Access is set';
$_['text_action_setting_error'] = 'Action Access is not configured correctly';

$_['heading_title'] = 'Access';

$_['text_success'] = 'Action settings saved';
$_['text_extension'] = 'Actions';

$_['error_permission'] = 'You do not have permission to edit the action';

$_['action_setting'] = 'Action has no settings';