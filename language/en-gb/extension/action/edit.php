<?php

$_['on_edit'] = 'translated the document into editing';
$_['on_save'] = 'saved the edited document';
$_['heading_title'] = 'Editing';

$_['text_success'] = 'Action settings saved';
$_['text_extension'] = 'Actions';

$_['error_permission'] = 'You do not have permission to edit the action';
$_['error_no_document']                 = 'Не найден документ для редактирования';

$_['action_setting'] = 'Action has no settings';
