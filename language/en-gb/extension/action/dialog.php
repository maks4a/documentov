<?php

$_['text_log'] = 'Action completed';

$_['heading_title'] = 'Dialog';

$_['text_success'] = 'Action settings saved';
$_['text_extension'] = 'Actions';

$_['error_permission'] = 'You do not have permission to edit the action';

$_['action_setting'] = 'Action has no settings';