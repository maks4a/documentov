<?php

$_['heading_title'] = 'Selection';

$_['text_success'] = 'Action settings saved';
$_['text_extension'] = 'Actions';

$_['error_permission'] = 'You do not have permission to edit the action';

$_['action_setting'] = 'Action has no settings';

