<?php

$_['heading_title'] = 'Record';

$_['text_success'] = 'Action settings saved';
$_['text_extension'] = 'Actions';

$_['error_permission'] = 'You do not have permission to edit the action';

$_['action_setting'] = 'Action has no settings';

$_['text_log'] = 'An entry was made in the field %s: %s';
