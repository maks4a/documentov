<?php

$_['text_title_filter']         = 'Filter';

$_['column_filter_field']       = 'Field';
$_['column_filter_condition']   = 'Comparison';
$_['column_filter_value']       = 'Value';

$_['button_filter']             = 'Filter';
$_['button_close']              = 'Close';
$_['button_reset']              = 'Reset';

$_['entry_filter_name']         = 'Filter name';

$_['text_total_documents']      = 'Total: ';
$_['text_show_documents']       = 'show by: ';
