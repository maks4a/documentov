<?php

$_['text_home'] = 'Home';
$_['text_delete_document_draft'] = 'Delete Draft';
$_['text_document_draft'] = 'Unsaved draft not shown';

$_['text_error_autocomplete_setting'] = 'Invalid field settings for displaying a list of values';

$_['template_error'] = 'Error while displaying the document template';
$_['error_field_twice'] = 'One and the same field is placed in the template more than once';
$_['error_validation_required'] = 'Required';
$_['error_validation_unique'] = 'You must enter a unique value for';