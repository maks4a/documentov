<?php
// Heading
$_['heading_title'] = 'Menu Items';

// Text
$_['text_success'] = 'Settings successfully changed!';
$_['text_list'] = 'List of menu items';
$_['text_add'] = 'Add';
$_['text_edit'] = 'Edit';
$_['text_action_folder'] = 'open folder';
$_['text_action_link'] = 'follow the link';
$_['text_type_text'] = 'text';
$_['text_type_divider'] = 'separator';
$_['text_confirm'] = 'Are you sure you want to delete the selected menu items?';
$_['text_hide_menu_0'] = 'Show the name of the menu and the image';
$_['text_hide_menu'] = 'Hide title with screen width less than';
$_['text_hide_menu_px'] = 'pixels';

// Column
$_['column_name'] = 'Items';
$_['column_sort_order'] = 'Sorting order';
$_['column_action'] = 'Action';

// Entry
$_['entry_name'] = 'Name';
$_['entry_parent'] = 'Parent menu';
$_['entry_filter'] = 'Filters';
$_['entry_image'] = 'Image of the item';
$_['entry_sort_order'] = 'Sorting order';
$_['entry_status'] = 'Status';
$_['entry_action'] = 'Action';
$_['entry_type'] = 'Type';
$_['entry_item_structure'] = 'To show';

// Help
$_['help_filter'] = '(AutoFill)';
$_['help_name'] = 'Enter the name of the menu item';
$_['help_action'] = 'Click action - open the document log or go hyperlink';
$_['help_type'] = 'Type of menu item: text or delimiter';
$_['help_item_structure'] = 'Choose from the department structure and / or employees to display this menu item';
        
// Error
$_['error_warning'] = 'Carefully check the form for errors!';
$_['error_permission'] = 'You do not have the right to change categories!';
$_['error_name'] = 'The category name must be between 1 and 255 characters!';
$_['error_meta_title'] = 'The keyword must be between 1 and 255 characters!';
$_['error_keyword'] = 'SEO URL is busy!';
$_['error_unique'] = 'SEO URL must be unique!';
$_['error_parent'] = 'Parent category is not selected correctly!';