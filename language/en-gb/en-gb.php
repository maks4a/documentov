<?php
// Locale
$_['code']                  = 'en';
$_['direction']             = 'ltr';
$_['date_format_short']     = 'd/m/Y';
$_['date_format_long']      = 'l dS F Y';
$_['time_format']           = 'h:i:s A';
$_['datetime_format']       = 'd/m/Y H:i:s';
$_['decimal_point']         = '.';
$_['thousand_point']        = ',';

// Text
$_['text_enabled']                  = 'Enable';
$_['text_disabled']                 = 'Disable';
$_['text_back']                     = 'Назад';
$_['text_forward']                  = 'Вперед';
$_['text_home']             = '<i class="fa fa-home"></i>';
$_['text_yes']              = 'Yes';
$_['text_no']               = 'No';
$_['text_none']             = ' --- None --- ';
$_['text_select']           = ' --- Please Select --- ';
$_['text_select_all']               = 'select all';
$_['text_deselect_all']             = 'deselect';
$_['text_empty_value']              = '< empty >';
$_['text_pagination']       = 'Showing %d to %d of %d (%d Pages)';
$_['text_loading']          = 'Loading...';
$_['text_no_results']       = 'No results!';
$_['text_condition_0']              = 'no select';
$_['text_condition_equal']          = 'equal';
$_['text_condition_notequal']      = 'not equal';
$_['text_condition_more']           = 'more';
$_['text_condition_moreequal']      = 'more or equal';
$_['text_condition_less']           = 'less';
$_['text_condition_lessequal']      = 'less or equal';
$_['text_condition_contains']       = 'contents';
$_['text_condition_notcontains']   = 'not contents';
$_['text_var_author_name']              = 'AUTHOR NAME';
$_['text_var_customer_id']              = 'USER ID';
$_['text_var_customer_name']            = 'ИМЯ ПОЛЬЗОВАТЕЛЯ';
$_['text_var_current_route_uid']         = 'ИДЕНТИФИКАТОР ТОЧКИ МАРШРУТА';
$_['text_var_current_route_name']       = 'НАЗВАНИЕ ТОЧКИ МАРШРУТА';
$_['text_var_current_route_description']= 'ОПИСАНИЕ ТОЧКИ МАРШРУТА';
$_['text_var_current_time']             = 'ТЕКУЩЕЕ ВРЕМЯ';
$_['text_folder']                       = 'Журнал';
$_['text_doctype']                      = 'Тип документа';
$_['text_default_method']           = '==стандартный метод==';
$_['text_system']                       = 'Система';
$_['text_currentdoc']                   = 'ТЕКУЩИЙ ДОКУМЕНТ';
$_['text_by_link_in_field']             = 'по ссылке из поля';
$_['text_macros_field']                     = array(
                            'simple'        => 'обычные поля',
                            'setting'       => 'настроечные поля',
                            'placeholder'   => 'Выберите поле',
                            'none'          => ' --- Не выбрано --- '
);
$_['text_select_field']                 = 'Выберите поле';
$_['text_asc']                      = 'по возрастанию';
$_['text_desc']                     = 'по убыванию';
$_['text_search']                   = 'Искать...';

// Buttons
$_['button_delete']         = 'Delete';
$_['button_download']       = 'Download';
$_['button_edit']           = 'Edit';
$_['button_open']           = 'Open';
$_['button_filter']         = 'Refine Search';
$_['button_login']          = 'Login';
$_['button_update']         = 'Update';
$_['button_remove']         = 'Remove';
$_['button_undo']           = 'Undo';
$_['button_search']         = 'Search';
$_['button_up']             = 'Up';
$_['button_down']           = 'Down';
$_['button_add']            = 'Add';
$_['button_save']           = 'Save';
$_['button_copy']           = 'Copy';
$_['button_cancel']         = 'Cancel';
$_['button_close']          = 'Close';
$_['button_confirm']        = 'Confirm';
$_['button_continue']       = 'Continue';

// Error
$_['error_access_denied']       = 'Access denied';
$_['error_general']             = 'Operation refuce. Error';
$_['error_general_1']           = 'Нет ни одного заполненного поля в сохраняемом документе';s
$_['error_cycle']               = 'Document is unlimited cycle';

/* When doing translations only include the matching language code */

// Datepicker
$_['datepicker']            = 'en-gb';

// Months
$_['January'] = 'January';
$_['February'] = 'February'; 
$_['March'] = 'March';
$_['April'] = 'April';
$_['May'] = 'May';
$_['June'] = 'June';
$_['July'] = 'July';
$_['August'] = 'August';
$_['September'] = 'September';
$_['October'] = 'October';
$_['November'] = 'November';
$_['December'] = 'December';

//Day of week
$_['Sunday'] = 'Su';
$_['Monday'] = 'Mo';
$_['Tuesday'] = 'Tu';
$_['Wednesday'] = 'We';
$_['Thursday'] =  'Th';
$_['Friday'] = 'Fr';
$_['Saturday'] = 'Sa';

$_['tab_data']                      = 'Data';
$_['tab_field']                     = 'Field';
$_['tab_button']                    = 'Button';
$_['tab_buttons']                   = 'Buttons';
$_['tab_action']                    = 'Action';
$_['tab_filter']                    = 'Filter';
$_['tab_route']                     = 'Route';
$_['tab_template']                  = 'Template';
$_['tab_general']                   = 'General';
$_['tab_additional']                = 'Additional';
$_['tab_setting']                   = 'Setting';

$_['column_name']                       = 'Name';
$_['column_description']                = 'Description';
$_['column_created']                    = 'Created';
$_['column_modified']                   = 'Changed';

//text editor
$_['text_editor'] = " 'en': {
        font: {
            bold: 'Bold',
            italic: 'Italic',
            underline: 'Underline',
            clear: 'Remove Font Style',
            height: 'Line Height',
            name: 'Font Family',
            strikethrough: 'Strikethrough',
            subscript: 'Subscript',
            superscript: 'Superscript',
            size: 'Font Size'
        },
        image: {
            image: 'Picture',
            insert: 'Insert Image',
            resizeFull: 'Resize Full',
            resizeHalf: 'Resize Half',
            resizeQuarter: 'Resize Quarter',
            floatLeft: 'Float Left',
            floatRight: 'Float Right',
            floatNone: 'Float None',
            shapeRounded: 'Shape: Rounded',
            shapeCircle: 'Shape: Circle',
            shapeThumbnail: 'Shape: Thumbnail',
            shapeNone: 'Shape: None',
            dragImageHere: 'Drag image or text here',
            dropImage: 'Drop image or Text',
            selectFromFiles: 'Select from files',
            maximumFileSize: 'Maximum file size',
            maximumFileSizeError: 'Maximum file size exceeded.',
            url: 'Image URL',
            remove: 'Remove Image',
            original: 'Original'
        },
        video: {
            video: 'Video',
            videoLink: 'Video Link',
            insert: 'Insert Video',
            url: 'Video URL',
            providers: '(YouTube, Vimeo, Vine, Instagram, DailyMotion or Youku)'
        },
        link: {
            link: 'Link',
            insert: 'Insert Link',
            unlink: 'Unlink',
            edit: 'Edit',
            textToDisplay: 'Text to display',
            url: 'To what URL should this link go?',
            openInNewWindow: 'Open in new window'
        },
        table: {
            table: 'Table',
            addRowAbove: 'Add row above',
            addRowBelow: 'Add row below',
            addColLeft: 'Add column left',
            addColRight: 'Add column right',
            delRow: 'Delete row',
            delCol: 'Delete column',
            delTable: 'Delete table'
        },
        hr: {
            insert: 'Insert Horizontal Rule'
        },
        style: {
            style: 'Style',
            p: 'Normal',
            blockquote: 'Quote',
            pre: 'Code',
            h1: 'Header 1',
            h2: 'Header 2',
            h3: 'Header 3',
            h4: 'Header 4',
            h5: 'Header 5',
            h6: 'Header 6'
        },
        lists: {
            unordered: 'Unordered list',
            ordered: 'Ordered list'
        },
        options: {
            help: 'Help',
            fullscreen: 'Full Screen',
            codeview: 'Code View'
        },
        paragraph: {
            paragraph: 'Paragraph',
            outdent: 'Outdent',
            indent: 'Indent',
            left: 'Align left',
            center: 'Align center',
            right: 'Align right',
            justify: 'Justify full'
        },
        color: {
            recent: 'Recent Color',
            more: 'More Color',
            background: 'Background Color',
            foreground: 'Foreground Color',
            transparent: 'Transparent',
            setTransparent: 'Set transparent',
            reset: 'Reset',
            resetToDefault: 'Reset to default'
        },
        shortcut: {
            shortcuts: 'Keyboard shortcuts',
            close: 'Close',
            textFormatting: 'Text formatting',
            action: 'Action',
            paragraphFormatting: 'Paragraph formatting',
            documentStyle: 'Document Style',
            extraKeys: 'Extra keys'
        },
        help: {
            'insertParagraph': 'Insert Paragraph',
            'undo': 'Undoes the last command',
            'redo': 'Redoes the last command',
            'tab': 'Tab',
            'untab': 'Untab',
            'bold': 'Set a bold style',
            'italic': 'Set a italic style',
            'underline': 'Set a underline style',
            'strikethrough': 'Set a strikethrough style',
            'removeFormat': 'Clean a style',
            'justifyLeft': 'Set left align',
            'justifyCenter': 'Set center align',
            'justifyRight': 'Set right align',
            'justifyFull': 'Set full align',
            'insertUnorderedList': 'Toggle unordered list',
            'insertOrderedList': 'Toggle ordered list',
            'outdent': 'Outdent on current paragraph',
            'indent': 'Indent on current paragraph',
            'formatPara': 'Change current block\'s format as a paragraph(P tag)',
            'formatH1': 'Change current block\'s format as H1',
            'formatH2': 'Change current block\'s format as H2',
            'formatH3': 'Change current block\'s format as H3',
            'formatH4': 'Change current block\'s format as H4',
            'formatH5': 'Change current block\'s format as H5',
            'formatH6': 'Change current block\'s format as H6',
            'insertHorizontalRule': 'Insert horizontal rule',
            'linkDialog.show': 'Show Link Dialog'
        },
        history: {
            undo: 'Undo',
            redo: 'Redo'
        },
        specialChar: {
            specialChar: 'SPECIAL CHARACTERS',
            select: 'Select Special characters'
        },
        documentov: {
            fields: 'Fields',
            variables: 'Variables',
            structure_fields: 'Structure'
        },
    }";