<?php

$_['entry_doctype'] = 'Document Type';
$_['entry_set_field_value'] = 'Match Fields';
$_['entry_target_field_name'] = 'The field of the created document';
$_['entry_source_field_name'] = 'Value from the current document field';
$_['entry_field_new_document'] = 'Write the ID of the new document';
$_['entry_field_new_document_author'] = 'Set author from the field';

$_['help_doctype'] = 'Select the type of document to be created';
$_['help_set_field_value'] = 'Selected values ​​of the current document will be written to the selected fields of the created document';
$_['help_field_new_document'] = 'If necessary, select the field where the ID of the created document will be written';
$_['help_field_new_document_author'] = 'If necessary, select the field from which the author ID of the created document will be received. If there are several employees in the field, an appropriate number of documents will be created when the action is launched through the route. If you do not fill this attribute, the current user will be installed as the author ';

$_['column_field_target'] = 'In the field';
$_['column_field_source'] = 'From field';

$_['text_select_doctype'] = 'Select the document type first';

$_['error_doctype_not_found'] = 'The type of document being created is not selected';

$_['text_document_create'] = 'Created a new <a href=\'%s\' targe=\'_blank\'> document </a>';
$_['text_description'] = 'Create a document of type %s';
$_['text_description_without_doctype'] = 'Document type not selected';

