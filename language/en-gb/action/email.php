<?php

$_['entry_template_message'] = 'Message';
$_['entry_template_subject'] = 'Theme';
$_['entry_field_email'] = 'E-mail';

$_['help_field_email'] = 'A field for specifying the email address where the message will be sent. The field can contain one or more e-mail addresses, separated by a comma ';

$_['text_fields'] = 'Fields';
$_['text_error_empty_to']               = 'Не найден почтовый адрес получателя';
$_['text_error_wrong_email']            = 'Неверный адрес получателя %s';
$_['text_mail_send']                    = 'Отправлены письма на';

$_['description_none_email'] = 'No email address selected';
$_['description_send_email'] = 'sending an email message to the address from the field %s';
$_['description_email_subject'] = 'with the theme %s';

$_['template_error'] = 'Error in the action template Email';