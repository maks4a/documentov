<?php

$_['entry_field_document'] = 'Document to move';
$_['entry_document_route'] = 'Destination point of the route';

$_['help_field_document'] = 'Select the document to move; the document can be current or obtained by reference from the field of the current document ';
$_['help_document_route'] = 'Select the waypoint to which the document selected above will be moved';

$_['text_currentdoc'] = 'CURRENT DOCUMENT';
$_['text_by_link_in_field'] = 'by reference from the field';
$_['text_description_documents'] = 'Documents moved';
$_['text_description_document'] = 'Document moved';
$_['text_description_move_doctype'] = 'Moving document';
$_['text_description_move_route'] = ', at the point';
$_['text_description_move'] = 'Moves';

$_['error_document_not_found'] = 'No documents found to move';
