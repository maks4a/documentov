<?php

$_['entry_timer_id'] = 'Timer ID';
$_['help_timer_action'] = 'Action above the timer';
$_['entry_timer_action'] = 'Action';
$_['text_start'] = 'Start Timer';
$_['text_stop'] = 'Stop the timer';
$_['text_stop_all'] = 'Stop all document timers';
$_['help_identifier_field'] = 'Select the field containing the timer ID. The identifier is useful if you need to turn off the timer before it triggers. The identifier can also be written to a field when the timer is triggered. If this is all you do not need, you can not fill out this parameter ';
$_['entry_identifier_field'] = 'Timer ID';
$_['entry_timer_doclink'] = 'Document';
$_['help_timer_doclink'] = 'Document, or field containing a link to the document to which the timer relates;';
$_['text_target_field_setter'] = 'Use field method:';
$_['help_target_field_setter'] = 'Use field method:';
$_['entry_target_field'] = 'Field to write from the identifier field';
$_['help_target_field'] = 'The field into which the value will be written from the identifier field when the timer is triggered';
$_['text_target_field'] = 'Write the identifier in the field';
$_['entry_exectime_field'] = 'Field containing the date and time of the timer';
$_['help_exectime_field'] = 'Document field with the date and time at which the timer will trigger';
$_['text_exectime_field'] = 'Response time';
$_['text_not_able_to_get_datetime'] = 'Unable to get the timer start time from the field';
$_['text_no_task_in_queue'] = 'No timer tasks in the queue';
$_['help_document_route'] = 'If the timer triggers, go to the point';
$_['entry_document_route'] = 'Jump to a point';
$_['text_description_start_1'] = 'Run timer (current document, start time from field: "%s", identifier from field: "%s")';
$_['text_description_start_2'] = 'Running the timer (document from the field "%s", time to start from the field: "%s", identifier from the field: "%s")';
$_['text_description_start_3'] = 'Run timer (current document, start time from field: "%s")';
$_['text_description_start_4'] = 'Start the timer (the document from the field "%s", the start time from the field: "%s")';
$_['text_description_stop_1'] = 'Stop the timer (the current document, the identifier from the field: "%s")';
$_['text_description_stop_2'] = 'Stop the timer (document from the field "%s", the identifier from the field: "%s")';
$_['text_description_stop_all_1'] = 'Stop all timers (current document)';
$_['text_description_stop_all_2'] = 'Stop all timers (document from the field "%s")';


