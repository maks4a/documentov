<?php

$_['entry_field_document'] = 'Document for redirection';

$_['help_field_document'] = 'Select a field with a link to the redirect document';

$_['text_by_link_in_field'] = 'by reference from the field';
$_['text_description'] = 'redirecting by reference from the field %s';

$_['error_document_not_found'] = 'No document found for redirection';
