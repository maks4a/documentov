<?php

$_['heading_title'] = 'Condition';

$_['entry_first_value'] = 'Field and method of the current document';
$_['help_first_value'] = 'Select the field and its method (if necessary) as the first comparison argument';
$_['entry_first_value_field'] = 'Field of the current document';
$_['entry_second_value'] = 'Second field and method of the current document';
$_['help_second_value'] = 'Select the field and its method (if necessary) as the second comparison argument';
$_['entry_second_value_field'] = 'Second field of the current document';

$_['text_comparison_method'] = 'Comparison method';
$_['text_second_value_type'] = 'Type of second value to compare';
$_['text_second_value_type_field'] = 'Field of the current document';
$_['text_second_value_type_variable'] = 'Variable';
$_['text_tab_condition'] = 'Condition';
$_['text_tab_actions_true'] = 'Condition is true';
$_['text_tab_actions_false'] = 'Condition false';
$_['text_add_action'] = 'Add action';
$_['text_field']                            = 'с содержимым поля';
$_['text_manual_input']                     = 'со значением';
$_['text_log']                              = 'Field %s %s %s';

$_['help_second_value_type'] = 'Type of second condition value';

$_['text_condition'] = 'IF %s %s %s, then:';
$_['text_condition_else'] = 'OTHERWISE:';

#

