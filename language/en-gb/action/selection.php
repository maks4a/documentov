<?php

$_['entry_field_document'] = 'Document field';
$_['entry_condition_selection'] = 'Sample Conditions';
$_['entry_field_result'] = 'Fields for recording the result';
$_['entry_document_source'] = 'Document Source';
$_['entry_doctype_list'] = 'Select the document type';
$_['entry_document_field'] = 'select the field with the documents';
$_['entry_doctype_field'] = 'Select the field with the document type';

$_['help_condition_selection'] = 'Enter the conditions on the basis of which the documents will be selected';
$_['help_field_result'] = 'The field in which the identifiers of the documents matching the conditions of the selection will be recorded';
$_['help_document_source'] = 'Specify the source of the documents for which the selection conditions are applied';
$_['help_doctype_list'] = 'The selection conditions will be applied to all documents of the selected type';
$_['help_document_field'] = 'The selection conditions will be applied to all documents from the selected field';
$_['help_doctype_field'] = 'The conditions of the selection will be applied to all documents of the type that are in the selected field (there can be several types)';

$_['column_field_1'] = 'Field';
$_['column_comparison'] = 'Comparison';
$_['column_field_2'] = 'Value';
$_['column_field_target'] = 'In the field';
$_['column_field_source'] = 'From field';


$_['text_and'] = 'AND';
$_['text_or'] = 'OR';
$_['text_description_document_field'] = 'Selecting documents from the field %s';
$_['text_description_doctype_field'] = 'Select all documents of the type from the field %s';
$_['text_description_doctype_list'] = 'Select all documents of type %s';
$_['text_description_field_result'] = 'The result is written to the field %s';
$_['text_description_without_field_document'] = 'No documents for sampling';
$_['text_document_source_document_field'] = 'Documents from the field';
$_['text_document_source_doctype_field_list'] = 'All documents of the specified type selected from the list';
$_['text_document_source_doctype_field'] = 'All documents of the specified type received from the field';