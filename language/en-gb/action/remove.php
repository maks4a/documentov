<?php

$_['entry_confirm'] = 'Confirm removal';
$_['entry_field_document'] = 'Document to delete';

$_['title_confirm'] = 'Confirm removal';

$_['text_unselect_folder'] = 'Select the document (s) to delete';
$_['text_description_current_doc'] = 'current document';
$_['text_description_field_doc'] = 'documents from the field %s';

$_['help_confirm'] = 'Before executing, the action can prompt the user for confirmation with the text entered in this parameter. This leaves this parameter blank, no confirmation will be requested ';
$_['help_field_document'] = 'The action can delete the current document or documents from any field of the document containing their identifiers';