<?php

$_['entry_field_subject'] = 'To';
$_['entry_field_object'] = 'Field';
$_['entry_object_type'] = 'Object';
$_['entry_object_author'] = 'Author of the object';
$_['entry_access'] = 'Access';

$_['text_object_type_document'] = 'Document by reference from the field';
$_['text_object_type_doctype'] = 'Document type by reference from the field';
$_['text_access_allow'] = 'Provide';
$_['text_access_deny'] = 'Delete';
$_['text_description'] = '%s access for %s to %s';
$_['text_object_type_doctype_list'] = 'Document type from the list';

$_['help_field_subject'] = 'Provide access to document (s) or document type';
$_['help_field_object'] = 'Select the field where the object you are accessing is configured';
$_['help_object_author'] = 'Choose from the structure of the employee or department whose documents are configured for access';
$_['help_access'] = 'Allow or deny subject access (To parameter) to object';
$_['help_object_type'] = 'Access object';
$_['help_doctype'] = 'Document Type';
$_['entry_doctype'] = 'Document Type';
