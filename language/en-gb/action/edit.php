<?php

$_['entry_field_document'] = 'Select the document to edit';
$_['entry_doctype_document'] = "Document Type";

$_['text_currently_document'] = '- Current Document -';
$_['text_not_remove'] = '- Do not move -';
$_['text_description_field']            = 'edit document from field "%s"';
$_['text_description_current']          = 'edit current document';

$_['help_field_document'] = 'Which document will be moved to the editing mode (by default, the current document). You can select a reference type field, and then in the edit mode the document from this field will be opened (if there are several documents in the field, the first one is from them) ';
$_['help_doctype_document'] = 'To configure the move, you must specify what type of document is expected in the field selected above';

$_['error_document_not_found'] = 'Document not found for editing';

$_['on_save'] = 'Document changed';
