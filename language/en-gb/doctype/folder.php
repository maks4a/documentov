<?php
// Heading
$_['heading_title']                     = 'Folders';
$_['title_select_folder_type']          = 'Выбор типа журнала';

$_['column_field_name']                 = 'Name';
$_['column_field_sort']                 = 'Sort';
$_['column_condition']                  = 'Comparing';
$_['column_value']                      = 'Value';
$_['column_action']                     = 'Action';
$_['column_field_field']                = 'Field';
$_['column_grouping']                   = 'Grouping';
$_['column_table']                      = 'Table part';
$_['column_grouping_content']           = 'Group content';
$_['column_button_name']                = 'Button';
$_['column_button_field']               = 'Who';
$_['column_button_route']               = 'Point of route';
$_['column_button_action']              = 'Action';

$_['text_add']                          = 'New';
$_['text_edit']                         = 'Edit';
$_['text_not_found']                    = 'Doctype not found';
$_['text_title_field_list']             = 'Folder field';
$_['text_name_not_select']              = 'name does not show';
$_['text_content_grouping_list']        = 'list of unique value';
$_['text_content_grouping_tree']        = 'tree by field under';
$_['text_content_grouping_tree2']       = 'tree by field';
$_['text_content_grouping_list2']       = 'list';
$_['text_no_parent']                    = 'not include';
$_['text_title_button_add']             = 'Button of folder';
$_['text_all_routes']                   = 'ALL POINTS OF ROUTE';
$_['text_title_filter']                 = 'Filter';
$_['text_action_hide']                  = 'Hide a document';
$_['text_action_style']                 = 'Change coloring';
$_['text_field_not_editable']           = 'Поле помечено на удаление, поэтому не может быть отредактировано';
$_['text_confirm']                      = 'Are you sure?';
$_['text_success']                      = 'Success';
$_['text_draft_message']                = 'It is unsave draft of folder. You cane save it or <a href="%s">delete</a>';
$_['text_toolbar_always_show']          = 'Всегда показывать';
$_['text_toolbar_empty_hide']           = 'Скрывать, если нет делегиованных кнопок';
$_['text_toolbar_always_hide']          = 'Всегда скрывать';
$_['text_navigation_always_show']       = 'Всегда показывать';
$_['text_navigation_toolbar_hidden_show']= 'Показывать, если скрыта панель кнопок';
$_['text_navigation_always_hide']       = 'Всегда скрывать';
$_['text_standard_type']                = 'Обычный журнал';
$_['text_select_folder_type']           = 'Выберите тип создаваемого журнала';

$_['entry_value_from_var']              = 'take from variable';
$_['entry_value_input']                 = 'entry value';
$_['entry_name']                        = 'Folder name';
$_['entry_short_description']           = 'Short description';
$_['entry_full_description']            = 'Full description';
$_['entry_doctype']                     = 'Doctype';
$_['entry_field_field']                 = 'Select field';
$_['entry_field_name']                  = 'Field name';
$_['entry_field_grouping']              = 'Grouping';
$_['entry_field_grouping_name']         = 'Group name';
$_['entry_content_grouping']            = 'Group content';
$_['entry_content_grouping_tree']       = 'Group content';
$_['entry_field_grouping_parent']       = 'Include the group into';
$_['entry_field_tcolumn']               = 'Show in the table';
$_['entry_field_tcolumn_name']          = 'Column name';
$_['entry_button_route']                = 'Point of route';
$_['entry_button_name']                 = 'Name';
$_['entry_button_description']          = 'Title';
$_['entry_button_picture']              = 'Picture';
$_['entry_button_color']                = 'Color';
$_['entry_button_field']                = 'Who';
$_['entry_filter_field']                = 'Field';
$_['entry_filter_condition']            = 'Comparison';
$_['entry_filter_value']                = 'Value';
$_['entry_filter_action']               = 'Action';
$_['entry_action']                      = 'Select action';
$_['entry_action_log']                  = 'Log';
$_['entry_action_executor']             = 'Write executor';
$_['entry_action_name']                 = 'Write action name';
$_['entry_action_move_route']           = 'Move current document';
$_['entry_sort_field']                  = 'Сортировка по умолчанию';
$_['entry_toolbar']                     = 'Панель кнопок';
$_['entry_navigation']                  = 'Панель навигации';


$_['help_doctype']                      = 'Выберите тип документа для отображения в журнале';
$_['help_field_grouping']               = 'Если включить группировку, поле будет отображаться в левой части журнала и по нему будут группироваться документы';
$_['help_field_grouping_name']          = 'При необходимости можно ввести название группы; если оставить поле пустым, то название отображаться не будет; для вложенных группа название не отображается';
$_['help_content_grouping']                = 'Содержимое группы может быть линейным списком или деревом';
$_['help_field_grouping_parent']        = 'Если нужно, чтобы данная группа была вложенной в другую, выберите из списка родительское поле группировки';
$_['help_field_tcolumn']                = 'Отображать поле в табличной части журанал';
$_['help_field_tcolumn_name']           = 'Название столбца поля в табличной части журнала';
$_['help_button_route']                 = 'Кнопка будет доступна и работать только для тех документов, которые находятся на указанной точке маршрута';
$_['help_button_field']                 = 'Выберите поле с сотрудником (-ами), которому будет доступна эта кнопка';
$_['help_action_move_route']            = 'После нажатия на кнопку и выполнения действия (если оно есть) выбранный документ может быть перемещен на указанную в этом параметре точку маршрута';


$_['add_button']                        = 'Добавить кнопку группового действия';
$_['edit_button']                       = 'Изменить кнопку группового действия';

$_['button_add_filter']                 = 'New filter';
$_['button_edit_filter']                = 'Change filter';
$_['button_add_field']                  = 'New field';
$_['button_open_folder']                = 'Open folder';
