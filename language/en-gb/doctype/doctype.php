<?php
// Heading
$_['heading_title']                     = 'Doctypes';

$_['text_home']                         = 'Main';
$_['text_add']                          = 'Add';
$_['text_edit']                         = 'Edit';
$_['text_title_button_add']             = 'New button';
$_['text_title_button_edit']            = 'Edit button';
$_['text_title_route_add']              = 'New point of route';
$_['text_title_route_edit']             = 'Edit point of route';
$_['text_title_action_add']             = 'New action';
$_['text_title_action_edit']            = 'Edit action';
$_['text_route_jump_description']       = 'This actions will execute after jump in point ';
$_['text_route_view_description']       = 'This actions will execute after any activities on documents (view, press on button, ets) in point';
$_['text_route_change_description']     = 'This actions will execute after change fields with setting "Subscribe to change" in point';
$_['text_route_create_description']     = 'This actions will execute after creation document before show to user in point';
$_['text_route_delegate']               = 'Buttons for users are available in the point';
$_['text_route_no_delete']              = 'This point does not remove becouse have documents on it';
$_['text_on_remove_route']              = 'This point of route marked as deleted. Save doctype and point will delete finally or cancel operation';
$_['text_error_access_button']          = 'You do not have access to this button';
$_['text_field_not_editable']           = 'Field marked as deleted and it does not edit';
$_['text_route_point']                  = 'Point';
$_['text_route_context']                = 'Context';
$_['text_route_action']                 = 'Action';
$_['text_route_description']            = 'Description';
$_['text_confirm']                      = 'Are you sure?';
$_['text_success']                      = 'Operation is succesful';
$_['text_error_document_exists']        = 'Removing is impossible becouse the documents of this type are in the system';
$_['text_draft_message']                = 'It is the unsave draft of doctype. You can save it or <a href="%s">delete</a>';
$_['text_access_form']                  = 'Access edit';
$_['text_access_view']                  = 'Access view';
$_['text_required']                     = 'Required value';
$_['text_unique']                       = 'Unique value';
$_['text_attr_change_field']            = "Subscribe to change";
$_['text_attr_access_form']             = "Access edit";
$_['text_attr_access_view']             = "Access view";
$_['text_attr_required']                = "Required value";
$_['text_attr_unique']                  = "Unique value";
$_['text_button']                       = "Button";
$_['text_field_id']                     = 'Идентификатор поля';


$_['tab_form']                          = 'Form';
$_['tab_attributes']                    = 'Attributes';

$_['column_field_name']                 = 'Name';
$_['column_field_type']                 = 'Type';
$_['column_field_value']                = 'Parameter value';
$_['column_field_params']               = 'Parameters';
$_['column_field_order']                = 'Sort';
$_['column_route_execute']              = 'Execute';
$_['column_route_delegate']             = 'Delegate';

$_['entry_name']                        = 'Doctype name';
$_['entry_short_description']           = 'Short description';
$_['entry_full_description']            = 'Full description';
$_['entry_date_added']                  = 'Date added';
$_['entry_route_name']                  = 'Name';
$_['entry_route_description']           = 'Description';
$_['entry_route_button_name']           = 'Name';
$_['entry_route_button_description']    = 'Title';
$_['entry_route_button_picture']        = 'Picture';
$_['entry_route_button_color']          = 'Color';
$_['entry_route_button_field']          = 'Who';
$_['entry_action']                      = 'Select action';
$_['entry_field']                       = 'Select field';
$_['entry_route']                       = 'Select point of view';
$_['entry_route_place']                 = 'Add before';
$_['entry_action_log']                  = 'Log';
$_['entry_action_executor']             = 'Write executer';
$_['entry_action_name']                 = 'Write action name';
$_['entry_action_move_route']           = 'Move current document';
$_['entry_field_log']                   = 'Select field to write log down';
$_['entry_template_type']               = 'Type of template';
$_['entry_template']                    = 'Template';
$_['entry_field_name']                  = 'Entry name';
$_['entry_field_type']                  = 'Select type';
$_['entry_button_uid']                  = 'Button ID';
$_['entry_change_field']                = 'Execute route context Changing';
$_['entry_show_after_button']           = 'Afret execution button shows';
$_['entry_status']                      = 'Состояние';

$_['button_edit_field']                 = 'Edit field';
$_['button_edit_value_field']           = 'Change field value';
$_['button_add_field']                  = 'Add new field';
$_['button_edit_route']                 = 'Rename or delete the point';
$_['button_add_route']                  = 'Add new point';

$_['route_add_button']                  = 'Add button';
$_['route_add_action']                  = 'Add action';
$_['route_copy_action']                 = 'Copy action';
$_['route_copy_button']                 = 'Copy button';
$_['route_edit_button']                 = 'Change action';

$_['button_field']                      = 'Fields';
$_['button_create_document']            = 'Create document';


$_['variable_field']                    = 'Variables';

$_['template_name']                     = array(
    'form'      => 'Template of form (for create document)',
    'view'      => 'Template of view (for view document)'
);

$_['text_error_connection']             = 'Error connection to server!';
$_['text_in_field']                     = 'in the field';
$_['text_field_name']                   = 'Field name';
$_['text_field_value']                   = 'Field value';
$_['text_field_type']                   = 'Field type';

$_['text_title_field_list']             = 'Adding field';
$_['text_change_field']                 = 'In changing field';
$_['text_select_fieldtype']             = 'Выберите тип поля';

$_['help_route_button_field']           = 'Выберите поле с сотрудником (-ами), которому будет доступна эта кнопка';
$_['help_action_log']                   = 'Записывать результат выполнения действия в ход работы';
$_['help_action_executor']              = 'Записывать исполнителя действия (нажавшего на кнопку) в поле';
$_['help_action_name']                  = 'Записывать название действия (или иное значение) в поле';
$_['help_ation_move_route']             = 'Выберите точку маршрута, на которую нужно переместить текущий документ после выполнения данного действия';
$_['help_field_log']                    = 'Выберите текстовое поле, в которое будет записываться ход работы над документом';
$_['help_route_place']                  = 'Чтобы вставить точку перед существующей точкой маршрута выберите ее из списка. Для простого добавления в конец маршрута ничего не выбирайте';
$_['help_access_form']                  = 'Если данный параметр пуст, то любой сотрудник, имеющий доступ к документу, может видеть поле в режиме редактирования (при размещении в соответствующем шаблоне). Если в данном параметре выбрать поле, то режим редактирования будет доступен только тем элементам структуры, которые будут находиться в нем';
$_['help_access_view']                  = 'Если данный параметр пуст, то любой сотрудник, имеющий доступ к документу, может видеть значение поля. Если в данном параметре выбрать поле, то значение поля увидят только те сотрудники (подразделения), которые будут находиться в нем';
$_['help_required']                     = 'Если поле будет размещено в шаблоне, пользователь должен будет его обязательно заполнить';
$_['help_unique']                       = 'Введенное пользователем значение будет проверено на уникальность среди всех значений данного поля других документов';
$_['help_show_after_button']            = 'Если документ будет открыт из журнала, то данный атрибут позволяет сразу перейти в журнал после выполнения данной кнопки';
$_['help_status']                       = 'Если выбрать вариант Отключено, то данное действие не будет запускаться при обработке маршрута документа';