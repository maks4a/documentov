<?php
// Heading
$_['heading_title']               = 'Installation complete';

// Text
$_['text_step_4']                 = 'Ready to start using Documentov!';
$_['text_catalog']                = 'Go to your Documentov!';
$_['text_admin']                  = 'Login to your Administration';
$_['text_loading']                = 'Loading modules...';
$_['text_extension']              = 'visit the extensions store';
$_['text_mail']                   = 'Join the mailing list';
$_['text_mail_description']       = 'Stay informed of Documentov updates and events.';
$_['text_forum']                  = 'Community forums';
$_['text_forum_description']      = 'Get help from other Documentov users';
$_['text_forum_visit']            = 'Visit our forums';
$_['text_commercial']             = 'Commercial support';
$_['text_commercial_description'] = 'Development services from Documentov partners';
$_['text_commercial_visit']       = 'Visit our partner page';
$_['text_price']                  = 'Price';
$_['text_view']                   = 'View details';
$_['text_download']               = 'Download';
$_['text_downloads']              = 'Downloads';

// Button
$_['button_mail']                 = 'Join here';
$_['button_setup']                = 'Set-up now';

// Error
$_['error_warning']               = 'Don\'t forget to delete your installation directory!';