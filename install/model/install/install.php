<?php

class ModelInstallInstall extends Model {

    public function database($data) {
        $db = new DB($data['db_driver'], htmlspecialchars_decode($data['db_hostname']), htmlspecialchars_decode($data['db_username']), htmlspecialchars_decode($data['db_password']), htmlspecialchars_decode($data['db_database']), $data['db_port']);

        $file = DIR_APPLICATION . 'documentov.sql';

        if (!file_exists($file)) {
            exit('Could not load sql file: ' . $file);
        }

        $lines = file($file);

        if ($lines) {
            $sql = '';

            foreach ($lines as $line) {
                if ($line && (substr($line, 0, 2) != '--') && (substr($line, 0, 1) != '#')) {
                    if ((strpos($line, 'START TRANSACTION;') === false && strpos($line, 'COMMIT;') === false && strpos($line, 'SET AUTOCOMMIT') === false && strpos($line, 'SET SQL_MODE') === false)) {

                        $sql .= $line;

                        if (preg_match('/;\s*$/', $line)) {
                            $sql = str_replace("DROP TABLE IF EXISTS `oc_", "DROP TABLE IF EXISTS `" . $data['db_prefix'], $sql);
                            $sql = str_replace("CREATE TABLE `oc_", "CREATE TABLE `" . $data['db_prefix'], $sql);
                            $sql = str_replace("INSERT INTO `oc_", "INSERT INTO `" . $data['db_prefix'], $sql);

                            $db->query($sql);

                            $sql = '';
                        }
                    } else {
                        //print_r($line);
                    }
                }
            }

            //$db->query("SET CHARACTER SET utf8");
            //$db->query("SET @@session.sql_mode = 'MYSQL40'");
            //обновление документа организации
            $org_uid = '5354df9c-1df9-11e8-a7fb-201a06f86b88';
            $org_name_field_uid = '54fde8ea-1df9-11e8-a7fb-201a06f86b88';

            $sql = "UPDATE " . $data['db_prefix'] . "field_value_string SET value='" . $db->escape($data['orgname']) . "', display_value='" . $db->escape($data['orgname']) . "' WHERE document_uid='" . $org_uid . "' AND field_uid='" . $org_name_field_uid . "'";
            $db->query($sql);

            //обновление документа сотрудника
            $struct_uid = '5354e0d0-1df9-11e8-a7fb-201a06f86b88';
            $struct_name_field_uid = '54fde8ea-1df9-11e8-a7fb-201a06f86b88';
            $struct_orglink_field_uid = '54fde977-1df9-11e8-a7fb-201a06f86b88';
            $struct_userlink_field_uid = '54fde9f3-1df9-11e8-a7fb-201a06f86b88';

            $sql = "UPDATE " . $data['db_prefix'] . "field_value_string SET value='" . $db->escape($data['username']) . "', display_value='" . $db->escape($data['username']) . "' WHERE document_uid='" . $struct_uid . "' AND field_uid='" . $struct_name_field_uid . "'";
            $db->query($sql);
            $sql = "UPDATE " . $data['db_prefix'] . "field_value_link SET display_value='" . $db->escape($data['orgname']) . "' WHERE document_uid='" . $struct_uid . "' AND field_uid='" . $struct_orglink_field_uid . "'";
            $db->query($sql);
            $sql = "UPDATE " . $data['db_prefix'] . "field_value_link SET display_value='" . $db->escape($data['email']) . "' WHERE document_uid='" . $struct_uid . "' AND field_uid='" . $struct_userlink_field_uid . "'";
            $db->query($sql);

            //обновление документа пользователя
            $user_uid = '5354daea-1df9-11e8-a7fb-201a06f86b88';
            $user_email_field_uid = '54fde55f-1df9-11e8-a7fb-201a06f86b88';
            $user_password_field_uid = '54fde735-1df9-11e8-a7fb-201a06f86b88';
            $user_structlink_field_uid = '54fdeea9-1df9-11e8-a7fb-201a06f86b88';

            $sql = "UPDATE " . $data['db_prefix'] . "field_value_string SET value='" . $db->escape($data['email']) . "', display_value='" . $db->escape($data['email']) . "' WHERE document_uid='" . $user_uid . "' AND field_uid='" . $user_email_field_uid . "'";
            $db->query($sql);
            $sql = "UPDATE " . $data['db_prefix'] . "field_value_hidden SET value='" . password_hash($data['password'], PASSWORD_DEFAULT) . "', display_value='*****' WHERE document_uid='" . $user_uid . "' AND field_uid='" . $user_password_field_uid . "'";
            $db->query($sql);
            $sql = "UPDATE " . $data['db_prefix'] . "field_value_link SET display_value='" . $db->escape($data['username']) . "' WHERE document_uid='" . $user_uid . "' AND field_uid='" . $user_structlink_field_uid . "'";
            $db->query($sql);

            //установка таймзоны
            $sql = "SELECT value FROM " . $data['db_prefix'] . "setting WHERE `key`='date.timezone'";
            $query = $db->query($sql);
            if ($query->num_rows > 0) {
                $tz = (new DateTime('now', new DateTimeZone($query->row['value'])))->format('P');
                $db->query("SET time_zone = '" . $db->escape($tz) . "' ");
            }
                    
        }
    }

}
