SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


DROP TABLE IF EXISTS `action_timer`;
CREATE TABLE `action_timer` (
  `identifier` varchar(255) NOT NULL,
  `document_uid` varchar(36) NOT NULL,
  `task_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `daemon_queue`;
CREATE TABLE `daemon_queue` (
  `task_id` int(11) NOT NULL,
  `action` varchar(256) NOT NULL,
  `action_params` mediumtext NOT NULL,
  `priority` tinyint(4) NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `exec_attempt` tinyint(4) DEFAULT '0',
  `PID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `doctype`;
CREATE TABLE `doctype` (
  `doctype_uid` varchar(36) NOT NULL,
  `field_log_uid` varchar(36) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_edited` datetime NOT NULL,
  `user_uid` varchar(36) NOT NULL,
  `draft` tinyint(4) NOT NULL,
  `draft_params` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `doctype` (`doctype_uid`, `field_log_uid`, `date_added`, `date_edited`, `user_uid`, `draft`, `draft_params`) VALUES
('51f800b5-1df9-11e8-a7fb-201a06f86b88', '', '2017-09-15 12:00:00', '2018-08-07 14:58:49', '5354e1f3-1df9-11e8-a7fb-201a06f86b88', 0, ''),
('51f803b2-1df9-11e8-a7fb-201a06f86b88', '8425a629-90c2-11e8-9039-485ab6e1c06f', '2017-11-02 10:21:45', '2018-07-27 13:54:31', '5354e1f3-1df9-11e8-a7fb-201a06f86b88', 0, ''),
('51f80627-1df9-11e8-a7fb-201a06f86b88', '', '2018-02-15 14:26:24', '2018-07-26 16:42:50', '5354e1f3-1df9-11e8-a7fb-201a06f86b88', 0, ''),
('666a0f8c-9172-11e8-8f8b-485ab6e1c06f', 'f123b3f1-9175-11e8-8f8b-485ab6e1c06f', '2018-07-27 13:55:16', '2018-07-27 16:04:46', '5354e0d0-1df9-11e8-a7fb-201a06f86b88', 0, ''),
('9f91722e-5823-11e8-841d-201a06f86b88', '', '2018-05-15 15:37:45', '2018-07-26 16:42:18', '5354e0d0-1df9-11e8-a7fb-201a06f86b88', 0, '');

DROP TABLE IF EXISTS `doctype_description`;
CREATE TABLE `doctype_description` (
  `doctype_uid` varchar(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `short_description` varchar(255) NOT NULL,
  `full_description` text NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `doctype_description` (`doctype_uid`, `name`, `short_description`, `full_description`, `language_id`) VALUES
('51f800b5-1df9-11e8-a7fb-201a06f86b88', 'Пользователи', 'Справочник пользователей системы', '', 2),
('51f803b2-1df9-11e8-a7fb-201a06f86b88', 'Structure', '', '', 1),
('51f803b2-1df9-11e8-a7fb-201a06f86b88', 'Структура', 'Структура организации', '', 2),
('51f80627-1df9-11e8-a7fb-201a06f86b88', 'Setting', '', '', 1),
('51f80627-1df9-11e8-a7fb-201a06f86b88', 'Настройки', 'Документ для хранения глобальных настроек конфигурации', '', 2),
('666a0f8c-9172-11e8-8f8b-485ab6e1c06f', 'Тестовый тип документа', 'Пример реализации типа документа', '', 2),
('9f91722e-5823-11e8-841d-201a06f86b88', 'User\'s profile', '', '', 1),
('9f91722e-5823-11e8-841d-201a06f86b88', 'Профиль пользователя', 'Документ профиля пользователя (меню Профиль)', '', 2);

DROP TABLE IF EXISTS `doctype_template`;
CREATE TABLE `doctype_template` (
  `doctype_uid` varchar(36) NOT NULL,
  `template` mediumtext NOT NULL,
  `type` varchar(255) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `doctype_template` (`doctype_uid`, `template`, `type`, `language_id`) VALUES
('51f800b5-1df9-11e8-a7fb-201a06f86b88', '&lt;h1 style=&quot;text-align: center; &quot;&gt;Пользователь&lt;/h1&gt;\n&lt;table class=&quot;table table-bordered&quot;&gt;&lt;tbody&gt;\n &lt;tr&gt;\n &lt;td&gt;E-mail:&lt;/td&gt;\n &lt;td&gt;{{ f_54fde55f1df911e8a7fb201a06f86b88 }}&lt;/td&gt;\n &lt;/tr&gt;\n &lt;tr&gt;\n &lt;td&gt;Пароль:&lt;/td&gt;\n &lt;td&gt;{{ f_54fde7351df911e8a7fb201a06f86b88 }}&lt;/td&gt;\n &lt;/tr&gt;\n \n &lt;tr&gt;\n &lt;td&gt;Администратор:&lt;/td&gt;\n &lt;td&gt;{{ f_48cae257325911e88ed6201a06f86b88 }}&lt;/td&gt;\n &lt;/tr&gt;\n &lt;tr&gt;\n &lt;td&gt;Язык:&lt;/td&gt;\n &lt;td&gt;{{ f_d0e9bc2a30b711e89fbe201a06f86b88 }}&lt;/td&gt;\n &lt;/tr&gt;\n &lt;tr&gt;\n &lt;td&gt;Кол-во неудачных попыток входа:&lt;/td&gt;\n &lt;td&gt;{{ f_54fde8581df911e8a7fb201a06f86b88 }}&lt;/td&gt;\n &lt;/tr&gt;\n   &lt;tr&gt;\n &lt;td&gt;Стартовая страница:&lt;/td&gt;\n &lt;td&gt;&lt;p&gt;{{ f_50dd591637fc11e8a97152540028bc1e }}&lt;/p&gt;&lt;/td&gt;\n &lt;/tr&gt;\n   &lt;tr&gt;\n &lt;td&gt;Состояние:&lt;/td&gt;\n &lt;td&gt;&lt;p&gt;{{ f_54fde7d41df911e8a7fb201a06f86b88 }}&lt;/p&gt;&lt;/td&gt;\n &lt;/tr&gt;  \n &lt;/tbody&gt;&lt;/table&gt;', 'form', 2),
('51f800b5-1df9-11e8-a7fb-201a06f86b88', '&lt;h1 style=&quot;text-align: center; &quot;&gt;Пользователь&lt;/h1&gt;\n&lt;table class=&quot;table table-bordered&quot;&gt;&lt;tbody&gt;\n &lt;tr&gt;\n &lt;td&gt;E-mail:&lt;/td&gt;\n &lt;td&gt;&lt;p&gt;{{ f_54fde55f1df911e8a7fb201a06f86b88 }}&lt;/p&gt;&lt;/td&gt;\n &lt;/tr&gt;\n &lt;tr&gt;\n &lt;td&gt;Пароль:&lt;/td&gt;\n &lt;td&gt;{{ f_54fde7351df911e8a7fb201a06f86b88 }}&lt;/td&gt;\n &lt;/tr&gt;\n \n &lt;tr&gt;\n &lt;td&gt;Администратор:&lt;/td&gt;\n &lt;td&gt;{{ f_48cae257325911e88ed6201a06f86b88 }}&lt;/td&gt;\n &lt;/tr&gt;\n &lt;tr&gt;\n &lt;td&gt;Язык:&lt;/td&gt;\n &lt;td&gt;{{ f_d0e9bc2a30b711e89fbe201a06f86b88 }}&lt;/td&gt;\n &lt;/tr&gt;\n &lt;tr&gt;\n &lt;td&gt;Кол-во неудачных попыток входа:&lt;/td&gt;\n &lt;td&gt;{{ f_54fde8581df911e8a7fb201a06f86b88 }}&lt;/td&gt;\n &lt;/tr&gt;\n&lt;tr&gt;\n &lt;td&gt;Стартовая страница:&lt;/td&gt;\n &lt;td&gt;&lt;p&gt;{{ f_50dd591637fc11e8a97152540028bc1e }}&lt;/p&gt;&lt;/td&gt;\n &lt;/tr&gt;\n&lt;tr&gt;\n &lt;td&gt;Последняя открытая страница:&lt;/td&gt;\n &lt;td&gt;&lt;p&gt;{{ f_425fd7d759be11e8958b201a06f86b88 }}&lt;/p&gt;&lt;/td&gt;\n &lt;/tr&gt;\n&lt;tr&gt;\n &lt;td&gt;Время последней активности:&lt;/td&gt;\n &lt;td&gt;&lt;p&gt;{{ f_0a009bb099f911e88da9485ab6e1c06f }}&lt;/p&gt;&lt;/td&gt;\n &lt;/tr&gt;\n&lt;tr&gt;\n &lt;td&gt;Состояние:&lt;/td&gt;\n &lt;td&gt;&lt;p&gt;{{ f_54fde7d41df911e8a7fb201a06f86b88 }}&lt;/p&gt;&lt;/td&gt;\n &lt;/tr&gt;\n\n  &lt;/tbody&gt;&lt;/table&gt;', 'view', 2),
('51f803b2-1df9-11e8-a7fb-201a06f86b88', '&lt;h1 style=&quot;text-align: center; &quot;&gt;\n   \n   Структура\n&lt;/h1&gt;&lt;table class=&quot;table table-bordered&quot; style=&quot;width: 957px;&quot;&gt;\n&lt;tbody&gt;\n  &lt;tr&gt;\n    &lt;td&gt;Отображаемое имя&lt;/td&gt;\n    &lt;td&gt;&lt;p&gt;{{ f_54fde8ea1df911e8a7fb201a06f86b88 }}&lt;/p&gt;&lt;/td&gt;\n  &lt;/tr&gt;\n    &lt;tr&gt;\n    &lt;td&gt;Должность&lt;/td&gt;\n    &lt;td&gt;&lt;p&gt;{{ f_54fded411df911e8a7fb201a06f86b88 }}&lt;/p&gt;&lt;/td&gt;\n  &lt;/tr&gt;\n  &lt;tr&gt;\n  &lt;/tr&gt;&lt;tr&gt;\n    &lt;td&gt;Подразделение&lt;/td&gt;\n    &lt;td&gt;{{ f_54fde9771df911e8a7fb201a06f86b88 }}&lt;/td&gt;\n  &lt;/tr&gt;\n    &lt;tr&gt;\n    &lt;td&gt;Номер телефона&lt;/td&gt;\n    &lt;td&gt;&lt;p&gt;{{ f_54fdedb91df911e8a7fb201a06f86b88 }}&lt;/p&gt;&lt;/td&gt;\n  &lt;/tr&gt;\n\n  &lt;tr&gt;\n    &lt;td&gt;Пользователь&lt;/td&gt;\n    &lt;td&gt;{{ f_54fde9f31df911e8a7fb201a06f86b88 }}&lt;/td&gt;\n  &lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;&lt;br&gt;', 'form', 1),
('51f803b2-1df9-11e8-a7fb-201a06f86b88', '&lt;h1 style=&quot;text-align: center; &quot;&gt;\n   \n   Структура\n&lt;/h1&gt;&lt;table class=&quot;table table-bordered&quot; style=&quot;width: 957px;&quot;&gt;\n&lt;tbody&gt;\n  &lt;tr&gt;\n    &lt;td&gt;Отображаемое имя&lt;/td&gt;\n    &lt;td&gt;&lt;p&gt;{{ f_54fde8ea1df911e8a7fb201a06f86b88 }}&lt;/p&gt;&lt;/td&gt;\n  &lt;/tr&gt;\n    &lt;tr&gt;\n    &lt;td&gt;Должность&lt;/td&gt;\n    &lt;td&gt;&lt;p&gt;{{ f_54fded411df911e8a7fb201a06f86b88 }}&lt;/p&gt;&lt;/td&gt;\n  &lt;/tr&gt;\n  &lt;tr&gt;\n  &lt;/tr&gt;&lt;tr&gt;\n    &lt;td&gt;Подразделение&lt;/td&gt;\n    &lt;td&gt;{{ f_54fde9771df911e8a7fb201a06f86b88 }}&lt;/td&gt;\n  &lt;/tr&gt;\n    &lt;tr&gt;\n    &lt;td&gt;Номер телефона&lt;/td&gt;\n    &lt;td&gt;&lt;p&gt;{{ f_54fdedb91df911e8a7fb201a06f86b88 }}&lt;/p&gt;&lt;/td&gt;\n  &lt;/tr&gt;\n\n  &lt;tr&gt;\n    &lt;td&gt;Пользователь&lt;/td&gt;\n    &lt;td&gt;{{ f_54fde9f31df911e8a7fb201a06f86b88 }}&lt;/td&gt;\n  &lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;&lt;br&gt;', 'form', 2),
('51f803b2-1df9-11e8-a7fb-201a06f86b88', '&lt;h1 style=&quot;text-align: center; &quot;&gt;\n   \n   Структура\n&lt;/h1&gt;&lt;table class=&quot;table table-bordered&quot; style=&quot;width: 957px;&quot;&gt;\n&lt;tbody&gt;\n  &lt;tr&gt;\n    &lt;td&gt;Отображаемое имя&lt;/td&gt;\n    &lt;td&gt;&lt;p&gt;{{ f_54fde8ea1df911e8a7fb201a06f86b88 }}&lt;/p&gt;&lt;/td&gt;\n  &lt;/tr&gt;\n    &lt;tr&gt;\n    &lt;td&gt;Должность&lt;/td&gt;\n    &lt;td&gt;&lt;p&gt;{{ f_54fded411df911e8a7fb201a06f86b88 }}&lt;/p&gt;&lt;/td&gt;\n  &lt;/tr&gt;\n  &lt;tr&gt;\n  &lt;/tr&gt;&lt;tr&gt;\n    &lt;td&gt;Подразделение&lt;/td&gt;\n    &lt;td&gt;{{ f_54fde9771df911e8a7fb201a06f86b88 }}&lt;/td&gt;\n  &lt;/tr&gt;\n    &lt;tr&gt;\n    &lt;td&gt;Номер телефона&lt;/td&gt;\n    &lt;td&gt;&lt;p&gt;{{ f_54fdedb91df911e8a7fb201a06f86b88 }}&lt;/p&gt;&lt;/td&gt;\n  &lt;/tr&gt;\n\n  &lt;tr&gt;\n    &lt;td&gt;Пользователь&lt;/td&gt;\n    &lt;td&gt;{{ f_54fde9f31df911e8a7fb201a06f86b88 }}&lt;/td&gt;\n  &lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;&lt;br&gt;', 'view', 1),
('51f803b2-1df9-11e8-a7fb-201a06f86b88', '&lt;h1 style=&quot;text-align: center; &quot;&gt;\n   \n   Структура\n&lt;/h1&gt;&lt;table class=&quot;table table-bordered&quot; style=&quot;width: 957px;&quot;&gt;\n&lt;tbody&gt;\n  &lt;tr&gt;\n    &lt;td&gt;Отображаемое имя&lt;/td&gt;\n    &lt;td&gt;&lt;p&gt;{{ f_54fde8ea1df911e8a7fb201a06f86b88 }}&lt;/p&gt;&lt;/td&gt;\n  &lt;/tr&gt;\n    &lt;tr&gt;\n    &lt;td&gt;Должность&lt;/td&gt;\n    &lt;td&gt;&lt;p&gt;{{ f_54fded411df911e8a7fb201a06f86b88 }}&lt;/p&gt;&lt;/td&gt;\n  &lt;/tr&gt;\n  &lt;tr&gt;\n  &lt;/tr&gt;&lt;tr&gt;\n    &lt;td&gt;Подразделение&lt;/td&gt;\n    &lt;td&gt;{{ f_54fde9771df911e8a7fb201a06f86b88 }}&lt;/td&gt;\n  &lt;/tr&gt;\n    &lt;tr&gt;\n    &lt;td&gt;Номер телефона&lt;/td&gt;\n    &lt;td&gt;&lt;p&gt;{{ f_54fdedb91df911e8a7fb201a06f86b88 }}&lt;/p&gt;&lt;/td&gt;\n  &lt;/tr&gt;\n\n  &lt;tr&gt;\n    &lt;td&gt;Пользователь&lt;/td&gt;\n    &lt;td&gt;{{ f_54fde9f31df911e8a7fb201a06f86b88 }}&lt;/td&gt;\n  &lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;&lt;br&gt;', 'view', 2),
('51f80627-1df9-11e8-a7fb-201a06f86b88', '', 'form', 1),
('51f80627-1df9-11e8-a7fb-201a06f86b88', '', 'form', 2),
('51f80627-1df9-11e8-a7fb-201a06f86b88', '', 'view', 1),
('51f80627-1df9-11e8-a7fb-201a06f86b88', '', 'view', 2),
('666a0f8c-9172-11e8-8f8b-485ab6e1c06f', '&lt;div class=&quot;form-horizontal&quot;&gt;\n  &lt;div class=&quot;form-group&quot;&gt;\n    &lt;label class=&quot;control-label col-sm-2&quot;&gt;Тема&lt;/label&gt;\n    &lt;div class=&quot;col-sm-10&quot;&gt;{{ f_7843a7a9917211e88f8b485ab6e1c06f }}&lt;/div&gt;\n  &lt;/div&gt;\n  &lt;div class=&quot;form-group&quot;&gt;\n    &lt;label class=&quot;control-label col-sm-2&quot;&gt;Подписант&lt;/label&gt;\n    &lt;div class=&quot;col-sm-10&quot;&gt;{{ f_89ff9965917211e88f8b485ab6e1c06f }}&lt;/div&gt;\n  &lt;/div&gt;    \n  &lt;div class=&quot;form-group&quot;&gt;\n    &lt;label class=&quot;control-label col-sm-2&quot;&gt;Адресат&lt;/label&gt;\n    &lt;div class=&quot;col-sm-10&quot;&gt;{{ f_9230b3d4917211e88f8b485ab6e1c06f }}&lt;/div&gt;\n  &lt;/div&gt;    \n\n  &lt;div class=&quot;form-group&quot;&gt;\n    &lt;label class=&quot;control-label col-sm-2&quot;&gt;Содержимое&lt;/label&gt;\n    &lt;div class=&quot;col-sm-10&quot;&gt;{{ f_7db6ef6b917211e88f8b485ab6e1c06f }}&lt;/div&gt; \n  &lt;/div&gt;  \n    \n&lt;/div&gt;', 'form', 2),
('666a0f8c-9172-11e8-8f8b-485ab6e1c06f', '&lt;div class=&quot;form-horizontal&quot;&gt;\n  &lt;div class=&quot;form-group&quot;&gt;\n    &lt;div class=&quot;col-sm-2&quot;&gt;&lt;b&gt;Тема&lt;/b&gt;&lt;/div&gt;\n    &lt;div class=&quot;col-sm-10&quot;&gt;{{ f_7843a7a9917211e88f8b485ab6e1c06f }}&lt;/div&gt;\n  &lt;/div&gt;\n  &lt;div class=&quot;form-group&quot;&gt;\n    &lt;div class=&quot;col-sm-2&quot;&gt;&lt;b&gt;Содержимое&lt;/b&gt;&lt;/div&gt;\n    &lt;div class=&quot;col-sm-10&quot;&gt;{{ f_7db6ef6b917211e88f8b485ab6e1c06f }}&lt;/div&gt; \n  &lt;/div&gt;  \n  &lt;div class=&quot;form-group&quot;&gt;\n    &lt;div class=&quot;col-sm-2&quot;&gt;&lt;b&gt;Подписант&lt;/b&gt;&lt;/div&gt;\n    &lt;div class=&quot;col-sm-10&quot;&gt;{{ f_89ff9965917211e88f8b485ab6e1c06f }}&lt;span style=&quot;color: rgb(156, 0, 255);&quot;&gt; {{ f_79861ef1917411e88f8b485ab6e1c06f }}&lt;/span&gt;&lt;br&gt;&lt;/div&gt;\n  &lt;/div&gt;    \n  &lt;div class=&quot;form-group&quot;&gt;\n    &lt;div class=&quot;col-sm-2&quot;&gt;&lt;b&gt;Адресат&lt;/b&gt;&lt;/div&gt;\n    &lt;div class=&quot;col-sm-10&quot;&gt;{{ f_9230b3d4917211e88f8b485ab6e1c06f }}&lt;/div&gt;\n  &lt;/div&gt;    \n  &lt;div class=&quot;form-group&quot;&gt;\n    &lt;div class=&quot;col-sm-2&quot;&gt;&lt;b&gt;Исполнитель&lt;/b&gt;&lt;/div&gt;\n    &lt;div class=&quot;col-sm-10&quot;&gt;{{ f_a03809a9917211e88f8b485ab6e1c06f }}&lt;/div&gt;\n  &lt;/div&gt;  \n  &lt;div class=&quot;form-group&quot;&gt;\n    &lt;div class=&quot;col-sm-2&quot;&gt;&lt;b&gt;Отчет об исполнении&lt;/b&gt;&lt;/div&gt;\n    &lt;div class=&quot;col-sm-10&quot;&gt;{{ f_8ef255ae917311e88f8b485ab6e1c06f }}&lt;/div&gt;\n  &lt;/div&gt;    \n  &lt;div class=&quot;form-group&quot;&gt;\n    &lt;div class=&quot;col-sm-2&quot;&gt;&lt;b&gt;Состояние&lt;/b&gt;&lt;/div&gt;\n    &lt;div class=&quot;col-sm-10&quot;&gt;{{ f_3e53bfb3918411e88f8b485ab6e1c06f }}&lt;br&gt;&lt;/div&gt;\n  &lt;/div&gt;   \n  &lt;div class=&quot;form-group&quot;&gt;\n    &lt;div class=&quot;col-sm-2&quot;&gt;&lt;b&gt;Автор&lt;/b&gt;&lt;/div&gt;\n    &lt;div class=&quot;col-sm-10&quot;&gt;{{ f_e064f290917311e88f8b485ab6e1c06f }}&lt;/div&gt;\n  &lt;/div&gt;     \n  &lt;div class=&quot;form-group&quot;&gt;\n    &lt;div class=&quot;col-sm-12&quot;&gt;{{ f_f123b3f1917511e88f8b485ab6e1c06f }}&lt;br&gt;&lt;/div&gt;\n  &lt;/div&gt;     \n\n&lt;/div&gt;', 'view', 2),
('9f91722e-5823-11e8-841d-201a06f86b88', '&lt;h2&gt;Пользовательские настройки&lt;/h2&gt; &lt;table class=&quot;table table-bordered&quot;&gt; &lt;tbody&gt; &lt;tr&gt; &lt;td&gt;Пароль&lt;/td&gt; &lt;td&gt;{{ f_ec8a023d582511e8a8d8201a06f86b88 }}&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;td&gt;Стартовая страница&lt;/td&gt; &lt;td&gt;{{ f_1f458fe7582611e8a8d8201a06f86b88 }}&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;td&gt;Язык&lt;/td&gt; &lt;td&gt;{{ f_c4443c3b58db11e8a8d8201a06f86b88 }}&lt;/td&gt; &lt;/tr&gt; &lt;/tbody&gt; &lt;/table&gt;', 'form', 1),
('9f91722e-5823-11e8-841d-201a06f86b88', '&lt;h2&gt;Пользовательские настройки&lt;/h2&gt; \n&lt;table class=&quot;table table-bordered&quot;&gt; \n  &lt;tbody&gt; \n    &lt;tr&gt; \n      &lt;td&gt;Ваше имя&lt;/td&gt; \n      &lt;td&gt;{{ f_167398615f2311e8b6a0201a06f86b88 }}&lt;/td&gt; \n    &lt;/tr&gt; \n    &lt;tr&gt; \n      &lt;td&gt;Ваша должность&lt;/td&gt; \n      &lt;td&gt;{{ f_24251a7f5f2311e8b6a0201a06f86b88 }}&lt;/td&gt; \n    &lt;/tr&gt; \n    \n    &lt;tr&gt; \n      &lt;td&gt;Пароль&lt;/td&gt; \n      &lt;td&gt;{{ f_ec8a023d582511e8a8d8201a06f86b88 }}&lt;/td&gt; \n    &lt;/tr&gt; \n    &lt;tr&gt; \n      &lt;td&gt;Стартовая страница&lt;/td&gt; \n      &lt;td&gt;{{ f_1f458fe7582611e8a8d8201a06f86b88 }}&lt;/td&gt; \n    &lt;/tr&gt; \n    &lt;tr&gt; \n      &lt;td&gt;Язык&lt;/td&gt; \n      &lt;td&gt;{{ f_c4443c3b58db11e8a8d8201a06f86b88 }}&lt;/td&gt; \n    &lt;/tr&gt; \n  &lt;/tbody&gt; \n&lt;/table&gt;', 'form', 2),
('9f91722e-5823-11e8-841d-201a06f86b88', '', 'view', 1),
('9f91722e-5823-11e8-841d-201a06f86b88', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'view', 2);

DROP TABLE IF EXISTS `document`;
CREATE TABLE `document` (
  `document_uid` varchar(36) NOT NULL,
  `doctype_uid` varchar(36) NOT NULL,
  `author_uid` varchar(36) NOT NULL,
  `route_uid` varchar(36) NOT NULL,
  `draft` tinyint(4) NOT NULL,
  `draft_params` mediumtext NOT NULL,
  `date_added` datetime NOT NULL,
  `date_edited` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `document` (`document_uid`, `doctype_uid`, `author_uid`, `route_uid`, `draft`, `draft_params`, `date_added`, `date_edited`) VALUES
('5354daea-1df9-11e8-a7fb-201a06f86b88', '51f800b5-1df9-11e8-a7fb-201a06f86b88', '5354e0d0-1df9-11e8-a7fb-201a06f86b88', '5fa8bd6b-1df9-11e8-a7fb-201a06f86b88', 0, '', '2017-11-14 00:00:00', '2017-11-14 00:00:00'),
('5354df9c-1df9-11e8-a7fb-201a06f86b88', '51f803b2-1df9-11e8-a7fb-201a06f86b88', '5354e0d0-1df9-11e8-a7fb-201a06f86b88', '5fa8b997-1df9-11e8-a7fb-201a06f86b88', 0, '', '2017-11-14 17:14:57', '2018-07-27 09:44:02'),
('5354e0d0-1df9-11e8-a7fb-201a06f86b88', '51f803b2-1df9-11e8-a7fb-201a06f86b88', '5354e0d0-1df9-11e8-a7fb-201a06f86b88', '5fa8b997-1df9-11e8-a7fb-201a06f86b88', 0, '', '2018-01-03 00:00:00', '2018-07-27 09:40:15'),
('fb75c209-58f6-11e8-a8d8-201a06f86b88', '9f91722e-5823-11e8-841d-201a06f86b88', '5354e0d0-1df9-11e8-a7fb-201a06f86b88', '392985cd-5826-11e8-a8d8-201a06f86b88', 3, '', '2018-05-16 16:50:43', '0000-00-00 00:00:00');

DROP TABLE IF EXISTS `document_access`;
CREATE TABLE `document_access` (
  `subject_uid` varchar(36) NOT NULL,
  `document_uid` varchar(36) NOT NULL,
  `doctype_uid` varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `document_access` (`subject_uid`, `document_uid`, `doctype_uid`) VALUES
('5354e0d0-1df9-11e8-a7fb-201a06f86b88', '5354daea-1df9-11e8-a7fb-201a06f86b88', '51f800b5-1df9-11e8-a7fb-201a06f86b88'),
('5354e1f3-1df9-11e8-a7fb-201a06f86b88', '5354daea-1df9-11e8-a7fb-201a06f86b88', '51f800b5-1df9-11e8-a7fb-201a06f86b88'),
('5354e0d0-1df9-11e8-a7fb-201a06f86b88', '5354df9c-1df9-11e8-a7fb-201a06f86b88', '51f803b2-1df9-11e8-a7fb-201a06f86b88'),
('5354e0d0-1df9-11e8-a7fb-201a06f86b88', '5354e0d0-1df9-11e8-a7fb-201a06f86b88', '51f803b2-1df9-11e8-a7fb-201a06f86b88'),
('5354e1f3-1df9-11e8-a7fb-201a06f86b88', '5354df9c-1df9-11e8-a7fb-201a06f86b88', '51f803b2-1df9-11e8-a7fb-201a06f86b88'),
('5354e1f3-1df9-11e8-a7fb-201a06f86b88', '5354e0d0-1df9-11e8-a7fb-201a06f86b88', '51f803b2-1df9-11e8-a7fb-201a06f86b88'),
('5354e0d0-1df9-11e8-a7fb-201a06f86b88', 'fb75c209-58f6-11e8-a8d8-201a06f86b88', '9f91722e-5823-11e8-841d-201a06f86b88');

DROP TABLE IF EXISTS `event`;
CREATE TABLE `event` (
  `event_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `trigger` text NOT NULL,
  `action` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `event` (`event_id`, `code`, `trigger`, `action`, `status`, `sort_order`) VALUES
(6, 'activity_customer_login', 'catalog/model/account/customer/deleteLoginAttempts/after', 'event/activity/login', 1, 0),
(15, 'mail_forgotten', 'catalog/model/account/customer/editCode/after', 'mail/forgotten', 1, 0),
(35, 'fild_link_edit_value', 'catalog/model/document/document/editFieldValue/after', 'extension/field/link/actualizeData', 1, 0),
(36, 'service_stat_customer_login', '/model/account/customer/deleteLoginAttempts/after', 'extension/service/stat/login', 1, 0),
(37, 'service_stat_customer_login_fail', '/model/account/customer/addLoginAttempt/after', 'extension/service/stat/login_fail', 1, 0),
(38, 'service_stat_customer_open_folder', '/controller/document/folder/after', 'extension/service/stat/open_folder', 1, 0),
(39, 'service_stat_customer_open_document1', '/controller/document/document/get_document/after', 'extension/service/stat/open_document', 1, 0),
(40, 'service_stat_customer_open_document2', '/controller/document/document/after', 'extension/service/stat/open_document', 1, 0),
(41, 'service_stat_customer_exec_folder_button', '/controller/document/folder/button/after', 'extension/service/stat/execute_folder_button', 1, 0),
(42, 'service_stat_customer_exec_document_button', '/controller/document/document/button/after', 'extension/service/stat/execute_document_button', 1, 0);

DROP TABLE IF EXISTS `extension`;
CREATE TABLE `extension` (
  `extension_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `extension` (`extension_id`, `type`, `code`) VALUES
(1, 'payment', 'cod'),
(2, 'total', 'shipping'),
(3, 'total', 'sub_total'),
(4, 'total', 'tax'),
(5, 'total', 'total'),
(6, 'module', 'banner'),
(7, 'module', 'carousel'),
(8, 'total', 'credit'),
(9, 'shipping', 'flat'),
(10, 'total', 'handling'),
(11, 'total', 'low_order_fee'),
(12, 'total', 'coupon'),
(13, 'module', 'category'),
(14, 'module', 'account'),
(15, 'total', 'reward'),
(16, 'total', 'voucher'),
(17, 'payment', 'free_checkout'),
(18, 'module', 'featured'),
(19, 'module', 'slideshow'),
(20, 'theme', 'default'),
(21, 'dashboard', 'activity'),
(22, 'dashboard', 'sale'),
(23, 'dashboard', 'recent'),
(24, 'dashboard', 'order'),
(25, 'dashboard', 'online'),
(26, 'dashboard', 'map'),
(27, 'dashboard', 'customer'),
(28, 'dashboard', 'chart'),
(29, 'report', 'sale_coupon'),
(31, 'report', 'customer_search'),
(32, 'report', 'customer_transaction'),
(33, 'report', 'product_purchased'),
(34, 'report', 'product_viewed'),
(35, 'report', 'sale_return'),
(36, 'report', 'sale_order'),
(37, 'report', 'sale_shipping'),
(38, 'report', 'sale_tax'),
(39, 'report', 'customer_activity'),
(40, 'report', 'customer_order'),
(41, 'report', 'customer_reward'),
(47, 'field', 'string'),
(51, 'action', 'edit'),
(55, 'field', 'link'),
(53, 'action', 'access'),
(54, 'field', 'int'),
(56, 'field', 'hidden'),
(57, 'field', 'datetime'),
(58, 'field', 'text'),
(59, 'action', 'dialog'),
(60, 'payment', 'bank_transfer'),
(61, 'action', 'creation'),
(63, 'action', 'record'),
(64, 'action', 'condition'),
(82, 'field', 'file'),
(74, 'action', 'remove'),
(84, 'field', 'list'),
(85, 'field', 'table'),
(86, 'action', 'move'),
(87, 'action', 'redirect'),
(88, 'action', 'selection'),
(89, 'action', 'timer'),
(90, 'action', 'email'),
(91, 'service', 'daemon'),
(92, 'service', 'stat'),
(93, 'service', 'export');

DROP TABLE IF EXISTS `extension_install`;
CREATE TABLE `extension_install` (
  `extension_install_id` int(11) NOT NULL,
  `extension_download_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `extension_install` (`extension_install_id`, `extension_download_id`, `filename`, `date_added`) VALUES
(1, 0, 'extension.ocmod.zip', '2017-10-18 16:07:46'),
(4, 0, 'auth.ocmod.zip', '2017-11-14 11:40:01');

DROP TABLE IF EXISTS `extension_path`;
CREATE TABLE `extension_path` (
  `extension_path_id` int(11) NOT NULL,
  `extension_install_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `field`;
CREATE TABLE `field` (
  `field_uid` varchar(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `doctype_uid` varchar(36) NOT NULL,
  `type` varchar(255) NOT NULL,
  `setting` tinyint(4) NOT NULL,
  `change_field` tinyint(4) NOT NULL,
  `access_form` text,
  `access_view` text,
  `required` tinyint(4) DEFAULT NULL,
  `unique` tinyint(4) DEFAULT NULL,
  `params` text NOT NULL,
  `sort` int(11) NOT NULL,
  `draft` int(11) NOT NULL,
  `draft_params` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `field` (`field_uid`, `name`, `doctype_uid`, `type`, `setting`, `change_field`, `access_form`, `access_view`, `required`, `unique`, `params`, `sort`, `draft`, `draft_params`) VALUES
('0a009bb0-99f9-11e8-8da9-485ab6e1c06f', 'Последняя активность', '51f800b5-1df9-11e8-a7fb-201a06f86b88', 'datetime', 0, 0, '', '', 0, 0, 'a:9:{s:6:\"format\";s:11:\"d.m.Y H:i:s\";s:10:\"field_name\";s:39:\"Последняя активность\";s:7:\"setting\";s:1:\"0\";s:12:\"change_field\";s:1:\"0\";s:8:\"required\";s:1:\"0\";s:6:\"unique\";s:1:\"0\";s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:4:\"sort\";i:14;}', 11, 0, ''),
('16739861-5f23-11e8-b6a0-201a06f86b88', 'Сотрудник', '9f91722e-5823-11e8-841d-201a06f86b88', 'link', 0, 0, 'd269d126-58db-11e8-a8d8-201a06f86b88', '', 0, 0, 'a:13:{s:11:\"doctype_uid\";s:36:\"51f803b2-1df9-11e8-a7fb-201a06f86b88\";s:17:\"doctype_field_uid\";s:36:\"54fde8ea-1df9-11e8-a7fb-201a06f86b88\";s:4:\"list\";s:1:\"0\";s:12:\"multi_select\";s:1:\"0\";s:9:\"delimiter\";s:2:\", \";s:4:\"href\";s:1:\"1\";s:10:\"field_name\";s:18:\"Сотрудник\";s:12:\"change_field\";i:0;s:11:\"access_form\";a:1:{i:0;s:36:\"d269d126-58db-11e8-a8d8-201a06f86b88\";}s:11:\"access_view\";a:0:{}s:8:\"required\";i:0;s:6:\"unique\";i:0;s:4:\"sort\";i:6;}', 6, 0, ''),
('1f458fe7-5826-11e8-a8d8-201a06f86b88', 'Стартовая страница', '9f91722e-5823-11e8-841d-201a06f86b88', 'string', 0, 0, '', '', 0, 0, 'a:9:{s:4:\"mask\";s:0:\"\";s:10:\"field_name\";s:35:\"Стартовая страница\";s:7:\"setting\";s:1:\"0\";s:12:\"change_field\";s:1:\"0\";s:8:\"required\";s:1:\"0\";s:6:\"unique\";s:1:\"0\";s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:4:\"sort\";i:2;}', 2, 0, ''),
('24251a7f-5f23-11e8-b6a0-201a06f86b88', 'Должность', '9f91722e-5823-11e8-841d-201a06f86b88', 'link', 0, 0, 'd269d126-58db-11e8-a8d8-201a06f86b88', '', 0, 0, 'a:13:{s:11:\"doctype_uid\";s:36:\"51f803b2-1df9-11e8-a7fb-201a06f86b88\";s:17:\"doctype_field_uid\";s:36:\"54fded41-1df9-11e8-a7fb-201a06f86b88\";s:4:\"list\";s:1:\"0\";s:12:\"multi_select\";s:1:\"0\";s:9:\"delimiter\";s:2:\", \";s:4:\"href\";s:1:\"1\";s:10:\"field_name\";s:18:\"Должность\";s:12:\"change_field\";i:0;s:11:\"access_form\";a:1:{i:0;s:36:\"d269d126-58db-11e8-a8d8-201a06f86b88\";}s:11:\"access_view\";a:0:{}s:8:\"required\";i:0;s:6:\"unique\";i:0;s:4:\"sort\";i:7;}', 7, 0, ''),
('39272f0e-90c0-11e8-9039-485ab6e1c06f', 'ВСЯ ОРГАНИЗАЦИЯ', '51f80627-1df9-11e8-a7fb-201a06f86b88', 'link', 1, 0, '', '', 0, 0, 'a:14:{s:11:\"doctype_uid\";s:36:\"51f803b2-1df9-11e8-a7fb-201a06f86b88\";s:17:\"doctype_field_uid\";s:36:\"54fde8ea-1df9-11e8-a7fb-201a06f86b88\";s:4:\"list\";s:1:\"0\";s:12:\"multi_select\";s:1:\"0\";s:9:\"delimiter\";s:2:\", \";s:4:\"href\";s:1:\"0\";s:10:\"field_name\";s:29:\"ВСЯ ОРГАНИЗАЦИЯ\";s:7:\"setting\";i:1;s:12:\"change_field\";s:1:\"0\";s:8:\"required\";s:1:\"0\";s:6:\"unique\";s:1:\"0\";s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:4:\"sort\";i:9;}', 9, 0, ''),
('3e53bfb3-9184-11e8-8f8b-485ab6e1c06f', 'Состояние', '666a0f8c-9172-11e8-8f8b-485ab6e1c06f', 'string', 0, 0, '', '', 0, 0, 'a:9:{s:4:\"mask\";s:0:\"\";s:10:\"field_name\";s:18:\"Состояние\";s:7:\"setting\";s:1:\"0\";s:12:\"change_field\";s:1:\"0\";s:8:\"required\";s:1:\"0\";s:6:\"unique\";s:1:\"0\";s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:4:\"sort\";i:11;}', 11, 0, ''),
('425fd7d7-59be-11e8-958b-201a06f86b88', 'Последняя страница', '51f800b5-1df9-11e8-a7fb-201a06f86b88', 'string', 0, 0, '', '', 0, 0, 'a:9:{s:4:\"mask\";s:0:\"\";s:10:\"field_name\";s:35:\"Последняя страница\";s:7:\"setting\";s:1:\"0\";s:12:\"change_field\";s:1:\"0\";s:8:\"required\";s:1:\"0\";s:6:\"unique\";s:1:\"0\";s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:4:\"sort\";i:12;}', 9, 0, ''),
('45978557-9184-11e8-8f8b-485ab6e1c06f', 'Дата создания', '666a0f8c-9172-11e8-8f8b-485ab6e1c06f', 'datetime', 0, 0, '', '', 0, 0, 'a:9:{s:6:\"format\";s:5:\"d.m.Y\";s:10:\"field_name\";s:25:\"Дата создания\";s:7:\"setting\";s:1:\"0\";s:12:\"change_field\";s:1:\"0\";s:8:\"required\";s:1:\"0\";s:6:\"unique\";s:1:\"0\";s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:4:\"sort\";i:12;}', 12, 0, ''),
('48cae257-3259-11e8-8ed6-201a06f86b88', 'Администратор', '51f800b5-1df9-11e8-a7fb-201a06f86b88', 'list', 0, 0, NULL, NULL, NULL, NULL, 'a:7:{s:6:\"values\";a:1:{i:1;a:2:{s:5:\"value\";s:1:\"1\";s:5:\"title\";s:4:\"Да\";}}s:12:\"multi_select\";s:1:\"1\";s:13:\"visualization\";s:1:\"0\";s:10:\"field_name\";s:26:\"Администратор\";s:7:\"setting\";s:1:\"0\";s:12:\"change_field\";s:1:\"0\";s:4:\"sort\";i:10;}', 7, 0, ''),
('48d0676a-90c0-11e8-9039-485ab6e1c06f', 'АДМИНИСТРАТОРЫ', '51f80627-1df9-11e8-a7fb-201a06f86b88', 'link', 1, 0, '', '', 0, 0, 'a:14:{s:11:\"doctype_uid\";s:36:\"51f803b2-1df9-11e8-a7fb-201a06f86b88\";s:17:\"doctype_field_uid\";s:36:\"54fde8ea-1df9-11e8-a7fb-201a06f86b88\";s:4:\"list\";s:1:\"0\";s:12:\"multi_select\";s:1:\"1\";s:9:\"delimiter\";s:2:\", \";s:4:\"href\";s:1:\"0\";s:10:\"field_name\";s:28:\"АДМИНИСТРАТОРЫ\";s:7:\"setting\";i:1;s:12:\"change_field\";s:1:\"0\";s:8:\"required\";s:1:\"0\";s:6:\"unique\";s:1:\"0\";s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:4:\"sort\";i:10;}', 10, 0, ''),
('50dd5916-37fc-11e8-a971-52540028bc1e', 'Стартовая страница', '51f800b5-1df9-11e8-a7fb-201a06f86b88', 'string', 0, 0, NULL, NULL, NULL, NULL, 'a:5:{s:4:\"mask\";s:0:\"\";s:10:\"field_name\";s:35:\"Стартовая страница\";s:7:\"setting\";s:1:\"0\";s:12:\"change_field\";s:1:\"0\";s:4:\"sort\";i:11;}', 8, 0, ''),
('54fde55f-1df9-11e8-a7fb-201a06f86b88', 'E-mail', '51f800b5-1df9-11e8-a7fb-201a06f86b88', 'string', 0, 0, '', '', 1, 1, 'a:7:{s:4:\"mask\";s:0:\"\";s:10:\"field_name\";s:6:\"E-mail\";s:12:\"change_field\";i:0;s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:8:\"required\";i:1;s:6:\"unique\";i:1;}', 1, 0, ''),
('54fde735-1df9-11e8-a7fb-201a06f86b88', 'Пароль', '51f800b5-1df9-11e8-a7fb-201a06f86b88', 'hidden', 0, 0, '', '', 1, 0, 'a:7:{s:9:\"type_hash\";s:1:\"1\";s:10:\"field_name\";s:12:\"Пароль\";s:12:\"change_field\";i:0;s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:8:\"required\";i:1;s:6:\"unique\";i:0;}', 2, 0, ''),
('54fde7d4-1df9-11e8-a7fb-201a06f86b88', 'Статус', '51f800b5-1df9-11e8-a7fb-201a06f86b88', 'list', 0, 1, '', '', 0, 0, 'a:10:{s:6:\"values\";a:2:{i:0;a:2:{s:5:\"value\";s:1:\"0\";s:5:\"title\";s:14:\"Активен\";}i:1;a:2:{s:5:\"value\";s:1:\"1\";s:5:\"title\";s:20:\"Блокирован\";}}s:12:\"multi_select\";s:1:\"0\";s:13:\"visualization\";s:1:\"0\";s:10:\"field_name\";s:12:\"Статус\";s:12:\"change_field\";i:1;s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:8:\"required\";i:0;s:6:\"unique\";i:0;s:4:\"sort\";i:15;}', 13, 0, ''),
('54fde858-1df9-11e8-a7fb-201a06f86b88', 'Количество попыток входа', '51f800b5-1df9-11e8-a7fb-201a06f86b88', 'int', 0, 0, NULL, NULL, NULL, NULL, 'a:6:{s:9:\"delimiter\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:10:\"field_name\";s:46:\"Количество попыток входа\";s:7:\"setting\";s:1:\"0\";s:4:\"sort\";i:10;}', 3, 0, ''),
('54fde8ea-1df9-11e8-a7fb-201a06f86b88', 'Отображаемое имя', '51f803b2-1df9-11e8-a7fb-201a06f86b88', 'string', 0, 0, '', '', 1, 1, 'a:7:{s:4:\"mask\";s:0:\"\";s:10:\"field_name\";s:31:\"Отображаемое имя\";s:12:\"change_field\";i:0;s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:8:\"required\";i:1;s:6:\"unique\";i:1;}', 1, 0, ''),
('54fde977-1df9-11e8-a7fb-201a06f86b88', 'Родитель', '51f803b2-1df9-11e8-a7fb-201a06f86b88', 'link', 0, 0, '', '', 1, 0, 'a:13:{s:11:\"doctype_uid\";s:36:\"51f803b2-1df9-11e8-a7fb-201a06f86b88\";s:17:\"doctype_field_uid\";s:36:\"54fde8ea-1df9-11e8-a7fb-201a06f86b88\";s:4:\"list\";s:1:\"0\";s:12:\"multi_select\";s:1:\"0\";s:9:\"delimiter\";s:2:\", \";s:4:\"href\";s:1:\"0\";s:10:\"field_name\";s:16:\"Родитель\";s:12:\"change_field\";i:0;s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:8:\"required\";i:1;s:6:\"unique\";i:0;s:4:\"sort\";i:2;}', 2, 0, ''),
('54fde9f3-1df9-11e8-a7fb-201a06f86b88', 'Пользователь', '51f803b2-1df9-11e8-a7fb-201a06f86b88', 'link', 0, 1, '', '', 0, 0, 'a:13:{s:11:\"doctype_uid\";s:36:\"51f800b5-1df9-11e8-a7fb-201a06f86b88\";s:17:\"doctype_field_uid\";s:36:\"54fde55f-1df9-11e8-a7fb-201a06f86b88\";s:4:\"list\";s:1:\"0\";s:12:\"multi_select\";s:1:\"0\";s:9:\"delimiter\";s:2:\", \";s:4:\"href\";s:1:\"0\";s:10:\"field_name\";s:24:\"Пользователь\";s:12:\"change_field\";i:1;s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:8:\"required\";i:0;s:6:\"unique\";i:0;s:4:\"sort\";i:1;}', 3, 0, ''),
('54fded41-1df9-11e8-a7fb-201a06f86b88', 'Должность', '51f803b2-1df9-11e8-a7fb-201a06f86b88', 'string', 0, 0, NULL, NULL, NULL, NULL, 'a:4:{s:4:\"mask\";s:0:\"\";s:10:\"field_name\";s:18:\"Должность\";s:7:\"setting\";s:1:\"0\";s:4:\"sort\";i:8;}', 8, 0, ''),
('54fdedb9-1df9-11e8-a7fb-201a06f86b88', 'Телефон', '51f803b2-1df9-11e8-a7fb-201a06f86b88', 'string', 0, 0, NULL, NULL, NULL, NULL, 'a:4:{s:4:\"mask\";s:16:\"+9(999)999-99-99\";s:10:\"field_name\";s:14:\"Телефон\";s:7:\"setting\";s:1:\"0\";s:4:\"sort\";i:9;}', 9, 0, ''),
('54fdeea9-1df9-11e8-a7fb-201a06f86b88', 'Сотрудник', '51f800b5-1df9-11e8-a7fb-201a06f86b88', 'link', 0, 0, NULL, NULL, NULL, NULL, 'a:6:{s:11:\"doctype_uid\";s:36:\"51f803b2-1df9-11e8-a7fb-201a06f86b88\";s:17:\"doctype_field_uid\";s:36:\"54fde8ea-1df9-11e8-a7fb-201a06f86b88\";s:12:\"multi_select\";s:1:\"0\";s:9:\"delimiter\";s:2:\", \";s:10:\"field_name\";s:18:\"Сотрудник\";s:4:\"sort\";i:6;}', 4, 0, ''),
('54fe1ad4-1df9-11e8-a7fb-201a06f86b88', 'Начальное кол-во попыток входа', '51f800b5-1df9-11e8-a7fb-201a06f86b88', 'int', 1, 0, NULL, NULL, NULL, NULL, 'a:5:{s:9:\"delimiter\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:10:\"field_name\";s:56:\"Начальное кол-во попыток входа\";s:4:\"sort\";i:7;}', 5, 0, ''),
('635eb9f2-5826-11e8-a8d8-201a06f86b88', 'Пользователь: Стартовая страница', '9f91722e-5823-11e8-841d-201a06f86b88', 'link', 0, 0, '', '', 0, 0, 'a:13:{s:11:\"doctype_uid\";s:36:\"51f800b5-1df9-11e8-a7fb-201a06f86b88\";s:17:\"doctype_field_uid\";s:36:\"50dd5916-37fc-11e8-a971-52540028bc1e\";s:4:\"list\";s:1:\"0\";s:12:\"multi_select\";s:1:\"0\";s:9:\"delimiter\";s:2:\", \";s:10:\"field_name\";s:61:\"Пользователь: Стартовая страница\";s:7:\"setting\";s:1:\"0\";s:12:\"change_field\";s:1:\"0\";s:8:\"required\";s:1:\"0\";s:6:\"unique\";s:1:\"0\";s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:4:\"sort\";i:3;}', 3, 0, ''),
('7843a7a9-9172-11e8-8f8b-485ab6e1c06f', 'Тема документа', '666a0f8c-9172-11e8-8f8b-485ab6e1c06f', 'string', 0, 0, '', '', 1, 0, 'a:8:{s:4:\"mask\";s:0:\"\";s:10:\"field_name\";s:27:\"Тема документа\";s:12:\"change_field\";i:0;s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:8:\"required\";i:1;s:6:\"unique\";i:0;s:4:\"sort\";i:1;}', 1, 0, ''),
('79861ef1-9174-11e8-8f8b-485ab6e1c06f', 'Результат подписи', '666a0f8c-9172-11e8-8f8b-485ab6e1c06f', 'list', 0, 1, '', '', 0, 0, 'a:10:{s:6:\"values\";a:2:{i:1;a:2:{s:5:\"value\";s:1:\"1\";s:5:\"title\";s:16:\"Согласен\";}i:2;a:2:{s:5:\"value\";s:1:\"2\";s:5:\"title\";s:21:\"Не согласен\";}}s:12:\"multi_select\";s:1:\"0\";s:13:\"visualization\";s:1:\"1\";s:10:\"field_name\";s:33:\"Результат подписи\";s:12:\"change_field\";i:1;s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:8:\"required\";i:0;s:6:\"unique\";i:0;s:4:\"sort\";i:9;}', 9, 0, ''),
('7db6ef6b-9172-11e8-8f8b-485ab6e1c06f', 'Содержимое документа', '666a0f8c-9172-11e8-8f8b-485ab6e1c06f', 'text', 0, 0, '', '', 1, 0, 'a:8:{s:14:\"editor_enabled\";s:4:\"true\";s:10:\"field_name\";s:39:\"Содержимое документа\";s:12:\"change_field\";i:0;s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:8:\"required\";i:1;s:6:\"unique\";i:0;s:4:\"sort\";i:2;}', 2, 0, ''),
('8425a629-90c2-11e8-9039-485ab6e1c06f', 'Ход работы', '51f803b2-1df9-11e8-a7fb-201a06f86b88', 'text', 0, 0, '', '', 0, 0, 'a:9:{s:14:\"editor_enabled\";s:4:\"true\";s:10:\"field_name\";s:19:\"Ход работы\";s:7:\"setting\";s:1:\"0\";s:12:\"change_field\";s:1:\"0\";s:8:\"required\";s:1:\"0\";s:6:\"unique\";s:1:\"0\";s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:4:\"sort\";i:10;}', 10, 0, ''),
('89ff9965-9172-11e8-8f8b-485ab6e1c06f', 'Подписант', '666a0f8c-9172-11e8-8f8b-485ab6e1c06f', 'link', 0, 0, '', '', 1, 0, 'a:13:{s:11:\"doctype_uid\";s:36:\"51f803b2-1df9-11e8-a7fb-201a06f86b88\";s:17:\"doctype_field_uid\";s:36:\"54fde8ea-1df9-11e8-a7fb-201a06f86b88\";s:4:\"list\";s:1:\"0\";s:12:\"multi_select\";s:1:\"0\";s:9:\"delimiter\";s:2:\", \";s:4:\"href\";s:1:\"0\";s:10:\"field_name\";s:18:\"Подписант\";s:12:\"change_field\";i:0;s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:8:\"required\";i:1;s:6:\"unique\";i:0;s:4:\"sort\";i:3;}', 3, 0, ''),
('8ef255ae-9173-11e8-8f8b-485ab6e1c06f', 'Отчет об исполнении', '666a0f8c-9172-11e8-8f8b-485ab6e1c06f', 'text', 0, 0, '', '', 0, 0, 'a:9:{s:14:\"editor_enabled\";s:4:\"true\";s:10:\"field_name\";s:36:\"Отчет об исполнении\";s:7:\"setting\";s:1:\"0\";s:12:\"change_field\";s:1:\"0\";s:8:\"required\";s:1:\"0\";s:6:\"unique\";s:1:\"0\";s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:4:\"sort\";i:8;}', 8, 0, ''),
('9230b3d4-9172-11e8-8f8b-485ab6e1c06f', 'Адресат', '666a0f8c-9172-11e8-8f8b-485ab6e1c06f', 'link', 0, 0, '', '', 1, 0, 'a:13:{s:11:\"doctype_uid\";s:36:\"51f803b2-1df9-11e8-a7fb-201a06f86b88\";s:17:\"doctype_field_uid\";s:36:\"54fde8ea-1df9-11e8-a7fb-201a06f86b88\";s:4:\"list\";s:1:\"0\";s:12:\"multi_select\";s:1:\"0\";s:9:\"delimiter\";s:2:\", \";s:4:\"href\";s:1:\"0\";s:10:\"field_name\";s:14:\"Адресат\";s:12:\"change_field\";i:0;s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:8:\"required\";i:1;s:6:\"unique\";i:0;s:4:\"sort\";i:4;}', 4, 0, ''),
('a03809a9-9172-11e8-8f8b-485ab6e1c06f', 'Исполнитель', '666a0f8c-9172-11e8-8f8b-485ab6e1c06f', 'link', 0, 0, '', '', 0, 0, 'a:13:{s:11:\"doctype_uid\";s:36:\"51f803b2-1df9-11e8-a7fb-201a06f86b88\";s:17:\"doctype_field_uid\";s:36:\"54fde8ea-1df9-11e8-a7fb-201a06f86b88\";s:4:\"list\";s:1:\"0\";s:12:\"multi_select\";s:1:\"0\";s:9:\"delimiter\";s:2:\", \";s:4:\"href\";s:1:\"0\";s:10:\"field_name\";s:22:\"Исполнитель\";s:12:\"change_field\";i:0;s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:8:\"required\";i:0;s:6:\"unique\";i:0;s:4:\"sort\";i:5;}', 5, 0, ''),
('a4846198-914d-11e8-8f8b-485ab6e1c06f', 'Текущий пользователь', '51f800b5-1df9-11e8-a7fb-201a06f86b88', 'link', 0, 0, '', '', 0, 0, 'a:14:{s:11:\"doctype_uid\";s:36:\"51f800b5-1df9-11e8-a7fb-201a06f86b88\";s:17:\"doctype_field_uid\";s:36:\"54fde55f-1df9-11e8-a7fb-201a06f86b88\";s:4:\"list\";s:1:\"0\";s:12:\"multi_select\";s:1:\"0\";s:9:\"delimiter\";s:2:\", \";s:4:\"href\";s:1:\"0\";s:10:\"field_name\";s:39:\"Текущий пользователь\";s:7:\"setting\";s:1:\"0\";s:12:\"change_field\";s:1:\"0\";s:8:\"required\";s:1:\"0\";s:6:\"unique\";s:1:\"0\";s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:4:\"sort\";i:13;}', 10, 0, ''),
('acd0bdad-914d-11e8-8f8b-485ab6e1c06f', 'Текущий документ', '51f800b5-1df9-11e8-a7fb-201a06f86b88', 'link', 0, 0, '', '', 0, 0, 'a:14:{s:11:\"doctype_uid\";s:36:\"51f800b5-1df9-11e8-a7fb-201a06f86b88\";s:17:\"doctype_field_uid\";s:36:\"54fde55f-1df9-11e8-a7fb-201a06f86b88\";s:4:\"list\";s:1:\"0\";s:12:\"multi_select\";s:1:\"0\";s:9:\"delimiter\";s:2:\", \";s:4:\"href\";s:1:\"0\";s:10:\"field_name\";s:31:\"Текущий документ\";s:7:\"setting\";s:1:\"0\";s:12:\"change_field\";s:1:\"0\";s:8:\"required\";s:1:\"0\";s:6:\"unique\";s:1:\"0\";s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:4:\"sort\";i:14;}', 12, 0, ''),
('ad0cde4c-9172-11e8-8f8b-485ab6e1c06f', 'Текст поручения', '666a0f8c-9172-11e8-8f8b-485ab6e1c06f', 'text', 0, 0, '', '', 0, 0, 'a:8:{s:10:\"field_name\";s:29:\"Текст поручения\";s:7:\"setting\";s:1:\"0\";s:12:\"change_field\";s:1:\"0\";s:8:\"required\";s:1:\"0\";s:6:\"unique\";s:1:\"0\";s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:4:\"sort\";i:7;}', 7, 0, ''),
('b21c5bf2-5cd0-11e8-b108-201a06f86b88', 'Замещающий', '51f803b2-1df9-11e8-a7fb-201a06f86b88', 'link', 0, 0, '', '', 0, 0, 'a:12:{s:11:\"doctype_uid\";s:36:\"51f803b2-1df9-11e8-a7fb-201a06f86b88\";s:17:\"doctype_field_uid\";s:36:\"54fde8ea-1df9-11e8-a7fb-201a06f86b88\";s:4:\"list\";s:1:\"0\";s:12:\"multi_select\";s:1:\"0\";s:9:\"delimiter\";s:2:\", \";s:10:\"field_name\";s:20:\"Замещающий\";s:12:\"change_field\";i:0;s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:8:\"required\";i:0;s:6:\"unique\";i:0;s:4:\"sort\";i:6;}', 6, 0, ''),
('c3b0321e-914c-11e8-8f8b-485ab6e1c06f', 'Текущий пользователь', '51f803b2-1df9-11e8-a7fb-201a06f86b88', 'link', 0, 0, '', '', 0, 0, 'a:14:{s:11:\"doctype_uid\";s:36:\"51f803b2-1df9-11e8-a7fb-201a06f86b88\";s:17:\"doctype_field_uid\";s:36:\"54fde8ea-1df9-11e8-a7fb-201a06f86b88\";s:4:\"list\";s:1:\"0\";s:12:\"multi_select\";s:1:\"0\";s:9:\"delimiter\";s:2:\", \";s:4:\"href\";s:1:\"0\";s:10:\"field_name\";s:39:\"Текущий пользователь\";s:7:\"setting\";s:1:\"0\";s:12:\"change_field\";s:1:\"0\";s:8:\"required\";s:1:\"0\";s:6:\"unique\";s:1:\"0\";s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:4:\"sort\";i:11;}', 11, 0, ''),
('c4443c3b-58db-11e8-a8d8-201a06f86b88', 'Язык', '9f91722e-5823-11e8-841d-201a06f86b88', 'list', 0, 0, '', '', 0, 0, 'a:11:{s:6:\"values\";a:2:{i:1;a:2:{s:5:\"value\";s:1:\"1\";s:5:\"title\";s:7:\"English\";}i:2;a:2:{s:5:\"value\";s:1:\"2\";s:5:\"title\";s:14:\"Русский\";}}s:12:\"multi_select\";s:1:\"0\";s:13:\"visualization\";s:1:\"0\";s:10:\"field_name\";s:8:\"Язык\";s:7:\"setting\";s:1:\"0\";s:12:\"change_field\";s:1:\"0\";s:8:\"required\";s:1:\"0\";s:6:\"unique\";s:1:\"0\";s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:4:\"sort\";i:4;}', 4, 0, ''),
('d0e9bc2a-30b7-11e8-9fbe-201a06f86b88', 'Язык', '51f800b5-1df9-11e8-a7fb-201a06f86b88', 'list', 0, 0, '', '', 0, 0, 'a:11:{s:6:\"values\";a:2:{i:1;a:2:{s:5:\"value\";s:1:\"1\";s:5:\"title\";s:7:\"English\";}i:2;a:2:{s:5:\"value\";s:1:\"2\";s:5:\"title\";s:14:\"Русский\";}}s:13:\"default_value\";s:1:\"2\";s:12:\"multi_select\";s:1:\"0\";s:13:\"visualization\";s:1:\"0\";s:10:\"field_name\";s:8:\"Язык\";s:12:\"change_field\";i:0;s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:8:\"required\";i:0;s:6:\"unique\";i:0;s:4:\"sort\";i:9;}', 6, 0, ''),
('d269d126-58db-11e8-a8d8-201a06f86b88', 'Пользователь: Язык', '9f91722e-5823-11e8-841d-201a06f86b88', 'link', 0, 0, '', '', 0, 0, 'a:13:{s:11:\"doctype_uid\";s:36:\"51f800b5-1df9-11e8-a7fb-201a06f86b88\";s:17:\"doctype_field_uid\";s:36:\"d0e9bc2a-30b7-11e8-9fbe-201a06f86b88\";s:4:\"list\";s:1:\"0\";s:12:\"multi_select\";s:1:\"0\";s:9:\"delimiter\";s:2:\", \";s:10:\"field_name\";s:34:\"Пользователь: Язык\";s:7:\"setting\";s:1:\"0\";s:12:\"change_field\";s:1:\"0\";s:8:\"required\";s:1:\"0\";s:6:\"unique\";s:1:\"0\";s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:4:\"sort\";i:5;}', 5, 0, ''),
('e064f290-9173-11e8-8f8b-485ab6e1c06f', 'Автор', '666a0f8c-9172-11e8-8f8b-485ab6e1c06f', 'link', 0, 0, '', '', 0, 0, 'a:14:{s:11:\"doctype_uid\";s:36:\"51f803b2-1df9-11e8-a7fb-201a06f86b88\";s:17:\"doctype_field_uid\";s:36:\"54fde8ea-1df9-11e8-a7fb-201a06f86b88\";s:4:\"list\";s:1:\"0\";s:12:\"multi_select\";s:1:\"0\";s:9:\"delimiter\";s:2:\", \";s:4:\"href\";s:1:\"0\";s:10:\"field_name\";s:10:\"Автор\";s:7:\"setting\";s:1:\"0\";s:12:\"change_field\";s:1:\"0\";s:8:\"required\";s:1:\"0\";s:6:\"unique\";s:1:\"0\";s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:4:\"sort\";i:6;}', 6, 0, ''),
('ec8a023d-5825-11e8-a8d8-201a06f86b88', 'Пароль', '9f91722e-5823-11e8-841d-201a06f86b88', 'hidden', 0, 0, '', '', 0, 0, 'a:9:{s:9:\"type_hash\";s:1:\"1\";s:10:\"field_name\";s:12:\"Пароль\";s:7:\"setting\";s:1:\"0\";s:12:\"change_field\";s:1:\"0\";s:8:\"required\";s:1:\"0\";s:6:\"unique\";s:1:\"0\";s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:4:\"sort\";i:1;}', 1, 0, ''),
('f123b3f1-9175-11e8-8f8b-485ab6e1c06f', 'Ход работы', '666a0f8c-9172-11e8-8f8b-485ab6e1c06f', 'text', 0, 0, '', '', 0, 0, 'a:9:{s:14:\"editor_enabled\";s:4:\"true\";s:10:\"field_name\";s:19:\"Ход работы\";s:7:\"setting\";s:1:\"0\";s:12:\"change_field\";s:1:\"0\";s:8:\"required\";s:1:\"0\";s:6:\"unique\";s:1:\"0\";s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:4:\"sort\";i:10;}', 10, 0, ''),
('f7c5e7ea-914c-11e8-8f8b-485ab6e1c06f', 'Текущий документ', '51f803b2-1df9-11e8-a7fb-201a06f86b88', 'link', 0, 0, '', '', 0, 0, 'a:14:{s:11:\"doctype_uid\";s:36:\"51f803b2-1df9-11e8-a7fb-201a06f86b88\";s:17:\"doctype_field_uid\";s:36:\"54fde8ea-1df9-11e8-a7fb-201a06f86b88\";s:4:\"list\";s:1:\"0\";s:12:\"multi_select\";s:1:\"0\";s:9:\"delimiter\";s:2:\", \";s:4:\"href\";s:1:\"0\";s:10:\"field_name\";s:31:\"Текущий документ\";s:7:\"setting\";s:1:\"0\";s:12:\"change_field\";s:1:\"0\";s:8:\"required\";s:1:\"0\";s:6:\"unique\";s:1:\"0\";s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:4:\"sort\";i:12;}', 12, 0, '');

DROP TABLE IF EXISTS `field_value_datetime`;
CREATE TABLE `field_value_datetime` (
  `field_uid` varchar(36) NOT NULL,
  `document_uid` varchar(36) NOT NULL,
  `value` datetime DEFAULT NULL,
  `display_value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `field_value_datetime` (`field_uid`, `document_uid`, `value`, `display_value`) VALUES
('0a009bb0-99f9-11e8-8da9-485ab6e1c06f', '5354daea-1df9-11e8-a7fb-201a06f86b88', '2018-08-07 14:58:49', '07.08.2018 14:58:49'),
('54fe17b8-1df9-11e8-a7fb-201a06f86b88', '5354f296-1df9-11e8-a7fb-201a06f86b88', '2018-02-16 16:57:12', '16.02.2018 16:57:12');

DROP TABLE IF EXISTS `field_value_file`;
CREATE TABLE `field_value_file` (
  `field_uid` varchar(36) NOT NULL,
  `document_uid` varchar(36) NOT NULL,
  `value` text,
  `display_value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `field_value_file_list`;
CREATE TABLE `field_value_file_list` (
  `file_uid` varchar(36) NOT NULL,
  `field_uid` varchar(36) NOT NULL,
  `document_uid` varchar(36) NOT NULL,
  `file_name` varchar(256) NOT NULL,
  `size` int(11) NOT NULL,
  `token` varchar(32) NOT NULL,
  `date_added` datetime NOT NULL,
  `user_uid` varchar(36) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `field_value_hidden`;
CREATE TABLE `field_value_hidden` (
  `field_uid` varchar(36) NOT NULL,
  `document_uid` varchar(36) NOT NULL,
  `value` varchar(255) NOT NULL,
  `display_value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `field_value_hidden` (`field_uid`, `document_uid`, `value`, `display_value`) VALUES
('54fde735-1df9-11e8-a7fb-201a06f86b88', '5354daea-1df9-11e8-a7fb-201a06f86b88', '$2y$10$9JJQio2RyLKvWIQSLHhfieNZdoAXAQsDwFvdxIATg8IGPtJI9J0eu', '*****');

DROP TABLE IF EXISTS `field_value_int`;
CREATE TABLE `field_value_int` (
  `field_uid` varchar(36) NOT NULL,
  `document_uid` varchar(36) NOT NULL,
  `value` int(11) DEFAULT NULL,
  `display_value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `field_value_int` (`field_uid`, `document_uid`, `value`, `display_value`) VALUES
('54fe1005-1df9-11e8-a7fb-201a06f86b88', '', 4, '4'),
('54fe1418-1df9-11e8-a7fb-201a06f86b88', '', 1, '1'),
('54fe1ad4-1df9-11e8-a7fb-201a06f86b88', '', 0, ''),
('54fe1ad4-1df9-11e8-a7fb-201a06f86b88', '0', 0, '0');

DROP TABLE IF EXISTS `field_value_link`;
CREATE TABLE `field_value_link` (
  `field_uid` varchar(36) NOT NULL,
  `document_uid` varchar(36) NOT NULL,
  `value` text NOT NULL,
  `display_value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `field_value_link` (`field_uid`, `document_uid`, `value`, `display_value`) VALUES
('16739861-5f23-11e8-b6a0-201a06f86b88', 'fb75c209-58f6-11e8-a8d8-201a06f86b88', '5354e0d0-1df9-11e8-a7fb-201a06f86b88', 'admin'),
('24251a7f-5f23-11e8-b6a0-201a06f86b88', 'fb75c209-58f6-11e8-a8d8-201a06f86b88', '5354e0d0-1df9-11e8-a7fb-201a06f86b88', 'должность'),
('39272f0e-90c0-11e8-9039-485ab6e1c06f', '0', '5354df9c-1df9-11e8-a7fb-201a06f86b88', 'ORG'),
('48d0676a-90c0-11e8-9039-485ab6e1c06f', '0', '5354e0d0-1df9-11e8-a7fb-201a06f86b88', 'admin'),
('54fde977-1df9-11e8-a7fb-201a06f86b88', '5354df9c-1df9-11e8-a7fb-201a06f86b88', '', ''),
('54fde977-1df9-11e8-a7fb-201a06f86b88', '5354e0d0-1df9-11e8-a7fb-201a06f86b88', '5354df9c-1df9-11e8-a7fb-201a06f86b88', 'ORG'),
('54fde9f3-1df9-11e8-a7fb-201a06f86b88', '5354df9c-1df9-11e8-a7fb-201a06f86b88', '', ''),
('54fde9f3-1df9-11e8-a7fb-201a06f86b88', '5354e0d0-1df9-11e8-a7fb-201a06f86b88', '5354daea-1df9-11e8-a7fb-201a06f86b88', 'ORG'),
('54fdeea9-1df9-11e8-a7fb-201a06f86b88', '', '0aad50b9-9172-11e8-8f8b-485ab6e1c06f', 'test22'),
('54fdeea9-1df9-11e8-a7fb-201a06f86b88', '5354daea-1df9-11e8-a7fb-201a06f86b88', '5354e0d0-1df9-11e8-a7fb-201a06f86b88', 'admin'),
('54fdf144-1df9-11e8-a7fb-201a06f86b88', '', '5354ecb5-1df9-11e8-a7fb-201a06f86b88', 'Исполненный'),
('635eb9f2-5826-11e8-a8d8-201a06f86b88', 'fb75c209-58f6-11e8-a8d8-201a06f86b88', '5354daea-1df9-11e8-a7fb-201a06f86b88', ''),
('a4846198-914d-11e8-8f8b-485ab6e1c06f', '5354daea-1df9-11e8-a7fb-201a06f86b88', '5354daea-1df9-11e8-a7fb-201a06f86b88', 'test@test.kz'),
('acd0bdad-914d-11e8-8f8b-485ab6e1c06f', '5354daea-1df9-11e8-a7fb-201a06f86b88', '5354daea-1df9-11e8-a7fb-201a06f86b88', 'test@test.kz'),
('d269d126-58db-11e8-a8d8-201a06f86b88', 'fb75c209-58f6-11e8-a8d8-201a06f86b88', '5354daea-1df9-11e8-a7fb-201a06f86b88', 'Русский');

DROP TABLE IF EXISTS `field_value_link_subscription`;
CREATE TABLE `field_value_link_subscription` (
  `subscription_document_uid` varchar(36) NOT NULL,
  `subscription_field_uid` varchar(36) NOT NULL,
  `document_uid` varchar(36) NOT NULL,
  `field_uid` varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `field_value_link_subscription` (`subscription_document_uid`, `subscription_field_uid`, `document_uid`, `field_uid`) VALUES
('0', '54', '', '54fdeea9-1df9-11e8-a7fb-201a06f86b88'),
('5354', '54', '0', '39272f0e-90c0-11e8-9039-485ab6e1c06f'),
('5354', '54', '0', '48d0676a-90c0-11e8-9039-485ab6e1c06f'),
('5354', '54', '0', '825994fc-58e9-11e8-a8d8-201a06f86b88'),
('5354', '54', '5354daea-1df9-11e8-a7fb-201a06f86b88', '54fdeea9-1df9-11e8-a7fb-201a06f86b88'),
('5354', '54', '5354daea-1df9-11e8-a7fb-201a06f86b88', 'a4846198-914d-11e8-8f8b-485ab6e1c06f'),
('5354', '54', '5354daea-1df9-11e8-a7fb-201a06f86b88', 'acd0bdad-914d-11e8-8f8b-485ab6e1c06f'),
('5354', '54', '5354e0d0-1df9-11e8-a7fb-201a06f86b88', '54fde977-1df9-11e8-a7fb-201a06f86b88'),
('5354', '54', '5354e0d0-1df9-11e8-a7fb-201a06f86b88', '54fde9f3-1df9-11e8-a7fb-201a06f86b88'),
('5354', '50', 'f6a69c53-58f6-11e8-a8d8-201a06f86b88', '635eb9f2-5826-11e8-a8d8-201a06f86b88'),
('5354', '0', 'f6a69c53-58f6-11e8-a8d8-201a06f86b88', 'd269d126-58db-11e8-a8d8-201a06f86b88'),
('5354', '54', 'fb75c209-58f6-11e8-a8d8-201a06f86b88', '16739861-5f23-11e8-b6a0-201a06f86b88'),
('5354', '54', 'fb75c209-58f6-11e8-a8d8-201a06f86b88', '24251a7f-5f23-11e8-b6a0-201a06f86b88'),
('5354', '50', 'fb75c209-58f6-11e8-a8d8-201a06f86b88', '635eb9f2-5826-11e8-a8d8-201a06f86b88'),
('5354', '0', 'fb75c209-58f6-11e8-a8d8-201a06f86b88', 'd269d126-58db-11e8-a8d8-201a06f86b88');

DROP TABLE IF EXISTS `field_value_list`;
CREATE TABLE `field_value_list` (
  `field_uid` varchar(36) NOT NULL,
  `document_uid` varchar(36) NOT NULL,
  `value` varchar(255) NOT NULL,
  `display_value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `field_value_list` (`field_uid`, `document_uid`, `value`, `display_value`) VALUES
('48cae257-3259-11e8-8ed6-201a06f86b88', '5354daea-1df9-11e8-a7fb-201a06f86b88', '1', 'Да'),
('c4443c3b-58db-11e8-a8d8-201a06f86b88', 'fb75c209-58f6-11e8-a8d8-201a06f86b88', '2', 'Русский'),
('d0e9bc2a-30b7-11e8-9fbe-201a06f86b88', '5354daea-1df9-11e8-a7fb-201a06f86b88', '2', 'Русский');

DROP TABLE IF EXISTS `field_value_string`;
CREATE TABLE `field_value_string` (
  `field_uid` varchar(36) NOT NULL,
  `document_uid` varchar(36) NOT NULL,
  `value` text NOT NULL,
  `display_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `field_value_string` (`field_uid`, `document_uid`, `value`, `display_value`) VALUES
('', '', '4', '4'),
('1f458fe7-5826-11e8-a8d8-201a06f86b88', 'fb75c209-58f6-11e8-a8d8-201a06f86b88', '', ''),
('425fd7d7-59be-11e8-958b-201a06f86b88', '5354daea-1df9-11e8-a7fb-201a06f86b88', 'index.php?route=doctype/doctype/edit&doctype_uid=51f800b5-1df9-11e8-a7fb-201a06f86b88', 'index.php?route=doctype/doctype/edit&doctype_uid=51f800b5-1df9-11e8-a7fb-201a06f86b88'),
('50dd5916-37fc-11e8-a971-52540028bc1e', '5354daea-1df9-11e8-a7fb-201a06f86b88', '', ''),
('54fde55f-1df9-11e8-a7fb-201a06f86b88', '5354daea-1df9-11e8-a7fb-201a06f86b88', 'test@test.kz', 'test@test.kz'),
('54fde8ea-1df9-11e8-a7fb-201a06f86b88', '5354df9c-1df9-11e8-a7fb-201a06f86b88', 'ORG', 'ORG'),
('54fde8ea-1df9-11e8-a7fb-201a06f86b88', '5354e0d0-1df9-11e8-a7fb-201a06f86b88', 'admin', 'admin'),
('54fded41-1df9-11e8-a7fb-201a06f86b88', '5354df9c-1df9-11e8-a7fb-201a06f86b88', '', ''),
('54fded41-1df9-11e8-a7fb-201a06f86b88', '5354e0d0-1df9-11e8-a7fb-201a06f86b88', 'должность', 'должность'),
('54fdedb9-1df9-11e8-a7fb-201a06f86b88', '5354df9c-1df9-11e8-a7fb-201a06f86b88', '', ''),
('54fdedb9-1df9-11e8-a7fb-201a06f86b88', '5354e0d0-1df9-11e8-a7fb-201a06f86b88', '', ''),
('54fe14b2-1df9-11e8-a7fb-201a06f86b88', '', '1', '1'),
('54fe1521-1df9-11e8-a7fb-201a06f86b88', '', '2', '2');

DROP TABLE IF EXISTS `field_value_table`;
CREATE TABLE `field_value_table` (
  `field_uid` varchar(36) NOT NULL,
  `document_uid` varchar(36) NOT NULL,
  `value` mediumtext,
  `display_value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `field_value_text`;
CREATE TABLE `field_value_text` (
  `field_uid` varchar(36) NOT NULL,
  `document_uid` varchar(36) NOT NULL,
  `value` mediumtext,
  `display_value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `field_value_text` (`field_uid`, `document_uid`, `value`, `display_value`) VALUES
('8425a629-90c2-11e8-9039-485ab6e1c06f', '5354df9c-1df9-11e8-a7fb-201a06f86b88', '27.07.2018 09:42:58 admin Изменить Документ изменен<br>\n27.07.2018 09:44:02 admin Изменить Документ изменен', '27.07.2018 09:42:58 admin Изменить Документ изменен<br>\n27.07.2018 09:44:02 admin Изменить Документ изменен'),
('8425a629-90c2-11e8-9039-485ab6e1c06f', '5354e0d0-1df9-11e8-a7fb-201a06f86b88', '27.07.2018 09:40:15 admin Изменить Документ изменен', '27.07.2018 09:40:15 admin Изменить Документ изменен');

DROP TABLE IF EXISTS `folder`;
CREATE TABLE `folder` (
  `folder_uid` varchar(36) NOT NULL,
  `type` varchar(256) NOT NULL,
  `doctype_uid` varchar(36) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_edited` datetime NOT NULL,
  `user_uid` varchar(36) NOT NULL,
  `additional_params` text NOT NULL,
  `draft` tinyint(4) NOT NULL,
  `draft_params` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `folder` (`folder_uid`, `type`, `doctype_uid`, `date_added`, `date_edited`, `user_uid`, `additional_params`, `draft`, `draft_params`) VALUES
('1812f272-9184-11e8-8f8b-485ab6e1c06f', '', '666a0f8c-9172-11e8-8f8b-485ab6e1c06f', '2018-07-27 16:01:56', '2018-07-27 16:08:48', '5354e0d0-1df9-11e8-a7fb-201a06f86b88', 'b:0;', 0, ''),
('5ea67a49-1df9-11e8-a7fb-201a06f86b88', '', '51f803b2-1df9-11e8-a7fb-201a06f86b88', '2018-02-13 15:29:56', '2018-07-27 13:45:28', '5354e0d0-1df9-11e8-a7fb-201a06f86b88', 'b:0;', 0, ''),
('5ea67c93-1df9-11e8-a7fb-201a06f86b88', '', '51f800b5-1df9-11e8-a7fb-201a06f86b88', '2018-02-14 14:14:05', '2018-07-27 13:46:53', '5354e0d0-1df9-11e8-a7fb-201a06f86b88', 'b:0;', 0, '');

DROP TABLE IF EXISTS `folder_button`;
CREATE TABLE `folder_button` (
  `folder_button_uid` varchar(36) NOT NULL,
  `folder_uid` varchar(36) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `color` varchar(6) NOT NULL,
  `background` varchar(6) NOT NULL,
  `action` varchar(255) NOT NULL,
  `action_log` tinyint(4) NOT NULL,
  `action_move_route_uid` varchar(36) NOT NULL,
  `action_params` text NOT NULL,
  `draft` int(11) NOT NULL,
  `draft_params` text NOT NULL,
  `sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `folder_button` (`folder_button_uid`, `folder_uid`, `picture`, `color`, `background`, `action`, `action_log`, `action_move_route_uid`, `action_params`, `draft`, `draft_params`, `sort`) VALUES
('01d4592a-9185-11e8-8f8b-485ab6e1c06f', '1812f272-9184-11e8-8f8b-485ab6e1c06f', '', '', '', 'remove', 1, '0', 'a:1:{s:7:\"confirm\";a:1:{i:2;s:20:\"Вы уверены?\";}}', 0, '', 2),
('1c3e0da5-90c1-11e8-9039-485ab6e1c06f', '5ea67c93-1df9-11e8-a7fb-201a06f86b88', '', '', '', '', 1, '990012ca-914d-11e8-8f8b-485ab6e1c06f', 'a:0:{}', 0, '', 3),
('60644e42-1df9-11e8-a7fb-201a06f86b88', '5ea67a49-1df9-11e8-a7fb-201a06f86b88', '', '', '', 'creation', 0, '0', 'a:3:{s:11:\"doctype_uid\";s:36:\"51f803b2-1df9-11e8-a7fb-201a06f86b88\";s:28:\"field_new_document_author_id\";s:0:\"\";s:22:\"field_new_document_uid\";s:0:\"\";}', 0, '', 2),
('60645061-1df9-11e8-a7fb-201a06f86b88', '5ea67a49-1df9-11e8-a7fb-201a06f86b88', '', '', '', '', 0, '23b37561-914c-11e8-8f8b-485ab6e1c06f', 'a:0:{}', 0, '', 3),
('606451e2-1df9-11e8-a7fb-201a06f86b88', '5ea67c93-1df9-11e8-a7fb-201a06f86b88', '', '', '', 'creation', 0, '0', 'a:3:{s:11:\"doctype_uid\";s:36:\"51f800b5-1df9-11e8-a7fb-201a06f86b88\";s:28:\"field_new_document_author_id\";s:0:\"\";s:22:\"field_new_document_uid\";s:0:\"\";}', 0, '', 2),
('ebf8e237-9184-11e8-8f8b-485ab6e1c06f', '1812f272-9184-11e8-8f8b-485ab6e1c06f', '', '', '', 'creation', 1, '0', 'a:3:{s:11:\"doctype_uid\";s:36:\"666a0f8c-9172-11e8-8f8b-485ab6e1c06f\";s:28:\"field_new_document_author_id\";s:0:\"\";s:22:\"field_new_document_uid\";s:0:\"\";}', 0, '', 1);

DROP TABLE IF EXISTS `folder_button_delegate`;
CREATE TABLE `folder_button_delegate` (
  `folder_button_uid` varchar(36) NOT NULL,
  `document_uid` varchar(36) NOT NULL,
  `structure_uid` varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `folder_button_delegate` (`folder_button_uid`, `document_uid`, `structure_uid`) VALUES
('01d4592a-9185-11e8-8f8b-485ab6e1c06f', '0', '5354e0d0-1df9-11e8-a7fb-201a06f86b88'),
('1c3e0da5-90c1-11e8-9039-485ab6e1c06f', '0', '5354e0d0-1df9-11e8-a7fb-201a06f86b88'),
('317756a5-58e8-11e8-a8d8-201a06f86b88', '0', '5354e0d0-1df9-11e8-a7fb-201a06f86b88'),
('60644e42-1df9-11e8-a7fb-201a06f86b88', '0', '5354e0d0-1df9-11e8-a7fb-201a06f86b88'),
('60645061-1df9-11e8-a7fb-201a06f86b88', '0', '5354e0d0-1df9-11e8-a7fb-201a06f86b88'),
('60645126-1df9-11e8-a7fb-201a06f86b88', '0', '5354e0d0-1df9-11e8-a7fb-201a06f86b88'),
('60645126-1df9-11e8-a7fb-201a06f86b88', '0', '5354e1f3-1df9-11e8-a7fb-201a06f86b88'),
('606451e2-1df9-11e8-a7fb-201a06f86b88', '0', '5354e0d0-1df9-11e8-a7fb-201a06f86b88'),
('7657828f-58e9-11e8-a8d8-201a06f86b88', '0', '5354e0d0-1df9-11e8-a7fb-201a06f86b88'),
('ebf8e237-9184-11e8-8f8b-485ab6e1c06f', '0', '5354df9c-1df9-11e8-a7fb-201a06f86b88');

DROP TABLE IF EXISTS `folder_button_description`;
CREATE TABLE `folder_button_description` (
  `folder_button_uid` varchar(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `folder_button_description` (`folder_button_uid`, `name`, `description`, `language_id`) VALUES
('01d4592a-9185-11e8-8f8b-485ab6e1c06f', 'Удалить', 'При помощи этой кнопки можно удалить выбранные документы', 2),
('1c3e0da5-90c1-11e8-9039-485ab6e1c06f', '', '', 1),
('1c3e0da5-90c1-11e8-9039-485ab6e1c06f', 'Удалить', '', 2),
('60644e42-1df9-11e8-a7fb-201a06f86b88', '', '', 1),
('60644e42-1df9-11e8-a7fb-201a06f86b88', 'Создать', '', 2),
('60645061-1df9-11e8-a7fb-201a06f86b88', '', '', 1),
('60645061-1df9-11e8-a7fb-201a06f86b88', 'Удалить', 'Изменить выделенного сотрудника / подразделение', 2),
('606451e2-1df9-11e8-a7fb-201a06f86b88', 'Create', '', 1),
('606451e2-1df9-11e8-a7fb-201a06f86b88', 'Создать', '', 2),
('9223372036854775807', '', '', 1),
('9223372036854775807', '', 'Добавить сотрудника / подразделение', 2),
('ebf8e237-9184-11e8-8f8b-485ab6e1c06f', 'Новый документ', 'Нажмите на эту кнопку, что создать новый документ', 2);

DROP TABLE IF EXISTS `folder_button_field`;
CREATE TABLE `folder_button_field` (
  `folder_button_uid` varchar(36) NOT NULL,
  `field_uid` varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `folder_button_field` (`folder_button_uid`, `field_uid`) VALUES
('01d4592a-9185-11e8-8f8b-485ab6e1c06f', '48d0676a-90c0-11e8-9039-485ab6e1c06f'),
('1c3e0da5-90c1-11e8-9039-485ab6e1c06f', '48d0676a-90c0-11e8-9039-485ab6e1c06f'),
('60644e42-1df9-11e8-a7fb-201a06f86b88', '48d0676a-90c0-11e8-9039-485ab6e1c06f'),
('60645061-1df9-11e8-a7fb-201a06f86b88', '48d0676a-90c0-11e8-9039-485ab6e1c06f'),
('606451e2-1df9-11e8-a7fb-201a06f86b88', '48d0676a-90c0-11e8-9039-485ab6e1c06f'),
('9223372036854775807', '54'),
('ebf8e237-9184-11e8-8f8b-485ab6e1c06f', '39272f0e-90c0-11e8-9039-485ab6e1c06f');

DROP TABLE IF EXISTS `folder_button_route`;
CREATE TABLE `folder_button_route` (
  `folder_button_uid` varchar(36) NOT NULL,
  `route_uid` varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `folder_button_route` (`folder_button_uid`, `route_uid`) VALUES
('01d4592a-9185-11e8-8f8b-485ab6e1c06f', '0'),
('1c3e0da5-90c1-11e8-9039-485ab6e1c06f', '5fa8bd6b-1df9-11e8-a7fb-201a06f86b88'),
('60644e42-1df9-11e8-a7fb-201a06f86b88', ''),
('60645061-1df9-11e8-a7fb-201a06f86b88', '5fa8b997-1df9-11e8-a7fb-201a06f86b88'),
('606451e2-1df9-11e8-a7fb-201a06f86b88', ''),
('9223372036854775807', '0'),
('ebf8e237-9184-11e8-8f8b-485ab6e1c06f', '0');

DROP TABLE IF EXISTS `folder_description`;
CREATE TABLE `folder_description` (
  `folder_uid` varchar(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `short_description` varchar(255) NOT NULL,
  `full_description` text NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `folder_description` (`folder_uid`, `name`, `short_description`, `full_description`, `language_id`) VALUES
('1812f272-9184-11e8-8f8b-485ab6e1c06f', 'Тестовый журнал', '', '', 2),
('5ea67a49-1df9-11e8-a7fb-201a06f86b88', 'Structure', '', '', 1),
('5ea67a49-1df9-11e8-a7fb-201a06f86b88', 'Структура', 'Журнал для отображения Структуры организации. ', '', 2),
('5ea67c93-1df9-11e8-a7fb-201a06f86b88', 'Users', '', '', 1),
('5ea67c93-1df9-11e8-a7fb-201a06f86b88', 'Пользователи', 'Пользователи системы', '', 2);

DROP TABLE IF EXISTS `folder_field`;
CREATE TABLE `folder_field` (
  `folder_field_uid` varchar(36) NOT NULL,
  `folder_uid` varchar(36) NOT NULL,
  `field_uid` varchar(36) NOT NULL,
  `grouping` tinyint(4) NOT NULL,
  `grouping_name` varchar(255) NOT NULL,
  `grouping_parent_uid` varchar(36) NOT NULL,
  `grouping_tree_uid` varchar(36) NOT NULL,
  `tcolumn` tinyint(4) NOT NULL,
  `tcolumn_name` varchar(255) NOT NULL,
  `draft` tinyint(4) NOT NULL,
  `draft_params` text NOT NULL,
  `sort_grouping` int(11) NOT NULL,
  `sort_tcolumn` int(11) NOT NULL,
  `default_sort` tinyint(4) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `folder_field` (`folder_field_uid`, `folder_uid`, `field_uid`, `grouping`, `grouping_name`, `grouping_parent_uid`, `grouping_tree_uid`, `tcolumn`, `tcolumn_name`, `draft`, `draft_params`, `sort_grouping`, `sort_tcolumn`, `default_sort`, `language_id`) VALUES
('28ef1d67-9184-11e8-8f8b-485ab6e1c06f', '1812f272-9184-11e8-8f8b-485ab6e1c06f', '7843a7a9-9172-11e8-8f8b-485ab6e1c06f', 0, '', '0', '0', 1, 'Тема документа', 0, '', 0, 1, 0, 2),
('2de63b99-9184-11e8-8f8b-485ab6e1c06f', '1812f272-9184-11e8-8f8b-485ab6e1c06f', 'e064f290-9173-11e8-8f8b-485ab6e1c06f', 0, '', '0', '0', 1, 'Автор', 0, '', 0, 2, 0, 2),
('36adbf8e-9184-11e8-8f8b-485ab6e1c06f', '1812f272-9184-11e8-8f8b-485ab6e1c06f', '9230b3d4-9172-11e8-8f8b-485ab6e1c06f', 1, 'по адресатам', '0', '0', 1, 'Адресат', 0, '', 0, 3, 0, 2),
('3e6ab6fa-5e7d-11e8-9f10-201a06f86b88', '5ea67a49-1df9-11e8-a7fb-201a06f86b88', '54fde8ea-1df9-11e8-a7fb-201a06f86b88', 1, '', '0', '54fde977-1df9-11e8-a7fb-201a06f86b88', 1, 'Name', 0, '', 1, 1, 0, 1),
('52443e30-5e7d-11e8-9f10-201a06f86b88', '5ea67a49-1df9-11e8-a7fb-201a06f86b88', '54fded41-1df9-11e8-a7fb-201a06f86b88', 0, '', '0', '0', 1, 'Position', 0, '', 0, 2, 0, 1),
('65fafcdc-1df9-11e8-a7fb-201a06f86b88', '5ea67a49-1df9-11e8-a7fb-201a06f86b88', '54fde8ea-1df9-11e8-a7fb-201a06f86b88', 1, '', '', '54fde977-1df9-11e8-a7fb-201a06f86b88', 1, 'Имя', 0, '', 0, 1, 0, 2),
('65faff89-1df9-11e8-a7fb-201a06f86b88', '5ea67a49-1df9-11e8-a7fb-201a06f86b88', '54fded41-1df9-11e8-a7fb-201a06f86b88', 0, '', '', '', 1, 'Должность', 0, '', 0, 3, 0, 2),
('65fb005f-1df9-11e8-a7fb-201a06f86b88', '5ea67a49-1df9-11e8-a7fb-201a06f86b88', '54fde977-1df9-11e8-a7fb-201a06f86b88', 0, '', '', '', 1, 'Подразделение', 0, '', 0, 4, 0, 2),
('65fb010a-1df9-11e8-a7fb-201a06f86b88', '5ea67a49-1df9-11e8-a7fb-201a06f86b88', '54fdedb9-1df9-11e8-a7fb-201a06f86b88', 0, '', '', '', 1, 'Телефон', 0, '', 0, 2, 0, 2),
('65fb01a8-1df9-11e8-a7fb-201a06f86b88', '5ea67c93-1df9-11e8-a7fb-201a06f86b88', '54fde55f-1df9-11e8-a7fb-201a06f86b88', 0, '', '', '', 1, 'Эл. почта', 0, '', 0, 3, 0, 2),
('65fb0242-1df9-11e8-a7fb-201a06f86b88', '5ea67c93-1df9-11e8-a7fb-201a06f86b88', '54fdeea9-1df9-11e8-a7fb-201a06f86b88', 0, '', '', '', 1, 'Сотрудник', 0, '', 0, 2, 0, 2),
('8b200159-9184-11e8-8f8b-485ab6e1c06f', '1812f272-9184-11e8-8f8b-485ab6e1c06f', '3e53bfb3-9184-11e8-8f8b-485ab6e1c06f', 1, 'по состояниям', '0', '0', 1, 'Состояние', 0, '', 0, 4, 0, 2),
('912296a6-9184-11e8-8f8b-485ab6e1c06f', '1812f272-9184-11e8-8f8b-485ab6e1c06f', '45978557-9184-11e8-8f8b-485ab6e1c06f', 0, '', '0', '0', 1, 'Дата', 0, '', 0, 5, 2, 2),
('bb4662d6-9184-11e8-8f8b-485ab6e1c06f', '1812f272-9184-11e8-8f8b-485ab6e1c06f', 'a03809a9-9172-11e8-8f8b-485ab6e1c06f', 1, 'по исполнителям', '0', '0', 0, '', 0, '', 1, 6, 0, 2),
('d6eb8811-5e7d-11e8-9f10-201a06f86b88', '5ea67a49-1df9-11e8-a7fb-201a06f86b88', '54fde977-1df9-11e8-a7fb-201a06f86b88', 0, '', '0', '0', 1, 'OU', 0, '', 0, 3, 0, 1),
('e51db3f9-5e7c-11e8-9f10-201a06f86b88', '5ea67c93-1df9-11e8-a7fb-201a06f86b88', '54fdeea9-1df9-11e8-a7fb-201a06f86b88', 0, '', '0', '0', 1, 'Employee', 0, '', 0, 1, 0, 1),
('ea33a331-5e7c-11e8-9f10-201a06f86b88', '5ea67c93-1df9-11e8-a7fb-201a06f86b88', '54fde55f-1df9-11e8-a7fb-201a06f86b88', 0, '', '0', '0', 1, 'E-mail', 0, '', 0, 2, 0, 1);

DROP TABLE IF EXISTS `folder_filter`;
CREATE TABLE `folder_filter` (
  `folder_filter_uid` varchar(36) NOT NULL,
  `folder_uid` varchar(36) NOT NULL,
  `field_uid` varchar(36) NOT NULL,
  `condition_value` varchar(20) NOT NULL,
  `type_value` varchar(20) NOT NULL,
  `value` varchar(256) NOT NULL,
  `action` varchar(20) NOT NULL,
  `action_params` text NOT NULL,
  `draft` tinyint(4) NOT NULL,
  `draft_params` text NOT NULL,
  `sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `folder_user_filter`;
CREATE TABLE `folder_user_filter` (
  `filter_id` int(11) NOT NULL,
  `folder_uid` varchar(36) NOT NULL,
  `user_uid` varchar(36) NOT NULL,
  `filter_name` varchar(255) NOT NULL,
  `filter` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `language`;
CREATE TABLE `language` (
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `code` varchar(5) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `image` varchar(64) NOT NULL,
  `directory` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `language` (`language_id`, `name`, `code`, `locale`, `image`, `directory`, `sort_order`, `status`) VALUES
(1, 'English', 'en-gb', 'en-US,en_US.UTF-8,en_US,en-gb,english', 'gb.png', 'english', 1, 0),
(2, 'Русский', 'ru-ru', 'ru-RU,ru_RU.UTF-8,ru_RU,russian', '', '', 0, 1);

DROP TABLE IF EXISTS `menu_item`;
CREATE TABLE `menu_item` (
  `menu_item_id` int(11) NOT NULL,
  `type` varchar(10) NOT NULL,
  `action` varchar(50) NOT NULL,
  `action_value` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `hide_name` int(11) DEFAULT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `menu_item` (`menu_item_id`, `type`, `action`, `action_value`, `parent_id`, `image`, `hide_name`, `sort_order`, `status`) VALUES
(1, 'text', 'folder', '', 0, 'catalog/menu/menu01.png', 0, 0, 1),
(2, 'text', 'folder', '', 0, 'catalog/menu/menu02.png', 0, 2, 1),
(3, 'text', 'folder', '5ea67a49-1df9-11e8-a7fb-201a06f86b88', 1, '', 0, 5, 1),
(7, 'text', 'folder', '5ea67c93-1df9-11e8-a7fb-201a06f86b88', 1, '', 0, 0, 1),
(12, 'divider', '', '', 1, '', NULL, 3, 1),
(13, 'text', 'folder', '', 0, 'catalog/menu/menu04.png', NULL, 6, 1),
(14, 'text', 'link', '/index.php?route=document/document&amp;doctype_uid=9f91722e-5823-11e8-841d-201a06f86b88', 13, '', 0, 0, 1),
(15, 'text', 'link', '/index.php?route=account/logout', 13, '', 0, 0, 1),
(16, 'text', 'link', '/admin', 0, 'catalog/menu/menu06.png', 0, 4, 1),
(17, 'text', 'link', '/index.php?route=doctype/doctype', 16, '', 0, 0, 1),
(18, 'text', 'link', '/index.php?route=doctype/folder', 16, '', 0, 2, 1),
(19, 'text', 'link', '/index.php?route=menu/item', 16, '', 0, 4, 1),
(20, 'divider', '', '', 16, '', NULL, 6, 1),
(21, 'text', 'link', '/index.php?route=tool/setting', 16, '', 0, 12, 1),
(22, 'text', 'link', '/index.php?route=marketplace/extension', 16, '', 0, 10, 1),
(23, 'text', 'folder', '1812f272-9184-11e8-8f8b-485ab6e1c06f', 2, '', 0, 0, 1),
(24, 'text', 'link', '/index.php?route=tool/service', 16, '', 0, 8, 1);

DROP TABLE IF EXISTS `menu_item_delegate`;
CREATE TABLE `menu_item_delegate` (
  `menu_item_id` int(11) NOT NULL,
  `structure_uid` varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `menu_item_delegate` (`menu_item_id`, `structure_uid`) VALUES
(1, '5354df9c-1df9-11e8-a7fb-201a06f86b88'),
(2, '5354df9c-1df9-11e8-a7fb-201a06f86b88'),
(3, '5354df9c-1df9-11e8-a7fb-201a06f86b88'),
(7, '5354df9c-1df9-11e8-a7fb-201a06f86b88'),
(12, '5354df9c-1df9-11e8-a7fb-201a06f86b88'),
(13, '5354df9c-1df9-11e8-a7fb-201a06f86b88'),
(14, '5354df9c-1df9-11e8-a7fb-201a06f86b88'),
(15, '5354df9c-1df9-11e8-a7fb-201a06f86b88'),
(16, '5354df9c-1df9-11e8-a7fb-201a06f86b88'),
(17, '5354df9c-1df9-11e8-a7fb-201a06f86b88'),
(18, '5354df9c-1df9-11e8-a7fb-201a06f86b88'),
(19, '5354df9c-1df9-11e8-a7fb-201a06f86b88'),
(20, '5354df9c-1df9-11e8-a7fb-201a06f86b88'),
(21, '5354df9c-1df9-11e8-a7fb-201a06f86b88'),
(22, '5354df9c-1df9-11e8-a7fb-201a06f86b88'),
(23, '5354df9c-1df9-11e8-a7fb-201a06f86b88'),
(24, '5354df9c-1df9-11e8-a7fb-201a06f86b88');

DROP TABLE IF EXISTS `menu_item_description`;
CREATE TABLE `menu_item_description` (
  `menu_item_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `menu_item_description` (`menu_item_id`, `name`, `language_id`) VALUES
(1, 'References', 1),
(1, 'Справочники', 2),
(2, 'Documents', 1),
(2, 'Документы', 2),
(3, 'Structure', 1),
(3, 'Структура', 2),
(7, 'Users', 1),
(7, 'Пользователи', 2),
(12, '_', 1),
(12, '_', 2),
(13, '', 1),
(13, '', 2),
(14, 'Profile', 1),
(14, 'Профиль', 2),
(15, 'Logout', 1),
(15, 'Выйти', 2),
(16, 'Administration', 1),
(16, 'Администрирование', 2),
(17, 'Doctypes', 1),
(17, 'Типы документов', 2),
(18, 'Folders', 1),
(18, 'Журналы документов', 2),
(19, 'Menu editor', 1),
(19, 'Редактор меню', 2),
(20, '_', 1),
(20, '_', 2),
(21, 'Settings', 1),
(21, 'Настройки', 2),
(22, 'Modules', 1),
(22, 'Модули', 2),
(23, 'Тестовые документы', 2),
(24, 'Services', 1),
(24, 'Сервисы', 2);

DROP TABLE IF EXISTS `menu_item_path`;
CREATE TABLE `menu_item_path` (
  `menu_item_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `menu_item_path` (`menu_item_id`, `path_id`, `level`) VALUES
(1, 1, 0),
(2, 2, 0),
(3, 1, 0),
(3, 3, 1),
(7, 1, 0),
(7, 7, 1),
(12, 1, 0),
(12, 12, 1),
(13, 13, 0),
(14, 13, 0),
(14, 14, 1),
(15, 13, 0),
(15, 15, 1),
(16, 16, 0),
(17, 16, 0),
(17, 17, 1),
(18, 16, 0),
(18, 18, 1),
(19, 16, 0),
(19, 19, 1),
(20, 16, 0),
(20, 20, 1),
(21, 16, 0),
(21, 21, 1),
(22, 16, 0),
(22, 22, 1),
(23, 2, 0),
(23, 23, 1),
(24, 16, 0),
(24, 24, 1);

DROP TABLE IF EXISTS `modification`;
CREATE TABLE `modification` (
  `modification_id` int(11) NOT NULL,
  `extension_install_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(64) NOT NULL,
  `author` varchar(64) NOT NULL,
  `version` varchar(32) NOT NULL,
  `link` varchar(255) NOT NULL,
  `xml` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `modification` (`modification_id`, `extension_install_id`, `name`, `code`, `author`, `version`, `link`, `xml`, `status`, `date_added`) VALUES
(1, 1, 'Добавление расширений полей и действий', 'Добавление расширений полей и действий', 'Documentov', '1.0', 'http://www.documentov.com', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<modification>\r\n    <name>Добавление расширений полей и действий</name>\r\n    <code>Добавление расширений полей и действий</code>\r\n    <version>1.0</version>\r\n    <author>Documentov</author>\r\n    <link>http://www.documentov.com</link>\r\n\r\n    <file path=\"admin/controller/startup/permission.php\">\r\n        <operation>\r\n            <search index=\"0\"><![CDATA[\r\n                \'extension/dashboard\',\r\n            ]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                \'extension/action\',\r\n                \'extension/field\',\r\n            ]]></add>\r\n        </operation>\r\n    </file>\r\n        \r\n</modification>', 1, '2017-10-18 16:07:47'),
(4, 4, 'Аутентификация пользователей', 'Аутентификация пользователей', 'Documentov', '1.0', 'http://www.documentov.com', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<modification>\r\n    <name>Аутентификация пользователей</name>\r\n    <code>Аутентификация пользователей</code>\r\n    <version>1.0</version>\r\n    <author>Documentov</author>\r\n    <link>http://www.documentov.com</link>\r\n\r\n    <file path=\"catalog/controller/startup/router.php\">\r\n        <operation>\r\n            <search index=\"0\"><![CDATA[\r\n                $route = preg_replace(\'/[^a-zA-Z0-9_\\/]/\', \'\', (string)$route);\r\n            ]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n				if(!$this->customer->isLogged() && $route != \'account/login\') {\r\n                    $this->response->redirect($this->url->link(\'account/login\'));\r\n                }\r\n            ]]></add>\r\n        </operation>\r\n    </file>\r\n        \r\n</modification>', 1, '2017-11-14 11:40:01');

DROP TABLE IF EXISTS `route`;
CREATE TABLE `route` (
  `route_uid` varchar(36) NOT NULL,
  `doctype_uid` varchar(36) NOT NULL,
  `sort` int(11) NOT NULL,
  `draft` int(11) NOT NULL,
  `draft_params` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `route` (`route_uid`, `doctype_uid`, `sort`, `draft`, `draft_params`) VALUES
('23b37561-914c-11e8-8f8b-485ab6e1c06f', '51f803b2-1df9-11e8-a7fb-201a06f86b88', 2, 0, ''),
('392985cd-5826-11e8-a8d8-201a06f86b88', '9f91722e-5823-11e8-841d-201a06f86b88', 1, 0, 'a:1:{s:12:\"descriptions\";a:2:{i:2;a:2:{s:4:\"name\";s:16:\"Создание\";s:11:\"description\";s:0:\"\";}i:1;a:2:{s:4:\"name\";s:8:\"Creation\";s:11:\"description\";s:0:\"\";}}}'),
('5fa8b997-1df9-11e8-a7fb-201a06f86b88', '51f803b2-1df9-11e8-a7fb-201a06f86b88', 1, 0, ''),
('5fa8bd6b-1df9-11e8-a7fb-201a06f86b88', '51f800b5-1df9-11e8-a7fb-201a06f86b88', 1, 0, 'a:1:{s:12:\"descriptions\";a:2:{i:2;a:2:{s:4:\"name\";s:16:\"Основная\";s:11:\"description\";s:0:\"\";}i:1;a:2:{s:4:\"name\";s:8:\"Creation\";s:11:\"description\";s:0:\"\";}}}'),
('990012ca-914d-11e8-8f8b-485ab6e1c06f', '51f800b5-1df9-11e8-a7fb-201a06f86b88', 2, 0, 'a:1:{s:12:\"descriptions\";a:2:{i:2;a:2:{s:4:\"name\";s:16:\"Удаление\";s:11:\"description\";s:0:\"\";}i:1;a:2:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";}}}'),
('c24a9c26-9173-11e8-8f8b-485ab6e1c06f', '666a0f8c-9172-11e8-8f8b-485ab6e1c06f', 1, 0, 'a:1:{s:12:\"descriptions\";a:2:{i:2;a:2:{s:4:\"name\";s:16:\"Создание\";s:11:\"description\";s:0:\"\";}i:1;a:2:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";}}}'),
('c4e6a835-9173-11e8-8f8b-485ab6e1c06f', '666a0f8c-9172-11e8-8f8b-485ab6e1c06f', 2, 0, 'a:1:{s:12:\"descriptions\";a:2:{i:2;a:2:{s:4:\"name\";s:14:\"Подпись\";s:11:\"description\";s:0:\"\";}i:1;a:2:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";}}}'),
('c84e9e03-9173-11e8-8f8b-485ab6e1c06f', '666a0f8c-9172-11e8-8f8b-485ab6e1c06f', 3, 0, 'a:1:{s:12:\"descriptions\";a:2:{i:2;a:2:{s:4:\"name\";s:24:\"Рассмотрение\";s:11:\"description\";s:0:\"\";}i:1;a:2:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";}}}'),
('cbc2085e-9173-11e8-8f8b-485ab6e1c06f', '666a0f8c-9172-11e8-8f8b-485ab6e1c06f', 4, 0, 'a:1:{s:12:\"descriptions\";a:2:{i:2;a:2:{s:4:\"name\";s:20:\"Исполнение\";s:11:\"description\";s:0:\"\";}i:1;a:2:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";}}}'),
('cee806dd-9173-11e8-8f8b-485ab6e1c06f', '666a0f8c-9172-11e8-8f8b-485ab6e1c06f', 5, 0, 'a:1:{s:12:\"descriptions\";a:2:{i:2;a:2:{s:4:\"name\";s:16:\"Выполнен\";s:11:\"description\";s:0:\"\";}i:1;a:2:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";}}}');

DROP TABLE IF EXISTS `route_action`;
CREATE TABLE `route_action` (
  `route_action_uid` varchar(36) NOT NULL,
  `route_uid` varchar(36) NOT NULL,
  `context` varchar(10) NOT NULL,
  `action` varchar(255) NOT NULL,
  `action_log` tinyint(4) NOT NULL,
  `params` text NOT NULL,
  `draft` tinyint(4) NOT NULL,
  `draft_params` text NOT NULL,
  `sort` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `route_action` (`route_action_uid`, `route_uid`, `context`, `action`, `action_log`, `params`, `draft`, `draft_params`, `sort`, `status`) VALUES
('067e53c9-914d-11e8-8f8b-485ab6e1c06f', '23b37561-914c-11e8-8f8b-485ab6e1c06f', 'jump', 'record', 0, 'a:10:{s:17:\"target_field_type\";s:1:\"0\";s:24:\"target_doclink_field_uid\";s:1:\"0\";s:16:\"target_field_uid\";s:36:\"f7c5e7ea-914c-11e8-8f8b-485ab6e1c06f\";s:19:\"target_field_setter\";s:0:\"\";s:11:\"source_type\";s:8:\"variable\";s:24:\"source_doclink_field_uid\";s:0:\"\";s:15:\"source_variable\";s:20:\"current_document_uid\";s:16:\"source_field_uid\";s:0:\"\";s:19:\"source_field_getter\";s:0:\"\";s:6:\"params\";a:1:{s:4:\"sort\";i:2;}}', 0, '', 3, 1),
('12f978d1-9175-11e8-8f8b-485ab6e1c06f', 'cbc2085e-9173-11e8-8f8b-485ab6e1c06f', 'jump', 'access', 0, 'a:7:{s:16:\"field_subject_id\";s:36:\"a03809a9-9172-11e8-8f8b-485ab6e1c06f\";s:11:\"object_type\";s:16:\"current_document\";s:11:\"doctype_uid\";s:0:\"\";s:15:\"field_object_id\";s:0:\"\";s:15:\"field_author_id\";s:0:\"\";s:6:\"access\";s:5:\"allow\";s:6:\"params\";a:1:{s:4:\"sort\";i:1;}}', 0, '', 1, 1),
('1a967585-58dc-11e8-a8d8-201a06f86b88', '392985cd-5826-11e8-a8d8-201a06f86b88', 'jump', 'record', 0, 'a:11:{s:17:\"target_field_type\";s:1:\"0\";s:24:\"target_doclink_field_uid\";s:36:\"d269d126-58db-11e8-a8d8-201a06f86b88\";s:16:\"target_field_uid\";s:36:\"d0e9bc2a-30b7-11e8-9fbe-201a06f86b88\";s:19:\"target_field_setter\";s:0:\"\";s:11:\"source_type\";s:8:\"document\";s:24:\"source_doclink_field_uid\";s:1:\"0\";s:15:\"source_variable\";s:9:\"author_id\";s:16:\"source_field_uid\";s:36:\"c4443c3b-58db-11e8-a8d8-201a06f86b88\";s:19:\"source_field_getter\";s:0:\"\";s:6:\"params\";a:1:{s:4:\"sort\";i:3;}s:4:\"sort\";i:2;}', 0, '', 3, 1),
('22823148-914d-11e8-8f8b-485ab6e1c06f', '23b37561-914c-11e8-8f8b-485ab6e1c06f', 'jump', 'condition', 0, 'a:9:{s:21:\"first_value_field_uid\";s:36:\"c3b0321e-914c-11e8-8f8b-485ab6e1c06f\";s:24:\"first_value_field_getter\";s:0:\"\";s:17:\"comparison_method\";s:8:\"notequal\";s:17:\"second_type_value\";s:1:\"0\";s:22:\"second_value_field_uid\";s:36:\"f7c5e7ea-914c-11e8-8f8b-485ab6e1c06f\";s:25:\"second_value_field_getter\";s:0:\"\";s:18:\"inner_actions_true\";a:1:{i:0;a:3:{s:6:\"action\";s:6:\"remove\";s:6:\"params\";a:2:{s:18:\"field_document_uid\";s:1:\"0\";s:7:\"confirm\";a:2:{i:1;s:0:\"\";i:2;s:0:\"\";}}s:3:\"new\";i:1;}}s:19:\"inner_actions_false\";a:1:{i:0;a:2:{s:6:\"action\";s:4:\"move\";s:6:\"params\";a:2:{s:18:\"field_document_uid\";s:1:\"0\";s:18:\"document_route_uid\";s:36:\"5fa8b997-1df9-11e8-a7fb-201a06f86b88\";}}}s:18:\"field_widget_value\";i:0;}', 0, '', 4, 1),
('390e1056-914e-11e8-8f8b-485ab6e1c06f', '5fa8b997-1df9-11e8-a7fb-201a06f86b88', 'change', 'record', 0, 'a:9:{s:17:\"target_field_type\";s:1:\"0\";s:24:\"target_doclink_field_uid\";s:36:\"54fde9f3-1df9-11e8-a7fb-201a06f86b88\";s:16:\"target_field_uid\";s:36:\"54fdeea9-1df9-11e8-a7fb-201a06f86b88\";s:19:\"target_field_setter\";s:0:\"\";s:11:\"source_type\";s:8:\"variable\";s:24:\"source_doclink_field_uid\";s:0:\"\";s:15:\"source_variable\";s:20:\"current_document_uid\";s:16:\"source_field_uid\";s:0:\"\";s:19:\"source_field_getter\";s:0:\"\";}', 0, '', 1, 1),
('3f2c93f8-5f23-11e8-b6a0-201a06f86b88', '392985cd-5826-11e8-a8d8-201a06f86b88', 'create', 'record', 0, 'a:10:{s:17:\"target_field_type\";s:1:\"0\";s:24:\"target_doclink_field_uid\";s:1:\"0\";s:16:\"target_field_uid\";s:36:\"16739861-5f23-11e8-b6a0-201a06f86b88\";s:19:\"target_field_setter\";s:0:\"\";s:11:\"source_type\";s:8:\"variable\";s:24:\"source_doclink_field_uid\";s:0:\"\";s:15:\"source_variable\";s:16:\"current_user_uid\";s:16:\"source_field_uid\";s:0:\"\";s:19:\"source_field_getter\";s:0:\"\";s:6:\"params\";a:1:{s:4:\"sort\";i:5;}}', 0, '', 5, 1),
('445b6c2c-9171-11e8-8f8b-485ab6e1c06f', '990012ca-914d-11e8-8f8b-485ab6e1c06f', 'view', 'move', 0, 'a:3:{s:18:\"field_document_uid\";s:1:\"0\";s:18:\"document_route_uid\";s:36:\"5fa8bd6b-1df9-11e8-a7fb-201a06f86b88\";s:6:\"params\";a:1:{s:4:\"sort\";i:1;}}', 0, '', 1, 1),
('45bb1192-582f-11e8-a8d8-201a06f86b88', '392985cd-5826-11e8-a8d8-201a06f86b88', 'jump', 'record', 0, 'a:10:{s:17:\"target_field_type\";s:1:\"0\";s:24:\"target_doclink_field_uid\";s:36:\"635eb9f2-5826-11e8-a8d8-201a06f86b88\";s:16:\"target_field_uid\";s:36:\"50dd5916-37fc-11e8-a971-52540028bc1e\";s:19:\"target_field_setter\";s:0:\"\";s:11:\"source_type\";s:8:\"document\";s:24:\"source_doclink_field_uid\";s:1:\"0\";s:15:\"source_variable\";s:9:\"author_id\";s:16:\"source_field_uid\";s:36:\"1f458fe7-5826-11e8-a8d8-201a06f86b88\";s:19:\"source_field_getter\";s:0:\"\";s:6:\"params\";a:1:{s:4:\"sort\";i:1;}}', 0, '', 1, 1),
('464e5cb0-58dc-11e8-a8d8-201a06f86b88', '392985cd-5826-11e8-a8d8-201a06f86b88', 'jump', 'record', 0, 'a:11:{s:17:\"target_field_type\";s:1:\"0\";s:24:\"target_doclink_field_uid\";s:36:\"d269d126-58db-11e8-a8d8-201a06f86b88\";s:16:\"target_field_uid\";s:36:\"54fde735-1df9-11e8-a7fb-201a06f86b88\";s:19:\"target_field_setter\";s:0:\"\";s:11:\"source_type\";s:8:\"document\";s:24:\"source_doclink_field_uid\";s:1:\"0\";s:15:\"source_variable\";s:9:\"author_id\";s:16:\"source_field_uid\";s:36:\"ec8a023d-5825-11e8-a8d8-201a06f86b88\";s:19:\"source_field_getter\";s:0:\"\";s:6:\"params\";a:1:{s:4:\"sort\";i:4;}s:4:\"sort\";i:3;}', 0, '', 3, 1),
('49ed28b9-5f23-11e8-b6a0-201a06f86b88', '392985cd-5826-11e8-a8d8-201a06f86b88', 'create', 'record', 0, 'a:10:{s:17:\"target_field_type\";s:1:\"0\";s:24:\"target_doclink_field_uid\";s:1:\"0\";s:16:\"target_field_uid\";s:36:\"24251a7f-5f23-11e8-b6a0-201a06f86b88\";s:19:\"target_field_setter\";s:0:\"\";s:11:\"source_type\";s:8:\"variable\";s:24:\"source_doclink_field_uid\";s:0:\"\";s:15:\"source_variable\";s:16:\"current_user_uid\";s:16:\"source_field_uid\";s:0:\"\";s:19:\"source_field_getter\";s:0:\"\";s:6:\"params\";a:1:{s:4:\"sort\";i:6;}}', 0, '', 6, 1),
('522a61e4-9184-11e8-8f8b-485ab6e1c06f', 'c24a9c26-9173-11e8-8f8b-485ab6e1c06f', 'jump', 'record', 0, 'a:10:{s:17:\"target_field_type\";s:1:\"0\";s:24:\"target_doclink_field_uid\";s:1:\"0\";s:16:\"target_field_uid\";s:36:\"3e53bfb3-9184-11e8-8f8b-485ab6e1c06f\";s:19:\"target_field_setter\";s:0:\"\";s:11:\"source_type\";s:8:\"variable\";s:24:\"source_doclink_field_uid\";s:0:\"\";s:15:\"source_variable\";s:18:\"current_route_name\";s:16:\"source_field_uid\";s:0:\"\";s:19:\"source_field_getter\";s:0:\"\";s:6:\"params\";a:1:{s:4:\"sort\";i:2;}}', 0, '', 2, 1),
('57c1ceda-9174-11e8-8f8b-485ab6e1c06f', 'c4e6a835-9173-11e8-8f8b-485ab6e1c06f', 'jump', 'access', 0, 'a:6:{s:16:\"field_subject_id\";s:36:\"89ff9965-9172-11e8-8f8b-485ab6e1c06f\";s:11:\"object_type\";s:16:\"current_document\";s:11:\"doctype_uid\";s:0:\"\";s:15:\"field_object_id\";s:0:\"\";s:15:\"field_author_id\";s:0:\"\";s:6:\"access\";s:5:\"allow\";}', 0, '', 1, 1),
('5b18944d-9184-11e8-8f8b-485ab6e1c06f', 'c24a9c26-9173-11e8-8f8b-485ab6e1c06f', 'jump', 'record', 0, 'a:10:{s:17:\"target_field_type\";s:1:\"0\";s:24:\"target_doclink_field_uid\";s:1:\"0\";s:16:\"target_field_uid\";s:36:\"45978557-9184-11e8-8f8b-485ab6e1c06f\";s:19:\"target_field_setter\";s:0:\"\";s:11:\"source_type\";s:8:\"variable\";s:24:\"source_doclink_field_uid\";s:0:\"\";s:15:\"source_variable\";s:12:\"current_date\";s:16:\"source_field_uid\";s:0:\"\";s:19:\"source_field_getter\";s:0:\"\";s:6:\"params\";a:1:{s:4:\"sort\";i:3;}}', 0, '', 3, 1),
('605e8c34-58dc-11e8-a8d8-201a06f86b88', '392985cd-5826-11e8-a8d8-201a06f86b88', 'create', 'record', 0, 'a:10:{s:17:\"target_field_type\";s:1:\"0\";s:24:\"target_doclink_field_uid\";s:1:\"0\";s:16:\"target_field_uid\";s:36:\"d269d126-58db-11e8-a8d8-201a06f86b88\";s:19:\"target_field_setter\";s:0:\"\";s:11:\"source_type\";s:8:\"variable\";s:24:\"source_doclink_field_uid\";s:0:\"\";s:15:\"source_variable\";s:21:\"current_user_user_uid\";s:16:\"source_field_uid\";s:0:\"\";s:19:\"source_field_getter\";s:0:\"\";s:6:\"params\";a:1:{s:4:\"sort\";i:3;}}', 0, '', 4, 1),
('60faf4f7-9184-11e8-8f8b-485ab6e1c06f', 'c4e6a835-9173-11e8-8f8b-485ab6e1c06f', 'jump', 'record', 0, 'a:10:{s:17:\"target_field_type\";s:1:\"0\";s:24:\"target_doclink_field_uid\";s:1:\"0\";s:16:\"target_field_uid\";s:36:\"3e53bfb3-9184-11e8-8f8b-485ab6e1c06f\";s:19:\"target_field_setter\";s:0:\"\";s:11:\"source_type\";s:8:\"variable\";s:24:\"source_doclink_field_uid\";s:0:\"\";s:15:\"source_variable\";s:18:\"current_route_name\";s:16:\"source_field_uid\";s:0:\"\";s:19:\"source_field_getter\";s:0:\"\";s:6:\"params\";a:1:{s:4:\"sort\";i:2;}}', 0, '', 6, 1),
('64292de4-9184-11e8-8f8b-485ab6e1c06f', 'c84e9e03-9173-11e8-8f8b-485ab6e1c06f', 'jump', 'record', 0, 'a:10:{s:17:\"target_field_type\";s:1:\"0\";s:24:\"target_doclink_field_uid\";s:1:\"0\";s:16:\"target_field_uid\";s:36:\"3e53bfb3-9184-11e8-8f8b-485ab6e1c06f\";s:19:\"target_field_setter\";s:0:\"\";s:11:\"source_type\";s:8:\"variable\";s:24:\"source_doclink_field_uid\";s:0:\"\";s:15:\"source_variable\";s:18:\"current_route_name\";s:16:\"source_field_uid\";s:0:\"\";s:19:\"source_field_getter\";s:0:\"\";s:6:\"params\";a:1:{s:4:\"sort\";i:2;}}', 0, '', 2, 1),
('67c657b0-9184-11e8-8f8b-485ab6e1c06f', 'cbc2085e-9173-11e8-8f8b-485ab6e1c06f', 'jump', 'record', 0, 'a:10:{s:17:\"target_field_type\";s:1:\"0\";s:24:\"target_doclink_field_uid\";s:1:\"0\";s:16:\"target_field_uid\";s:36:\"3e53bfb3-9184-11e8-8f8b-485ab6e1c06f\";s:19:\"target_field_setter\";s:0:\"\";s:11:\"source_type\";s:8:\"variable\";s:24:\"source_doclink_field_uid\";s:0:\"\";s:15:\"source_variable\";s:18:\"current_route_name\";s:16:\"source_field_uid\";s:0:\"\";s:19:\"source_field_getter\";s:0:\"\";s:6:\"params\";a:1:{s:4:\"sort\";i:2;}}', 0, '', 2, 1),
('6c309668-9184-11e8-8f8b-485ab6e1c06f', 'cee806dd-9173-11e8-8f8b-485ab6e1c06f', 'jump', 'record', 0, 'a:10:{s:17:\"target_field_type\";s:1:\"0\";s:24:\"target_doclink_field_uid\";s:1:\"0\";s:16:\"target_field_uid\";s:36:\"3e53bfb3-9184-11e8-8f8b-485ab6e1c06f\";s:19:\"target_field_setter\";s:0:\"\";s:11:\"source_type\";s:8:\"variable\";s:24:\"source_doclink_field_uid\";s:0:\"\";s:15:\"source_variable\";s:18:\"current_route_name\";s:16:\"source_field_uid\";s:0:\"\";s:19:\"source_field_getter\";s:0:\"\";s:6:\"params\";a:1:{s:4:\"sort\";i:2;}}', 0, '', 1, 1),
('704028b1-5826-11e8-a8d8-201a06f86b88', '392985cd-5826-11e8-a8d8-201a06f86b88', 'create', 'record', 0, 'a:9:{s:17:\"target_field_type\";s:1:\"0\";s:24:\"target_doclink_field_uid\";s:1:\"0\";s:16:\"target_field_uid\";s:36:\"635eb9f2-5826-11e8-a8d8-201a06f86b88\";s:19:\"target_field_setter\";s:0:\"\";s:11:\"source_type\";s:8:\"variable\";s:24:\"source_doclink_field_uid\";s:0:\"\";s:15:\"source_variable\";s:21:\"current_user_user_uid\";s:16:\"source_field_uid\";s:0:\"\";s:19:\"source_field_getter\";s:0:\"\";}', 0, '', 2, 1),
('8173f05d-5826-11e8-a8d8-201a06f86b88', '392985cd-5826-11e8-a8d8-201a06f86b88', 'create', 'record', 0, 'a:10:{s:17:\"target_field_type\";s:1:\"0\";s:24:\"target_doclink_field_uid\";s:1:\"0\";s:16:\"target_field_uid\";s:36:\"1f458fe7-5826-11e8-a8d8-201a06f86b88\";s:19:\"target_field_setter\";s:0:\"\";s:11:\"source_type\";s:8:\"document\";s:24:\"source_doclink_field_uid\";s:1:\"0\";s:15:\"source_variable\";s:9:\"author_id\";s:16:\"source_field_uid\";s:36:\"635eb9f2-5826-11e8-a8d8-201a06f86b88\";s:19:\"source_field_getter\";s:26:\"get_display_value_not_link\";s:6:\"params\";a:1:{s:4:\"sort\";i:2;}}', 0, '', 2, 1),
('8316c679-582f-11e8-a8d8-201a06f86b88', '392985cd-5826-11e8-a8d8-201a06f86b88', 'jump', 'remove', 0, 'a:3:{s:18:\"field_document_uid\";s:1:\"0\";s:6:\"params\";a:1:{s:4:\"sort\";i:2;}s:4:\"sort\";i:4;}', 0, '', 5, 1),
('896fa29a-58dc-11e8-a8d8-201a06f86b88', '392985cd-5826-11e8-a8d8-201a06f86b88', 'create', 'record', 0, 'a:9:{s:17:\"target_field_type\";s:1:\"0\";s:24:\"target_doclink_field_uid\";s:1:\"0\";s:16:\"target_field_uid\";s:36:\"c4443c3b-58db-11e8-a8d8-201a06f86b88\";s:19:\"target_field_setter\";s:12:\"set_by_title\";s:11:\"source_type\";s:8:\"document\";s:24:\"source_doclink_field_uid\";s:1:\"0\";s:15:\"source_variable\";s:9:\"author_id\";s:16:\"source_field_uid\";s:36:\"d269d126-58db-11e8-a8d8-201a06f86b88\";s:19:\"source_field_getter\";s:26:\"get_display_value_not_link\";}', 0, '', 4, 1),
('97281d04-917d-11e8-8f8b-485ab6e1c06f', 'c4e6a835-9173-11e8-8f8b-485ab6e1c06f', 'change', 'condition', 0, 'a:9:{s:21:\"first_value_field_uid\";s:36:\"79861ef1-9174-11e8-8f8b-485ab6e1c06f\";s:24:\"first_value_field_getter\";s:0:\"\";s:17:\"comparison_method\";s:5:\"equal\";s:17:\"second_type_value\";s:1:\"1\";s:22:\"second_value_field_uid\";s:0:\"\";s:25:\"second_value_field_getter\";s:0:\"\";s:18:\"field_widget_value\";s:1:\"2\";s:18:\"inner_actions_true\";a:1:{i:0;a:2:{s:6:\"action\";s:4:\"move\";s:6:\"params\";a:2:{s:18:\"field_document_uid\";s:1:\"0\";s:18:\"document_route_uid\";s:36:\"c24a9c26-9173-11e8-8f8b-485ab6e1c06f\";}}}s:19:\"inner_actions_false\";a:0:{}}', 0, '', 4, 1),
('b8a32fd5-914d-11e8-8f8b-485ab6e1c06f', '990012ca-914d-11e8-8f8b-485ab6e1c06f', 'jump', 'record', 0, 'a:10:{s:17:\"target_field_type\";s:1:\"0\";s:24:\"target_doclink_field_uid\";s:1:\"0\";s:16:\"target_field_uid\";s:36:\"a4846198-914d-11e8-8f8b-485ab6e1c06f\";s:19:\"target_field_setter\";s:0:\"\";s:11:\"source_type\";s:8:\"variable\";s:24:\"source_doclink_field_uid\";s:0:\"\";s:15:\"source_variable\";s:21:\"current_user_user_uid\";s:16:\"source_field_uid\";s:0:\"\";s:19:\"source_field_getter\";s:0:\"\";s:6:\"params\";a:1:{s:4:\"sort\";i:1;}}', 0, '', 2, 1),
('c4bc1461-914d-11e8-8f8b-485ab6e1c06f', '990012ca-914d-11e8-8f8b-485ab6e1c06f', 'jump', 'record', 0, 'a:10:{s:17:\"target_field_type\";s:1:\"0\";s:24:\"target_doclink_field_uid\";s:1:\"0\";s:16:\"target_field_uid\";s:36:\"acd0bdad-914d-11e8-8f8b-485ab6e1c06f\";s:19:\"target_field_setter\";s:0:\"\";s:11:\"source_type\";s:8:\"variable\";s:24:\"source_doclink_field_uid\";s:0:\"\";s:15:\"source_variable\";s:20:\"current_document_uid\";s:16:\"source_field_uid\";s:0:\"\";s:19:\"source_field_getter\";s:0:\"\";s:6:\"params\";a:1:{s:4:\"sort\";i:2;}}', 0, '', 3, 1),
('c64e4819-9174-11e8-8f8b-485ab6e1c06f', 'c4e6a835-9173-11e8-8f8b-485ab6e1c06f', 'jump', 'record', 0, 'a:10:{s:17:\"target_field_type\";s:1:\"0\";s:24:\"target_doclink_field_uid\";s:1:\"0\";s:16:\"target_field_uid\";s:36:\"79861ef1-9174-11e8-8f8b-485ab6e1c06f\";s:19:\"target_field_setter\";s:0:\"\";s:11:\"source_type\";s:6:\"manual\";s:24:\"source_doclink_field_uid\";s:0:\"\";s:15:\"source_variable\";s:9:\"author_id\";s:16:\"source_field_uid\";s:0:\"\";s:19:\"source_field_getter\";s:0:\"\";s:6:\"params\";a:1:{s:4:\"sort\";i:2;}}', 0, '', 5, 1),
('cedb870b-914c-11e8-8f8b-485ab6e1c06f', '23b37561-914c-11e8-8f8b-485ab6e1c06f', 'jump', 'record', 0, 'a:10:{s:17:\"target_field_type\";s:1:\"0\";s:24:\"target_doclink_field_uid\";s:1:\"0\";s:16:\"target_field_uid\";s:36:\"c3b0321e-914c-11e8-8f8b-485ab6e1c06f\";s:19:\"target_field_setter\";s:0:\"\";s:11:\"source_type\";s:8:\"variable\";s:24:\"source_doclink_field_uid\";s:0:\"\";s:15:\"source_variable\";s:9:\"author_id\";s:16:\"source_field_uid\";s:0:\"\";s:19:\"source_field_getter\";s:0:\"\";s:6:\"params\";a:1:{s:4:\"sort\";i:1;}}', 0, '', 2, 1),
('d9acf91c-914d-11e8-8f8b-485ab6e1c06f', '990012ca-914d-11e8-8f8b-485ab6e1c06f', 'jump', 'condition', 0, 'a:10:{s:21:\"first_value_field_uid\";s:36:\"a4846198-914d-11e8-8f8b-485ab6e1c06f\";s:24:\"first_value_field_getter\";s:0:\"\";s:17:\"comparison_method\";s:8:\"notequal\";s:17:\"second_type_value\";s:1:\"0\";s:22:\"second_value_field_uid\";s:36:\"acd0bdad-914d-11e8-8f8b-485ab6e1c06f\";s:25:\"second_value_field_getter\";s:0:\"\";s:18:\"inner_actions_true\";a:1:{i:0;a:3:{s:6:\"action\";s:6:\"remove\";s:6:\"params\";a:2:{s:18:\"field_document_uid\";s:1:\"0\";s:7:\"confirm\";a:2:{i:1;s:0:\"\";i:2;s:0:\"\";}}s:3:\"new\";i:1;}}s:19:\"inner_actions_false\";a:1:{i:0;a:3:{s:6:\"action\";s:4:\"move\";s:6:\"params\";a:2:{s:18:\"field_document_uid\";s:1:\"0\";s:18:\"document_route_uid\";s:36:\"5fa8bd6b-1df9-11e8-a7fb-201a06f86b88\";}s:3:\"new\";i:1;}}s:18:\"field_widget_value\";i:0;s:6:\"params\";a:1:{s:4:\"sort\";i:3;}}', 0, '', 4, 1),
('daca9098-9174-11e8-8f8b-485ab6e1c06f', 'c4e6a835-9173-11e8-8f8b-485ab6e1c06f', 'change', 'condition', 0, 'a:9:{s:21:\"first_value_field_uid\";s:36:\"79861ef1-9174-11e8-8f8b-485ab6e1c06f\";s:24:\"first_value_field_getter\";s:0:\"\";s:17:\"comparison_method\";s:5:\"equal\";s:17:\"second_type_value\";s:1:\"1\";s:22:\"second_value_field_uid\";s:0:\"\";s:25:\"second_value_field_getter\";s:0:\"\";s:18:\"field_widget_value\";s:1:\"1\";s:18:\"inner_actions_true\";a:1:{i:0;a:3:{s:6:\"action\";s:4:\"move\";s:6:\"params\";a:2:{s:18:\"field_document_uid\";s:1:\"0\";s:18:\"document_route_uid\";s:36:\"c84e9e03-9173-11e8-8f8b-485ab6e1c06f\";}s:3:\"new\";i:1;}}s:19:\"inner_actions_false\";a:0:{}}', 0, '', 3, 1),
('e3375bab-9174-11e8-8f8b-485ab6e1c06f', 'c84e9e03-9173-11e8-8f8b-485ab6e1c06f', 'jump', 'access', 0, 'a:7:{s:16:\"field_subject_id\";s:36:\"9230b3d4-9172-11e8-8f8b-485ab6e1c06f\";s:11:\"object_type\";s:16:\"current_document\";s:11:\"doctype_uid\";s:0:\"\";s:15:\"field_object_id\";s:0:\"\";s:15:\"field_author_id\";s:0:\"\";s:6:\"access\";s:5:\"allow\";s:6:\"params\";a:1:{s:4:\"sort\";i:1;}}', 0, '', 1, 1),
('e9cc0931-917b-11e8-8f8b-485ab6e1c06f', 'c4e6a835-9173-11e8-8f8b-485ab6e1c06f', 'view', 'record', 0, 'a:9:{s:17:\"target_field_type\";s:1:\"0\";s:24:\"target_doclink_field_uid\";s:1:\"0\";s:16:\"target_field_uid\";s:36:\"79861ef1-9174-11e8-8f8b-485ab6e1c06f\";s:19:\"target_field_setter\";s:0:\"\";s:11:\"source_type\";s:6:\"manual\";s:24:\"source_doclink_field_uid\";s:0:\"\";s:15:\"source_variable\";s:9:\"author_id\";s:16:\"source_field_uid\";s:0:\"\";s:19:\"source_field_getter\";s:0:\"\";}', 0, '', 2, 0),
('f5f5183e-9a1f-11e8-8da9-485ab6e1c06f', '5fa8bd6b-1df9-11e8-a7fb-201a06f86b88', 'change', 'record', 0, 'a:10:{s:17:\"target_field_type\";s:1:\"0\";s:24:\"target_doclink_field_uid\";s:1:\"0\";s:16:\"target_field_uid\";s:36:\"a4846198-914d-11e8-8f8b-485ab6e1c06f\";s:19:\"target_field_setter\";s:0:\"\";s:11:\"source_type\";s:8:\"variable\";s:24:\"source_doclink_field_uid\";s:0:\"\";s:15:\"source_variable\";s:21:\"current_user_user_uid\";s:16:\"source_field_uid\";s:0:\"\";s:19:\"source_field_getter\";s:0:\"\";s:6:\"params\";a:1:{s:4:\"sort\";i:1;}}', 0, '', 1, 1),
('f5fea32a-9a1f-11e8-8da9-485ab6e1c06f', '5fa8bd6b-1df9-11e8-a7fb-201a06f86b88', 'change', 'record', 0, 'a:10:{s:17:\"target_field_type\";s:1:\"0\";s:24:\"target_doclink_field_uid\";s:1:\"0\";s:16:\"target_field_uid\";s:36:\"acd0bdad-914d-11e8-8f8b-485ab6e1c06f\";s:19:\"target_field_setter\";s:0:\"\";s:11:\"source_type\";s:8:\"variable\";s:24:\"source_doclink_field_uid\";s:0:\"\";s:15:\"source_variable\";s:20:\"current_document_uid\";s:16:\"source_field_uid\";s:0:\"\";s:19:\"source_field_getter\";s:0:\"\";s:6:\"params\";a:1:{s:4:\"sort\";i:2;}}', 0, '', 2, 1),
('f6054f1b-9a1f-11e8-8da9-485ab6e1c06f', '5fa8bd6b-1df9-11e8-a7fb-201a06f86b88', 'change', 'condition', 0, 'a:11:{s:21:\"first_value_field_uid\";s:36:\"a4846198-914d-11e8-8f8b-485ab6e1c06f\";s:24:\"first_value_field_getter\";s:0:\"\";s:17:\"comparison_method\";s:5:\"equal\";s:17:\"second_type_value\";s:1:\"0\";s:22:\"second_value_field_uid\";s:36:\"acd0bdad-914d-11e8-8f8b-485ab6e1c06f\";s:25:\"second_value_field_getter\";s:0:\"\";s:18:\"inner_actions_true\";a:1:{i:1;a:3:{s:6:\"action\";s:6:\"record\";s:6:\"params\";a:10:{s:17:\"target_field_type\";s:1:\"0\";s:24:\"target_doclink_field_uid\";s:1:\"0\";s:16:\"target_field_uid\";s:36:\"54fde7d4-1df9-11e8-a7fb-201a06f86b88\";s:19:\"target_field_setter\";s:0:\"\";s:11:\"source_type\";s:6:\"manual\";s:24:\"source_doclink_field_uid\";s:0:\"\";s:15:\"source_variable\";s:9:\"author_id\";s:16:\"source_field_uid\";s:0:\"\";s:19:\"source_field_getter\";s:0:\"\";s:18:\"field_widget_value\";s:1:\"0\";}s:3:\"new\";i:1;}}s:19:\"inner_actions_false\";a:0:{}s:26:\"inner_actions_true_deleted\";a:1:{i:0;a:3:{s:6:\"action\";s:6:\"remove\";s:6:\"params\";a:2:{s:18:\"field_document_uid\";s:1:\"0\";s:7:\"confirm\";a:2:{i:1;s:0:\"\";i:2;s:0:\"\";}}s:7:\"deleted\";i:1;}}s:27:\"inner_actions_false_deleted\";a:1:{i:0;a:3:{s:6:\"action\";s:4:\"move\";s:6:\"params\";a:2:{s:18:\"field_document_uid\";s:1:\"0\";s:18:\"document_route_uid\";s:36:\"5fa8bd6b-1df9-11e8-a7fb-201a06f86b88\";}s:7:\"deleted\";i:1;}}s:18:\"field_widget_value\";i:0;}', 0, '', 3, 1),
('f63fd2e9-9173-11e8-8f8b-485ab6e1c06f', 'c24a9c26-9173-11e8-8f8b-485ab6e1c06f', 'jump', 'record', 0, 'a:10:{s:17:\"target_field_type\";s:1:\"0\";s:24:\"target_doclink_field_uid\";s:1:\"0\";s:16:\"target_field_uid\";s:36:\"e064f290-9173-11e8-8f8b-485ab6e1c06f\";s:19:\"target_field_setter\";s:0:\"\";s:11:\"source_type\";s:8:\"variable\";s:24:\"source_doclink_field_uid\";s:0:\"\";s:15:\"source_variable\";s:9:\"author_id\";s:16:\"source_field_uid\";s:0:\"\";s:19:\"source_field_getter\";s:0:\"\";s:6:\"params\";a:1:{s:4:\"sort\";i:1;}}', 0, '', 1, 1);

DROP TABLE IF EXISTS `route_button`;
CREATE TABLE `route_button` (
  `route_button_uid` varchar(36) NOT NULL,
  `route_uid` varchar(36) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `color` varchar(6) NOT NULL,
  `background` varchar(6) NOT NULL,
  `action` varchar(255) NOT NULL,
  `action_log` tinyint(4) NOT NULL,
  `action_move_route_uid` varchar(36) NOT NULL,
  `action_params` text NOT NULL,
  `show_after_execute` tinyint(4) NOT NULL,
  `draft` int(11) NOT NULL,
  `draft_params` text NOT NULL,
  `sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `route_button` (`route_button_uid`, `route_uid`, `picture`, `color`, `background`, `action`, `action_log`, `action_move_route_uid`, `action_params`, `show_after_execute`, `draft`, `draft_params`, `sort`) VALUES
('040f901c-9175-11e8-8f8b-485ab6e1c06f', 'c84e9e03-9173-11e8-8f8b-485ab6e1c06f', '', '', '', 'dialog', 1, 'cbc2085e-9173-11e8-8f8b-485ab6e1c06f', 'a:2:{s:8:\"template\";a:1:{i:2;s:257:\"                      &lt;p&gt;                      \n              Исполнитель: {{ f_a03809a9917211e88f8b485ab6e1c06f }}&lt;/p&gt;&lt;p&gt;Текст поручения: {{ f_ad0cde4c917211e88f8b485ab6e1c06f }}&lt;br&gt;&lt;/p&gt;\n              \";}s:5:\"title\";a:1:{i:2;s:35:\"Создание поручения\";}}', 0, 0, '', 1),
('2ef27783-9175-11e8-8f8b-485ab6e1c06f', 'cbc2085e-9173-11e8-8f8b-485ab6e1c06f', '', '', '', 'dialog', 1, 'cee806dd-9173-11e8-8f8b-485ab6e1c06f', 'a:3:{s:8:\"template\";a:1:{i:2;s:162:\"&lt;p&gt;                      \n              Отчет об исполнении:&lt;/p&gt;&lt;p&gt;{{ f_8ef255ae917311e88f8b485ab6e1c06f }}&lt;br&gt;&lt;/p&gt;\";}s:5:\"title\";a:1:{i:2;s:36:\"Отчет об исполнении\";}s:6:\"params\";a:1:{s:4:\"sort\";i:1;}}', 0, 0, '', 1),
('4bc09024-9174-11e8-8f8b-485ab6e1c06f', 'c24a9c26-9173-11e8-8f8b-485ab6e1c06f', '', '', '', '', 1, 'c4e6a835-9173-11e8-8f8b-485ab6e1c06f', 'a:0:{}', 0, 0, '', 2),
('664b6d76-9174-11e8-8f8b-485ab6e1c06f', 'c4e6a835-9173-11e8-8f8b-485ab6e1c06f', '', '', '', 'dialog', 1, '', 'a:2:{s:8:\"template\";a:1:{i:2;s:250:\"                      &lt;p&gt;&lt;b&gt;Выберите вариант:&lt;/b&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;{{ f_79861ef1917411e88f8b485ab6e1c06f }}                                            \n              \n              &lt;/p&gt;\n              \";}s:5:\"title\";a:1:{i:2;s:33:\"Подпись документа\";}}', 0, 0, '', 1),
('69aea64f-1df9-11e8-a7fb-201a06f86b88', '5fa8b997-1df9-11e8-a7fb-201a06f86b88', '', '', '', 'edit', 1, '0', 'a:1:{s:18:\"field_document_uid\";s:1:\"0\";}', 0, 0, '', 1),
('ef3d6bea-90c0-11e8-9039-485ab6e1c06f', '5fa8bd6b-1df9-11e8-a7fb-201a06f86b88', '', '', '', 'edit', 1, '', 'a:2:{s:18:\"field_document_uid\";s:1:\"0\";s:6:\"params\";a:1:{s:4:\"sort\";i:2;}}', 0, 0, '', 2),
('ff88d79c-9173-11e8-8f8b-485ab6e1c06f', 'c24a9c26-9173-11e8-8f8b-485ab6e1c06f', '', '', '', 'edit', 1, '', 'a:1:{s:18:\"field_document_uid\";s:1:\"0\";}', 0, 0, '', 1);

DROP TABLE IF EXISTS `route_button_delegate`;
CREATE TABLE `route_button_delegate` (
  `route_button_uid` varchar(36) NOT NULL,
  `document_uid` varchar(36) NOT NULL,
  `structure_uid` varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `route_button_delegate` (`route_button_uid`, `document_uid`, `structure_uid`) VALUES
('040f901c-9175-11e8-8f8b-485ab6e1c06f', '10a930b2-9185-11e8-8f8b-485ab6e1c06f', '5354e0d0-1df9-11e8-a7fb-201a06f86b88'),
('040f901c-9175-11e8-8f8b-485ab6e1c06f', '334f1e36-9175-11e8-8f8b-485ab6e1c06f', '5354e0d0-1df9-11e8-a7fb-201a06f86b88'),
('2ef27783-9175-11e8-8f8b-485ab6e1c06f', '334f1e36-9175-11e8-8f8b-485ab6e1c06f', '5354e0d0-1df9-11e8-a7fb-201a06f86b88'),
('4bc09024-9174-11e8-8f8b-485ab6e1c06f', '10a930b2-9185-11e8-8f8b-485ab6e1c06f', '5354e0d0-1df9-11e8-a7fb-201a06f86b88'),
('4bc09024-9174-11e8-8f8b-485ab6e1c06f', '334f1e36-9175-11e8-8f8b-485ab6e1c06f', '5354e0d0-1df9-11e8-a7fb-201a06f86b88'),
('664b6d76-9174-11e8-8f8b-485ab6e1c06f', '10a930b2-9185-11e8-8f8b-485ab6e1c06f', '5354e0d0-1df9-11e8-a7fb-201a06f86b88'),
('664b6d76-9174-11e8-8f8b-485ab6e1c06f', '334f1e36-9175-11e8-8f8b-485ab6e1c06f', '5354e0d0-1df9-11e8-a7fb-201a06f86b88'),
('69aea64f-1df9-11e8-a7fb-201a06f86b88', '0', '5354e0d0-1df9-11e8-a7fb-201a06f86b88'),
('ef3d6bea-90c0-11e8-9039-485ab6e1c06f', '0', '5354e0d0-1df9-11e8-a7fb-201a06f86b88'),
('ff88d79c-9173-11e8-8f8b-485ab6e1c06f', '10a930b2-9185-11e8-8f8b-485ab6e1c06f', '5354e0d0-1df9-11e8-a7fb-201a06f86b88'),
('ff88d79c-9173-11e8-8f8b-485ab6e1c06f', '334f1e36-9175-11e8-8f8b-485ab6e1c06f', '5354e0d0-1df9-11e8-a7fb-201a06f86b88');

DROP TABLE IF EXISTS `route_button_description`;
CREATE TABLE `route_button_description` (
  `route_button_uid` varchar(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `route_button_description` (`route_button_uid`, `name`, `description`, `language_id`) VALUES
('040f901c-9175-11e8-8f8b-485ab6e1c06f', 'Создать поручение', '', 2),
('2ef27783-9175-11e8-8f8b-485ab6e1c06f', 'Исполнить', '', 2),
('4bc09024-9174-11e8-8f8b-485ab6e1c06f', 'На подпись', '', 2),
('664b6d76-9174-11e8-8f8b-485ab6e1c06f', 'Подписать', '', 2),
('69aea64f-1df9-11e8-a7fb-201a06f86b88', '', 'Edit document', 1),
('69aea64f-1df9-11e8-a7fb-201a06f86b88', 'Изменить', 'Редактировать документ', 2),
('ef3d6bea-90c0-11e8-9039-485ab6e1c06f', '', '', 1),
('ef3d6bea-90c0-11e8-9039-485ab6e1c06f', 'Изменить', '', 2),
('ff88d79c-9173-11e8-8f8b-485ab6e1c06f', 'Изменить', '', 2);

DROP TABLE IF EXISTS `route_button_field`;
CREATE TABLE `route_button_field` (
  `route_button_uid` varchar(36) NOT NULL,
  `field_uid` varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `route_button_field` (`route_button_uid`, `field_uid`) VALUES
('040f901c-9175-11e8-8f8b-485ab6e1c06f', '9230b3d4-9172-11e8-8f8b-485ab6e1c06f'),
('2ef27783-9175-11e8-8f8b-485ab6e1c06f', 'a03809a9-9172-11e8-8f8b-485ab6e1c06f'),
('4bc09024-9174-11e8-8f8b-485ab6e1c06f', 'e064f290-9173-11e8-8f8b-485ab6e1c06f'),
('664b6d76-9174-11e8-8f8b-485ab6e1c06f', '89ff9965-9172-11e8-8f8b-485ab6e1c06f'),
('69aea64f-1df9-11e8-a7fb-201a06f86b88', '48d0676a-90c0-11e8-9039-485ab6e1c06f'),
('ef3d6bea-90c0-11e8-9039-485ab6e1c06f', '48d0676a-90c0-11e8-9039-485ab6e1c06f'),
('ff88d79c-9173-11e8-8f8b-485ab6e1c06f', 'e064f290-9173-11e8-8f8b-485ab6e1c06f');

DROP TABLE IF EXISTS `route_description`;
CREATE TABLE `route_description` (
  `route_uid` varchar(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `route_description` (`route_uid`, `name`, `description`, `language_id`) VALUES
('23b37561-914c-11e8-8f8b-485ab6e1c06f', '', '', 1),
('23b37561-914c-11e8-8f8b-485ab6e1c06f', 'Удаление', '', 2),
('392985cd-5826-11e8-a8d8-201a06f86b88', 'Creation', '', 1),
('392985cd-5826-11e8-a8d8-201a06f86b88', 'Создание', '', 2),
('5fa8b997-1df9-11e8-a7fb-201a06f86b88', 'Point of route', '', 1),
('5fa8b997-1df9-11e8-a7fb-201a06f86b88', 'Основная', '', 2),
('5fa8bd6b-1df9-11e8-a7fb-201a06f86b88', 'Creation', '', 1),
('5fa8bd6b-1df9-11e8-a7fb-201a06f86b88', 'Основная', '', 2),
('990012ca-914d-11e8-8f8b-485ab6e1c06f', '', '', 1),
('990012ca-914d-11e8-8f8b-485ab6e1c06f', 'Удаление', '', 2),
('c24a9c26-9173-11e8-8f8b-485ab6e1c06f', '', '', 1),
('c24a9c26-9173-11e8-8f8b-485ab6e1c06f', 'Создание', '', 2),
('c4e6a835-9173-11e8-8f8b-485ab6e1c06f', '', '', 1),
('c4e6a835-9173-11e8-8f8b-485ab6e1c06f', 'Подпись', '', 2),
('c84e9e03-9173-11e8-8f8b-485ab6e1c06f', '', '', 1),
('c84e9e03-9173-11e8-8f8b-485ab6e1c06f', 'Рассмотрение', '', 2),
('cbc2085e-9173-11e8-8f8b-485ab6e1c06f', '', '', 1),
('cbc2085e-9173-11e8-8f8b-485ab6e1c06f', 'Исполнение', '', 2),
('cee806dd-9173-11e8-8f8b-485ab6e1c06f', '', '', 1),
('cee806dd-9173-11e8-8f8b-485ab6e1c06f', 'Выполнен', '', 2);

DROP TABLE IF EXISTS `service_stat`;
CREATE TABLE `service_stat` (
  `type` varchar(32) NOT NULL,
  `customer_uid` varchar(36) DEFAULT NULL,
  `customer_name` varchar(256) NOT NULL,
  `doctype_uid` varchar(36) DEFAULT NULL,
  `document_uid` varchar(36) DEFAULT NULL,
  `folder_uid` varchar(36) DEFAULT NULL,
  `button_uid` varchar(36) DEFAULT NULL,
  `button_name` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `session`;
CREATE TABLE `session` (
  `session_id` varchar(32) NOT NULL,
  `data` text NOT NULL,
  `expire` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `setting`;
CREATE TABLE `setting` (
  `setting_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(128) NOT NULL,
  `key` varchar(128) NOT NULL,
  `value` text NOT NULL,
  `serialized` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES
(627, 0, 'dv_field_hidden', 'hidden_field_password', 'IE3Hg4VmXrpS0vQRP78Zx1GMtLDBJWCiA2hbOkTonFl9YNafUjsqKzewd65cyu', 0),
(626, 0, 'dv_user', 'pagination_limits', '5,10,20,50,100,150,200', 0),
(625, 0, 'dv_user', 'pagination_limit', '20', 0),
(624, 0, 'dv_system', 'user_field_login_type', 'string', 0),
(609, 0, 'config', 'config_error_display', '1', 0),
(610, 0, 'config', 'config_error_log', '1', 0),
(611, 0, 'config', 'config_error_filename', 'error.log', 0),
(612, 0, 'dv_user', 'menu_image_width', '32', 0),
(613, 0, 'dv_user', 'menu_image_height', '32', 0),
(615, 0, 'dv_system', 'structure_id', '51f803b2-1df9-11e8-a7fb-201a06f86b88', 0),
(616, 0, 'dv_system', 'structure_field_name_id', '54fde8ea-1df9-11e8-a7fb-201a06f86b88', 0),
(617, 0, 'dv_system', 'structure_field_user_id', '54fde9f3-1df9-11e8-a7fb-201a06f86b88', 0),
(618, 0, 'dv_system', 'user_field_password_id', '54fde735-1df9-11e8-a7fb-201a06f86b88', 0),
(619, 0, 'dv_system', 'structure_type', 'link', 0),
(620, 0, 'dv_system', 'structure_field_parent_id', '54fde977-1df9-11e8-a7fb-201a06f86b88', 0),
(621, 0, 'dv_system', 'user_field_password_type', 'hidden', 0),
(622, 0, 'dv_system', 'structure_field_user_type', 'link', 0),
(623, 0, 'dv_system', 'structure_field_name_type', 'string', 0),
(601, 0, 'config', 'config_compression', '0', 0),
(595, 0, 'dv_zsystem', 'config_mail_smtp_timeout', '5', 0),
(594, 0, 'dv_zsystem', 'config_mail_smtp_port', '25', 0),
(593, 0, 'dv_zsystem', 'config_mail_smtp_password', '', 0),
(592, 0, 'dv_zsystem', 'config_mail_smtp_username', '', 0),
(590, 0, 'dv_zsystem', 'config_mail_parameter', '', 0),
(591, 0, 'dv_zsystem', 'config_mail_smtp_hostname', '', 0),
(589, 0, 'dv_zsystem', 'config_mail_engine', 'mail', 0),
(588, 0, 'config', 'config_icon', 'catalog/fv.png', 0),
(587, 0, 'config', 'config_logo', 'catalog/logo.png', 0),
(147, 0, 'theme_default', 'theme_default_directory', 'default', 0),
(148, 0, 'theme_default', 'theme_default_status', '1', 0),
(571, 0, 'dv_zsystem', 'config_login_attempts', '10', 0),
(555, 0, 'config', 'config_language', 'ru-ru', 0),
(549, 0, 'dv_zsystem', 'config_email', 'znaon1@gmail.com', 0),
(545, 0, 'config', 'config_name', 'Documentov', 0),
(544, 0, 'config', 'config_theme', 'default', 0),
(687, 0, 'dv_system', 'user_field_lastactivity_id', '0a009bb0-99f9-11e8-8da9-485ab6e1c06f', 0),
(686, 0, 'dv_system', 'user_field_lastactivity_type', 'datetime', 0),
(685, 0, 'dv_system', 'user_field_status_id', '54fde7d4-1df9-11e8-a7fb-201a06f86b88', 0),
(684, 0, 'dv_system', 'user_field_status_type', 'list', 0),
(628, 0, 'dv_field_hidden', 'hidden_field_iv', 'ASuroVRZhUkOvD18', 0),
(629, 0, 'dv_zsystem', 'date.timezone', 'Asia/Almaty', 0),
(652, 0, 'dv_field_file', 'config_file_ext_allowed', 'zip,txt,png,jpe,jpeg,jpg,gif,bmp,ico,tiff,tif,svg,svgz,zip,rar,msi,cab,mp3,qt,mov,pdf,psd,ai,eps,ps,doc,xls,docx,xlsx,odt,dot,htm,html,ppt,pptx,rtf,xlsm', 0),
(653, 0, 'dv_field_file', 'config_file_mime_allowed', 'text/plain,image/png,image/jpeg,image/gif,image/bmp,image/tiff,image/svg+xml,application/zip,application/zip,application/x-zip,application/x-zip,application/x-zip-compressed,application/x-zip-compressed,application/rar,application/rar,application/x-rar,application/x-rar,application/x-rar-compressed,application/x-rar-compressed,application/octet-stream,application/octet-stream,audio/mpeg,video/quicktime,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel.sheet.macroEnabled.12,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation', 0),
(654, 0, 'dv_system', 'user_field_attempt_login_type', 'int', 0),
(655, 0, 'dv_system', 'user_field_attempt_login_id', '54fde858-1df9-11e8-a7fb-201a06f86b88', 0),
(656, 0, 'dv_system', 'user_field_login_id', '54fde55f-1df9-11e8-a7fb-201a06f86b88', 0),
(657, 0, 'dv_zsystem', 'daemon_timeout', '10', 0),
(658, 0, 'dv_zsystem', 'daemon_exec_attempt', '10', 0),
(659, 0, 'dv_zsystem', 'daemon_pull_size', '10', 0),
(660, 0, 'dv_system', 'user_field_language_type', 'list', 0),
(661, 0, 'dv_system', 'user_field_language_id', 'd0e9bc2a-30b7-11e8-9fbe-201a06f86b88', 0),
(662, 0, 'dv_system', 'user_field_email_type', 'string', 0),
(663, 0, 'dv_system', 'user_field_email_id', '54fde55f-1df9-11e8-a7fb-201a06f86b88', 0),
(664, 0, 'dv_system', 'user_field_admin_type', 'list', 0),
(665, 0, 'dv_system', 'user_field_admin_id', '48cae257-3259-11e8-8ed6-201a06f86b88', 0),
(666, 0, 'dv_system', 'user_field_startpage_type', 'string', 0),
(667, 0, 'dv_system', 'user_field_startpage_id', '50dd5916-37fc-11e8-a971-52540028bc1e', 0),
(688, 0, 'dv_system', 'version_db', '0.8.1', 0),
(669, 0, 'dv_service_stat', 'service_stat_minute_last_time', '5', 0),
(671, 0, 'dv_system', 'user_field_lastpage_id', '425fd7d7-59be-11e8-958b-201a06f86b88', 0),
(672, 0, 'dv_system', 'user_field_lastpage_type', 'string', 0),
(673, 0, 'dv_system', 'structure_field_position_id', '54fded41-1df9-11e8-a7fb-201a06f86b88', 0),
(674, 0, 'dv_system', 'structure_field_position_type', 'string', 0),
(675, 0, 'dv_system', 'structure_field_deputy_id', 'b21c5bf2-5cd0-11e8-b108-201a06f86b88', 0),
(676, 0, 'dv_system', 'structure_field_deputy_type', 'link', 0),
(680, 0, 'dv_zsystem', 'anonymous_user_id', '', 0),
(681, 0, 'dv_zsystem', 'recursion_depth', '100', 0),
(682, 0, 'dv_zsystem', 'default_start_page', '', 0);

DROP TABLE IF EXISTS `theme`;
CREATE TABLE `theme` (
  `theme_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `theme` varchar(64) NOT NULL,
  `route` varchar(64) NOT NULL,
  `code` mediumtext NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `theme` (`theme_id`, `store_id`, `theme`, `route`, `code`, `date_added`) VALUES
(3, 0, 'default', 'account/account', '{{ header }}\n&lt;div id=&quot;account-account&quot; class=&quot;container&quot;&gt;11112222233333\n  &lt;ul class=&quot;breadcrumb&quot;&gt;\n    {% for breadcrumb in breadcrumbs %}\n    &lt;li&gt;&lt;a href=&quot;{{ breadcrumb.href }}&quot;&gt;{{ breadcrumb.text }}&lt;/a&gt;&lt;/li&gt;\n    {% endfor %}\n  &lt;/ul&gt;\n  {% if success %}\n  &lt;div class=&quot;alert alert-success alert-dismissible&quot;&gt;&lt;i class=&quot;fa fa-check-circle&quot;&gt;&lt;/i&gt; {{ success }}&lt;/div&gt;\n  {% endif %}\n  &lt;div class=&quot;row&quot;&gt;{{ column_left }}\n    {% if column_left and column_right %}\n    {% set class = \'col-sm-6\' %}\n    {% elseif column_left or column_right %}\n    {% set class = \'col-sm-9\' %}\n    {% else %}\n    {% set class = \'col-sm-12\' %}\n    {% endif %}\n    &lt;div id=&quot;content&quot; class=&quot;{{ class }}&quot;&gt;{{ content_top }}\n      &lt;h2&gt;{{ text_my_account }}&lt;/h2&gt;\n      &lt;ul class=&quot;list-unstyled&quot;&gt;\n        &lt;li&gt;&lt;a href=&quot;{{ edit }}&quot;&gt;{{ text_edit }}&lt;/a&gt;&lt;/li&gt;\n        &lt;li&gt;&lt;a href=&quot;{{ password }}&quot;&gt;{{ text_password }}&lt;/a&gt;&lt;/li&gt;\n        &lt;li&gt;&lt;a href=&quot;{{ address }}&quot;&gt;{{ text_address }}&lt;/a&gt;&lt;/li&gt;\n        &lt;li&gt;&lt;a href=&quot;{{ wishlist }}&quot;&gt;{{ text_wishlist }}&lt;/a&gt;&lt;/li&gt;\n      &lt;/ul&gt;\n      {% if credit_cards %}\n      &lt;h2&gt;{{ text_credit_card }}&lt;/h2&gt;\n      &lt;ul class=&quot;list-unstyled&quot;&gt;\n        {% for credit_card in credit_cards %}\n        &lt;li&gt;&lt;a href=&quot;{{ credit_card.href }}&quot;&gt;{{ credit_card.name }}&lt;/a&gt;&lt;/li&gt;\n        {% endfor %}\n      &lt;/ul&gt;\n      {% endif %}\n      &lt;h2&gt;{{ text_my_orders }}&lt;/h2&gt;\n      &lt;ul class=&quot;list-unstyled&quot;&gt;\n        &lt;li&gt;&lt;a href=&quot;{{ order }}&quot;&gt;{{ text_order }}&lt;/a&gt;&lt;/li&gt;\n        &lt;li&gt;&lt;a href=&quot;{{ download }}&quot;&gt;{{ text_download }}&lt;/a&gt;&lt;/li&gt;\n        {% if reward %}\n        &lt;li&gt;&lt;a href=&quot;{{ reward }}&quot;&gt;{{ text_reward }}&lt;/a&gt;&lt;/li&gt;\n        {% endif %}\n        &lt;li&gt;&lt;a href=&quot;{{ return }}&quot;&gt;{{ text_return }}&lt;/a&gt;&lt;/li&gt;\n        &lt;li&gt;&lt;a href=&quot;{{ transaction }}&quot;&gt;{{ text_transaction }}&lt;/a&gt;&lt;/li&gt;\n        &lt;li&gt;&lt;a href=&quot;{{ recurring }}&quot;&gt;{{ text_recurring }}&lt;/a&gt;&lt;/li&gt;\n      &lt;/ul&gt;\n      &lt;h2&gt;{{ text_my_affiliate }}&lt;/h2&gt;\n      &lt;ul class=&quot;list-unstyled&quot;&gt;\n        {% if not tracking %}\n        &lt;li&gt;&lt;a href=&quot;{{ affiliate }}&quot;&gt;{{ text_affiliate_add }}&lt;/a&gt;&lt;/li&gt;\n        {% else %}\n        &lt;li&gt;&lt;a href=&quot;{{ affiliate }}&quot;&gt;{{ text_affiliate_edit }}&lt;/a&gt;&lt;/li&gt;\n        &lt;li&gt;&lt;a href=&quot;{{ tracking }}&quot;&gt;{{ text_tracking }}&lt;/a&gt;&lt;/li&gt;\n        {% endif %}\n      &lt;/ul&gt;\n      &lt;h2&gt;{{ text_my_newsletter }}&lt;/h2&gt;\n      &lt;ul class=&quot;list-unstyled&quot;&gt;\n        &lt;li&gt;&lt;a href=&quot;{{ newsletter }}&quot;&gt;{{ text_newsletter }}&lt;/a&gt;&lt;/li&gt;\n      &lt;/ul&gt;\n      {{ content_bottom }}&lt;/div&gt;\n    {{ column_right }}&lt;/div&gt;\n&lt;/div&gt;\n{{ footer }}', '2017-09-28 11:24:36');


ALTER TABLE `daemon_queue`
  ADD PRIMARY KEY (`task_id`),
  ADD KEY `start_time` (`start_time`),
  ADD KEY `status` (`status`);

ALTER TABLE `doctype`
  ADD PRIMARY KEY (`doctype_uid`);

ALTER TABLE `doctype_description`
  ADD PRIMARY KEY (`doctype_uid`,`language_id`);

ALTER TABLE `doctype_template`
  ADD PRIMARY KEY (`doctype_uid`,`type`,`language_id`);

ALTER TABLE `document`
  ADD PRIMARY KEY (`document_uid`),
  ADD KEY `doctype_uid` (`doctype_uid`);

ALTER TABLE `document_access`
  ADD PRIMARY KEY (`subject_uid`,`document_uid`),
  ADD KEY `doctype_uid` (`doctype_uid`);

ALTER TABLE `event`
  ADD PRIMARY KEY (`event_id`);

ALTER TABLE `extension`
  ADD PRIMARY KEY (`extension_id`);

ALTER TABLE `extension_install`
  ADD PRIMARY KEY (`extension_install_id`);

ALTER TABLE `extension_path`
  ADD PRIMARY KEY (`extension_path_id`);

ALTER TABLE `field`
  ADD PRIMARY KEY (`field_uid`),
  ADD KEY `doctype_uid` (`doctype_uid`);

ALTER TABLE `field_value_datetime`
  ADD PRIMARY KEY (`field_uid`,`document_uid`),
  ADD KEY `value` (`value`),
  ADD KEY `display_value` (`display_value`);

ALTER TABLE `field_value_file`
  ADD PRIMARY KEY (`field_uid`,`document_uid`),
  ADD KEY `display_value` (`display_value`),
  ADD KEY `value` (`value`(256));

ALTER TABLE `field_value_file_list`
  ADD PRIMARY KEY (`file_uid`),
  ADD KEY `field_uid` (`field_uid`),
  ADD KEY `document_uid` (`document_uid`);

ALTER TABLE `field_value_hidden`
  ADD PRIMARY KEY (`field_uid`,`document_uid`),
  ADD KEY `value` (`value`),
  ADD KEY `display_value` (`display_value`);

ALTER TABLE `field_value_int`
  ADD PRIMARY KEY (`field_uid`,`document_uid`),
  ADD KEY `value` (`value`),
  ADD KEY `display_value` (`display_value`);

ALTER TABLE `field_value_link`
  ADD PRIMARY KEY (`field_uid`,`document_uid`),
  ADD KEY `value` (`value`(250));

ALTER TABLE `field_value_link_subscription`
  ADD PRIMARY KEY (`document_uid`,`field_uid`),
  ADD KEY `subscription_document_uid` (`subscription_document_uid`),
  ADD KEY `subscription_field_uid` (`subscription_field_uid`);

ALTER TABLE `field_value_list`
  ADD PRIMARY KEY (`field_uid`,`document_uid`),
  ADD KEY `value` (`value`),
  ADD KEY `display_value` (`display_value`);

ALTER TABLE `field_value_string`
  ADD PRIMARY KEY (`field_uid`,`document_uid`),
  ADD KEY `value` (`value`(250)),
  ADD KEY `display_value` (`display_value`(250));

ALTER TABLE `field_value_table`
  ADD PRIMARY KEY (`field_uid`,`document_uid`),
  ADD KEY `value` (`value`(250)),
  ADD KEY `display_value` (`display_value`);

ALTER TABLE `field_value_text`
  ADD PRIMARY KEY (`field_uid`,`document_uid`),
  ADD KEY `value` (`value`(250)),
  ADD KEY `display_value` (`display_value`);

ALTER TABLE `folder`
  ADD PRIMARY KEY (`folder_uid`);

ALTER TABLE `folder_button`
  ADD PRIMARY KEY (`folder_button_uid`),
  ADD KEY `folder_uid` (`folder_uid`);

ALTER TABLE `folder_button_delegate`
  ADD PRIMARY KEY (`folder_button_uid`,`document_uid`,`structure_uid`),
  ADD KEY `folder_button_uid` (`folder_button_uid`,`document_uid`,`structure_uid`);

ALTER TABLE `folder_button_description`
  ADD PRIMARY KEY (`folder_button_uid`,`language_id`);

ALTER TABLE `folder_button_field`
  ADD PRIMARY KEY (`folder_button_uid`,`field_uid`);

ALTER TABLE `folder_button_route`
  ADD PRIMARY KEY (`folder_button_uid`,`route_uid`);

ALTER TABLE `folder_description`
  ADD PRIMARY KEY (`folder_uid`,`language_id`);

ALTER TABLE `folder_field`
  ADD PRIMARY KEY (`folder_field_uid`),
  ADD KEY `folder_uid` (`folder_uid`),
  ADD KEY `grouping` (`grouping`),
  ADD KEY `tcolumn` (`tcolumn`),
  ADD KEY `sort_grouping` (`sort_grouping`),
  ADD KEY `sort_tcolumn` (`sort_tcolumn`);

ALTER TABLE `folder_filter`
  ADD PRIMARY KEY (`folder_filter_uid`),
  ADD KEY `folder_uid` (`folder_uid`);

ALTER TABLE `folder_user_filter`
  ADD PRIMARY KEY (`filter_id`),
  ADD KEY `folder_uid` (`folder_uid`),
  ADD KEY `user_uid` (`user_uid`);

ALTER TABLE `language`
  ADD PRIMARY KEY (`language_id`),
  ADD KEY `name` (`name`);

ALTER TABLE `menu_item`
  ADD PRIMARY KEY (`menu_item_id`),
  ADD KEY `sort_order` (`sort_order`),
  ADD KEY `parent_id` (`parent_id`);

ALTER TABLE `menu_item_delegate`
  ADD PRIMARY KEY (`menu_item_id`,`structure_uid`);

ALTER TABLE `menu_item_description`
  ADD UNIQUE KEY `menu_item_id` (`menu_item_id`,`language_id`);

ALTER TABLE `menu_item_path`
  ADD PRIMARY KEY (`menu_item_id`,`path_id`);

ALTER TABLE `modification`
  ADD PRIMARY KEY (`modification_id`);

ALTER TABLE `route`
  ADD PRIMARY KEY (`route_uid`),
  ADD KEY `doctype_uid` (`doctype_uid`),
  ADD KEY `sort` (`sort`);

ALTER TABLE `route_action`
  ADD PRIMARY KEY (`route_action_uid`),
  ADD KEY `route_uid` (`route_uid`),
  ADD KEY `context` (`context`),
  ADD KEY `sort` (`sort`);

ALTER TABLE `route_button`
  ADD PRIMARY KEY (`route_button_uid`),
  ADD KEY `route_uid` (`route_uid`);

ALTER TABLE `route_button_delegate`
  ADD PRIMARY KEY (`route_button_uid`,`document_uid`,`structure_uid`),
  ADD KEY `route_button_uid` (`route_button_uid`,`document_uid`,`structure_uid`);

ALTER TABLE `route_button_description`
  ADD PRIMARY KEY (`route_button_uid`,`language_id`);

ALTER TABLE `route_button_field`
  ADD PRIMARY KEY (`route_button_uid`,`field_uid`);

ALTER TABLE `route_description`
  ADD PRIMARY KEY (`route_uid`,`language_id`);

ALTER TABLE `service_stat`
  ADD KEY `date` (`date`);

ALTER TABLE `session`
  ADD PRIMARY KEY (`session_id`);

ALTER TABLE `setting`
  ADD PRIMARY KEY (`setting_id`),
  ADD KEY `key` (`key`);

ALTER TABLE `theme`
  ADD PRIMARY KEY (`theme_id`);


ALTER TABLE `daemon_queue`
  MODIFY `task_id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
ALTER TABLE `extension`
  MODIFY `extension_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;
ALTER TABLE `extension_install`
  MODIFY `extension_install_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
ALTER TABLE `extension_path`
  MODIFY `extension_path_id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `folder_user_filter`
  MODIFY `filter_id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `language`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
ALTER TABLE `menu_item`
  MODIFY `menu_item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
ALTER TABLE `modification`
  MODIFY `modification_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
ALTER TABLE `setting`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=689;
ALTER TABLE `theme`
  MODIFY `theme_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
