$(document).ready(function () {
    $('[data-toggle=\'summernote\']').each(function () {
        var element = this;
        $(element).summernote({
            lang: language_code,
            disableDragAndDrop: true,
            height: 300,
            emptyPara: '',
            codemirror: {// codemirror options
                mode: 'text/html',
                htmlMode: true,
                lineNumbers: true,
                theme: 'monokai'
            },
            fontsize: ['8', '9', '10', '11', '12', '14', '16', '18', '20', '24', '30', '36', '48', '64'],
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'image']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ],
            
            buttons: {
                image: function () {
                    var ui = $.summernote.ui;
                    // create button
                    var button = ui.button({
                        contents: '<i class="note-icon-picture" />',
//                        tooltip: $.summernote.lang[$.summernote.options.lang].image.image,
                        tooltip: $.summernote.lang[language_code].image.image,
                        click: function () {
                            $('#modal-image').remove();

                            $.ajax({
                                url: 'index.php?route=common/filemanager',
                                dataType: 'html',
                                beforeSend: function () {
                                    $('#button-image i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                                    $('#button-image').prop('disabled', true);
                                },
                                complete: function () {
                                    $('#button-image i').replaceWith('<i class="fa fa-upload"></i>');
                                    $('#button-image').prop('disabled', false);
                                },
                                success: function (html) {
                                    $('body').append('<div id="modal-image" class="modal">' + html + '</div>');

                                    $('#modal-image').modal('show');

                                    $('#modal-image').delegate('a.thumbnail', 'click', function (e) {
                                        e.preventDefault();

                                        $(element).summernote('insertImage', $(this).attr('href'));

                                        $('#modal-image').modal('hide');
                                    });
                                }
                            });
                        }
                    });

                    return button.render();
                }
            }
        });
    });
    
    
    $('[data-toggle=\'summernote_s\']').each(function () {
        init_summernote_s(this);        
    });

});

var template_vars = [];
var route = getURLVar('route');
if (route) {
    if (route.indexOf('doctype') > -1 || route.indexOf('extension') > -1) { //запрос выполняется только для админ части
        $.ajax({
            url: 'index.php?route=doctype/doctype/autocomplete_var',
            dataType: 'json',
            async: false,
            cache: false,
            success: function(json) {
                $.each(json, function(){
                    template_vars.push(this);
                });
            }
        });    
    }    
}



function init_summernote_s(element, doctype_uid) {
    if (doctype_uid === undefined) {
        doctype_uid = getURLVar('doctype_uid');
    }
    
    $(element).summernote({
        lang: language_code,
        disableDragAndDrop: true,
        height: 300,
        emptyPara: '',
        codemirror: {// codemirror options
            mode: 'text/html',
            htmlMode: true,
            lineNumbers: true,
            theme: 'monokai'
        },
        fontsize: ['8', '9', '10', '11', '12', '14', '16', '18', '20', '24', '30', '36', '48', '64'],
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline','clear']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'image', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']],
            ['fields', ['fields']],
            ['variables', ['variables']]
        ],
        callbacks: {
            onBlur: function () {
                autosave();
            }
        },
        buttons: {
            fields: function (context) {
                var ui = $.summernote.ui;
                var event = ui.buttonGroup([
                    ui.button({
                        contents: ' ' + $.summernote.lang[language_code].documentov.fields + ' <i class="fa fa-caret-down" aria-hidden="true"></i>',
                        tooltip: $.summernote.lang[language_code].documentov.fields,
                        data: {
                            toggle: 'dropdown'
                        },
                        click: function () {
                            context.invoke('saveRange');
                        }
                    }),
                    ui.dropdown({
                        contents: function() {                      
                            return '<div class="btn-group"><input type="text" class="input-select_field" value="" placeholder="Поле" class="form-control"></div>';
                        },
                        callback: function (items) {
                            $(items).find('input').autocomplete({
                                'source': function(request, response) {
                                    $.ajax({
                                        url: 'index.php?route=doctype/doctype/autocomplete_field&filter_name=' +  encodeURIComponent(request) + '&doctype_uid=' + doctype_uid,
                                        dataType: 'json',
                                        cache: false,
                                        success: function(json) {
                                            response($.map(json, function(item) {
                                                    return {
                                                            label: item['name'],
                                                            value: item['field_uid']
                                                    }
                                            }));
                                        }
                                    });
                                },
                                'select': function(item) {
                                    context.invoke('restoreRange');
                                    context.invoke("editor.insertText", '\{\{ ' +  item['label'] + ' \}\}');                                    
                                }                              
                            });
                        }
                    })
                ]);

                return event.render();   // return button as jquery object
            },
            variables: function (context) {
                var ui = $.summernote.ui;
                var event = ui.buttonGroup([
                    ui.button({
                        contents: ' ' + $.summernote.lang[language_code].documentov.variables + ' <i class="fa fa-caret-down" aria-hidden="true"></i>',
                        tooltip: $.summernote.lang[language_code].documentov.variables,
                        data: {
                            toggle: 'dropdown'
                        }
                    }),
                    ui.dropdown({
                        items: template_vars,
                        callback: function (items) {
                            $(items).find('li a').on('click', function(e){
                                context.invoke("editor.insertText", '{{ ' + $(this).html() + ' }}');
                                e.preventDefault();
                            })

                        }
                    })
                ]);

                return event.render();   // return button as jquery object
            },
            image: function () {
                var ui = $.summernote.ui;
                // create button
                var button = ui.button({
                    contents: '<i class="note-icon-picture" />',
//                        tooltip: $.summernote.lang[$.summernote.options.lang].image.image, $.summernote.options.lang - всегда en-US
                    tooltip: $.summernote.lang[language_code].image.image,
                    click: function () {
                        $('#modal-image').remove();

                        $.ajax({
                            url: 'index.php?route=common/filemanager',
                            dataType: 'html',
                            beforeSend: function () {
                                $('#button-image i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                                $('#button-image').prop('disabled', true);
                            },
                            complete: function () {
                                $('#button-image i').replaceWith('<i class="fa fa-upload"></i>');
                                $('#button-image').prop('disabled', false);
                            },
                            success: function (html) {
                                $('body').append('<div id="modal-image" class="modal">' + html + '</div>');

                                $('#modal-image').modal('show');

                                $('#modal-image').delegate('a.thumbnail', 'click', function (e) {
                                    e.preventDefault();

                                    $(element).summernote('insertImage', $(this).attr('href'));

                                    $('#modal-image').modal('hide');
                                });
                            }
                        });
                    }
                });

                return button.render();
            }
        }
    });    
}


