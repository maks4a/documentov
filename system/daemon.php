<?php
/**
 * @package		Documentov
 * @author		Roman V Zhukov
 * @copyright           Copyright (c) 2018 Andrey V Surov, Roman V Zhukov (https://www.documentov.com/)
 * @license		https://opensource.org/licenses/GPL-3.0
 * @link		https://www.documentov.com
*/

//Проверка окружения запуска
if (php_sapi_name() != 'cli') {
    print_r('attempt to exec daemon in not cli environment...');
    return;
};
//Находим путь к корневой директории Documentov
if (strtoupper(substr(php_uname("s"), 0, 3)) === 'WIN') {
    $dir = substr(__DIR__, 0, strrpos(__DIR__, "\\"));
    $dir = str_replace("\\", "/", $dir);
    $windows = true;
} else {
    $dir = substr(__DIR__, 0, strrpos(__DIR__, "/"));
    $windows = false;
}

//Добавляем глобальную переменную
$server_name = "-s";
if (isset($argv[2])) {
    $_SERVER['SERVER_NAME'] = $argv[2];
    $server_name = $_SERVER['SERVER_NAME'];
}

//Загружаем файл конфигруации
if (is_file($dir . '/config.php')) {
    require_once($dir . '/config.php');
} else {
    print_r("Daemon wasn't able to load config.php...");
    return;
}

//Проверка на наличие запущеного демона
global $argv;
$daemon_loc = DIR_STORAGE . "logs/daemon.lock";
$is_another_started = false;
$daemon_pid = 0;
if (is_file($daemon_loc)) {
    $dl = fopen($daemon_loc, 'r');
    $daemon_time_stamp = fgets($dl);
    $daemon_pid = fgets($dl);
    if (time() - (0 + (int) $daemon_time_stamp) < 10) {
        $is_another_started = true;
    }
    fclose($dl);
}
//автозагрузка классов из системных библиотек
function library($class) {
    $file = DIR_SYSTEM . 'library/' . str_replace('\\', '/', strtolower($class)) . '.php';
    if (is_file($file)) {
        include_once($file);
        return true;
    } else {
        return false;
    }
}

spl_autoload_register('library');
spl_autoload_extensions('.php');
$db = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);
//отметку в бд об остановке демона
$db->query("REPLACE INTO " . DB_PREFIX . "variable SET `name`='daemon_started', `value`='0' ");                        

if (isset($argv[1])) {
    switch ($argv[1]) {
        case 'stop':
            if ($is_another_started && $daemon_pid) {
                exec("kill " . $daemon_pid);
            } else {
                echo "daemon was not found\r\n";
            }
            return;
        case 'restart':
            if ($is_another_started && $daemon_pid) {
                exec("kill " . $daemon_pid);
                $is_another_started = false;
            } else {
                echo "daemon was not found\r\n";
                return;
            }
            break;
    }
}

//устанавливаем отметку о запуске демона в бд для последующего контроля его работы
$db->query("REPLACE INTO " . DB_PREFIX . "variable SET `name`='daemon_started', `value`='1' ");     
//сбрасываем статус запуска сервисных служб
$db->query("REPLACE INTO " . DB_PREFIX . "variable SET `name`='subscription_started', `value`='0' ");     


if ($is_another_started) {
    echo "another daemon process was found\r\n";
    return;
}
//отмечаем запущенный процесс в файле блокировки
$dl = fopen($daemon_loc, 'w');
fwrite($dl, time() . "\r\n" . getmypid());
fclose($dl); 

                     

$sql = "SELECT value FROM " . DB_PREFIX . "setting WHERE `key`='date.timezone'";
$query = $db->query($sql);
$dbformat = 'Y-m-d H:i:s';
$timezone = new DateTimeZone($query->rows[0]['value']);
//Получаем настройки демона из базы данных
$sql = "SELECT * FROM " . DB_PREFIX . "setting WHERE `code`='dv_zsystem' AND (`key`='daemon_timeout' OR `key`='daemon_exec_attempt' OR `key`='daemon_pull_size')";
$query = $db->query($sql);
foreach ($query->rows as $daemon_setting) {
    switch ($daemon_setting['key']) {
        case 'daemon_timeout':
            $daemon_timeout = (int)$daemon_setting['value'];
            break;
        case 'daemon_exec_attempt':
            $daemon_exec_attempt = (int)$daemon_setting['value'];
            break;
        case 'daemon_pull_size':
            $daemon_pull_size = (int)$daemon_setting['value'];
             break;
    }
}
if (!isset($daemon_timeout)) {
    $daemon_timeout = 3;
}
if (!isset($daemon_exec_attempt)) {
    $daemon_exec_attempt = 3;
}
if (!isset($daemon_pull_size)) {
    $daemon_pull_size = 3;
}

//Основной цикл
$cnt = 0;

while ($cnt < 1) {
    if (is_file($daemon_loc)) {    
        $dl = fopen($daemon_loc, 'r');
        $daemon_time_stamp = fgets($dl);
        $daemon_pid = fgets($dl);
        fclose($dl);
        if (trim($daemon_pid) != getmypid()) {
            //PID текущего процесса и того процесса, который сделал последнюю запись в LOCK файл не совпадают
            //это означает, что данный процесс не является актуальным, завершаем его
            exit;
        } 
    }
    $dl = fopen($daemon_loc, 'w');
    fwrite($dl, time() . "\r\n" . getmypid());
    fclose($dl);        
    


//Получаем список выполняемых в данное время задач
    $sql = "SELECT * FROM " . DB_PREFIX . "daemon_queue WHERE status='1' AND start_time IS NOT NULL AND PID > 0 ORDER BY task_id";
    $query = $db->query($sql);
    $now = new DateTime('now', $timezone);
    $time_diff = "";
    $running_procs = 0;
    foreach ($query->rows as $task) {
        $task_start_time = DateTime::createFromFormat($dbformat, $task['start_time'], $timezone);
        $time_diff = $now->getTimestamp() - $task_start_time->getTimestamp();
        if ($time_diff > $daemon_timeout * 60) {
            //вышел таймаут, убиваем процесс
            print_r("\r\n" . $now->format($dbformat) . "\tTASK: " . $task['task_id'] . " (" . $task['action'] . "), " . " ATTEMPT: " . $task['exec_attempt'] . ". Task has exceeded timeout. Killing process: " . $task['PID']);
            exec("kill " . $task['PID']);
            if ($task['exec_attempt'] < $daemon_exec_attempt) {
                //если число попыток не исчерпано, перезапускаем задачу
                if ($windows) {
                    //для WIN
                    $php_command = 'start /b php ' . $dir . '\oc_cli.php ' . $server_name . ' daemon/queue/runTask ' . $task['task_id'] . " > nul";
                } else {
                    $php_command = 'php ' . $dir . '/oc_cli.php ' . $server_name . ' daemon/queue/runTask ' . $task['task_id'] . " 2>&1 >> /dev/null &";
                }
                print_r("\r\n" . $now->format($dbformat) . "\tTASK: " . $task['task_id'] . " (" . $task['action'] . "), " . " ATTEMPT: " . ($task['exec_attempt'] + 1) . ". Restarting task...");
                $handle = popen($php_command, 'r');
                pclose($handle);
                $running_procs++;
            } else {
                $db->query("DELETE FROM " . DB_PREFIX . "daemon_queue WHERE "
                        . "task_id=" . (int) $task['task_id']);
            }
        } else {
            $running_procs++;
        }
    }
    $proc_avaliable = $daemon_pull_size - $running_procs;
    //Получаем список задач для запуска
    $sql = "SELECT * FROM " . DB_PREFIX . "daemon_queue WHERE status=0 AND (start_time IS NULL OR start_time < NOW()) ORDER BY priority DESC LIMIT " . $proc_avaliable;
    $query = $db->query($sql);
    foreach ($query->rows as $task) {
        //помечаем запуск задачи, чтобы демон не запустил задачу повторно
        $db->query("UPDATE " . DB_PREFIX . "daemon_queue SET status=1 WHERE task_id=" . (int) $task['task_id']);        
        if ($windows) {
            //для WIN
            $php_command = 'start /b php.exe ' . $dir . '/oc_cli.php ' . $server_name . ' daemon/queue/runTask ' . $task['task_id'] . " > nul ";
        } else {
            $php_command = 'php ' . $dir . '/oc_cli.php ' . $server_name . ' daemon/queue/runTask ' . $task['task_id'] . " 2>&1 >> /dev/null &";
        }
        print_r("\r\n" . $now->format($dbformat) . "\tTASK: " . $task['task_id'] . " " . $task['action'] . "");
        $handle = popen($php_command, 'r');
        pclose($handle);
        $running_procs++;
    }
    if (!$running_procs) {
        //ни один процесс не был запущен, поэтому можем запустить низкоприоритетные служебные задачи
        if ($windows) {
            //для WIN
            $php_command = "start /b php.exe " . $dir . "/oc_cli.php " . $server_name . " daemon/queue/runServiceTask > nul ";
        } else {
            $php_command = "php " . $dir . "/oc_cli.php " . $server_name . " daemon/queue/runTask service 2>&1 >> /dev/null &";
        }
        $handle = popen($php_command, 'r');
        pclose($handle);

        
    }    
    //$cnt++;
    //сервисные задачи по времени
    //очистка таблицы сессий
    $ntm = $now->format("His");
    if ($ntm == "150000" || $ntm == "030000") {
        print_r("\r\n" . $now->format('Y-m-d H:i:s') . "\tTASK: SERVICE (clear sessions)");
        $db->query("DELETE FROM `" . DB_PREFIX . "session` WHERE expire < NOW()");
    }
    sleep(1);
}

return;
