<?php

/**
 * @package		Documentov
 * @author		Andrey V Surov
 * @copyright           Copyright (c) 2018 Andrey V Surov, Roman V Zhukov (https://www.documentov.com/)
 * @license		https://opensource.org/licenses/GPL-3.0
 * @link		https://www.documentov.com
 */
abstract class FieldModel extends Model {
    
    
    /**
     * Метод для редактирования значения поля
     */
    abstract public function editValue($field_uid, $document_uid, $value);

    /**
     * Метод возвращающий значение поля; при получении $widget_value очищает его и возвращает в качестве значения
     */
    abstract public function getValue($field_uid, $document_uid, $widget_value='');


    /**
     * Метод для удаления значения поля
     */
    abstract public function removeValue($field_uid, $document_uid);
    
    /**
     * Метод для удаления всех значений поля (вызывается при удалении поля)
     */
    abstract public function removeValues($field_uid);    
    
    /**
     * Метод для установки поля в систему
     */
    abstract public function install();
    
    /**
     * Метод для удаления поля из системы
     */
    abstract public function uninstall();
    
    /**
     * Метод для обновления отображаемого значения display_value
     */
    public function refreshDisplayValues($data) {}    
    
    /**
     * Этот метод вызывается демоном, когда изменится целевое поле по подписке
     */
    public function subscription($field_uid, $document_uids) {}
    
    

}
