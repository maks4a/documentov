<?php

class ControllerToolMail extends Controller {
    public function send($data) {
        if (!getenv('SERVER_NAME')) {
            //используется для EHLO, при пустом значении - ошибка
            if ($this->request->server['HTTPS']) {
                    $server = HTTPS_SERVER;
            } else {
                    $server = HTTP_SERVER;
            }    
            $domain = parse_url($server);
            putenv("SERVER_NAME=" . $domain['host']);
        }
        $this->load->language('tool/mail');
        echo sprintf($this->language->get('text_send_mail'), $data['to']);
        $mail = new Mail($this->config->get('config_mail_engine'));
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
        $mail->setTo($data['to']);
        $mail->setFrom($this->config->get('config_email'));
        $mail->setSender("Documentov");
        if (!$data['subject']){
            $data['subject'] = "Documentov";
        }
        $mail->setSubject(html_entity_decode($data['subject'], ENT_QUOTES, 'UTF-8'));
        $mail->setHtml($data['message']);        
        $mail->send();
        
    }
}

