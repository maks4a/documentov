<?php

class ControllerToolGenerator extends Controller {
    public function index() {
        $document_uid = "22e12dfb-633c-11e8-b6a0-201a06f86b88"; //идентификатор документа для копирования
        $count = 20000; //количество копируемых документов (большое количество ставить нет смысла, т.к. скорее всего скрипт прекратит работу по таймауту)
        
        $query = $this->db->query("SELECT * FROM document WHERE document_uid = '" . $document_uid  . "'");
        $this->load->model('setting/extension');
        $extensions = $this->model_setting_extension->getInstalled('field');
        $fields = array();
        foreach ($extensions as $field) {
            $query_field = $this->db->query("SELECT * FROM `field_value_" . $field . "` WHERE `document_uid`='" . $document_uid . "' AND value != ''");
            $fields[$field] = $query_field->rows;
        }
        for ($i=0; $i<$count; $i++) {
            $query_uid = $this->db->query("SELECT UUID() AS uid");
            $document_uid = $query_uid->row['uid'];
            $sql = "INSERT INTO document SET document_uid='" . $document_uid . "', ";
            $sets = array();
            foreach ($query->row as $name => $value) {
                if ($name == "document_uid") {
                    continue;
                }
                $sets[] = $name . "='" . $value . "'";
            }
            $this->db->query($sql . implode(", ", $sets));
            $this->db->query("INSERT INTO document_access SET subject_uid='5354e0d0-1df9-11e8-a7fb-201a06f86b88', document_uid='" .  $document_uid . "', doctype_uid='51f8059a-1df9-11e8-a7fb-201a06f86b88'");
            foreach ($fields as $field_name => $field_rows) {
                if ($field_rows) {
                    foreach ($field_rows as $row) {
                        $sets = array();
                        foreach ($row as $name => $value) {
                            if ($name == "document_uid") {
                                continue;
                            }
                            $sets[] = $name . "='" . $value . "'";     
                        }
                        $this->db->query("INSERT INTO field_value_" . $field_name . " SET document_uid='" . $document_uid . "', " . implode(", ", $sets));                        
                    }                    
                }
            }            
        }
        echo "OK";
        
    }
}
