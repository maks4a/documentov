<?php

class ControllerStartupRouter extends Controller {

    public function index() {
        $this->load->model('account/customer');
        // Route
        
        if (isset($this->request->get['route']) && $this->request->get['route'] != 'startup/router') {
            $route = $this->request->get['route'];
        } elseif ($this->customer->isLogged()) {
            $start_page = $this->model_account_customer->getStartPage();
            if ($start_page) {
                $this->response->redirect($start_page);
            } else {
                $route = $this->config->get('action_default');
            }
        } else {
            $route = $this->config->get('action_default');
        }


        if (!$this->customer->isLogged() && $route != 'account/login' && $route != 'account/logout') {
            //проверяем разрешение на анонимный вход
            $user_uid = $this->config->get('anonymous_user_id');
            if ($user_uid) {
                if ($this->customer->login($user_uid, '', true)) {
                    if (isset($this->request->get['route']) && $this->request->get['route'] != 'startup/router') {
                        $route = $this->request->get['route'];
                    } else {
                        $route = $this->config->get('action_default');
//                        $start_page = $this->model_account_customer->getStartPage();
//                        if ($start_page) {
//                            $this->response->redirect($start_page);
//                        } else {
//                            $route = $this->config->get('action_default');
//                        }                                            
                    } 
                } else {
                    $this->response->redirect($this->url->link('account/login'));
                }
            } else {
                $this->response->redirect($this->url->link('account/login'));
            }
        }

        if (!$this->customer->isAdmin()) {
            $admin_routes = array(
                'doctype/',
                'doctype/',
                'extension/',
                'menu/',
                'marketplace/',
                'tool/'
            );
            foreach ($admin_routes as $aroute) {
                if (strpos($route, $aroute) !== false) {
                    //простому пользователю сюда нельзя
                    $this->response->redirect($this->url->link('error/access_denied'));
                }
            }
            if (trim($this->config->get('technical_break')) && $route != 'account/login' && $route != 'account/logout') {
                $this->response->setOutput($this->load->controller('info/info/getView', array('text_info' => html_entity_decode($this->config->get('technical_break')))));
                $this->response->output();
                exit;
            }
        }

        $route = preg_replace('/[^a-zA-Z0-9_\/]/', '', (string) $route);

        // Trigger the pre events
        $result = $this->event->trigger('controller/' . $route . '/before', array(&$route, &$data));

        if (!is_null($result)) {
            return $result;
        }

        // We dont want to use the loader class as it would make an controller callable.
        $action = new Action($route);

        // Any output needs to be another Action object.
        $output = $action->execute($this->registry);

        // Trigger the post events
        $result = $this->event->trigger('controller/' . $route . '/after', array(&$route, &$data, &$output));

        if (!is_null($result)) {
            return $result;
        }

        return $output;
    }

}
