<?php

class ControllerCommonHeader extends Controller {

    public function index() {
        // Analytics
        $this->load->model('setting/extension');

        $data['analytics'] = array();

        $analytics = $this->model_setting_extension->getExtensions('analytics');

        foreach ($analytics as $analytic) {
            if ($this->config->get('analytics_' . $analytic['code'] . '_status')) {
                $data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get('analytics_' . $analytic['code'] . '_status'));
            }
        }

        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }

        if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
            $this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
        }

        $data['title'] = $this->document->getTitle();

        $data['base'] = $server;
        $data['description'] = $this->document->getDescription();
        $data['keywords'] = $this->document->getKeywords();
        $data['links'] = $this->document->getLinks();
        $data['styles'] = $this->document->getStyles();
        $data['scripts'] = $this->document->getScripts('header');
        $data['lang'] = $this->language->get('code');
        $data['direction'] = $this->language->get('direction');

        $data['name'] = $this->config->get('config_name');

        if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
            $data['logo'] = $server . 'image/' . $this->config->get('config_logo');
        } else {
            $data['logo'] = '';
        }

        $this->load->language('common/header');

        $data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));

        $data['home'] = $this->url->link('common/home');
///		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
        $data['logged'] = $this->customer->isLogged();
        $data['account'] = $this->url->link('account/account', '', true);
        $data['register'] = $this->url->link('account/register', '', true);
        $data['login'] = $this->url->link('account/login', '', true);
        $data['order'] = $this->url->link('account/order', '', true);
        $data['transaction'] = $this->url->link('account/transaction', '', true);
        $data['download'] = $this->url->link('account/download', '', true);
        $data['logout'] = $this->url->link('account/logout', '', true);
        $data['shopping_cart'] = $this->url->link('checkout/cart');
        $data['checkout'] = $this->url->link('checkout/checkout', '', true);
        $data['contact'] = $this->url->link('information/contact');
        $data['telephone'] = $this->config->get('config_telephone');


        if ($this->customer->isLogged()) {//готовим меню, если пользователь не на странице входа в систему
            $this->load->model('menu/item');
            $this->load->model('tool/image');
            $data['menu_items'] = $this->getMenuItems($this->model_menu_item->getMenuItems(0));
        }

        return $this->load->view('common/header', $data);
    }

    private function getMenuItems($items) {
        $result = array();
        foreach ($items as $item) {
            if ($item['type'] == "text") {
                if ($item['action'] == 'folder') {
                    $link = $item['action_value'] ? $this->url->link('document/folder', '&folder_uid=' . $item['action_value'], true) : "";
                } else {
                    $link = $item['action_value'];
                }
            } else {
                $link = "";
            }
            $result[] = array(
                'name'      => $item['name'],
                'type'      => $item['type'],
                'image'     => $item['image'] ? $this->model_tool_image->resize($item['image'], $this->config->get('menu_image_width'), $this->config->get('menu_image_height')) : "",
                'hide_name' => $item['hide_name'],
                'link'      => $link,
                'children'  => $this->getMenuItems($item['children'])
            );
        }
        return $result;
    }

}
