<?php

/**
 * @package		Documentov
 * @author		Andrey V Surov
 * @copyright           Copyright (c) 2018 Andrey V Surov, Roman V Zhukov (https://www.documentov.com/)
 * @license		https://opensource.org/licenses/GPL-3.0
 * @link		https://www.documentov.com
 */
class ControllerDocumentDocument extends Controller {

    private $step = 0;

    public function index() {
        $this->load->language('document/document');

        $this->load->model('document/document');
        $this->load->model('doctype/doctype');

        if (!empty($this->request->get['document_uid'])) {
            if ($this->request->server['REQUEST_METHOD'] == 'POST') {
                //сохранение созданного документа из формы
                //сохраняем поля
                //получаем шаблон доктайпа
                $document_info = $this->model_document_document->getDocument($this->request->get['document_uid'], true);
                if ($document_info) {
                    $template = $this->model_document_document->getTemplate($this->request->get['document_uid'], 'form');
                    if (!empty($this->request->post['field'])) {
                        $data_eft = array(
                            'fields' => $this->request->post['field'],
                            'template' => $template,
                            'document_uid' => $this->request->get['document_uid']
                        );
                        $result = $this->editFieldsTemplate($data_eft);
                        $append = array();
                        if (!empty($result['append'])) {
                            $append = $result['append'];
                        }                        
                        if (isset($result['success'])) {
                            $route_result = $this->route($this->request->get['document_uid'], "jump"); //маршрут может переместить документ на какой-то другой адрес
                            if (!empty($route_result['redirect']) || !empty($route_result['reload']) || !empty($route_result['window'])) {
                               $json = $route_result;
                            } else {
                                $json = array(
                                    'redirect' => str_replace('&amp;', '&', $this->url->link("document/document", "document_uid=" . $this->request->get['document_uid'])),
                                    'document_uid' => $this->request->get['document_uid']
                                );
                                if (!empty($route_result['append'])) {
                                    //действие вернуло добавляемый к странице контент
                                    $append = array_merge($append, $route_result['append']);
                                }
                            }
                        } else {
                            $json = $result;
                        }
                        if ($append) {
                            $json['append'] = $append;
                        }
                        $this->response->addHeader("Content-type: application/json");
                        $this->response->setOutput(json_encode($json));                        
                    } else {
                        $json = array(
                            'redirect' => str_replace('&amp;', '&', $this->url->link("error/general_error",'error=1'))
                        );
                        $this->response->addHeader("Content-type: application/json");
                        $this->response->setOutput(json_encode($json));
                    }
                } else {
                    if (!empty($this->request->get['folder_uid'])) {
                        $url = $this->url->link('error/not_found','folder_uid=' . $this->request->get['folder_uid']);
                    } else {
                         $url = $this->url->link('error/not_found');
                    }
                    $this->response->redirect($url);
                }
            } elseif (!empty($this->request->get['folder_uid'])) { ////просмотр документа, определяем откуда создается документ - из журнала или из отдельного открытого документа
                //из журнала
                $this->get_document();
            } else {
                $this->load->model('account/customer');
                $this->model_account_customer->setLastPage($this->url->link('document/document', 'document_uid=' . $this->request->get['document_uid'], true, true));
                $route_result = $this->route($this->request->get['document_uid'], 'view');
                if (!empty($route_result['redirect'])) { 
                    $this->response->redirect($route_result['redirect']);
                } else {
                    $this->load->model('doctype/doctype');
                    $document_info = $this->model_document_document->getDocument($this->request->get['document_uid']);
                    if ($document_info) {
                        $doctype_info = $this->model_doctype_doctype->getDoctype($document_info['doctype_uid']);
                        $this->document->setTitle($doctype_info['name']);
                        $header = $this->load->controller('common/header');
                        $header_doc = $this->load->view('document/view_header', array());
                        $footer = $this->load->controller('common/footer');
                        $footer_doc = $this->load->view('document/view_footer', array());
                        $button_toolbar = $this->getButtons($this->request->get['document_uid']);
                        $this->response->setOutput($header . $button_toolbar . $header_doc . 
                                $this->getView($this->request->get['document_uid']) . 
                                (!empty($route_result['append']) ? implode("\r\n ", $route_result['append']) : "") . 
                                $footer_doc . $footer);
                    } else {
                        if (!empty($this->request->get['folder_uid'])) {
                            $url = $this->url->link('error/access_denied','folder_uid=' . $this->request->get['folder_uid']);
                        } else {
                             $url = $this->url->link('error/access_denied');
                        }                        
                        $this->response->redirect($url);
                    }
                }
            }
        } elseif (!empty($this->request->get['doctype_uid'])) {
            //создается новый документ
            $this->load->model('account/customer');
            $this->model_account_customer->setLastPage($this->url->link('document/document', 'doctype_uid=' . $this->request->get['doctype_uid'], true, true));

            //сохраняем его в базе
            $document_uid = $this->model_document_document->addDocument($this->request->get['doctype_uid']);
            $doctype_info = $this->model_doctype_doctype->getDoctype($this->request->get['doctype_uid']);
            $this->document->setTitle($doctype_info['name']);
            $append = array();
            $route_result = $this->route($document_uid, 'create');
            if (isset($route_result['append'])) {
                $append = $route_result['append'];
            }
            if (empty($this->request->get['folder_uid'])) {
                //создание документа в отдельном окне
                if (!empty($route_result['redirect'])) {
                    $this->response->addHeader("Content-type: application/json");
                    $this->response->redirect($route_result['redirect']);
                }
                $this->document->setTitle($doctype_info['name']);
                $footer = $this->load->controller('common/footer');
                $header = $this->load->controller('common/header');
                $header_doc = $this->load->view('document/form_header', array());
                $footer_doc = $this->load->view('document/form_footer', array('document_uid' => $document_uid));
                $data_button = array(
                    'document_uid' => $document_uid,
                );
                $toolbar = $this->load->view('document/form_button', $data_button);
                $form = $this->getForm($document_uid) . $footer;
                $this->response->setOutput($header . $toolbar . $header_doc . $form . implode('\r\n',$append) . $footer_doc . $footer);
            } else {
                //документ создается из журнала
                if (!empty($route_result['redirect'])) {
                    $this->response->addHeader("Content-type: application/json");
                    $this->response->setOutput(json_encode($route_result));
                } else {
                    $header = $this->load->view('document/form_header', array());
                    $footer = $this->load->view('document/form_footer',  array('document_uid' => $document_uid));
                    $this->response->addHeader('Content-type: application/json');
                    $json = array();
                    $data_button = array(
                        'document_uid' => $document_uid,
                        'folder_uid' => $this->request->get['folder_uid'] //запуск идет из журнала, пробрасываем folder_uid, чтобы при сохранении знать об этом
                    );
                    $json['toolbar'] = $this->load->view('document/form_button', $data_button);
                    $json['form'] = $header . $this->getForm($document_uid) . $footer;
                    $this->response->setOutput(json_encode($json));
                }
            }
        }
    }

    /**
     * Обработка значений полей шаблона
     * @param type $data - содержит fields - именованный массив field_uid=>field_value, template, document_uid
     * @return type
     */
    public function editFieldsTemplate($data) {
        $this->load->language('document/document');
        $this->load->model('doctype/doctype');
        $error_validation = array();
        $field_values = array();
        foreach ($data['fields'] as $field_uid => $field_value) {
            //проверяем наличие поля в шаблоне редактирования; это нужно для исключения возможности ручной передачи значения поля злоумышленником, которое отсутствует в шаблоне
            if (strpos($data['template'], 'f_' . str_replace("-", "", $field_uid)) !== false) {
                $field_info = $this->model_doctype_doctype->getField($field_uid);
                $value_access = "";
                $access = true;
                if (!empty(($field_info['params']['access_form']))) {
                    $access = false;
                    //есть ограничение на доступ к просмотру
                    foreach ($field_info['params']['access_form'] as $access_field_uid) {
                        $value_access .= "," . $this->model_document_document->getFieldValue($access_field_uid, $data['document_uid']);
                    }
                }
                if (!$access && $value_access) {
                    foreach ($this->customer->getStructureIds() as $customer_uid) {
                        if (strpos($value_access, $customer_uid) !== false) {
                            $access = true;
                            break;
                        }
                    }
                }
                if ($access) {
                    //проверим уникальность
                    if ($field_info['unique']) {
                        //значение должно быть уникально
                        $uniques = $this->model_document_document->getFieldByValue($field_uid, $field_value);
                        if ($uniques) {
                            if (count($uniques) > 1 || $uniques[0]['document_uid'] != $data['document_uid']) {
                                //уникальности нет
                                $error_validation['unique'][] = $field_info['name'];
                                continue;
                            }
                        }
                    }
                    $field_values[$field_uid] = $field_value;
                }
            }

            $empty_array = false;
            if (is_array($field_value)) {
                $empty_array = true;
                foreach ($field_value as $value) {
                    if ($value && $value != "null") {
                        $empty_array = false;
                        break;
                    }
                }
            }
            if ($field_value === "" || $empty_array) {
                //значения нет, проверим - не обязательно ли это поле для заполнения?
                $field_info = $this->model_doctype_doctype->getField($field_uid);
                if ($field_info['required']) {
                    //поле, обязательное для заполнения, не заполнено
                    $error_validation['required'][] = $field_info['name'];
                }
            }
        }
        $append = array(); //при изменении поля может сработать кон. Изменение, который вернет добавляемый блок, который нужно вернуть
        if (!$error_validation) {
            foreach ($field_values as $field_uid => $field_value) {
                $result = $this->model_document_document->editFieldValue($field_uid, $data['document_uid'], $field_value);
                if (!empty($result['append'])) {
                    $append = array_merge($append,$result['append']);
                }
            }
            //сохраняем документ
            $this->model_document_document->editDocument($data['document_uid']);
            if ($append) {
                return array(
                    'success' => true,
                    'append'  => $append);
            } else {
                return array('success' => true);
            }
            
        } else {
            //валидация не пройдена
            $result = array();
            if (!empty($error_validation['required'])) {
                $result[] = $this->language->get('error_validation_required') . implode(", ", $error_validation['required']);
            }
            if (!empty($error_validation['unique'])) {
                $result[] = $this->language->get('error_validation_unique') . implode(", ", $error_validation['unique']);
            }
            return array(
                'error' => implode("<br>", $result)
            );
        }
    }

    public function remove() {
        $this->load->model('document/document');
        $document_info = $this->model_document_document->getDocument($this->request->get['document_uid']);
        if ($document_info && $document_info['draft'] && $document_info['author_uid'] == $this->customer->getStructureId()) {
            //автор удаляет свой черновик
            $this->model_document_document->removeDocument($this->request->get['document_uid']);
        }
    }

    public function getView($document_uid) {

        $document_info = $this->model_document_document->getDocument($document_uid, true);
        if (!$document_info) {
            return $this->load->view('error/access_denied', array()); 
        }
        $template = htmlspecialchars_decode($this->model_document_document->getTemplate($document_uid, "view"));
        $data = array(
            'document_uid' => $document_uid,
            'doctype_uid' => $document_info['doctype_uid'],
            'template' => $template,
            'mode' => 'view'
        );
        return $this->renderTemplate($data);
    }

    public function getForm($document_uid) {
        $this->load->model('document/document');
        $document_info = $this->model_document_document->getDocument($document_uid);
        $template = htmlspecialchars_decode($this->model_document_document->getTemplate($document_uid,"form"));
        $data = array(
            'document_uid' => $document_uid,
            'doctype_uid' => $document_info['doctype_uid'],
            'mode' => 'form',
            'draft' => true,
            'template' => $template
        );
        //проверяме наличие черновика и, если он есть, используем его данные
        if (!empty($document_info['draft_params'])) {
            $draft_params = unserialize($document_info['draft_params']);
            $data['values'] = $draft_params;
        }
        return $this->renderTemplate($data);
    }

    public function button() {
        $this->load->model('document/document');
        $document_uid = $this->request->get['document_uid'];
        if ($this->validateButton()) {
            $this->session->data['current_button_uid'] = $this->request->get['button_uid'];//идентификатор нажатой кнопки
            $button_info = $this->model_document_document->getButton($this->request->get['button_uid']);            
            $document_info = $this->model_document_document->getDocument($document_uid);
            $route_uid = $document_info['route_uid'];
            $append = array();
            if ($button_info['action']) {
                $data = $this->request->post;
                $data['document_uid'] = $document_uid;
                $data['button_uid'] = $this->request->get['button_uid'];
                $data['params'] = $button_info['action_params'];

                $this->load->model('tool/utils');
                $this->model_tool_utils->addLog($document_uid, 'docbutton_action', $button_info['action'], $data['button_uid'], array_merge($data['params'],array('button_name' => $this->db->escape($button_info['name']))));
                $result = $this->load->controller("extension/action/" . $button_info['action'] . "/executeButton", $data);
                if (!empty($result['append'])) {
                    $append[] = $result['append'];
                }
                if (isset($result['log'])) {
                    //записываем результат выполнения действия
                    if ($button_info['action_log'] && $document_info['field_log_uid']) {
                        $data_log = array(
                            'date' => $this->getCurrentDateTime("d.m.Y H:i:s"),
                            'name' => $this->customer->getName(),
                            'button' => $button_info['name'],
                            'action_log' => $result['log']
                        );
                        $this->model_document_document->appendLogFieldValue(
                                $document_info['field_log_uid'], $document_uid, $data_log);
                    }
                }
                if (!isset($result['window']) && !isset($result['replace']) && !isset($result['error']) && !isset($result['redirect'])) {
                    //действие отработало    
                    //ДЕЙСТВИЕ В КНОПКЕ НЕ МОЖЕТ ПЕРЕМЕЩАТЬ ДОКУМЕНТ, но если вдруг стороннее действие это сделало, все же проверим перемещение
                    $document_info = $this->model_document_document->getDocument($document_uid);
                    if ($document_info) {
                        //проверяем наличие документа; если он был удален - $document_info пуста
                        if ($route_uid !== $document_info['route_uid']) {
                            //документ перемещен
                            $route_result = $this->route($document_uid, 'jump');
                        } else {
                            //действие не перемещало документ
                            //запускаем контекст активности
                            $route_result = $this->route($document_uid, 'view');
                            if ($button_info['action_move_route_uid'] && !isset($result['replace'])) {
                                //если действие вернуло replace, оно что-то меняет на текущей странице, перемещать док нельзя
                                if ($route_uid == $document_info['route_uid'] && $this->model_document_document->moveRoute($document_uid, $button_info['action_move_route_uid'])) {
                                    $route_result = $this->route($document_uid, 'jump');
                                }
                            }
                        }
                        if (!empty($route_result['append'])) {
                            $append[] = $route_result['append'];
                        }
                        
                    }
                }
            } else {
                //в кнопке нет действия
                if ($button_info['action_move_route_uid']) {
                    $document_info = $this->model_document_document->getDocument($document_uid);
                    if ($route_uid == $document_info['route_uid'] && $this->model_document_document->moveRoute($document_uid, $button_info['action_move_route_uid'])) {
                        $route_result = $this->route($document_uid, 'jump');
                    }
                    if (!empty($route_result['append'])) {
                        $append[] = $route_result['append'];
                    }
                }
                //Записываем название кнопки в ход работы
                if (empty($route_result['redirect']) && empty($route_result['replace']) && empty($route_result['window']) 
                        && empty($route_result['reload'])) {
                    $document_info = $this->model_document_document->getDocument($document_uid);
                    if ($document_info) {
                        $route_result = $this->route($document_uid, 'view');
                        if (!empty($route_result['append'])) {
                            $append[] = $route_result['append'];
                        }                        
                        //документ мог быть удален, например, в контекте перехода могло быть д. Удаление... Но этого не случилось
                        if ($button_info['action_log'] && $document_info['field_log_uid']) {
                            $data_log = array(
                                'date' => $this->getCurrentDateTime("d.m.Y H:i:s"),
                                'name' => $this->customer->getName(),
                                'button' => $button_info['name'],
                                'action_log' => ""
                            );
                            $this->model_document_document->appendLogFieldValue(
                                    $document_info['field_log_uid'], $document_uid, $data_log);
                        }
                    }
                }    
            }
            unset($this->session->data['current_button_uid']);//сброс последней нажатой кнопки
            if (!empty($route_result['redirect']) || !empty($route_result['reload']) || !empty($route_result['replace']) || !empty($route_result['window'])) {
                $this->response->addHeader("Content-type: application/json");
                $this->response->setOutput(json_encode($route_result));
            } elseif (!empty($result['redirect'])) {
                $route_result = $this->route($document_uid, 'view');
                if (!empty($result['redirect'])) {
                    $this->response->addHeader("Content-type: application/json");
                    $this->response->setOutput(json_encode($result));
                } elseif (!empty($route_result['redirect'])) {
                    $this->response->addHeader("Content-type: application/json");
                    $this->response->setOutput(json_encode($route_result));
                } elseif (isset($result)) {
                    $this->response->addHeader('Content-type: application/json');
                    $this->response->setOutput(json_encode($result));
                } else {
                    $this->response->addHeader('Content-type: application/json');
                    $result = array(
                        'reload' => str_replace('&amp;', '&', $this->url->link('document/document', 'document_uid=' . $document_uid . '&_=' . rand(100000000, 999999999)))
                    );
                    $this->response->setOutput(json_encode($result));
                }
            } elseif (isset($result['replace']) || isset($result['window']) || isset($result['reload'])) {
                $this->response->addHeader('Content-type: application/json');
                $this->response->setOutput(json_encode($result));
            } elseif (isset($result['error'])) {
                $this->response->addHeader('Content-type: application/json');
                $this->response->setOutput(json_encode($result));
            } else { //if(isset($result) || isset($route_result))
                $result = array();
                if ($button_info['show_after_execute']) {
                    $result['reload'] = str_replace('&amp;', '&', $this->url->link('document/document', 'document_uid=' . $document_uid . '&nocache=' . rand(100000000, 999999999)));
                } else {
                    $result['redirect'] = str_replace('&amp;', '&', $this->url->link('document/document', 'document_uid=' . $document_uid . '&nocache=' . rand(100000000, 999999999)));
                }
                if ($append) {
                    $result['append'] = $append;
                }
                $this->response->addHeader('Content-type: application/json');
                $this->response->setOutput(json_encode($result));
            }
        } else {
            $this->load->language('doctype/doctype');
//            $json = array(
//                'error' => $this->language->get('text_error_access_button')
//            );
            //если просто выдавать сообщение, как ранее (коммент. строки сверху), то странца не перезагружается и пользователь
            //может очень долго нажимать на кнопку и гадать, чего от него хотя
            $json = array(
                'redirect' => str_replace('&amp;', '&', $this->url->link('document/document', 'document_uid=' . $document_uid . '&nocache=' . rand(100000000, 999999999)))
            );            
            $this->response->addHeader('Content-type: application/json');
            $this->response->setOutput(json_encode($json));
        }
    }

    public function route_cli($data) {
        return $this->route($data['document_uid'], $data['context']);
    }

    public function route($document_uid, $context) {
        if (defined("ROUTE_RECURSION_DEPTH")) {
            $recursion_depth = ROUTE_RECURSION_DEPTH;
        } else {
            $recursion_depth = $this->config->get('recursion_depth');
        }
        if (++$this->step > $recursion_depth) {
            return array('redirect' => $this->url->link('error/cycle'));
        }
        
        $this->load->model('document/document');
        $document_info = $this->model_document_document->getDocument($document_uid, false);
        
        if ($document_info) {
            if ($context == "setting") {
                $route_uid = $this->model_document_document->getFirstRoute($document_info['doctype_uid']);
            } else {
                $route_uid = $document_info['route_uid'];
            }
            
            $actions = $this->model_document_document->getRouteActions($route_uid);
            $this->load->model('tool/utils');
            $append = array();
            if (!empty($actions[$context])) {
                
                foreach ($actions[$context] as $action) {
                    $action['document_uid'] = $document_uid;
                    $data = array(
                        'document_uid' => $document_uid,
                        'route_uid' => $route_uid,
                        'params' => $action['params'],
                    );
                    $this->model_tool_utils->addLog($document_uid, 'action', $action['action'], "", array_merge($data['params'],array('context' => $context)));

                    $result = $this->load->controller("extension/action/" . $action['action'] . "/executeRoute", $data);
                    $document_info = $this->model_document_document->getDocument($document_uid, false);
                    if ($document_info) {
                        //если $document_info есть, значит документ не был удален
                        if (isset($result['log']) && $action['action_log'] && $document_info['field_log_uid']) {
                            $data_log = array(
                                'date' => $this->getCurrentDateTime("d.m.Y H:i:s"),
                                'name' => $this->language->get('text_system'),
                                'button' => $this->load->controller("extension/action/" . $action['action'] . "/getTitle"),
                                'action_log' => $result['log']
                            );
                            $this->model_document_document->appendLogFieldValue($document_info['field_log_uid'], $data['document_uid'], $data_log);
                        }
                        if (!empty($result['append'])) {
                            if (is_array($result['append'])) {
                                $append = array_merge($append, $result['append']);
                            } else {
                                $append[] = $result['append'];
                            }                            
                        }
                        
                        if (!empty($result['redirect']) || !empty($result['replace'])) {
                            //происходит редирект, это может быть действие Перенаправление
                            if ($append) {
                                $result['append'] = $append;
                            }
                            return $result;
                        }

                        if ($route_uid != $document_info['route_uid']) {
                            //документ был перемещен
                            break;
                        }
                    } else {                        
                        //текущий документ был удален
                        if ($append) {
                            $result['append'] = $append;
                        }                        
                        return $result;                        
                    }
                }
                $route_result = array();
                if ($route_uid != $document_info['route_uid']) {
                    //изменена точка, запускаем jump
                    if ($context != "change" || ($this->step == 1 && empty($this->request->get['button_uid']))) { 
                    // контекст изменения вызывается в результате выполнения действия, запускаемого через маршрут 
                    // или от кнопки, поэтому перемещения будут обрабованы на более высоких уровнях в контекстах (route) 
                    // или кнопках (view)
                    // возможен только вариант - если изменение произошло при создании документа, в этом случае $this->step == 1 и
                    // запуск не должен быть от кнопки    
                        
                        $route_result = $this->route($document_uid, 'jump');
                         if (!empty($route_result['append'])) {
                             $append = array_merge($append, $route_result['append']);
                         }
                    }
                } 
                if ($append) {
                    $route_result['append'] = $append;
                }
                $this->step--;
                return $route_result;
                
            }
        } else {
            if ($document_uid) {
                if (!empty($this->request->get['folder_uid'])) {
                    $url = $this->url->link('error/access_denied','folder_uid=' . $this->request->get['folder_uid']);
                } else {
                    $url = $this->url->link('error/access_denied');
                }                 
            } else {
                //возможно, не передали document_uid
                if (!empty($this->request->get['folder_uid'])) {
                    $url = $this->url->link('error/not_found','folder_uid=' . $this->request->get['folder_uid']);
                } else {
                    $url = $this->url->link('error/not_found');
                }                 
            }
            //документ не найден или к нему нет доступа
           
            return array('redirect' => $url);
        }
    }

    /**
     * 
     * @param type $data: 
     *      'doctype_uid' => обязательный параметр, 
     *      'document_uid' => передается, если нужно установить значения в полях шаблона, 
     *      'draft'         => TRUE или FALSE - черновики брать или нет
     *      'template'  => обязательный параметр
     *      'mode' => view | edit, 
     *      'values' => значения, которые нужно установить в поля шаблона, document_uid в этом случае будет проигнорирован
     *      'params' => доп. параметры, которые будут переданы в TWIG
     * @return type
     */
    public function renderTemplate($data) {
        $this->load->model('doctype/doctype');
        $this->load->model('document/document');
        foreach ($this->model_document_document->getFields($data['doctype_uid']) as $field) {
            $fid = 'f_' . str_replace("-", "", $field['field_uid']);
            if (strpos($data['template'], $fid) !== FALSE) {
                //Поле есть в шаблоне, подготавливаем данные для передачи для контроллера поля
                $d = $field['params'];
                $d['document_uid'] = $data['document_uid'];
                $d['field_uid'] = $field['field_uid'];
                if (!empty($data['values'])) {
                    if (isset($data['values'][$field['field_uid']])) {
                        $d['field_value'] = $data['values'][$field['field_uid']];
                    }
                } 
                if (!isset($d['field_value']) && !empty($data['document_uid'])) {
                    //поля не было в черновике и есть document_uid, поэтому пробуем получить значение поля из базы
                    $d['field_value'] = $this->model_document_document->getFieldValue($field['field_uid'], $data['document_uid'], $data['draft'] ?? false);
                }
                
                $access_view = true;
                $access_form = true;
                if (!empty($field['params']['access_view'])) {
                    //есть ограничение на доступ к просмотру поля
                    $access_view = false;
                    foreach ($field['params']['access_view'] as $access_field_uid) {
                        $value = $this->model_document_document->getFieldValue($access_field_uid, $data['document_uid']);
                        foreach ($this->customer->getStructureIds() as $structure_uid) {
                            if (strpos($value, $structure_uid) !== false) {
                                $access_view = true;
                                break;
                            }
                        }
                        if ($access_view) {
                            break;
                        }
                    }
                }
//                if ($access_view) {
                    //проверим ограничение на доступ к виджету формы
                    if (!empty($field['params']['access_form'])) {
                        //есть ограничение на доступ к просмотру поля
                        $access_form = false;
                        foreach ($field['params']['access_form'] as $access_field_uid) {
                            $value = $this->model_document_document->getFieldValue($access_field_uid, $data['document_uid']);
                            foreach ($this->customer->getStructureIds() as $structure_uid) {
                                if (strpos($value, $structure_uid) !== false) {
                                    $access_form = true;
                                    break;
                                }
                            }
                            if ($access_form) {
                                break;
                            }
                        }
                    }
//                }
                    
                 $data[$fid] = "";    
                if ($data['mode'] == 'form' && $access_form) {
                    $data[$fid] = $this->load->controller('extension/field/' . $field['type'] . '/getForm', $d);
                }  elseif ($access_view) {
                    $data[$fid] = $this->load->controller('extension/field/' . $field['type'] . '/getView', $d);
                }
                  
//                if ($access_view) {
//                    if ($data['mode'] == 'form' && $access_form) {
//                        $data[$fid] = $this->load->controller('extension/field/' . $field['type'] . '/getForm', $d);
//                    } else {
//                        $data[$fid] = $this->load->controller('extension/field/' . $field['type'] . '/getView', $d);
//                    }
//                } else {
//                    $data[$fid] = "";
//                }
            }
        }

        //получаем значения переменных, если есть document_uid
        if (!empty($data['document_uid'])) {
            foreach ($this->model_doctype_doctype->getTemplateVariables() as $var_id => $var_name) {
                $data[$var_id] = $this->model_document_document->getVariable($var_id);
            }
        }
        if (isset($data['params'])) {
            $data = $data['params'];
        }
        include_once(DIR_SYSTEM . 'library/template/Twig/Autoloader.php');

        Twig_Autoloader::register();

        $loader = new \Twig_Loader_Filesystem(DIR_TEMPLATE);

        $config = array('autoescape' => false);

        if ($this->config->get('template_cache')) {
            $config['cache'] = DIR_CACHE;
        }
        $twig = new \Twig_Environment($loader, $config);
        try {
            return $twig->createTemplate($data['template'])->render($data);
        } catch (Exception $ex) {
            $this->load->language('document/document');
            return $this->language->get('template_error') . ": " . $ex->getMessage();
        }
    }

    public function folder_route($data) {
        return $this->route($data['document_uid'], $data['context']);
    }

    public function autocomplete() {
        $json = array();
        $this->load->model('document/document');
        $this->load->model('doctype/doctype');                
        $this->load->language('document/document');
         
        if (!empty($this->request->get['field_uid'])) {
            $field_uid = $this->request->get['field_uid'];
        } else {
            //field_uid для автокомплита не указан, будем использовать первое поле доктайпа
            $data = array(
                'doctype_uid'   => $this->request->get['doctype_uid'],
                'setting'       => 0,
                'limit'         => 1
            );
            $fields = $this->model_doctype_doctype->getFields($data);
            $field_uid = $fields[0]['field_uid'] ?? ""; 
        }       
        $field_info = $this->model_doctype_doctype->getField($field_uid);
        if ($field_info['access_view']) {
            //если для поля есть ограниченные настройки доступа выдаем ошибку: проверять имеет ли текущий доступ к полю каждого документа слишком накладно по ресурсам
            $json[] = array(
                    'document_uid' => 0,
                    'name' => $this->language->get('text_error_autocomplete_access')
            );
        } else {
            if ((!empty($this->request->get['doctype_uid']) || !empty($this->request->get['source_field_uid'])) && $field_uid) {
                $filter_data = array();
                if (!empty($this->request->get['filters'])) {
                    $filter_data['filter_names'] = array();
                    $filter_data['field_uids'] = array($field_uid);
                    foreach ($this->request->get['filters'] as $filter) {
                        // в $filter['value'] передает UID поля документа, в котором находится значение для фильтра
                        
                        $filter_value = $this->model_document_document->getFieldValue($filter['value'], $this->request->get['document_uid'] ?? 0, true) ?? "";

                        if (!isset($filter_data['filter_names'][$filter['field_uid']])) {
                            $filter_data['filter_names'][$filter['field_uid']] = array(array(
                                'value'     => $filter_value, //$filter['value'],
                                'condition' => $filter['condition']
                            ));
                        } else {
                            $filter_data['filter_names'][$filter['field_uid']][] = array(
                                'value'     => $filter_value, //$filter['value'],
                                'condition' => $filter['condition']
                            );                            
                        }
                    }
                } else {
                     $filter_data['field_uids'] = array($field_uid);
                }
                if (!empty($this->request->get['filter_name'])) {
                    //добавляем фильтр по введенному пользователем тексту в поле
                    if (!empty($filter_data)) {
                        if (!isset($filter_data['filter_names'][$field_uid])) {
                            $filter_data['filter_names'][$field_uid] = array(array(
                                'value'     => $this->request->get['filter_name'],
                                'condition' => "contains"
                            ));
                        } else {
                            $filter_data['filter_names'][$field_uid][] = array(
                                'value'     => $this->request->get['filter_name'],
                                'condition' => "contains"
                            );                            
                        }                        
                    } else {
                        $filter_data['filter_name'] = !empty($this->request->get['filter_name']) ? $this->request->get['filter_name'] : "";                                      
                    }
                } 
                if (!empty($this->request->get['doctype_uid'])) {
                    $filter_data['doctype_uid'] = $this->request->get['doctype_uid'];
                }
                if (!empty($this->request->get['source_field_uid'])) {
                    $source_field_value = $this->model_document_document->getFieldValue($this->request->get['source_field_uid'], $this->request->get['document_uid'] ?? 0, TRUE);
                    if (is_array($source_field_value)) {
                        $filter_data['document_uids'] = $source_field_value;
                    } else {
                        $filter_data['document_uids'] = explode(",",$source_field_value);
                    }
                    
                }   
                $filter_data['start'] = 0;
                $filter_data['limit'] = 100;
                $filter_data['draft_less'] = 2;
                $filter_data['sort'] = $field_uid;
                $filter_data['order'] = "asc";
                $results = $this->model_document_document->getDocuments($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'document_uid' => $result['document_uid'],
                        'name' => mb_substr(strip_tags(html_entity_decode($result['display_value'] ?? $result['v' . str_replace("-", "", $field_uid)], ENT_QUOTES, 'UTF-8')), 0, 100),
                    );
                }
            } else {               
                $json[] = array(
                    'document_uid' => 0,
                    'name' => $this->language->get('text_error_autocomplete_setting')
                );
            }
        }
        
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function get_document() {
        if (!empty($this->request->get['document_uid'])) {
            $route_result = $this->route($this->request->get['document_uid'], 'view');
            if (!empty($route_result['redirect']) || !empty($route_result['reload']) || !empty($route_result['replace'])) { //запускаем сначала контекст активности маршрута
                $this->response->addHeader("Content-type: application/json");
                $this->response->setOutput(json_encode($route_result));
            } else {
                $header = $this->load->view('document/view_header', array());
                $footer = $this->load->view('document/view_footer', array());
                $json = array();
                $json['toolbar'] = $this->getButtons($this->request->get['document_uid']);
                $json['form'] = $header . $this->getView($this->request->get['document_uid']) . $footer;
                $json['document_uid'] = $this->request->get['document_uid'];
                if (!empty($route_result['append'])) {
                    //какое-то действие маршрута добавляет что-то для отображения, например, Сообщение - модальное окно
                    $json['append'] = $route_result['append'];
                }
                $this->response->addHeader('Content-type: application/json');
                $this->response->setOutput(json_encode($json));
            }
        }
    }

    public function getButtons($document_uid) {
        $buttons = array();
        $this->load->model('document/document');
        $this->load->model('tool/image');
        foreach ($this->model_document_document->getButtons($document_uid) as $button) {
            if ($button['picture']) {
                if ($button['name']) {
                    $picture25 = $this->model_tool_image->resize($button['picture'], 28, 28);
                } else {
                    $picture25 = $this->model_tool_image->resize($button['picture'], 28, 28);
                }
            } else {
                $picture25 = "";
            }

            $buttons['buttons'][] = array(
                'button_uid' => $button['route_button_uid'],
                'name' => $button['name'],
                'title' => $button['description'],
                'picture' => $picture25,
                'hide_button_name' => $button['hide_button_name'],
                'color' => $button['color'],
                'background' => $button['background']
            );
        }
        $buttons['document_uid'] = $document_uid;
        return $this->load->view('document/view_button', $buttons);
    }

    public function save_draft() {
        $this->load->model('document/document');
        $document_info = $this->model_document_document->getDocument($this->request->get['document_uid']);
        if (isset($document_info['route_uid']) && $document_info['route_uid'] == $this->model_document_document->getFirstRoute($document_info['doctype_uid']) && //документ находится на нулевой точке
                $document_info['author_uid'] == $this->customer->getStructureId()) { //и с ним работает его автор
            $this->model_document_document->saveDraftDocument($this->request->get['document_uid'], $this->request->post['field']);
        }
    }

    public function validateButton() {
        if (empty($this->request->get['button_uid']) || empty($this->request->get['document_uid'])) {
            return false;
        }
        return $this->model_document_document->hasAccessButton($this->request->get['button_uid'], $this->request->get['document_uid']);
    }

    private function getCurrentDateTime($format) {
        $date = new DateTime("now", new DateTimeZone($this->config->get('date.timezone')));
        return $date->format($format);
    }
    
    /**
     * Метод загрузки файлов
     * @param type $data
     *      [file_extes] - массив с разрешенными расширениями
     *      [file_mimes] - массив с разрешенными MIME
     *      [size_file]  - макс размер файла
     *      [dir_file_upload] - куда загружаем файлы
     */
    public function uploadFile($data) {
        $result = array();
        
        $this->load->language('document/document');
        
                
        if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name']) && !empty($this->request->get['field_uid'])) {  
            
            $filename = rawurldecode(basename(rawurlencode(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'))));

            if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 250)) {
                $result['error'] = $this->language->get('error_upload_filename');
            }

            if (defined("DISK_SPACE_AVAILABLE")) {
                $this->load->model('tool/utils');
                if (DISK_SPACE_AVAILABLE < ($this->model_tool_utils->getUseFieldDisk()  + filesize($this->request->files['file']['tmp_name']))) {
                    $result['error'] = $this->language->get('error_upload_diskspace');
                }
            }
            if (!$result) {
                $allowed = array();

                if (!empty($data['file_extes'])) {
                    foreach ($data['file_extes'] as $filetype) {
                            $allowed[] = trim($filetype);
                    }                
                }

                if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
                    $result['error'] = sprintf($this->language->get('error_upload_filetype'), strtolower(substr(strrchr($filename, '.'), 1)));
                }
                
            }
            if (!$result && !empty($data['file_mimes'])) {
                $allowed = array();
                foreach ($data['file_mimes'] as $filemime) {
                        $allowed[] = trim($filemime);
                }                
                
                if (!in_array($this->request->files['file']['type'], $allowed)) {
                    $result['error'] = sprintf($this->language->get('error_upload_filetype'), $this->request->files['file']['type']);
                }
                
            }
            if (!$result && !empty($data['size_file']) && filesize($this->request->files['file']['tmp_name']) > $data['size_file']*1024) {
                    $result['error'] = $this->language->get('error_upload_filesize');
            }                                
            

            if (!$result) {
//                $content = file_get_contents($this->request->files['file']['tmp_name']);
//
//                if (preg_match('/\<\?php/i', $content)) {
//                    $result['error'] = $this->language->get('error_upload_filetype');
//                }
//
//                if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
//                        $result['error'] = $this->language->get('error_upload') . $this->request->files['file']['error'];
//                }
                
            }

            if (!$result) {
                $token = token(32);
                if (!file_exists($data['dir_file_upload'] . $this->request->get['field_uid'] . date('/Y/m'))) {
                    mkdir($data['dir_file_upload'] . $this->request->get['field_uid'] . date('/Y/m'), 0700, true);                    
                }
                move_uploaded_file($this->request->files['file']['tmp_name'], $data['dir_file_upload'] .  $this->request->get['field_uid'] . date('/Y/m/') . $token . $filename);                
                $result['token'] = $token;
            }
        } else {
            $result['error'] = $this->language->get('error_upload');
        }

        return $result;
    }  
    
    /**
     * Метод запускает обработку контекстов, может выполняться демоном
     * @param type $data
     *  doctype_uid => все документа данного типа
     *  field_uid   => измененное поле
     *  context     => контекст для запуска
     */
    public function executeDeferred($data) {
        $this->load->model('document/document');
        //получаем все документы данного типа
        if (!empty($data['doctype_uid'])) {
            $document_uids = $this->model_document_document->getDocumentIds(array('doctype_uids' => array($data['doctype_uid'])));
            //устанавливаем измененное поле
            $this->request->get['field_uid'] = $data['field_uid'];
            foreach ($document_uids as $document_uid) {
                //запускаем маршрут
                $this->route($document_uid, $data['context']);
            }            
        }
    }    

    

}
