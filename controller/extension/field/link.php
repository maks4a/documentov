<?php
/**
 * @package		Documentov
 * @author		Andrey V Surov
 * @copyright           Copyright (c) 2018 Andrey V Surov, Roman V Zhukov (https://www.documentov.com/)
 * @license		https://opensource.org/licenses/mit-license.php
 * @link		https://www.documentov.com
*/

class ControllerExtensionFieldLink extends FieldController {

    const FIELD_INFO = array(
        'methods' => array(
            array('type' => 'getter', 'name' => 'get_first_link'),
            array('type' => 'getter', 'name' => 'get_first_link_url'),
            array('type' => 'getter', 'name' => 'get_first_link_text'),
            array('type' => 'getter', 'name' => 'get_display_value_not_link'),
            array('type' => 'getter', 'name' => 'get_display_value_link'),
            array('type' => 'getter', 'name' => 'get_another_field_value', 'params' => array('another_field_uid')),
            array('type' => 'getter', 'name' => 'get_another_field_display', 'params' => array('another_field_uid')),
            array('type' => 'setter', 'name' => 'append_link', 'params' => array('standard_setter_param')),
            array('type' => 'setter', 'name' => 'remove_link', 'params' => array('standard_setter_param'))
        )
    );

    public function setting() {
        $this->load->language('extension/field/link');

        $data['cancel'] = $this->url->link('marketplace/extension', 'type=field', true);

        $this->response->setOutput($this->load->view('extension/field/link', $data));
    }

    public function index() {
    }

    public function install() {
        $this->load->model('extension/field/link');
        $this->model_extension_field_link->install();
    }

    public function uninstall() {
        $this->load->model('extension/field/link');
        $this->model_extension_field_link->uninstall();
    }

    /**
    * Возвращает неизменяемую информацию о поле
    * @return array()
    */
    public function getFieldInfo() {
        return ControllerExtensionFieldLink::FIELD_INFO;
    }
    
    /**
     * Метод возвращает название поля в соответствии с выбранным языком
     * @return type
     */
    public function getTitle() {

        $this->language->load('extension/field/link');
        return $this->language->get('heading_title');
    }

    /**
     * Метод возвращает описание параметров поля
     */
    public function getDescriptionParams($params) {
        $this->load->model('doctype/doctype');
        $this->load->language('extension/field/link');
        $result = array();
        if (!empty($params['doctype_uid'])) {
            $doctype_info = $this->model_doctype_doctype->getDoctype($params['doctype_uid']);
            if (isset($doctype_info['name'])) {
                $result[] = sprintf($this->language->get('text_description_doctype'), $doctype_info['name']);
            }
        if (!empty($params['doctype_field_uid'])) {
            $field_info = $this->model_doctype_doctype->getField($params['doctype_field_uid']);
            if (!empty($field_info['name'])) {
                $result[] = sprintf($this->language->get('text_description_field'), $field_info['name']);
            }
            
        }            
        }
        return implode("; ", $result);
    }

    /**
     * Возвращает форму поля для настройки администратором
     * @param type $data
     */
    public function getAdminForm($data) {
        $this->load->model('doctype/doctype');
        if (!empty($data['params']['doctype_uid'])) {
            $doctype_description = $this->model_doctype_doctype->getDoctypeDescriptions($data['params']['doctype_uid']);
            $data['doctype_name'] = isset($doctype_description[$this->config->get('config_language_id')]['name']) ? $doctype_description[$this->config->get('config_language_id')]['name'] : "";
        }
        if (!empty($data['params']['doctype_field_uid'])) {
            $field_info = $this->model_doctype_doctype->getField($data['params']['doctype_field_uid'],1);
            $data['doctype_field_name'] = $field_info['name'] ?? "";
        }
        if (!empty($data['params']['source_type']) && $data['params']['source_type'] == "field") {
            //источник для автокомплита - поле
            if (!empty($data['params']['source_field_uid'])) {
                $source_field_info = $this->model_doctype_doctype->getField($data['params']['source_field_uid'],1);
                $data['source_field_name'] = $source_field_info['name'] ?? "";
            }
        }         
        return $this->load->view($this->config->get('config_theme') . '/template/field/link/link_form', $data);
    }

    /**
     * Возвращает виджет поля для режима создания / редактирования поля
     *  $data = $field['params'], 'field_uid', 'document_uid'
     */
    public function getForm($data) {
        $this->load->model('doctype/doctype');
        $this->load->model('document/document');        
        if (empty($data['doctype_field_uid'])) {
            //не задано отображаемое поле
            $data_f = array(
                'doctype_uid'   => $data['doctype_uid'],
                'setting'       => 0,
                'limit'         => 1
            );            
            $fields = $this->model_doctype_doctype->getFields($data_f);
            $field_uid = $fields[0]['field_uid'] ?? "";             
        } else {
            $field_uid = $data['doctype_field_uid'];
        }
        $field_info = $this->model_doctype_doctype->getField($field_uid);
        if (!empty($data['list'])) {
            
            
            $filter_data = array(
                'filter_name' => "",
                'field_uid' => $field_uid
            );
            if (!empty($data['source_type']) && $data['source_type'] == "field" && !empty($data['source_field_uid'])) {
                //источник данных - поле
                $source_field_value = $this->model_document_document->getFieldValue($data['source_field_uid'], $data['document_uid'] ?? 0);
                $filter_data['document_uids'] = explode(",",$source_field_value);
            } else {
                //все документы заданного доктайпа
                $filter_data['doctype_uid'] = $data['doctype_uid'];
            }
            $results = $this->model_document_document->getDocuments($filter_data);
            $data['documents'] = array();
            foreach ($results as $result) {
                $data['documents'][] = array(
                    'document_uid' => $result['document_uid'],
                    'name' => strip_tags(html_entity_decode($result['display_value'], ENT_QUOTES, 'UTF-8')),
                    
                );
            }
        }
        if (isset($data['field_value']) || isset($data['value'])) { //$data['value'] - значение может пробрасываться из виджета фильтра, например
            $this->load->model('document/document');
            $vid = "";
            if (isset($data['field_value'])) {
                $vid = $data['field_value'];
            } else {
                $vid = $data['value'];
            }

            if ($data['multi_select']) {
                if (is_array($vid)) {
                    $values = $vid;
                } else {
                    $values = explode(",", $vid);
                }
                $data['values'] = array();
                foreach ($values as $value) {
                    if (!$value)
                        continue;
                    $data['values'][] = array(
                        'id' => $value,
                        'name' => htmlentities(strip_tags(html_entity_decode($this->model_document_document->getFieldDisplay($field_uid, !$field_info['setting'] ? $value : 0))))
                        //вырезаем теги, чтобы не показывать их при редактировании, если отображаемое поле, к примеру, текстовое с тегами
                    );
                }
            } else {
                //в $vid не должен быть массив, но если было множественное поле, создали док, сохранился черновик, изменили множественный выбор в ССылке на одиночных и снова создали док- то получится массив
                if (is_array($vid)) {
                    $data['value_id'] = $vid[0];
                } else {
                    $data['value_id'] = $vid;
                }                
                
                $data['value_name'] = "";
                if ($vid) {
                    $data['value_name'] = htmlentities(strip_tags(html_entity_decode($this->model_document_document->getFieldDisplay($field_uid, !$field_info['setting'] ? $data['value_id'] : 0))));
                    //вырезаем теги, чтобы не показывать их при редактировании, если отображаемое поле, к примеру, текстовое с тегами
                }
            }
        }


        return $this->load->view('field/link/link_widget_form', $data);
    }

    /**
     * Возвращает  поле для режима просмотра
     */
    public function getView($data) {
        $this->load->model('document/document');
        $this->load->model('extension/field/link');
        $display_value = "";
        $result = array();
        if (!empty($data['field_value'])) {
            if (!empty($data['field_uid'])) {
                $field_value = $this->model_extension_field_link->getFieldValue($data['field_uid'], $data['document_uid']);
                if ($data['field_value'] == $field_value['value'] && $field_value['full_display_value']) {
                    return htmlspecialchars_decode($field_value['full_display_value']);
                }
            }
            return str_replace(", ",htmlspecialchars_decode($data['delimiter']), $this->model_extension_field_link->getDisplay($data['document_uid'] ?? "", $data['field_uid'] ?? "", $data['field_value'], array('params'=>$data)));
        }
        return "";
    }


    //Метод возвращает форму настройки параметров метода
    public function getFieldMethodForm($data) {
        $this->language->load('extension/field/link');
        switch ($data['method_name']) {
            case "get_another_field_value":
                $this->load->model('doctype/doctype');
                $field_info = $this->model_doctype_doctype->getField($data['field_uid']);
                $data['doctype_uid'] = $field_info['params']['doctype_uid'];
                if (!empty($data['method_params']['another_field_uid'])) {
                    $another_field_info = $this->model_doctype_doctype->getField($data['method_params']['another_field_uid']['value'],1);
                    $data['another_field_name'] = $another_field_info['name'];
                }
                return $this->load->view('field/link/method_another_field_form', $data);                
            case "get_another_field_display":
                $this->load->model('doctype/doctype');
                $field_info = $this->model_doctype_doctype->getField($data['field_uid']);
                $data['doctype_uid'] = $field_info['params']['doctype_uid'];
                if (!empty($data['method_params']['another_field_uid'])) {
                    $another_field_info = $this->model_doctype_doctype->getField($data['method_params']['another_field_uid']['value'],1);
                    $data['another_field_name'] = $another_field_info['name'];
                }
                return $this->load->view('field/link/method_another_field_form', $data);                
            default:
                return '';
        }
    }

    //геттеры
    public function get_first_link_url($params) {
        $this->load->model('document/document');
        $this->load->model('doctype/doctype');
        $value = explode(",", $this->model_document_document->getFieldValue($params['field_uid'], $params['document_uid']))[0];
        $field_info = $this->model_doctype_doctype->getField($params['field_uid'], 0);
        $linktext = $this->model_document_document->getFieldValue($field_info['params']['doctype_field_uid'], $value);
        $url = str_replace('&amp;', '&', $this->url->link('document/document', 'document_uid=' . $value));
        return '<a href="' . $url . '" class="linkfield" data-document_uid="' . $params['document_uid'] . '">' . $linktext . '</a>';
    }

    public function get_first_link_text($params) {
        $this->load->model('document/document');
        $this->load->model('doctype/doctype');
        $value = explode(",", $this->model_document_document->getFieldValue($params['field_uid'], $params['document_uid']))[0];
        $field_info = $this->model_doctype_doctype->getField($params['field_uid'], 0);
        $linktext = $this->model_document_document->getFieldValue($field_info['params']['doctype_field_uid'], $value);
        return $linktext;
    }

    public function get_first_link($params) {
        $this->load->model('document/document');
        $value = explode(",", $this->model_document_document->getFieldValue($params['field_uid'], $params['document_uid']))[0];
        return $value;
    }

    public function get_display_value_not_link($params) {
        $this->load->model('extension/field/link');
        $this->load->model('doctype/doctype');
        $field_info = $this->model_doctype_doctype->getField($params['field_uid']);
        $display = $this->model_extension_field_link->getDisplay($params['document_uid'],$params['field_uid'],"",$field_info);           
        return trim(str_replace(", ",$field_info['params']['delimiter'],strip_tags($display)));
    }

    public function get_display_value_link($params) {
        $this->load->model('extension/field/link');
        $field_value = $this->model_extension_field_link->getFieldValue($params['field_uid'],$params['document_uid']);      
        return $field_value['full_display_value'];        
    }

    public function get_another_field_value($params) {
        $this->load->model('document/document');
        $this->load->model('doctype/doctype');
        if (!empty($params['method_params']['another_field_uid'])) {
            $field_info = $this->model_doctype_doctype->getField($params['field_uid']);
            $values = explode(",", $this->model_document_document->getFieldValue($params['field_uid'], $params['document_uid']));
            $result = array();
            foreach ($values as $document_uid) {
                $result[] = $this->model_document_document->getFieldValue($params['method_params']['another_field_uid'], $document_uid);
            }
            return implode($field_info['params']['delimiter'], $result);
        }                
    }

    public function get_another_field_display($params) {
        $this->load->model('document/document');
        $this->load->model('doctype/doctype');
        if (!empty($params['method_params']['another_field_uid'])) {
            $field_info = $this->model_doctype_doctype->getField($params['field_uid']);
            $values = explode(",", $this->model_document_document->getFieldValue($params['field_uid'], $params['document_uid']));
            $result = array();
            foreach ($values as $document_uid) {
                $result[] = $this->model_document_document->getFieldDisplay($params['method_params']['another_field_uid'], $document_uid);
            }
            return implode($field_info['params']['delimiter'], $result);
        }                
    }

    //сеттеры
    public function append_link($params) {
        $this->load->model('document/document');
        $val = $this->model_document_document->getFieldValue($params['field_uid'], $params['document_uid']);
        $val = $val . "," .$params['method_params']['standard_setter_param'];
        $val = implode(",", array_unique(explode(",", $val)));
        return $this->model_document_document->editFieldValue($params['field_uid'], $params['document_uid'], $val);
    }

    public function remove_link($params) {
        $this->load->model('document/document');
        $val = explode(",", $this->model_document_document->getFieldValue($params['field_uid'], $params['document_uid']));
        $rmval = explode(",", $params['method_params']['standard_setter_param']);
        $newval = implode(",", array_unique(array_diff($val, $rmval)));
        return $this->model_document_document->editFieldValue($params['field_uid'], $params['document_uid'], $newval);
    }

}
