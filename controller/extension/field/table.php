<?php
/**
 * @package		Documentov
 * @author		Romav V Zhukov
 * @copyright           Copyright (c) 2018 Andrey V Surov, Roman V Zhukov (https://www.documentov.com/)
 * @license		https://opensource.org/licenses/mit-license.php
 * @link		https://www.documentov.com
*/
class ControllerExtensionFieldTable extends FieldController {

    const FIELD_INFO = array(
        'methods' => array(
//            array('type' => 'getter', 'name' => 'gets_row_count'),
//            array('type' => 'getter', 'name' => 'get_cell_value', 'params' => array('row', 'col')),
//            array('type' => 'getter', 'name' => 'find_cell_values', 'params' => array('find_col', 'val', 'result_col')),
//            array('type' => 'setter', 'name' => 'set_cell_value', 'params' => array('row', 'col'))
        ),
        'work_progress_log' => true,
        'compound' => true,
    );

    public function index() {
    }

    public function setting() {
        $this->load->language('extension/field/table');
        $data['cancel'] = $this->url->link('marketplace/extension', 'type=field', true);
        $this->response->setOutput($this->load->view('extension/field/table', $data));
    }

    public function install() {
        $this->load->model('extension/field/table');
        $this->model_extension_field_table->install();
    }

    public function uninstall() {
        $this->load->model('extension/field/table');
        $this->model_extension_field_table->uninstall();

    }

    /**
     * Возвращает неизменяемую информацию о поле
     * @return array()
     */
    public function getFieldInfo() {
        return ControllerExtensionFieldTable::FIELD_INFO;
    }

    /**
     * Метод возвращает название поля в соответствии с выбранным языком
     * @return type
     */
    public function getTitle() {

        $this->language->load('extension/field/table');
        return $this->language->get('heading_title');
    }

    /**
     * Метод возвращает описание параметров поля
     */
    public function getDescriptionParams($params) {
        $language_id = $this->config->get('config_language_id');
        $result = array();
        if (!empty($params['inner_fields'])) {
            $inner_fields = "";
            if (is_array($params['inner_fields'])) {
                $inner_fields = $params['inner_fields'];
            } else {
                $inner_fields = json_decode(htmlspecialchars_decode($params['inner_fields']), true);
            }

            foreach ($inner_fields as $ifld) {
                $result[] = '"' . $ifld['params']['column_title'][$language_id] . '" (' . $this->load->controller('extension/field/' . $ifld['field_type'] . "/getTitle") . ')';
            }
        }
        return implode("; ", $result);
    }

    /**
     * Метод возвращает методы поля (геттеры или сеттеры)
     */

    //Метод возвращает форму настройки параметров метода
    public function getFieldMethodForm($data) {
        $this->language->load('extension/field/table');
        switch ($data['method']) {
            case 'get_row_count':
                return '';
            case 'get_cell_value':
            case 'set_cell_value':
                return $this->load->view('field/table/method_celladdress_form', $data);
            case "find_cell_values":
                return $this->load->view('field/table/method_find_form', $data);
            default:
                return '';
        }
    }


    public function getAdminForm($data) {

        $ifld_descriptions = array();
        if (!empty($data['params']) && !empty($data['params']['inner_fields'])) {
            foreach ($data['params']['inner_fields'] as $ifld) {
                $ifld_descriptions[] = array(
                    'name' => $this->load->controller('extension/field/' . $ifld['field_type'] . "/getTitle")
                );
            }
            $data['params']['inner_fields'] = json_encode($data['params']['inner_fields']);
            $data['inner_fields_descriptions'] = json_encode($ifld_descriptions);
        }
        $data['language_id'] = $this->config->get('config_language_id');
        return $this->load->view($this->config->get('config_theme') . '/template/field/table/table_form', $data);
    }

    /**
     * Возвращает виджет поля для режима создания / редактирования поля
     *  $data = $field['params'], 'field_uid', 'document_uid'
     */
    public function getForm($data) {
        //$this->load->model('doctype/doctype');
        $language_id = $this->config->get('config_language_id');
        $data['language_id'] = $language_id;
        $table_view = array();
        if (isset($data['field_value']) && empty($data['filter_form'])) { //filter_form - запрос формы для фильтра журнала, это форма из простого input
            if (is_array($data['field_value'])) {
                $table_data = $data['field_value'];
                $data['field_value'] = json_encode($table_data);
            } else {
                $table_data = json_decode(htmlspecialchars_decode($data['field_value']), true);
            }        
            if (is_array($table_data)) {
                foreach ($table_data as $key => $row_data) {
                    $table_view[$key] = $this->getTableRowView($row_data, $data['inner_fields']);
                }                
            }
        }     
        
        $data['table_view'] = $table_view; 
        return $this->load->view('field/table/table_widget_form', $data);
    }

    public function getTableRowView($row_data, $inner_fields_info) {
        $row_view = '';
        foreach ($inner_fields_info as $key => $field_info) {
            $field_type = $field_info['field_type'];
            $field_param = $field_info['params'];
            $inner_field_uid = $field_info['inner_field_uid'];
            
            
            $row_view .= '<td>';
            if (isset($row_data[$inner_field_uid])) {
                $this->load->model('extension/field/' . $field_type);
                $model = "model_extension_field_" . $field_type;
                if ($row_data[$inner_field_uid]) {
                    $cell_value = $this->$model->getValue('', 0, $row_data[$inner_field_uid], $field_info);
                } else {
                    $cell_value = '';
                }
                $field_param['field_value'] = $cell_value;
            } 
            $field_param['document_uid'] = $this->request->get['document_uid'] ?? 0;
            $row_view .= $this->load->controller('extension/field/' . $field_type . '/getView', $field_param) . '</td>';
        }
        return $row_view;
    }

    //возвращает проверенные данные строки таблицы и ее представление
    //метод вынесен в field/table.php => убрать 
//    public function getTableRow() {
//        $this->load->model('doctype/doctype');
//        $this->load->language('extension/field/table');
//        $field_uid = $this->request->get['field_uid'];
//        $row_data = $this->request->post['field'];
//        $inner_fields_info = $this->model_doctype_doctype->getField($field_uid)['params']['inner_fields'];
//        $cnt = 0;
//        $row_values = array();
//        $data = array('rows' => array());
//
//        foreach ($inner_fields_info as $key => $field_info) {
//            $field_type = $field_info['field_type'];
//            $field_param = $field_info['params'];
//            $inner_field_uid = $field_info['inner_field_uid'];
//            $field_name = 'inner' . $inner_field_uid . $field_type;
//
//            if (isset($row_data[$field_name])) {
//                $this->load->model('extension/field/' . $field_type);
//                $model = "model_extension_field_" . $field_type;
//                if ($row_data[$field_name]) {
//                    $cell_value = $this->$model->getValue('', 0, $row_data[$field_name], $field_info);
//                } else {
//                    $cell_value = '';
//                }
//                $row_values[$inner_field_uid] = $cell_value;
//                $field_param['field_value'] = $cell_value;
//            } else {
//                $row_values[$inner_field_uid] = '';
//            }
//
//            $cnt++;
//            $field_param['document_uid'] = $this->request->get['document_uid'] ?? 0;
//            $data['rows'][] = $this->load->controller('extension/field/' . $field_type . '/getView', $field_param) . '</td>';
//        }
//        $this->response->addHeader('Content-type: application/json');
//        $this->response->setOutput(json_encode(array('row_values' => $row_values, 'row_views' => $this->load->view('field/table/table_widget_row', $data))));
//
//    }

    //Возвращает форму для редактирования строки таблицы
    public function getTableRowForm() {
        $this->load->model('doctype/doctype');
        $this->load->language('extension/field/table');
        $data = array();
        $field_uid = $this->request->get['field_uid'];
        //$data['doctype_uid'] = $doctype_uid;
        $data['languages'] = $this->model_localisation_language->getLanguages();
        $inner_fields = $this->model_doctype_doctype->getField($field_uid)['params']['inner_fields'];

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            // редактирование вложенных полей
            $row_data = json_decode(htmlspecialchars_decode($this->request->post['row_data']), true);
            foreach ($inner_fields as $key => $field) {

                $field_type = $field['field_type'];
                $field_param = $field['params'];
                $inner_field_uid = $field['inner_field_uid'];
                $field_param['field_uid'] = 'inner' . $inner_field_uid . $field_type;

                $this->load->model('extension/field/' . $field_type);
                $model = "model_extension_field_" . $field_type;
                if ($row_data[$inner_field_uid]) {
                    $field_param['field_value'] = $this->$model->getValue('', 0, $row_data[$inner_field_uid], $field);
                } else {
                    $field_param['field_value'] = '';
                }
                $inner_fields[$key]['field_form'] = $this->load->controller('extension/field/' . $field_type . '/getForm', $field_param);
            }
            $data['language_id'] = $this->config->get('config_language_id');
            $data['inner_fields'] = $inner_fields;
            $this->response->setOutput($this->load->view('field/table/table_row_widget_form', $data));
            return;
        } else {
            // добавление вложенных полей
            $cnt = 0;
            foreach ($inner_fields as $key => $field) {
                $field_type = $field['field_type'];
                $field_param = $field['params'];
                $inner_field_uid = $field['inner_field_uid'];
                $field_param['field_uid'] = 'inner' . $inner_field_uid . $field_type;
                $inner_fields[$key]['field_form'] = $this->load->controller('extension/field/' . $field_type . '/getForm', $field_param);
                $cnt++;
            }
            $data['language_id'] = $this->config->get('config_language_id');
            $data['inner_fields'] = $inner_fields;
            $this->response->setOutput($this->load->view('field/table/table_row_widget_form', $data));
            return;
        }

        //$this->response->setOutput($this->load->view('field/table/table_row_widget_form', $data));
    }

    /**
     * Возвращает  поле для режима просмотра
     */
    public function getView($data) {
        $language_id = $this->config->get('config_language_id');
        $data['language_id'] = $language_id;
        $table_view = array();
        if (!empty($data['field_value']) && !isset($data['filter_view'])) { //наличие значения, вызов не из фильтров админки журнала (filter_view)
            $table_data = json_decode(htmlspecialchars_decode($data['field_value']), true);
            foreach ($table_data as $key=>$row_data) {
                $table_view[$key] = $this->getTableRowView($row_data, $data['inner_fields']);
            }
        }
        
        $data['table_view'] = $table_view; 
        return $this->load->view('field/table/table_widget_view', $data);
    }

    //получить форму для вложенных полей
    public function getInnerFieldForm() {
        $this->load->model('doctype/doctype');
        $this->load->language('extension/field/table');
        $this->load->language('doctype/doctype');
        $doctype_uid = $this->request->get['doctype_uid'];
        $data = array();
        //$avaliable_fields = $this->model_doctype_doctype->getFields(array('doctype_uid' => $doctype_uid, 'setting' => 0));

        $avaliable_field_types = $this->load->controller('doctype/doctype/getFieldtypes');
        //убираем из списка составные поля
        $param = 'compound';
        foreach ($avaliable_field_types as $key => $field_type) {
            $field_info = $this->load->controller('extension/field/' . $field_type['name'] . '/getFieldInfo');
            if (isset($field_info[$param]) && $field_info[$param] === true) {
                unset($avaliable_field_types[$key]);
            }
        }
        $data['inner_fields'] = $avaliable_field_types;
        $data['doctype_uid'] = $doctype_uid;
        $data['languages'] = $this->model_localisation_language->getLanguages();

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            // редактирование вложенного поля
            $params = $this->request->post['params'];
            $field_type = $this->request->post['field_type'];
            $field_data = array();
            $field_data['params'] = $params;
            $field_data['doctype_uid'] = $doctype_uid;
            $data['inner_field_form'] = $this->load->controller('extension/field/' . $field_type . '/getAdminForm', $field_data);
            $data['inner_field_type'] = $field_type;
            $data['params'] = array('column_title' => $params['column_title']);
            $this->response->setOutput($this->load->view('field/table/table_inner_field_form', $data));
            return;
        } else {
            // добавление вложенного поля (после выбора типа поля подгружаем форму поля)
            if (isset($this->request->get['field_type'])) {
                $field_type = $this->request->get['field_type'];
                $this->response->setOutput($this->load->controller('extension/field/' . $field_type . '/getAdminForm', $data));
                return;
            }
        }
        // добавление вложенного поля, тип поля еще не выбран

        $this->response->setOutput($this->load->view('field/table/table_inner_field_form', $data));
    }

    public function getInnerFieldDescription() {
        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            $field_type = $this->request->post['field_type'];
            $if_description = array('name' => $this->load->controller('extension/field/' . $field_type . "/getTitle"));
            $this->response->setOutput(json_encode($if_description));
        }
    }

    //Поле является составным
    public function isCompound() {
        return true;
    }

    public function setParams($data) {
        if (!empty($data['inner_fields'])) {
            $data['inner_fields'] = json_decode(htmlspecialchars_decode($data['inner_fields']), true);
        }
        return $data;
    }
}
