<?php
/**
 * @package		Documentov
 * @author		Andrey V Surov
 * @copyright           Copyright (c) 2018 Andrey V Surov, Roman V Zhukov (https://www.documentov.com/)
 * @license		https://opensource.org/licenses/mit-license.php
 * @link		https://www.documentov.com
*/
class ControllerExtensionFieldString extends FieldController {
    const FIELD_INFO = array(
        'methods' => array(
            array('type' => 'getter', 'name' => 'get_first_chars', 'params'         => array('count')),
            array('type' => 'setter', 'name' => 'append_text','params'              => array('standard_setter_param')),
            array('type' => 'setter', 'name' => 'insert_text','params'              => array('standard_setter_param')),
            array('type' => 'setter', 'name' => 'append_text_separator', 'params'   => array('standard_setter_param', 'separator'))

        )
    );
    
    public function setting() {
        $this->load->language('extension/field/string');
        $data['cancel'] = $this->url->link('marketplace/extension', 'type=field', true);
        $this->response->setOutput($this->load->view('extension/field/string', $data));
    }
    
    public function index() {}

    public function install() {
        $this->load->model('extension/field/string');
        $this->model_extension_field_string->install(); 
    }
    
    public function uninstall() {
        $this->load->model('extension/field/string');
        $this->model_extension_field_string->uninstall();  
    }   
    
    /**
     * Метод возвращает название поля в соответствии с выбранным языком
     * @return type
     */
    public function getTitle() {
        
        $this->language->load('extension/field/string');
        return $this->language->get('heading_title');
    }
    
    /**
     * Метод возвращает описание параметров поля
     */
    public function getDescriptionParams($params) {
//        $params = unserialize($params);
        $result = array();
        if(!empty($params['mask'])) {
            $result[] = sprintf($this->language->get('text_description_mask'), $params['mask']);
        }
        if(!empty($params['default'])) {
            $result[] = sprintf($this->language->get('text_description_default'), $params['default']);
        }
        
        return implode("; ", $result);
    }
    
    
    /**
     * Возвращает форму поля для настройки администратором
     * @param type $data
     */
    public function getAdminForm($data) {
        return $this->load->view($this->config->get('config_theme') . '/template/field/string/string_form', $data);
    }
    
    /**
     * Возвращает виджет поля для режима создания / редактирования поля
     *  $data = $field['params'], 'field_uid', 'document_uid'
     */
    public function getForm($data) {    
        if (!empty($data['field_value'])) {
            if (is_array($data['field_value'])) {
                $data['field_value'] = json_encode($data['field_value']); //если в строку пытаются записать, скажем, таблицу
            }
            $data['field_value'] = str_replace("&amp;","&",htmlentities($data['field_value'])); //если в поле записать, скажем, гиперссылку, то при отображении ссылка будет интерпретирована htmlentities 
        }
        return $this->load->view('field/string/string_widget_form', $data);
    }
     /**
     * Возвращает  поле для режима просмотра
     */
    public function getView($data) {      
        $this->load->model('extension/field/string');      
        return $this->load->view('field/string/string_widget_view', $data);
    }    

    //Метод возвращает форму настройки параметров метода
    public function getFieldMethodForm($data) {
        $this->language->load('extension/field/string');
        switch ($data['method_name']) {
            case "get_first_chars":
                return $this->load->view('field/string/method_one_int_param_form', $data);
            case "append_text_separator":
                return $this->load->view('field/string/method_append_text_separator_form', $data);                   
            default:
                return '';
        }
    }    
    
        //геттеры
    public function get_first_chars($params) {
        $this->load->model('document/document');
        $this->load->model('doctype/doctype');
        $val = $this->model_document_document->getFieldValue($params['field_uid'], $params['document_uid']);
        if (!empty($params['method_params']['char_count'])) {
            $count = $params['method_params']['char_count'];
            if (intval($count) === 0) {
                $count = mb_strlen($val);
            } else {
                $count = intval($count);
            }
        } else {
            $count = mb_strlen($val);
        }
        $val = mb_substr($val, 0, $count);
        return $val;
    }

    //cеттеры
    public function append_text($params) {
        $this->load->model('document/document');
        $this->load->model('doctype/doctype');
        $val = $this->model_document_document->getFieldValue($params['field_uid'], $params['document_uid']);
        $val = $val .  $params['method_params']['standard_setter_param'];
        return $this->model_document_document->editFieldValue($params['field_uid'], $params['document_uid'], $val);
    }
    
    public function append_text_separator($params) {
        $this->load->model('document/document');
        $this->load->model('doctype/doctype');
        $val = $this->model_document_document->getFieldValue($params['field_uid'], $params['document_uid']);
        $val = $val . ($params['method_params']['separator'] ?? " ") . $params['method_params']['standard_setter_param'];        
        return $this->model_document_document->editFieldValue($params['field_uid'], $params['document_uid'], $val);
    }
    
    public function insert_text($params) {
        $this->load->model('document/document');
        $this->load->model('doctype/doctype');
        $val = $this->model_document_document->getFieldValue($params['field_uid'], $params['document_uid']);
        $val =  $params['method_params']['standard_setter_param'] . $val;        
        return $this->model_document_document->editFieldValue($params['field_uid'], $params['document_uid'], $val);
    }    

    
}
