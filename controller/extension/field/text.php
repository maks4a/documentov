<?php
/**
 * @package		Documentov
 * @author		Roman V Zhukov
 * @copyright           Copyright (c) 2018 Andrey V Surov, Roman V Zhukov (https://www.documentov.com/)
 * @license		https://opensource.org/licenses/mit-license.php
 * @link		https://www.documentov.com
*/

class ControllerExtensionFieldText extends FieldController {

    const FIELD_INFO = array(
        'methods' => array(
            array('type' => 'getter', 'name' => 'get_first_chars', 'params' => array('count')),
            array('type' => 'setter', 'name' => 'append_text', 'params' => array('standard_setter_param')),
            array('type' => 'setter', 'name' => 'append_text_new_line', 'params' => array('standard_setter_param')),
            array('type' => 'setter', 'name' => 'insert_text', 'params' => array('standard_setter_param')),
            array('type' => 'setter', 'name' => 'insert_text_new_line', 'params' => array('standard_setter_param')),
            array('type' => 'setter', 'name' => 'append_text_separator', 'params' => array('standard_setter_param', 'separator'))
        ),
        'work_progress_log' => true,
    );

    public function setting() {
        $this->load->language('extension/field/text');

        $data['cancel'] = $this->url->link('marketplace/extension', 'type=field', true);

        $this->response->setOutput($this->load->view('extension/field/text', $data));
    }

    public function index() {
    }

    public function install() {
        $this->load->model('extension/field/text');
        $this->model_extension_field_text->install();
    }

    public function uninstall() {
        $this->load->model('extension/field/text');
        $this->model_extension_field_text->uninstall();
    }

    /**
     * Метод возвращает название поля в соответствии с выбранным языком
     * @return type
     */
    public function getTitle() {

        $this->language->load('extension/field/text');
        return $this->language->get('heading_title');
    }

    /**
     * Метод возвращает описание параметров поля
     */
    public function getDescriptionParams($params) {
        $result = array();
        if (!empty($params['default'])) {
            $result[] = sprintf($this->language->get('text_ field_description_default'));
        }
        if (!empty($params['editor_enabled'])) {
            $result[] = sprintf($this->language->get('text_description_editor_enabled'), $params['editor_enabled']);
        }
        return implode("; ", $result);
    }
   
    //Метод возвращает форму настройки параметров метода
    public function getFieldMethodForm($data) {
        //print_r($data);exit;
        $info = $this->language->load('extension/field/text');
        switch ($data['method_name']) {
            case "get_first_chars":
                return $this->load->view('field/text/method_one_int_param_form', $data);
            case "append_text_separator":
                return $this->load->view('field/text/method_append_text_separator_form', $data);            
            default:
                return '';
        }
    }


    /**
     * Возвращает форму поля для настройки администратором
     * @param type $data
     */
    public function getAdminForm($data) {
        return $this->load->view($this->config->get('config_theme') . '/template/field/text/text_form', $data);
    }

    /**
     * Возвращает виджет поля для режима создания / редактирования поля
     *  $data = $field['params'], 'field_uid', 'document_uid'
     */
    public function getForm($data) {
        if (isset($data['field_value'])) {
            $val = html_entity_decode($data['field_value']);
            $data['value'] = $val;
        }
        return $this->load->view('field/text/text_widget_form', $data);
    }

    /**
     * Возвращает  поле для режима просмотра
     */
    public function getView($data) {
        $this->load->model('extension/field/text');
        if (isset($data['field_value'])) {
            $val = $data['field_value'];
            if (!empty($data['editor_enabled']) && strcmp($data['editor_enabled'], "true") === 0) {
                $data['field_value'] = htmlspecialchars_decode($val);
            } else {
                $data['field_value'] = $val;
            }
        }
        return $this->load->view('field/text/text_widget_view', $data);
    }

    //геттеры
    public function get_first_chars($params) {
        $this->load->model('document/document');
        $row_value = $this->model_document_document->getFieldValue($params['field_uid'], $params['document_uid']);
        $val = strip_tags(htmlspecialchars_decode($row_value));
        if (!empty($params['method_params']['char_count'])) {
            $count = $params['method_params']['char_count'];
            if (intval($count) === 0) {
                $count = mb_strlen($val);
            } else {
                $count = intval($count);
            }
        } else {
            $count = mb_strlen($val);
        }
        $val = mb_substr($val, 0, $count);
        return $val;
    }

    //cеттеры
    public function append_text($params) {
        $this->load->model('document/document');
        $this->load->model('doctype/doctype');
        $val = $this->model_document_document->getFieldValue($params['field_uid'], $params['document_uid']);
        $val = $val . $params['method_params']['standard_setter_param'];
        return $this->model_document_document->editFieldValue($params['field_uid'], $params['document_uid'], $val);
    }
    
    public function append_text_new_line($params) {
        $this->load->model('document/document');
        $this->load->model('doctype/doctype');
        $val = $this->model_document_document->getFieldValue($params['field_uid'], $params['document_uid']);
        $field_info = $this->model_doctype_doctype->getField($params['field_uid']);
        if (!empty($field_info['params']['editor_enabled'])) {
            $val = $val . "<br>\r\n" . $params['method_params']['standard_setter_param'];; 
        } else {
            $val = $val . "\r\n" . $params['method_params']['standard_setter_param'];; 
        }
               
        return $this->model_document_document->editFieldValue($params['field_uid'], $params['document_uid'], $val);
    }
    
    public function append_text_separator($params) {
        $this->load->model('document/document');
        $this->load->model('doctype/doctype');
        $val = $this->model_document_document->getFieldValue($params['field_uid'], $params['document_uid']);
        $val = $val . ($params['method_params']['separator'] ?? " ") . $params['method_params']['standard_setter_param'];        
        return $this->model_document_document->editFieldValue($params['field_uid'], $params['document_uid'], $val);
    }

    public function insert_text($params) {
        $this->load->model('document/document');
        $this->load->model('doctype/doctype');
        $val = $this->model_document_document->getFieldValue($params['field_uid'], $params['document_uid']);
        $val = $params['method_params']['standard_setter_param'] . $val;        
        return $this->model_document_document->editFieldValue($params['field_uid'], $params['document_uid'], $val);
    }
    
    public function insert_text_new_line($params) {
        $this->load->model('document/document');
        $this->load->model('doctype/doctype');
        $val = $this->model_document_document->getFieldValue($params['field_uid'], $params['document_uid']);
        $field_info = $this->model_doctype_doctype->getField($params['field_uid']);
        if (!empty($field_info['params']['editor_enabled'])) {
            $val = $params['method_params']['standard_setter_param'] . "<br>\r\n" . $val; 
        } else {
            $val = $params['method_params']['standard_setter_param'] . "\r\n" . $val; 
        }               
        return $this->model_document_document->editFieldValue($params['field_uid'], $params['document_uid'], $val);
    }

}
