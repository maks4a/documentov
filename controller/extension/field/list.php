<?php
/**
 * @package		Documentov
 * @author		Andrey V Surov
 * @copyright           Copyright (c) 2018 Andrey V Surov, Roman V Zhukov (https://www.documentov.com/)
 * @license		https://opensource.org/licenses/mit-license.php
 * @link		https://www.documentov.com
*/
class ControllerExtensionFieldList extends FieldController {

    const FIELD_INFO = array(
        'methods' => array(
        array('type' => 'getter', 'name' => 'get_display_value'),
        array('type' => 'setter', 'name' => 'set_by_title', 'params' => array('standard_setter_param')),    
        )
    );
    
    /**
     * Настройки поля в Модулях
     */
    public function setting() {
        $this->load->language('extension/field/list');
        $data['cancel'] = $this->url->link('marketplace/extension', 'type=field', true);
        $this->response->setOutput($this->load->view('extension/field/list', $data));
    }

    public function index() {
    }

    public function install() {
        $this->load->model('extension/field/list');
        $this->model_extension_field_list->install();
    }

    public function uninstall() {
        $this->load->model('extension/field/list');
        $this->model_extension_field_list->uninstall();
    }

    /**
    * Возвращает неизменяемую информацию о поле
    * @return array()
    */
    public function getFieldInfo() {
        return ControllerExtensionFieldList::FIELD_INFO;
    }
    
    /**
     * Метод возвращает название поля в соответствии с выбранным языком
     * @return type
     */
    public function getTitle() {
        $this->language->load('extension/field/list');
        return $this->language->get('heading_title');
    }

    /**
     * Метод возвращает описание параметров поля
     */
    public function getDescriptionParams($params) {        
        $this->load->language('extension/field/list');
        if (empty($params['source_type']) || $params['source_type'] == 'table') {
            if (!empty($params['values'])) {
                $values = array();
                foreach ($params['values'] as $value) {
                    if (!empty($value['title'])) {
                        $values[] = $value['title'];
                    }
                }
                return $this->language->get('description_list') . ": " . implode(", ", $values);
            } else {
                return $this->language->get('description_list_empty');
            }            
        } else {
            if (!empty($params['source_field_uid'])) {
                $this->load->model('doctype/doctype');
                $source_field_info = $this->model_doctype_doctype->getField($params['source_field_uid'], 1);
                return sprintf($this->language->get('description_list_field'), $source_field_info['name'] ?? "", $params['separator_values'] ?? "||", $params['separator_value_title'] ?? "::");
            } else {
                return $this->language->get('description_list_field_empty');
            }
        }
        
    }
    
    /**
     * Возвращает форму поля для настройки администратором
     * @param type $data
     */
    public function getAdminForm($data) {
        $this->load->model('localisation/language');
        $this->load->model('doctype/doctype');
        if (!empty($data['params']['source_type']) && $data['params']['source_type'] == "field") {
            if (!empty($data['params']['source_field_uid'])) {
                $source_field_info = $this->model_doctype_doctype->getField($data['params']['source_field_uid'], 1);
                $data['source_field_name'] = $source_field_info['name'] ?? "";
            } 
        } else {
            //в зависимости от типа выбора (множественный или одиночный) значение по умолчанию может быть массивом или строкой
            if (isset($data['params']['default_value']) && isset($data['params']['values'])) {
                if (is_array($data['params']['default_value'])) {
                    $default_values = $data['params']['default_value'];
                } else {
                    $default_values = array($data['params']['default_value']);
                }
                foreach ($data['params']['values'] as &$value) {
                    if (array_search($value['value'], $default_values) !== FALSE) {
                        $value['checked'] = 1;
                    }
                }
            }
            
        }
        return $this->load->view($this->config->get('config_theme') . '/template/field/list/list_form', $data);
    }

    /**
     * Возвращает виджет поля для режима создания / редактирования поля
     *  $data = $field['params'], 'field_uid', 'document_uid'
     */
    public function getForm($data) { 
        if(isset($data['field_value'])) {
            $value = $data['field_value'];
            $data['field_value'] = array();
            if (!is_array($value)) {
                $value = explode(',', $value);
            }
            foreach ($value as $val) {
                $data['field_value'][$val] = '1';
            }
        } 
        if (!empty($data['source_type']) && $data['source_type'] == "field") {
            //значения для списка получаем из поля
            if (!empty($data['source_field_uid'])) {
                if (!empty($data['filter_form'])) {
                    //фильтр журнала, документ-июда нет, а данные нужно получать из поля, возможно НЕнастроечного. Проверяем
                    $this->load->model('doctype/doctype');
                    $this->load->model('document/document');
                    $source_field_info = $this->model_doctype_doctype->getField($data['source_field_uid']);
                    if ($source_field_info['setting']) {
                        //повезло, поле настроечное
                        $data['values'] = $this->getValuesFromField($data);
                    } else {
                        //не повезло - поле-источник обычное
                        //получаем все уникальные пары value-display данного поля списка, чтобы показать их в фильтре  
                        $result = array();
                        $data['values'] = array();
                        foreach ($this->model_document_document->getFieldValues($data['field_uid']) as $val) {
                            if ($val['value']) {
                                if(isset($result[$val['value']])) {
                                    if (array_search($val['display_value'], $result[$val['value']]) === false) {
                                        $result[$val['value']][] = $val['display_value'];
                                    }
                                } else {
                                    $result[$val['value']][] = $val['display_value'];
                                }
                            }
                        }
                        foreach ($result as $value => $titles) {
                            $data['values'][] = array(
                                'value' => $value,
                                'title' => implode("/",$titles)
                            );
                        }                        
                    }
                } else {
                    $data['values'] = $this->getValuesFromField($data);
                }
                
            }
        } else {
            //значения для списка введены в таблице
            if (!isset($data['field_value']) || $data['field_value'] == "") {
                //документ создается (а не редактируется), устанавливаем значение по умолчанию
                $data['field_value'] = array();
                if (isset($data['default_value']) && isset($data['values'])) {
                    if (is_array($data['default_value'])) {
                        $default_values = $data['default_value'];
                    } else {
                        $default_values = array($data['default_value']);
                    }
                    foreach ($data['values'] as &$value) {
                        if (array_search($value['value'], $default_values) !== FALSE) {
                            $data['field_value'][$value['value']] = 1;
                        }
                    }
                }            
            }               
        }

        return $this->load->view('field/list/list_widget_form', $data);
    }

    /**
     * Возвращает  поле для режима просмотра
     */
    public function getView($data) {
        $this->load->model('document/document');
        $this->load->model('doctype/doctype');
        //$field_info = $this->model_doctype_doctype->getField($data['field_uid']);
        if (isset($data['field_value'])) {
            $values = explode(',',$data['field_value']);     
            $displays = array();
            if (!empty($data['source_type']) && $data['source_type'] == "field") {
                //значения для списка получаем из поля
                if (!empty($data['source_field_uid'])) {
                    foreach ($this->getValuesFromField($data) as $value) {
                        foreach ($values as $value_f) {
                            if ($value['value'] == $value_f) {
                             $displays[] = $value['title'];   
                            }
                        }
                    }
                }
            } else {
                foreach ($values as $value) {
                    if (isset($data['values'][$value]['title'])) {
                        $displays[] = $data['values'][$value]['title'];
                    }
                }                    
            }            
            
            $data['text'] = implode(",", $displays);
        }
        
        
        return $this->load->view('field/list/list_widget_view', $data);
    }
    
    public function getValuesFromField($data) {
        $this->load->model('document/document');
        $source_field_value = $this->model_document_document->getFieldValue($data['source_field_uid'], $data['document_uid']);
        $values = explode($data['separator_values'] ?? "||", $source_field_value);
        $result = array();
        if ($values) {
            foreach ($values as $val) {
                $value_title = explode($data['separator_value_title'] ?? "::", $val);
                if(!empty($value_title[0])) {
                    $result[] = array(
                        'value'  => $value_title[0],
                        'title'  => $value_title[1] ?? ""
                    );
                }
            }
        } 
        return $result;
    }
  

    //Метод возвращает форму настройки параметров метода
    public function getFieldMethodForm($data) {
        $this->language->load('extension/field/file');
        switch ($data['method_name']) {
            case "get_display_value":
            default:
                return '';
        }
    }
    
    public function get_display_value($params) {
        $this->load->model('document/document');
        $value = explode(",", $this->model_document_document->getFieldDisplay($params['field_uid'], $params['document_uid']))[0];
        return $value;    
    }
    
    public function set_by_title($params) {
        $this->load->model('document/document');
        $this->load->model('doctype/doctype');
        $field_info = $this->model_doctype_doctype->getField($params['field_uid']);
        $result = array();
        foreach (explode(",", $params['method_params']['standard_setter_param']) as $value) {
            foreach ($field_info['params']['values'] as $list_value) {
                if ($value == $list_value['title']) {
                    $result[] = $list_value['value'];
                }
            }
        }
        if ($result && $field_info['params']['multi_select']) {
            return $this->model_document_document->editFieldValue($params['field_uid'], $params['document_uid'], implode(",",$result));
        } elseif ($result && !$field_info['params']['multi_select']) {
            return $this->model_document_document->editFieldValue($params['field_uid'], $params['document_uid'], $result[0]);
        } else {
            return $this->model_document_document->editFieldValue($params['field_uid'], $params['document_uid'], "");
        }
    }
}
