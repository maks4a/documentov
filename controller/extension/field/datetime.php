<?php

/**
 * @package		Documentov
 * @author		Roman V Zhukov
 * @copyright           Copyright (c) 2018 Andrey V Surov, Roman V Zhukov (https://www.documentov.com/)
 * @license		https://opensource.org/licenses/mit-license.php
 * @link		https://www.documentov.com
*/

class ControllerExtensionFieldDateTime extends FieldController {

    private $timezone = null;
    private $dbformat = null;

    const FIELD_INFO = array(
        'methods' => array(
            array('type' => 'getter', 'name' => 'get_display_value'),
            array('type' => 'getter', 'name' => 'get_year'),
            array('type' => 'getter', 'name' => 'get_month'),
            array('type' => 'getter', 'name' => 'get_day'),
            array('type' => 'getter', 'name' => 'get_hour'),
            array('type' => 'getter', 'name' => 'get_min'),
            array('type' => 'getter', 'name' => 'get_sec'),
            array('type' => 'getter', 'name' => 'get_number_day'),
            array('type' => 'getter', 'name' => 'get_number_week'),
            array('type' => 'getter', 'name' => 'get_difference', 'params' => array('other_datetime_value')),
            array('type' => 'getter', 'name' => 'get_difference_h', 'params' => array('other_datetime_value')),
            array('type' => 'getter', 'name' => 'get_difference_m', 'params' => array('other_datetime_value')),
            array('type' => 'setter', 'name' => 'adjust_year_plus', 'params' => array('standard_setter_param')),
            array('type' => 'setter', 'name' => 'adjust_month_plus', 'params' => array('standard_setter_param')),
            array('type' => 'setter', 'name' => 'adjust_date_plus', 'params' => array('standard_setter_param')),
            array('type' => 'setter', 'name' => 'adjust_hour_plus', 'params' => array('standard_setter_param')),
            array('type' => 'setter', 'name' => 'adjust_minute_plus', 'params' => array('standard_setter_param')),
            array('type' => 'setter', 'name' => 'adjust_year_minus', 'params' => array('standard_setter_param')),
            array('type' => 'setter', 'name' => 'adjust_month_minus', 'params' => array('standard_setter_param')),
            array('type' => 'setter', 'name' => 'adjust_date_minus', 'params' => array('standard_setter_param')),
            array('type' => 'setter', 'name' => 'adjust_hour_minus', 'params' => array('standard_setter_param')),
            array('type' => 'setter', 'name' => 'adjust_minute_minus', 'params' => array('standard_setter_param'))
        )
    );

    function __construct($registry) {
        parent::__construct($registry);
        $this->dbformat = 'Y-m-d H:i:s';
        $this->timezone = new DateTimeZone($this->config->get('date.timezone'));
    }

    public function setting() {
        $this->load->language('extension/field/datetime');
        $data['cancel'] = $this->url->link('marketplace/extension', 'type=field', true);
        $this->response->setOutput($this->load->view('extension/field/datetime', $data));
    }

    public function index() {
    }

    public function install() {
        $this->load->model('extension/field/datetime');
        $this->model_extension_field_datetime->install();
    }

    public function uninstall() {
        $this->load->model('extension/field/datetime');
        $this->model_extension_field_datetime->uninstall();
    }

    /**
     * Возвращает неизменяемую информацию о поле
     * @return array()
     */
    public function getFieldInfo() {
        return ControllerExtensionFieldDateTime::FIELD_INFO;
    }

    /**
     * Метод возвращает название поля в соответствии с выбранным языком
     * @return type
     */
    public function getTitle() {

        $this->language->load('extension/field/datetime');
        return $this->language->get('heading_title');
    }

    /**
     * Метод возвращает описание параметров поля
     */
    public function getDescriptionParams($params) {
//        $params = unserialize($params);
        $result = array();
        $replace = array(
            'search'    => array("."," ","/","-",":"),
            'replace'   => array("q","_","v","x","e")
        );
        
        if (!empty($params['format'])) {
            $result[] = sprintf($this->language->get('text_description_format'), $this->language->get('text_format_' . str_replace($replace['search'], $replace['replace'], $params['format'])));
        }
        return implode("; ", $result);
    }

    /**
     * Возвращает форму поля для настройки администратором
     * @param type $data
     */
    public function getAdminForm($data) {
        return $this->load->view($this->config->get('config_theme') . '/template/field/datetime/datetime_form', $data);
    }

    /**
     * Возвращает виджет поля для режима создания / редактирования поля
     *  $data = $field['params'], 'field_uid', 'document_uid'
     */
    public function getForm($data) {
        if (isset($data['field_value'])) {
            if (!empty($data['format'])) {
                $format = $data['format'];
                $date = DateTime::createFromFormat($this->dbformat, $data['field_value'], $this->timezone);
                if ($date) {
                    $data['field_value'] = $date->format($format);
                }
            }
        }
        return $this->load->view('field/datetime/datetime_widget_form', $data);
    }

    /**
     * Возвращает  поле для режима просмотра
     */
    public function getView($data) {
        $this->language->load('extension/field/datetime');
        if (isset($data['field_value'])) {
            if (!empty($data['format'])) {
                $format = $data['format'];
                $date = DateTime::createFromFormat($this->dbformat, $data['field_value'], $this->timezone);
                if ($date) {
                    $data['field_value'] = $date->format($format);
                }
            }
        }
        return $this->load->view('field/datetime/datetime_widget_view', $data);
    }

    //Метод возвращает форму настройки параметров метода
    public function getFieldMethodForm($data) {
        $this->language->load('extension/field/datetime');
        $this->load->model('document/document');
        switch ($data['method_name']) {
            case "get_difference":
                return $this->load->view('field/datetime/method_get_difference_form', $data);
            case "get_difference_h":
                return $this->load->view('field/datetime/method_get_difference_form', $data);
            case "get_difference_m":
                return $this->load->view('field/datetime/method_get_difference_form', $data);
            case "adjust_year_plus":
            case "adjust_month_plus":
            case "adjust_date_plus":
            case "adjust_hour_plus":
            case "adjust_minute_plus":
            case "adjust_year_minus":
            case "adjust_month_minus":
            case "adjust_date_minus":
            case "adjust_hour_minus":
            case "adjust_minute_minus":
                return $this->load->view('field/datetime/method_date_adjustment_form', $data);
            default:
                return '';
        }
    }

    //геттеры
    public function get_difference($params) {
        $this->load->model('document/document');
        $field_value = $this->model_document_document->getFieldValue($params['field_uid'], $params['document_uid']);
        if (!empty($params['method_params']['other_datetime_value'])) {
            $other_field_val = $params['method_params']['other_datetime_value'];
            $timezone = new DateTimeZone($this->config->get('date.timezone'));
            $datetime1 = new DateTime($field_value, $timezone);
            $datetime2 = new DateTime($other_field_val, $timezone);
            $interval = $datetime1->diff($datetime2);
            $res = $interval->days;
        } else {
            $res = 0;
        }
        return $res;
    }
    public function get_difference_h($params) {
        $this->load->model('document/document');
        $field_value = $this->model_document_document->getFieldValue($params['field_uid'], $params['document_uid']);
        if (!empty($params['method_params']['other_datetime_value'])) {
            $other_field_val = $params['method_params']['other_datetime_value'];
            $timezone = new DateTimeZone($this->config->get('date.timezone'));
            $datetime1 = new DateTime($field_value, $timezone);
            $datetime2 = new DateTime($other_field_val, $timezone);
            $interval = $datetime1->diff($datetime2);
            $res = $interval->format("%H");
        } else {
            $res = 0;
        }
        return $res;
    }
    public function get_difference_m($params) {
        $this->load->model('document/document');
        $field_value = $this->model_document_document->getFieldValue($params['field_uid'], $params['document_uid']);
        if (!empty($params['method_params']['other_datetime_value'])) {
            $other_field_val = $params['method_params']['other_datetime_value'];
            $timezone = new DateTimeZone($this->config->get('date.timezone'));
            $datetime1 = new DateTime($field_value, $timezone);
            $datetime2 = new DateTime($other_field_val, $timezone);
            $interval = $datetime1->diff($datetime2);
            $res = $interval->format("%I");
        } else {
            $res = 0;
        }
        return $res;
    }    
    public function get_display_value($params) {
        $this->load->model('document/document');
        $this->load->model('doctype/doctype');
        $field_value = $this->model_document_document->getFieldValue($params['field_uid'], $params['document_uid']);
//        $timezone = new DateTimeZone($this->config->get('date.timezone'));
//        $datetime = new DateTime($field_value, $timezone);
        $field_info = $this->model_doctype_doctype->getField($params['field_uid']);
        
        $date = DateTime::createFromFormat($this->dbformat, $field_value, $this->timezone);
        if ($date) {
            return $date->format($field_info['params']['format']);
        }        
//        return (int) $datetime->format('Y');
    }
    
    public function get_year($params) {
        $this->load->model('document/document');
        $field_value = $this->model_document_document->getFieldValue($params['field_uid'], $params['document_uid']);
        $timezone = new DateTimeZone($this->config->get('date.timezone'));
        $datetime = new DateTime($field_value, $timezone);
        return (int) $datetime->format('Y');
    }

    public function get_month($params) {
        return $this->getDateFormat('m', $params);
    }

    public function get_day($params) {
        return $this->getDateFormat('d', $params);
    }
    
    public function get_hour($params) {
        return $this->getDateFormat('H', $params);
    }
    
    public function get_min($params) {
        return $this->getDateFormat('i', $params);
    }
    
    public function get_sec($params) {
        return $this->getDateFormat('s', $params);
    }
    
    public function get_number_day($params) {
        return (int) $this->getDateFormat('z', $params) + 1;
    }
    
    public function get_number_week($params) {
        return $this->getDateFormat('W', $params);
    }
    
    private function getDateFormat($format, $params) {
        $this->load->model('document/document');
        $field_value = $this->model_document_document->getFieldValue($params['field_uid'], $params['document_uid']);
        $timezone = new DateTimeZone($this->config->get('date.timezone'));
        $datetime = new DateTime($field_value, $timezone);
        return $datetime->format($format);
    }

    //сеттеры

    public function adjust_date_plus($params) {
        $this->calc_date($params['field_uid'], $params['document_uid'], $params['method_params']['standard_setter_param'], "P", "D", "add");
    }
    
    public function adjust_month_plus($params) {
        $this->calc_date($params['field_uid'], $params['document_uid'], $params['method_params']['standard_setter_param'], "P", "M", "add");
    }
    public function adjust_year_plus($params) {
        $this->calc_date($params['field_uid'], $params['document_uid'], $params['method_params']['standard_setter_param'], "P", "Y", "add");
    }
    
    public function adjust_hour_plus($params) {
        $this->calc_date($params['field_uid'], $params['document_uid'], $params['method_params']['standard_setter_param'], "PT", "H", "add");
    }
    
    public function adjust_minute_plus($params) {
        $this->calc_date($params['field_uid'], $params['document_uid'], $params['method_params']['standard_setter_param'], "PT", "M", "add");
    }
    
    public function adjust_date_minus($params) {
        $this->calc_date($params['field_uid'], $params['document_uid'], $params['method_params']['standard_setter_param'], "P", "D", "sub");
    }
    
    public function adjust_month_minus($params) {
        $this->calc_date($params['field_uid'], $params['document_uid'], $params['method_params']['standard_setter_param'], "P", "M", "sub");
    }
    
    public function adjust_year_minus($params) {
        $this->calc_date($params['field_uid'], $params['document_uid'], $params['method_params']['standard_setter_param'], "P", "Y", "sub");
    }

    public function adjust_hour_minus($params) {
        $this->calc_date($params['field_uid'], $params['document_uid'], $params['method_params']['standard_setter_param'], "PT", "H", "sub");
    }    

    public function adjust_minute_minus($params) {
        $this->calc_date($params['field_uid'], $params['document_uid'], $params['method_params']['standard_setter_param'], "PT", "M", "sub");
    }    

    private function calc_date($field_uid, $document_uid, $value, $period, $interval, $operation='add') {
        $this->load->model('document/document');
        $this->load->model('doctype/doctype');
        $field_value = $this->model_document_document->getFieldValue($field_uid, $document_uid);
        $timezone = new DateTimeZone($this->config->get('date.timezone'));
        $datetime = new DateTime($field_value, $timezone);
        if ($operation == "add") {
            $datetime->add(new DateInterval($period . (int) $value . $interval));
        } else {
            $datetime->sub(new DateInterval($period . (int) $value . $interval));
        }

        $field_params = $this->model_doctype_doctype->getField($field_uid, 0);
        if (!empty($field_params['params']['format'])) {
            $format = $field_params['params']['format'];
        } else {
            $format = 'Y-m-d H:i:s';
        }
        $val = $datetime->format($format);
        return $this->model_document_document->editFieldValue($field_uid, $document_uid, $val);        
    }
    
}
