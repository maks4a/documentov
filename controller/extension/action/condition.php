<?php

/**
 * @package		Documentov
 * @author		Roman V Zhukov
 * @copyright           Copyright (c) 2018 Andrey V Surov, Roman V Zhukov (https://www.documentov.com/)
 * @license		https://opensource.org/licenses/mit-license.php
 * @link		https://www.documentov.com
 */
class ControllerExtensionActionCondition extends Controller {

    const ACTION_INFO = array(
        'name' => 'condition',
        'inRouteContext' => true,
        'isCompound' => 'true');

    public function index() {
        $this->load->language('extension/action/condition');

        $data['cancel'] = $this->url->link('marketplace/extension', 'type=action', true);

        $this->response->setOutput($this->load->view('extension/action/condition', $data));
    }

    public function install() {
        
    }

    public function uninstall() {
        
    }

    /**
     * Метод возвращает название действия в соответствии с выбранным языком
     * @return type
     */
    public function getTitle() {

        $this->language->load('extension/action/condition');
        return $this->language->get('heading_title');
    }

    /**
     * Метод возвращает описание действия, исходя из параметров
     */
    public function getDescription($params) {
         $this->load->language('doctype/doctype');
        $this->load->language('action/condition');
        $this->load->model('doctype/doctype');
        $first_field_setting = '';
        $second_field_setting = '';
        $first_field_name = '';
        $second_field_name = '';

        if (!empty($params['first_value_field_uid'])) {
            $first_value_field_description = $this->model_doctype_doctype->getField($params['first_value_field_uid'], 0);
            $first_field_name = '"' . $first_value_field_description['name'] . '"';
        }

        if (!empty($params['second_type_value']) && $params['second_type_value'] == 1 && isset($params['field_widget_value'])) {
            $second_field_name = $params['field_widget_value'];
        } elseif (!empty($params['second_type_value']) && $params['second_type_value'] == 2 && isset($params['second_value_var'])) {
            $second_field_name = $this->language->get('text_' . $params['second_value_var']);
        } else if (!empty($params['second_value_field_uid'])) {
            $second_value_field_description = $this->model_doctype_doctype->getField($params['second_value_field_uid'], 0);
            $second_field_name = '"' . $second_value_field_description['name'] . '"';
        }
        $condition = mb_strtoupper($this->language->get('text_condition_' . $params['comparison_method']));
        $actions_true = '';
        $actions_false = '';
        $actions_true_arr = array();
        if (is_array($params['inner_actions_true'])) {
            $actions_true_arr = $params['inner_actions_true'];
        } else {
            //если в параметрах вложенные действия пререданы в виде строки через post
            $actions_true_arr = json_decode(htmlspecialchars_decode($params['inner_actions_true']), true);
        }
        if ($actions_true_arr) {
            foreach ($actions_true_arr as $action) {
                if (($actions_true) !== '') {
                    $actions_true .= ', ';
                }
                $actions_true .= '"' . $this->load->controller('extension/action/' . $action['action'] . "/getTitle") . '"';
            }
        }
        $actions_false_arr = array();
        if (is_array($params['inner_actions_false'])) {
            $actions_false_arr = $params['inner_actions_false'];
        } else {
            //если в параметрах вложенные действия пререданы в виде строки через post
            $actions_false_arr = json_decode(htmlspecialchars_decode($params['inner_actions_false']), true);
        }
        if ($actions_false_arr) {
            foreach ($actions_false_arr as $action) {
                if (($actions_false) !== '') {
                    $actions_false .= ', ';
                }
                $actions_false .= '"' . $this->load->controller('extension/action/' . $action['action'] . "/getTitle") . '"';
            }
        }

        $description = sprintf($this->language->get('text_condition'), $first_field_name, $condition, $second_field_name) . ' (' . $actions_true . ') ' . $this->language->get('text_condition_else') . ' (' . $actions_false . ')';
        return $description;
    }

    public function getInnerActionDescription() {
        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            $action_name = $this->request->post['action'];
            $action_params = $this->request->post['params'];
            $ia_description = array('name' => $this->load->controller('extension/action/' . $action_name . "/getTitle"),
                'description' => $this->clearString($this->load->controller('extension/action/' . $action_name . "/getDescription", $action_params)));
            $this->response->setOutput(json_encode($ia_description));
        }
    }

    /**
     * Метод возвращает форму действия для типа документа
     * @param type $data - массив, включающий doctype_uid, route_uid
     */
    public function getForm($data) {
        $this->load->model('doctype/doctype');
        if (isset($data['action_id'])) {
            $action_description = $this->model_doctype_doctype->getRouteAction($data['action_id']);
            $is_draft = $action_description['draft'] === '1' ? true : false;
        } else {
            $is_draft = true;
        }

        $this->load->language('action/condition');
        $this->load->language('doctype/doctype');
        //Первое поле
        if (!empty($data['action']['first_value_field_uid'])) {
            $first_value_field_description = $this->model_doctype_doctype->getField($data['action']['first_value_field_uid'], 0);
            $first_value_field_type = $first_value_field_description['type'];
            $data['first_value_field_name'] = $first_value_field_description['name'];
            $data['first_value_field_avaliable_getters'] = $this->load->controller('extension/field/' . $first_value_field_type . '/getFieldMethods', 'getter');
            if (!empty($data['action']['first_value_field_getter'])) {
                $method_data = array(
                    'doctype_uid' => $data['doctype_uid'],
                    'field_uid' => $data['action']['first_value_field_uid'],
                );
                if (isset($data['action']['first_value_field_getter'])) {
                    $method_data['method_name'] = $data['action']['first_value_field_getter'];
                }
                if (isset($data['action']['first_value_method_params'])) {
                    $method_data['method_params'] = $data['action']['first_value_method_params'];
                } else {
                    $method_data['method_params'] = array();
                }
                $method_data['method_params_name_hierarchy'] = '[first_value_method_params]';

                $data['first_value_field_getter_form'] = $this->load->controller('extension/field/' . $first_value_field_type . '/getMethodForm', $method_data);
            }
        }

        //Втрое поле
        if (empty($data['action']['second_type_value']) && !empty($data['action']['second_value_field_uid'])) {
            $second_value_field_description = $this->model_doctype_doctype->getField($data['action']['second_value_field_uid'], 0);
            $second_value_field_type = $second_value_field_description['type'];
            $data['second_value_field_name'] = $second_value_field_description['name'];
            $data['second_value_field_avaliable_getters'] = $this->load->controller('extension/field/' . $second_value_field_type . '/getFieldMethods', 'getter');
            if (!empty($data['action']['second_value_field_getter'])) {
                $method_data = array(
                    'doctype_uid' => $data['doctype_uid'],
                    'field_uid' => $data['action']['second_value_field_uid'],
                );
                if (isset($data['action']['second_value_field_getter'])) {
                    $method_data['method_name'] = $data['action']['second_value_field_getter'];
                }
                if (isset($data['action']['second_value_method_params'])) {
                    $method_data['method_params'] = $data['action']['second_value_method_params'];
                } else {
                    $method_data['method_params'] = array();
                }
                $method_data['method_params_name_hierarchy'] = '[second_value_method_params]';
                $data['second_value_field_getter_form'] = $this->load->controller('extension/field/' . $second_value_field_type . '/getMethodForm', $method_data);
            }
        }
        //действия при выполнении условия
        $ia_descriptions_true = array();
        $inner_actions_true = array();

        if (!empty($data['action']) && !empty($data['action']['inner_actions_true'])) {
            //если не драфтовое действие, то вырезаем все флаги 'new' вложенных действий
            if ($is_draft) {
                if (!empty($data['action']['inner_actions_true_deleted'])) {
                    $inner_actions_true = ($data['action']['inner_actions_true'] + $data['action']['inner_actions_true_deleted']);
                    ksort($inner_actions_true);
                } else {
                    $inner_actions_true = $data['action']['inner_actions_true'];
                }
            } else {
                $inner_actions_true = $data['action']['inner_actions_true'];
                foreach ($inner_actions_true as &$iatnew) {
                    unset($iatnew['new']);
                }
            }
            $inner_actions_true = array_values($inner_actions_true);
            foreach ($inner_actions_true as $iat) {
                $ia_descriptions_true[] = array(
                    'name' => $this->load->controller('extension/action/' . $iat['action'] . "/getTitle"),
                    'description' => $this->clearString($this->load->controller('extension/action/' . $iat['action'] . "/getDescription", $iat['params']))
                );
            }
        }
        $data['action']['inner_actions_true'] = json_encode($inner_actions_true);
        $data['inner_actions_description_true'] = json_encode($ia_descriptions_true);
        //действия при не выполненни условия
        $ia_descriptions_false = array();
        $inner_actions_false = array();
        if (!empty($data['action']) && !empty($data['action']['inner_actions_false'])) {
            //если не драфтовое действие, то вырезаем все флаги 'new' вложенных действий
            if ($is_draft) {
                if (!empty($data['action']['inner_actions_false_deleted'])) {
                    $inner_actions_false = array_values($data['action']['inner_actions_false'] + $data['action']['inner_actions_false_deleted']);
                    ksort($inner_actions_false);
                } else {
                    $inner_actions_false = array_values($data['action']['inner_actions_false']);
                }
            } else {
                $inner_actions_false = $data['action']['inner_actions_false'];
                foreach ($inner_actions_false as &$iatnew) {
                    unset($iatnew['new']);
                }
            }
            $inner_actions_false = array_values($inner_actions_false);
            foreach ($inner_actions_false as $iaf) {
                $ia_descriptions_false[] = array(
                    'name' => $this->load->controller('extension/action/' . $iaf['action'] . "/getTitle"),
                    'description' => $this->clearString($this->load->controller('extension/action/' . $iaf['action'] . "/getDescription", $iaf['params']))
                );
            }
        }
        $data['action']['inner_actions_false'] = json_encode($inner_actions_false);
        $data['inner_actions_description_false'] = json_encode($ia_descriptions_false);
        $data['second_type_value'] = $data['action']['second_type_value'] ?? 0;
        if (isset($data['action']['field_widget_value'])) {
            if (is_array($data['action']['field_widget_value'])) {
                $value = json_encode($data['action']['field_widget_value']);
            } else {
                $value = $data['action']['field_widget_value'];
            }
        } else {
            $value = "";
        }
        $data['field_widget_value'] = urlencode($value);
        //список переменных
        $data['vars'] = $this->model_doctype_doctype->getVariables();     
        return $this->load->view('action/condition/condition_context_form', $data);
    }

    public function getInnerActionForm() {
        $this->load->model('doctype/doctype');
        $this->load->language('action/condition');
        $this->load->language('doctype/doctype');
        $data = array();
        $actions = $this->load->controller('doctype/doctype/getActions', 'inRouteContext');
        $data['actions'] = array();
        $data['doctype_uid'] = $this->request->get['doctype_uid'];
        $i = 0;
        foreach ($actions as $action) {
            if (!isset($action['isCompound']) || $action['isCompound'] === false) {
                $data['actions'][$i] = $action;
                $i++;
            }
        }
        $doctype_uid = $this->request->get['doctype_uid'];
        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            $params = $this->request->post['params'];
            $action_name = $this->request->post['action'];
            $action_data = array();
            $action_data['action'] = $params;
            $action_data['doctype_uid'] = $doctype_uid;

            $data['inner_action_form'] = $this->load->controller('extension/action/' . $action_name . '/getForm', $action_data);
            $data['action_name'] = $action_name;
            $this->response->setOutput($this->load->view('action/condition/condition_inner_action_form', $data));
            return;
            // редактирование вложенного действия`
        } else {
            // добавление вложенного действия
            if (!empty($this->request->get['action_name'])) {
                $action_name = $this->request->get['action_name'];
                $this->response->setOutput($this->load->controller('extension/action/' . $action_name . '/getForm', $data));
                return;
            }
        }

        $this->response->setOutput($this->load->view('action/condition/condition_inner_action_form', $data));
    }

    /**
     * Возвращает неизменяемую информацию о действии
     * @return array()
     */
    public function getActionInfo() {
        return ControllerExtensionActionCondition::ACTION_INFO;
    }

    /**
     * Контекстное ли действие или нет, то есть может запускаться через контексты маршрута или нет.
     * @return boolean
     */
    public function inRouteContext() {
        return true;
    }

    /**
     * Может ли действие использоваться в кнопках
     * @return boolean
     */
    public function inRouteButton() {
        return false;
    }

    /**
     * Может ли действие использоваться в кнопках в журналах
     * @return boolean
     */
    public function inFolderButton() {
        return false;
    }

    /**
     * является ли составным
     * @return boolean
     */
    public function isCompound() {
        return true;
    }

    public function setParams($data) {
        if (!empty($data['params']['action']['inner_actions_true'])) {

            $inner_actions_true = json_decode(htmlspecialchars_decode($data['params']['action']['inner_actions_true']), true);
            $inner_actions_true_deleted = array();

            foreach ($inner_actions_true as $key => $action) {
                if (isset($action['deleted'])) {
                    $inner_actions_true_deleted[$key] = $action;
                    unset($inner_actions_true[$key]);
                }
            }
            $data['params']['action']['inner_actions_true'] = $inner_actions_true;
            if (!empty($inner_actions_true_deleted)) {
                $data['params']['action']['inner_actions_true_deleted'] = $inner_actions_true_deleted;
            }
        }
        if (!empty($data['params']['action']['inner_actions_false'])) {
            $inner_actions_false = json_decode(htmlspecialchars_decode($data['params']['action']['inner_actions_false']), true);
            $inner_actions_false_deleted = array();

            foreach ($inner_actions_false as $key => $action) {
                if (isset($action['deleted'])) {
                    $inner_actions_false_deleted[$key] = $action;
                    unset($inner_actions_false[$key]);
                }
            }
            $data['params']['action']['inner_actions_false'] = $inner_actions_false;
            if (!empty($inner_actions_false_deleted)) {
                $data['params']['action']['inner_actions_false_deleted'] = $inner_actions_false_deleted;
            }
        }

        if (!empty($data['params']['method'])) {
            $data['params']['action']['method'] = $data['params']['method'];
        }
        if ($data['params']['action']['inner_actions_true']) {


            foreach ($data['params']['action']['inner_actions_true'] as &$action) {
                $data_action = array(
                    'params' => array(
                        'action' => $action['params']
                ));
                if (isset($data['route_action_uid'])) {
                    $data_action['route_action_uid'] = $data['route_action_uid'];
                }
                if (isset($data['route_uid'])) {
                    $data_action['route_uid'] = $data['route_uid'];
                }
                $action['params'] = $action_params = $this->load->controller('extension/action/' . $action['action'] . '/setParams', $data_action);
            }
        }
        if ($data['params']['action']['inner_actions_false']) {
            foreach ($data['params']['action']['inner_actions_false'] as &$action) {
                $data_action = array(
                    'params' => array(
                        'action' => $action['params']
                ));
                if (isset($data['route_action_uid'])) {
                    $data_action['route_action_uid'] = $data['route_action_uid'];
                }
                if (isset($data['route_uid'])) {
                    $data_action['route_uid'] = $data['route_uid'];
                }
                $action['params'] = $action_params = $this->load->controller('extension/action/' . $action['action'] . '/setParams', $data_action);
            }
        }
        $data['params']['action']['field_widget_value'] = $data['params']['action']['field_widget_value'] ?? 0;
        return $data['params']['action'];
    }

    /**
     * 
     * @param type $data  = array('document_uid', 'button_uid', 'params');
     */
    public function executeButton($data) {
        
    }

    public function executeRoute($data) {

        $this->load->model('document/document');
        $this->load->model('doctype/doctype');
        $this->load->language('action/condition');
        $first_value = '';
        $first_value_document_uid = '0';
        $second_value = '';
        $second_value_document_uid = '0';
        $first_value_field_uid = '';
        if (!empty($data['params']['first_value_field_uid'])) {
            $method_params = array(
                'type' => 'document',
                'document_uid' => $data['document_uid'],
                'doclink_field_uid' => '0',
                'field_uid' => $data['params']['first_value_field_uid'],
                'method_name' => $data['params']['first_value_field_getter'],
                'current_document_uid' => $data['document_uid'],
                //'target_document_uid' => $data['document_uid'],
            );
            if (isset($data['params']['first_value_method_params'])) {
                $method_params['method_params'] = $data['params']['first_value_method_params'];
            }
            $first_value_field_uid = $method_params['field_uid'];
            $first_value_field_info = $this->model_doctype_doctype->getField($method_params['field_uid']);
            $first_value = $this->load->controller('extension/field/' . $first_value_field_info['type'] . '/executeMethod', $method_params);
        }
        else {
            return;
        }
        if (empty($data['params']['second_type_value']) && !empty($data['params']['second_value_field_uid'])) {
            $method_params = array(
                'type' => 'document',
                'document_uid' => $data['document_uid'],
                'doclink_field_uid' => '0',
                'field_uid' => $data['params']['second_value_field_uid'],
                'method_name' => $data['params']['second_value_field_getter'],
                'current_document_uid' => $data['document_uid'],
                //'target_document_uid' => $data['document_uid'],
            );
            if (isset($data['params']['second_value_method_params'])) {
                $method_params['method_params'] = $data['params']['second_value_method_params'];
            }
            $second_value_field_info = $this->model_doctype_doctype->getField($method_params['field_uid']);
            $second_value = $this->load->controller('extension/field/' . $second_value_field_info['type'] . '/executeMethod', $method_params);
        } elseif (!empty($data['params']['second_type_value']) && $data['params']['second_type_value'] == 1 && isset($data['params']['field_widget_value'])) {
            //преобразуем введенное значение во внутреннее значение поля (например, 01.05.2018 в 2018-05-01
            $model = "model_extension_field_" . $first_value_field_info['type'];
            $this->load->model('extension/field/' . $first_value_field_info['type']);
            $second_value = $this->$model->getValue($first_value_field_uid, 0, $data['params']['field_widget_value'], $first_value_field_info);
        } elseif (!empty($data['params']['second_type_value']) && $data['params']['second_type_value'] == 2 && isset($data['params']['second_value_var'])) {
            $second_value = $this->model_document_document->getVariable($data['params']['second_value_var'], $data['document_uid']);
        }
        
        $result = false;
        switch ($data['params']['comparison_method']) {
            case 'equal':
                if ($first_value == $second_value) {
                    $log = sprintf($this->language->get('text_log'), ($first_value_field_info['name'] ?? ""), $this->language->get('text_condition_equal'), $second_value);
                    $result = true;
                } else {
                    $log = sprintf($this->language->get('text_log'), ($first_value_field_info['name'] ?? ""), $this->language->get('text_condition_notequal'), $second_value);
                }
                break;
            case 'notequal':
                if ($first_value != $second_value) {
                    $log = sprintf($this->language->get('text_log'), ($first_value_field_info['name'] ?? ""), $this->language->get('text_condition_notequal'), $second_value);
                    $result = true;
                } else {
                    $log = sprintf($this->language->get('text_log'), ($first_value_field_info['name'] ?? ""), $this->language->get('text_condition_equal'), $second_value);
                }
                break;
            case 'more' :
                if ($first_value > $second_value) {
                    $log = sprintf($this->language->get('text_log'), ($first_value_field_info['name'] ?? ""), $this->language->get('text_condition_more'), $second_value);
                    $result = true;
                } else {
                    $log = sprintf($this->language->get('text_log'), ($first_value_field_info['name'] ?? ""), $this->language->get('text_condition_less'), $second_value);
                }
                break;
            case 'moreequal':
                if ($first_value >= $second_value) {
                    $log = sprintf($this->language->get('text_log'), ($first_value_field_info['name'] ?? ""), $this->language->get('text_condition_moreequal'), $second_value);
                    $result = true;
                } else {
                    $log = sprintf($this->language->get('text_log'), ($first_value_field_info['name'] ?? ""), $this->language->get('text_condition_lessequal'), $second_value);
                }
                break;
            case 'less':
                if ($first_value < $second_value) {
                    $log = sprintf($this->language->get('text_log'), ($first_value_field_info['name'] ?? ""), $this->language->get('text_condition_less'), $second_value);
                    $result = true;
                } else {
                    $log = sprintf($this->language->get('text_log'), ($first_value_field_info['name'] ?? ""), $this->language->get('text_condition_more'), $second_value);
                }
                break;
            case 'lessequal':
                if ($first_value <= $second_value) {
                    $log = sprintf($this->language->get('text_log'), ($first_value_field_info['name'] ?? ""), $this->language->get('text_condition_lessequal'), $second_value);
                    $result = true;
                } else {
                    $log = sprintf($this->language->get('text_log'), ($first_value_field_info['name'] ?? ""), $this->language->get('text_condition_moreequal'), $second_value);
                }
                break;
            case 'contains':
                if (mb_strpos($first_value, $second_value) !== FALSE) {
                    $log = sprintf($this->language->get('text_log'), ($first_value_field_info['name'] ?? ""), $this->language->get('text_condition_contains'), $second_value);
                    $result = true;
                } else {
                    $log = sprintf($this->language->get('text_log'), ($first_value_field_info['name'] ?? ""), $this->language->get('text_condition_notcontains'), $second_value);
                }
                break;
            case 'notcontains':
                if (mb_strpos($first_value, $second_value) === FALSE) {
                    $log = sprintf($this->language->get('text_log'), ($first_value_field_info['name'] ?? ""), $this->language->get('text_condition_notcontains'), $second_value);
                    $result = true;
                } else {
                    $log = sprintf($this->language->get('text_log'), ($first_value_field_info['name'] ?? ""), $this->language->get('text_condition_contains'), $second_value);
                }
                break;
        }
        if ($result) {
            if (!empty($data['params']['inner_actions_true'])) {
                $result_action = $this->executeInnerActions($data['params']['inner_actions_true'], $data['document_uid'], $data['route_uid']);
            }
        } else {
            if (!empty($data['params']['inner_actions_false'])) {
                $result_action = $this->executeInnerActions($data['params']['inner_actions_false'], $data['document_uid'], $data['route_uid']);
            }
        }
        
        $result_action['log'] = $log ?? $result_action['log'] ?? ""; 
        return $result_action;
    }

    private function executeInnerActions($inner_actions, $document_uid, $route_uid) {
        $append = array();
        foreach ($inner_actions as $action) {
            $action['document_uid'] = $document_uid;
            $document_info = $this->model_document_document->getDocument($document_uid, false);
            if ($route_uid != $document_info['route_uid']) {
                //документ был перемещен
                break;
            }
            $action['route_uid'] = $route_uid;
            $result = $this->load->controller("extension/action/" . $action['action'] . "/executeRoute", $action);
            if (!empty($result['append'])) {
                $append[] = $result['append'];
            }
            if (!empty($result['redirect'])) {
                if ($append) {
                    $result['append'] = $append;
                }
                return $result;
            }
        }
        if ($append) {
            return array('append' => $append);
        }        
    }

    private function clearString($string) {
        return str_replace(array('"',"'","&quot;"), "\"", $string); 
    }
}
