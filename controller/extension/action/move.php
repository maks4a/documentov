<?php

/**
 * @package		Documentov
 * @author		Andrey V Surov
 * @copyright           Copyright (c) 2018 Andrey V Surov, Roman V Zhukov (https://www.documentov.com/)
 * @license		https://opensource.org/licenses/mit-license.php
 * @link		https://www.documentov.com
*/

class ControllerExtensionActionMove extends Controller {

    const ACTION_INFO = array(
        'name' => 'move',
//        'inRouteButton' => true,
//        'inFolderButton' => true,
        'inRouteContext' => true
    );

    public function index() {
        $this->load->language('extension/action/move');

        $data['cancel'] = $this->url->link('marketplace/extension', 'type=action', true);

        $this->response->setOutput($this->load->view('extension/action/move', $data));
    }

    public function install() {
        
    }

    public function uninstall() {
        
    }

    /**
     * Метод возвращает название действия в соответствии с выбранным языком
     * @return type
     */
    public function getTitle() {

        $this->language->load('extension/action/move');
        return $this->language->get('heading_title');
    }

    /**
     * Метод возвращает описание действия, исходя из параметров
     */
    public function getDescription($params) {
        $this->load->language('action/move');
        $this->load->model('doctype/doctype');
        $description_move = "";
        if (!empty($params['field_document_uid'])) {
            //перемещение по ссылке из поля
            $field_info = $this->model_doctype_doctype->getField($params['field_document_uid']);
            $description_move = $this->language->get('text_description_move_doctype') . ' ' . $this->language->get('text_by_link_in_field') . " " . $field_info['name'];
        } else {
            $description_move = $this->language->get('text_description_move') . ' ' . $this->language->get('text_currentdoc');
        }
        if (!empty($params['document_route_uid'])) {
            $route_info = $this->model_doctype_doctype->getRoute($params['document_route_uid']);
            $description_move .= $this->language->get('text_description_move_route') . ' ' . ($route_info['name'] ?? "___");
        }
        return $description_move;
    }

    /**
     * Метод возвращает форму действия для типа документа
     * @param type $data - массив, включающий doctype_uid, route_uid
     */
    public function getForm($data) {
        $this->load->language('action/move');
        $this->load->language('doctype/doctype');

        $data['field_document_name'] = $this->language->get('text_currentdoc');
        $field_document_info = '';
        if (empty($data['action']['field_document_uid'])) {
            $data['action']['field_document_uid'] = 0;
        } else {
            $field_document_info = $this->model_doctype_doctype->getField($data['action']['field_document_uid']);
            $data['field_document_name'] = $this->language->get('text_by_link_in_field') . ' &quot;' . $field_document_info['name'] . '&quot;';
            $data['field_document_setting'] = $field_document_info['setting'];
        }

        if (!empty($data['action']['document_route_uid'])) {
            $route_info = $this->model_doctype_doctype->getRoute($data['action']['document_route_uid']);
            $doctype_name = '';
            if ($field_document_info) {
                $doctype_uid = $route_info['doctype_uid'];
                $language_id = $this->config->get('config_language_id');
                $doctype_info = $this->model_doctype_doctype->getDoctypeDescriptions($doctype_uid)[$language_id];
                $doctype_name = $doctype_info['name'] . ' - ';
            } 
            $data['document_route_name'] = $doctype_name . (isset($route_info['name']) ? $route_info['name'] : "");
        }

        return $this->load->view('action/move/move_form', $data);
    }

    /**
     * Метод позволяет изменить сохраняемые в базу параметры действия (при необходимости)
     * @param type $data
     * @return type
     */
    public function setParams($data) {
        return $data['params']['action'];
    }

    /**
     * Возвращает неизменяемую информацию о действии
     * @return array()
     */
    public function getActionInfo() {
        return $this::ACTION_INFO;
    }

    /**
     * 
     * @param type $data  = array('document_uid', 'button_uid', 'params');
     */
    public function executeButton($data) {
        $this->load->language('action/move');
        if (!$data['params']['document_route_uid']) {
            $result = array(
                'error' => $this->language->get('error_route_uid_not_found'),
                'log' => $this->language->get('error_route_uid_not_found')
            );
            return $result;
        }
        $this->load->model('document/document');

        if (!empty($data['folder_uid'])) {
            //запуск через журнал
            if (!empty($data['document_uids'])) {
                $document_uids = $data['document_uids'];
            }
        } else {
            $document_uids = array($data['document_uid']);
        }

        if (!$document_uids) {
            $result = array(
                'error' => $this->language->get('error_document_not_found'),
                'log' => $this->language->get('error_document_not_found')
            );
            return $result;
        }
        $log = array();

        if (!empty($data['params']['field_document_uid'])) { //редактируем документ по ссылке из поля
            foreach ($document_uids as $document_uid) {
                $value = $this->model_document_document->getFieldValue($data['params']['field_document_uid'], $document_uid);
                $external_document_uids = explode(",", $value);
                $daemon_queue_move = array();
                $result = array();
                foreach ($external_document_uids as $external_document_uid) {
                    $this->model_document_document->moveRoute($external_document_uid, $data['params']['document_route_uid']);
                    if ($external_document_uid !== $document_uid) {
                        $daemon_queue_move[] = $external_document_uid;
                    }
                }
                if ($daemon_queue_move) { //обработка перемещения сторонних документов (не текущ)
                    $this->load->model('extension/service/daemon');                    
                    $this->load->model('daemon/queue');
                    $queue_data = array(
                        'document_uids' => $daemon_queue_move,
                        'document_route_uid' => $data['params']['document_route_uid']
                    );
                    if ($this->variable->get('daemon_started') && $this->model_extension_service_daemon->getStatus()) {
                        $this->model_daemon_queue->addTask('extension/action/move/executeDeferred', $queue_data, 1);                    
                    } else {
                        $this->executeDeferred($queue_data);
                    }
                    
                }
            }
        } else {
            foreach ($document_uids as $document_uid) {
                $this->model_document_document->moveRoute($document_uid, $data['params']['document_route_uid']);
                $log[$document_uid] = $this->language->get('text_description_document');
            }
        }
        if (!empty($data['folder_uid'])) {
            $result = array(
                'reload' => 'table',
                'log' => $log
            );
        } else {
            $result = array(
                'reload' => str_replace('&amp;', '&', $this->url->link('document/document', 'document_uid=' . $data['document_uid'] . '&_=' . rand(100000000, 999999999))),
                'log' => count($document_uids) > 1 ? $this->language->get('text_description_documents') : $this->language->get('text_description_document')
            );
        }

        return $result;
    }

    /**
     * 
     * @param type $data  = array('document_uid', 'button_uid', 'params');
     */
    public function executeRoute($data) {
        if (!empty($data['params']['field_document_uid'])) { //перемещаем документы по ссылке из поля
            $value = $this->model_document_document->getFieldValue($data['params']['field_document_uid'], $data['document_uid']);
            $document_uids = explode(",", $value);
        } else {
            $document_uids = array($data['document_uid']);
        }
        if ($document_uids && $data['params']['document_route_uid']) {
            $daemon_queue_move = array();
            foreach ($document_uids as $document_uid) {
                $this->model_document_document->moveRoute($document_uid, $data['params']['document_route_uid']);
                if ($data['document_uid'] != $document_uid) { //проверяем, что перемещаем не текущий документ
                    $daemon_queue_move[] = $document_uid;
                }
            }
            if (count($document_uids) > 1) {
                $result = array(
                    'log' => $this->language->get('text_description_documents')
                );
            } else {
                $result = array(
                    'log' => $this->language->get('text_description_document')
                );
            }
            //планируем обработку точек перемещенных сторонних доков
            if ($daemon_queue_move) {
                $this->load->model('extension/service/daemon');                
                $this->load->model('daemon/queue');
                $queue_data = array(
                    'document_uids' => $daemon_queue_move,
                    'document_route_uid' => $data['params']['document_route_uid']
                );
                if ($this->variable->get('daemon_started') && $this->model_extension_service_daemon->getStatus()) {
                    $this->model_daemon_queue->addTask('extension/action/move/executeDeferred', $queue_data, 1);                    
                } else {
                    $this->executeDeferred($queue_data);
                }
            }
        } else {
            $result = array(
                'error' => $this->language->get('error_document_not_found'),
                'log' => $this->language->get('error_document_not_found')
            );
        }
        return $result;
    }
 
    public function executeDeferred($data) {
        $this->load->model('document/document');
        foreach ($data['document_uids'] as $document_uid) {
            $this->model_document_document->moveRoute($document_uid, $data['document_route_uid']);
            $params = array("document_uid" => $document_uid, "context" => 'jump');
            $this->load->controller("document/document/route_cli", $params);
        }
    }

}
