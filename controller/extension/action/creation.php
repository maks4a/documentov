<?php

/**
 * @package		Documentov
 * @author		Andrey V Surov
 * @copyright           Copyright (c) 2018 Andrey V Surov, Roman V Zhukov (https://www.documentov.com/)
 * @license		https://opensource.org/licenses/mit-license.php
 * @link		https://www.documentov.com
*/

class ControllerExtensionActionCreation extends Controller {
    
    const ACTION_INFO = array(
        'name' => 'creation',
        'inFolderButton' => true,
        'inRouteButton' => true,
        'inRouteContext' => true,
    );
    
    public function index() {
        $this->load->language('extension/action/creation');

        $data['cancel'] = $this->url->link('marketplace/extension', 'type=action', true);

        $this->response->setOutput($this->load->view('extension/action/creation', $data));
    }

    public function install() {

    }
    
    public function uninstall() {

    }   
    
    /**
     * Метод возвращает название действия в соответствии с выбранным языком
     * @return type
     */
    public function getTitle() {
        
        $this->language->load('extension/action/creation');
        return $this->language->get('heading_title');
    }
    
    /**
     * Метод позволяет изменить сохраняемые в базу параметры действия (при необходимости)
     * @param type $data
     * @return type
     */
    public function setParams($data) {
        return $data['params']['action'];
    }
    
    /**
     * Метод возвращает описание действия, исходя из параметров
     */
    public function getDescription($params) {
        $this->load->language('action/creation');
        if (!empty($params['doctype_uid'])) {
            $this->load->model('doctype/doctype');            
            $doctype_info = $this->model_doctype_doctype->getDoctype($params['doctype_uid']);
            return sprintf($this->language->get('text_description'), $doctype_info['name'] ?? "");
        } else {
            return $this->language->get('text_description_without_doctype');
        }
        
    }
    
    
    /**
     * Метод возвращает форму действия для типа документа
     * @param type $data - массив, включающий doctype_uid, route_uid
     */
    public function getForm($data) {
        $this->load->language('action/creation');
        $this->load->language('doctype/doctype');
         $this->load->model('doctype/doctype');
        if (!empty($data['action']['doctype_uid'])) {
            $doctype_info = $this->model_doctype_doctype->getDoctype($data['action']['doctype_uid']);
            $data['action']['doctype_name'] = $doctype_info['name'];
        }
        $data['action']['sets'] = array();
        if (!empty($data['action']['set'])) {
            foreach ($data['action']['set'] as $set) {   
                $sourse_field_info = $this->model_doctype_doctype->getField($set['source_field_uid']);
                $target_field_info = $this->model_doctype_doctype->getField($set['target_field_uid']);
                $data['action']['sets'][] = array(
                    'source_field_uid'   => $set['source_field_uid'],
                    'source_field_name' => isset($sourse_field_info['name']) ? $sourse_field_info['name'] : "",
                    'target_field_uid'   => $set['target_field_uid'],
                    'target_field_name' => isset($target_field_info['name']) ? $target_field_info['name'] : "",
                );
            }
        }
        
        if (!empty($data['action']['field_new_document_uid']))  {
            $field_new_document_info = $this->model_doctype_doctype->getField($data['action']['field_new_document_uid']);
            $data['action']['field_new_document_name'] = isset($field_new_document_info['name']) ? $field_new_document_info['name'] : "";
        }
        if (!empty($data['action']['field_new_document_author_id']))  {
            $field_new_document_author_info = $this->model_doctype_doctype->getField($data['action']['field_new_document_author_id']);
            $data['action']['field_new_document_author_name'] = $field_new_document_author_info['name'] ?? "";
            $data['action']['field_new_document_author_setting'] = $field_new_document_author_info['setting'] ?? 0;
        }
        return $this->load->view('action/creation/creation_form', $data);        
    }
    
     /**
     * Возвращает неизменяемую информацию о действии
     * @return array()
     */
    public function getActionInfo() {
        return ControllerExtensionActionCreation::ACTION_INFO;
    }
    

    /**
     * 
     * @param type $data  = array('document_uid', 'button_uid', 'params');
     */
    public function executeButton($data) {
        $this->load->model('doctype/doctype');
        $this->load->language('action/creation');
        if (!empty($data['params']['doctype_uid'])) {
            $append = array();
            $result = array();
            $document_uid = 0;
            if (!empty($data['folder_uid'])) {
                //запуск из журнала
                if (!empty($data['document_uids'])) {
                    $document_uid = $data['document_uids'][0]; //используем  первый по списку выбранный документ
                }
            } else {
                $document_uid = $data['document_uid'];
            }  
            
            $this->load->model('document/document');
            if (!empty($this->request->server['REQUEST_METHOD'] == "POST")) {
                //сохранение документа
                if (isset($this->request->get['draft'])) {
                    //это автосохранение
                    if (!empty($this->request->get['new_document_uid'])) {
                        $this->model_document_document->saveDraftDocument($this->request->get['new_document_uid'], $this->request->post['field']);
                    }                    
                } else {                     
                    //получаем шаблон доктайпа
                    $new_document_uid = $this->request->get['new_document_uid'] ?? "";
                    if ($new_document_uid) {
                        $document_info = $this->model_document_document->getDocument($new_document_uid);
                        if ($document_info) {
                            $template = $this->model_document_document->getTemplate($new_document_uid, 'form');

                            $data_eft = array(
                                    'fields'         => $this->request->post['field'],
                                    'template'      => $template,
                                    'document_uid'  => $new_document_uid
                                );
                            $result_save_fields = $this->load->controller('document/document/editFieldsTemplate',$data_eft);
                            $append = array();
                            if (isset($result_save_fields['success'])) {
                                $data_route = array(
                                    'document_uid' => $new_document_uid,
                                    'context'      => 'jump'
                                );                    
                                $result_route = $this->load->controller('document/document/route_cli', $data_route);                                                 

                                if (!empty($data['params']['field_new_document_uid'])) {
                                    $result_save_new_document = $this->model_document_document->editFieldValue($data['params']['field_new_document_uid'], $document_uid, $new_document_uid);
                                }
                                if (empty($result_route['window']) && empty($result_route['redirect']) && empty($result_route['replace'])) {
                                    $result = array(    
                                        'redirect' => str_replace('&amp;', '&', $this->url->link('document/document', 'document_uid=' . $new_document_uid . (!empty($data['folder_uid']) ? '&folder_uid=' . $data['folder_uid'] : "") )),
                                        'document_uid' => $new_document_uid,
                                        'log'      => sprintf($this->language->get('text_document_create'),$new_document_uid)
                                    );                                                                     
                                } else {
                                    $result = $result_route;
                                    $result['document_uid'] = $new_document_uid;
                                }
                                if (!empty($result_save_fields['append'])) {
                                    $append = array_merge($append,$result_save_fields['append']);
                                }
                                if (!empty($result_save_new_document['append'])) {
                                    $append = array_merge($append,$result_save_new_document['append']);
                                }
                                if (!empty($result_route['append'])) {
                                    $append = array_merge($append,$result_route['append']);
                                }
                                if ($append) {
                                    $result['append'] = $append;
                                }
                            } else {
                                return $result_save_fields;
                            }                           
                            
                        }
                    }
                }                
            }
            //GET-запрос
            elseif (isset($this->request->get['remove_draft'])) {
                //удаляется черновик
                if (!empty($this->request->get['new_document_uid'])) {
                    $this->model_document_document->removeDraftDocument($this->request->get['new_document_uid']);
                }     
               
            } else {
                
                //подготовка формы
                if (!empty($data['params']['field_new_document_author_id'])) {
                    $value_author_field = $this->model_document_document->getFieldValue($data['params']['field_new_document_author_id'],$document_uid);
                    $author_ids = explode(',', $value_author_field);
                    $author_id = !empty($author_ids[0]) ? $author_ids[0] : 0;
                } else {
                    $author_id = 0;
                }
                $new_document_uid = $this->model_document_document->addDocument($data['params']['doctype_uid'], $author_id);
                if(!empty($data['params']['set'])) {
                    //устанавливаем значения полей в созданном документе на основании сопоставления полей в действий
                    foreach ($data['params']['set'] as $set) {                    
                        $value = $this->model_document_document->getFieldValue($set['source_field_uid'], $document_uid);
                        $result_set_field = $this->model_document_document->editFieldValue($set['target_field_uid'], $new_document_uid, $value);
                        if (!empty($result_set_field['append'])) {
                            $append = array_merge($append, $result_set_field['append']);
                        }
                    }                
                }  

                $data_route = array(
                    'document_uid' => $new_document_uid,
                    'context'      => 'create'
                );                    
                $result = $this->load->controller('document/document/route_cli', $data_route);
                if (!empty($result['append'])) {
                    $append = array_merge($append, $result['append']);
                }                
                if (isset($result['redirect'])) {
                    //поскольку редирект произошел через контекст создания, просто возвращаем его и ничего не делаем
                } else {
                    //готовим форму создания / редактирования документа
                    $data_form = array(
                        'draft'         => 3,
                        'folder_uid'    => ""
                    );       
                    $data_toolbar = array(
                        'button_uid'        => $data['button_uid'],
                        'document_uid'      => $document_uid,
                        'new_document_uid'  => $new_document_uid
                    );

                    if (!empty($data['folder_uid'])) {
                        //запуск через документ
                        $data_form['folder_uid'] = $data['folder_uid'];
                        $data_toolbar['folder_uid'] = $data['folder_uid'];
                        $result = array(
                            'replace' => array(
                                'tcolumn'         => $this->load->view('action/creation/creation_header', $data_form) . $this->load->controller('document/document/getForm',$new_document_uid) . (isset($result['append']) ? implode("\r\n", $result['append']) : "") . $this->load->view('action/creation/creation_footer', $data_form),
                                'folder_toolbar'  => $this->load->view('action/creation/creation_toolbar', $data_toolbar)
                            ),
                        );
                    } else {
                        //запуск через журнал
                        $result = array(
                            'replace' => array(
                                'document_form'         => $this->load->view('action/creation/creation_header', $data_form) . $this->load->controller('document/document/getForm',$new_document_uid) . (isset($result['append']) ? implode("\r\n", $result['append']) : "") . $this->load->view('action/creation/creation_footer', $data_form),
                                'document_toolbar'      => $this->load->view('action/creation/creation_toolbar', $data_toolbar)
                            ),
                        );                        
                    }                
                    
                }

                
            }
            if ($append) {
                $result['append'] = $append;
            }
            return $result;
        } else {
            return array(
                'error' => $this->language->get('error_doctype_not_found'),
                'log'   => $this->language->get('error_doctype_not_found')
            );
        }       
    }
    
    public function executeRoute($data) {
        $this->load->language('action/creation');
        if (!empty($data['params']['doctype_uid'])) {
            $document_uid = $data['document_uid'];
            $this->load->model('document/document');
            if (!empty($data['params']['field_new_document_author_id'])) {
                $value_author_field = $this->model_document_document->getFieldValue($data['params']['field_new_document_author_id'],$document_uid);
                $author_ids = explode(',', $value_author_field);
            } else {
                $author_ids = array(0);
            }
            $new_document_uids = array();
            $new_document_links = array();
            $values = array();
            if(!empty($data['params']['set']) && $document_uid) {
                foreach ($data['params']['set'] as $set) { //получаем значения одним циклом
                    $values[$set['source_field_uid']] = $this->model_document_document->getFieldValue($set['source_field_uid'], $document_uid);                    
                }                 
            }        
            foreach ($author_ids as $author_id) {
                $new_doc = $this->model_document_document->addDocument($data['params']['doctype_uid'], $author_id);
                $data_route = array(
                    'document_uid' => $new_doc,
                    'context'      => 'create'
                );                    
                //запускаем "создание" с созданном доке; то, что возвращает маршрут не анализируем, то есть не выполняем
                //редиректы, апенды и пр, поскольку работаем в рамках родительского документа
                $this->load->controller('document/document/route_cli', $data_route);                   
                $this->model_document_document->editDocument($new_doc);
                if(!empty($values)) {
                    foreach ($data['params']['set'] as $set) {
                        $this->model_document_document->editFieldValue($set['target_field_uid'], $new_doc, $values[$set['source_field_uid']]);
                    }                                    
                }                
                $data_route['context'] = 'jump';
                //запускаем "создание" с созданном доке; то, что возвращает маршрут не анализируем, то есть не выполняем
                //редиректы, апенды и пр, поскольку работаем в рамках родительского документа                
                $this->load->controller('document/document/route_cli', $data_route); 
                $new_document_uids[] = $new_doc;
                $new_document_links[] = $this->url->link('document/document', 'document_uid=' . $new_document_uids[count($new_document_uids)-1]);
            }
            
            if (!empty($data['params']['field_new_document_uid'])) {
                $this->model_document_document->editFieldValue($data['params']['field_new_document_uid'], $document_uid, implode(",",$new_document_uids));
            }           
            return array(
               'log'        => sprintf($this->language->get('text_document_create'), implode(", ", $new_document_links))
            );
        } else {
            return array(
                'log'   => $this->language->get('error_doctype_not_found')
            );
        }       
        
    }
     
}