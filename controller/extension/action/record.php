<?php

/**
 * @package		Documentov
 * @author		Roman V Zhukov
 * @copyright           Copyright (c) 2018 Andrey V Surov, Roman V Zhukov (https://www.documentov.com/)
 * @license		https://opensource.org/licenses/mit-license.php
 * @link		https://www.documentov.com
 */
class ControllerExtensionActionRecord extends Controller {

    const ACTION_INFO = array(
        'name' => 'record',
        'inRouteContext' => true,
        'inRouteButton' => true,
        'inFolderButton' => true
    );
    const max_length_description_manual = 70;

    public function index() {
        $this->load->language('extension/action/record');

        $data['cancel'] = $this->url->link('marketplace/extension', 'type=action', true);

        $this->response->setOutput($this->load->view('extension/action/record', $data));
    }

    public function install() {
        
    }

    public function uninstall() {
        
    }

    /**
     * Метод возвращает название действия в соответствии с выбранным языком
     * @return type
     */
    public function getTitle() {

        $this->language->load('extension/action/record');
        return $this->language->get('heading_title');
    }

    /**
     * Метод возвращает описание действия, исходя из параметров
     */
    public function getDescription($params) {
        $this->load->language('action/record');
        $this->load->model('doctype/doctype');

        //запись из: поля 'sf' документа по ссылке из поля 'slf' текущего документа
        //запись из: поля 'sf' текущего документа
        //запись из настроечного поля 'nf' типа документа
        //запись из переменной 'var'
        //в поле 'tf' документа по ссылке из поля 'tlf'
        //в поле 'tf' текущего документа

        $source_info = "";
       
        if (isset($params['method_params']) && isset($params['method_params']['standard_setter_param'])) {
            switch ($params['method_params']['standard_setter_param']['type']) {
                case 'variable':
                    if (!empty($params['method_params']['standard_setter_param']['var_id'])) {
                        $source_variable_name = $this->language->get('text_' . $params['method_params']['standard_setter_param']['var_id']);
                        $source_info = sprintf($this->language->get('text_description_source_4'), $source_variable_name);
                    } else {
                        $source_info = sprintf($this->language->get('text_description_source_4'), "");
                    }
                    break;
                case 'document':
                    if (!empty($params['method_params']['standard_setter_param']['field_uid'])) {
                        $source_field_description = $this->model_doctype_doctype->getField($params['method_params']['standard_setter_param']['field_uid']);
                        $source_field_name = $source_field_description['name'];
                        if (strcmp($params['method_params']['standard_setter_param']['doclink_field_uid'], '0') === 0) {
                            $source_info = sprintf($this->language->get('text_description_source_2'), '"' . $source_field_name . '"');
                        } elseif ($params['method_params']['standard_setter_param']['doclink_field_uid'] === 1) {
                            $source_info = sprintf($this->language->get('text_description_source_5'), '"' . $source_field_name . '"');
                        } else {
                            $source_doclink_field_description = $this->model_doctype_doctype->getField($params['method_params']['standard_setter_param']['doclink_field_uid']);
                            $source_doclink_field_name = $source_doclink_field_description['name'];
                            $source_info = sprintf($this->language->get('text_description_source_1'), '"' . $source_field_name . '"', '"' . $source_doclink_field_name . '"');
                        }
                    } else {
                        $source_info = sprintf($this->language->get('text_description_source_2'), "");
                    }
                    break;
                case 'doctype' :
                    if (!empty($params['method_params']['standard_setter_param']['field_uid'])) {
                        $source_field_description = $this->model_doctype_doctype->getField($params['method_params']['standard_setter_param']['field_uid']);
                        if ($source_field_description) {
                            $source_field_name = $source_field_description['name'];
                            $source_info = sprintf($this->language->get('text_description_source_3'), '"' . $source_field_name . '"');
                        }
                    } else {
                        $source_info = sprintf($this->language->get('text_description_source_3'), "", "");
                    }
                    break;
                case 'value' :
                    if (isset($params['method_params']['standard_setter_param']['value'])) {
                        if (is_array($params['method_params']['standard_setter_param']['value'])) {
                            $value = implode(", ", $params['method_params']['standard_setter_param']['value']);
                        }
                        $source_info = strip_tags(html_entity_decode($value ?? $params['method_params']['standard_setter_param']['value']));
                        if (mb_strlen($source_info) > $this::max_length_description_manual) {
                            $source_info = mb_substr($source_info, 0, $this::max_length_description_manual) . "...";
                        }
                    }
            }
        }
        $target_info = "";
        if (!empty($params['target_field_uid'])) {
            $target_field_description = $this->model_doctype_doctype->getField($params['target_field_uid']);
            $target_field_name = $target_field_description['name'];
            if ($params['target_doclink_field_uid'] === '0') {
                $target_info = sprintf($this->language->get('text_description_target_2'), '"' . $target_field_name . '"');
            } else {
                $target_doclink_field_description = $this->model_doctype_doctype->getField($params['target_doclink_field_uid']);
                $target_doclink_field_name = $target_doclink_field_description['name'];
                $target_info = sprintf($this->language->get('text_description_target_1'), '"' . $target_field_name . '"', '"' . $target_doclink_field_name . '"');
            }
            if (!empty($params['target_field_method_name']) && $params['target_field_method_name'] !== 'standard_setter') {
                $this->load->language('extenstion/field' . $target_field_description['type']);
                $target_info .= " (" . $this->language->get('text_method_' . $params['target_field_method_name']) . ")";
            }
        } else {
            $target_info = sprintf($this->language->get('text_description_target_1'), "", "");
        }

        return $source_info . ' ' . $target_info;
    }

    /**
     * Метод возвращает форму действия для типа документа
     * @param type $data - массив, включающий doctype_uid, route_uid
     */
    public function getForm($data) {

        $this->load->model('doctype/doctype');
        $this->load->language('action/record');
        $this->load->language('doctype/doctype');
        $lang_id = (int) $this->config->get('config_language_id');

        //приемник
        
        $data['target_doclink_field_name'] = $this->language->get('text_currentdoc');
        if (empty($data['action']['target_doclink_field_uid'])) {
            $data['action']['target_doclink_field_uid'] = '0';
        } else {
            $target_doclink_field = $this->model_doctype_doctype->getField($data['action']['target_doclink_field_uid']);
            $target_doclink_field_name = $this->language->get('text_by_link_in_field') . ' &quot;' . $target_doclink_field['name'] . '&quot;';
            $data['target_doclink_field_name'] = $target_doclink_field_name;
            $data['target_doclink_field_setting'] = $target_doclink_field['setting'];
        }

        if (!empty($data['action']['target_field_uid'])) {
            $target_field_description = $this->model_doctype_doctype->getField($data['action']['target_field_uid']);

            if ($target_field_description) {
                $target_field_type = $target_field_description['type'];
                $target_field_doctype_uid = $target_field_description['doctype_uid'];
                $doctypename = $this->model_doctype_doctype->getDoctypeDescriptions($target_field_doctype_uid)[$lang_id]['name'] ?? "";
                if ($data['action']['target_doclink_field_uid'] === '0') {
                    $data['target_field_name'] = $target_field_description['name'];
                } else {                    
                    $data['target_field_name'] = $doctypename . ' - ' . $target_field_description['name'];
                }                
                $data['avaliable_setters'] = $this->load->controller('extension/field/' . $target_field_type . '/getFieldMethods', 'setter');
                $method_data = array();
                $method_data['doctype_uid'] = $data['doctype_uid'];
                $method_data['field_uid'] = $data['action']['target_field_uid'];
                if (isset($data['action']['target_field_method_name'])) {
                    $method_data['method_name'] = $data['action']['target_field_method_name'];
                } else {
                    $method_data['method_name'] = 'standard_setter';
                    $data['target_field_method_name'] = 'standard_setter';
                }
                if (isset($data['action']['method_params'])) {
                    $method_data['method_params'] = $data['action']['method_params'];
                } else {
                    $method_data['method_params'] = array();
                }
                $method_data['method_params_name_hierarchy'] = '[method_params]';
                $data['target_method_form'] = $this->load->controller('extension/field/' . $target_field_type . '/getMethodForm', $method_data);
            } 
        } else {
            $data['target_method_form'] = $this->language->get('text_select_field');
        }

        return $this->load->view('action/record/record_context_form', $data);
    }

    /**
     * Возвращает неизменяемую информацию о действии
     * @return array()
     */
    public function getActionInfo() {
        return ControllerExtensionActionRecord::ACTION_INFO;
    }

    public function setParams($data) {
        $this->load->model('doctype/doctype');
        if ($data['params']['action'] !== "standard_setter") {
            //возможно методу поля необходимо обработать параметры перед сохранением
            $field_info = $this->model_doctype_doctype->getField($data['params']['action']['target_field_uid']);
            if (!empty($field_info['type'])) {
                $method_data = array(
                    'method_name'   => $data['params']['action']['target_field_method_name'],
                    'method_params' => $data['params']['action']['method_params'],
                    'field_uid'     => $data['params']['action']['target_field_uid']
                );
                $method_params = $this->load->controller('extension/field/' . $field_info['type'] . "/setMethodParams", $method_data);
                if ($method_params !== NULL) {
                    $data['params']['action']['method_params'] = $method_params;
                }
            }
        }        
        return $data['params']['action'];
    }

    /**
     * 
     * @param type $data  = array('document_uid', 'button_uid', 'params');
     */
    public function executeButton($data) {
        if (isset($data['document_uid'])) { //есть document_uid - запуск действия из документ
            $result = $this->executeRoute($data);
        } else { //запуск из журнала
            foreach ($data['document_uids'] as $document_uid) {
                $data['document_uid'] = $document_uid;
                $result = $this->executeRoute($data);
            }
        }

        return $result;
    }

    public function executeRoute($data) {
        if (empty($data['params']['target_field_uid'])) {
            return;
        }
        $this->load->model('document/document');
        $this->load->model('doctype/doctype');
       
        $method_params = array(
            'type' => $data['params']['target_type'],
            //'document_uid' => $data['document_uid'],
            'doclink_field_uid' => $data['params']['target_doclink_field_uid'],
            'field_uid' => $data['params']['target_field_uid'],
            'method_name' => $data['params']['target_field_method_name'],
            'current_document_uid' => $data['document_uid'],
            'method_params' => $data['params']['method_params']            
        );
     
        $target_field_info = $this->model_doctype_doctype->getField($method_params['field_uid']);        
        $result = $this->load->controller('extension/field/' . $target_field_info['type'] . '/executeMethod', $method_params);     

        $this->load->language('extension/action/record');
        if (!is_array($result)) {
            $result = array();
        }
        $result['log'] = sprintf($this->language->get('text_log'), $target_field_info['name'], $this->model_document_document->getFieldDisplay($data['params']['target_field_uid'], $data['document_uid']));
        return $result;
    }

}

