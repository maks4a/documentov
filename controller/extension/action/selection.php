<?php

/**
 * @package		Documentov
 * @author		Andrey V Surov
 * @copyright           Copyright (c) 2018 Andrey V Surov, Roman V Zhukov (https://www.documentov.com/)
 * @license		https://opensource.org/licenses/mit-license.php
 * @link		https://www.documentov.com
*/

class ControllerExtensionActionSelection extends Controller {
    
    const ACTION_INFO = array(
        'name' => 'selection',
        'inRouteContext' => true,
    );
    
    public function index() {
        $this->load->language('extension/action/selection');

        $data['cancel'] = $this->url->link('marketplace/extension', 'type=action', true);

        $this->response->setOutput($this->load->view('extension/action/selection', $data));
    }

    public function install() {

    }
    
    public function uninstall() {

    }   
    
    /**
     * Метод возвращает название действия в соответствии с выбранным языком
     * @return type
     */
    public function getTitle() {
        
        $this->language->load('extension/action/selection');
        return $this->language->get('heading_title');
    }
    
    /**
     * Метод позволяет изменить сохраняемые в базу параметры действия (при необходимости)
     * @param type $data
     * @return type
     */
    public function setParams($data) {
        return $data['params']['action'];
    }
    
    /**
     * Метод возвращает описание действия, исходя из параметров
     */
    public function getDescription($params) {
        $this->load->language('action/selection');
        $this->load->model('doctype/doctype');
        $description = array();
        if (!empty($params['document_source'])) {
            switch ($params['document_source']) {
                case "document_field":
                    if (!empty($params['document_field_id'])) {
                        $field_info = $this->model_doctype_doctype->getField($params['document_field_id'],1);
                        $description[] = sprintf($this->language->get('text_description_document_field'), $field_info['name'] ?? "");
                    }                    
                    break;
                case "doctype_list":
                    if (!empty($params['doctype_uid'])) {
                        $doctype_info = $this->model_doctype_doctype->getDoctype($params['doctype_uid']);
                        $description[] = sprintf($this->language->get('text_description_doctype_list'), $doctype_info['name'] ?? "");
                        $params['action']['doctype_name'] = $doctype_info['name'] ?? "";
                    }        
                    break;
                case "doctype_field":
                    if (!empty($params['doctype_field_id'])) {
                        $field_info = $this->model_doctype_doctype->getField($params['doctype_field_id'],1);
                        $description[] = sprintf($this->language->get('text_description_doctype_field'), $field_info['name'] ?? "");
                    }           
                    break;
            }            
        }        
        if ($description) {
            if (!empty($params['field_result_id'])) {
                $field_info = $this->model_doctype_doctype->getField($params['field_result_id']);
                $description[] = sprintf($this->language->get('text_description_field_result'), $field_info['name']);
            }  

            return implode("; ", $description);
            
        }
        return $this->language->get('text_description_without_field_document');
        
//
//        if (!empty($params['field_document_uid'])) {
//            $this->load->model('doctype/doctype');            
//            $field_info = $this->model_doctype_doctype->getField($params['field_document_uid']);
//            $description[] = sprintf($this->language->get('text_description_field_document'), $field_info['name']);
//             if (!empty($params['field_result_id'])) {
//                $field_info = $this->model_doctype_doctype->getField($params['field_result_id']);
//                $description[] = sprintf($this->language->get('text_description_field_result'), $field_info['name']);
//            }  
//            return implode("; ", $description);
//        } else {
//            return $this->language->get('text_description_without_field_document');
//        }
        
    }
    
    
    /**
     * Метод возвращает форму действия для типа документа
     * @param type $data - массив, включающий doctype_uid, route_uid
     */
    public function getForm($data) {
//        print_r($data);exit;
        $this->load->language('action/selection');
        $this->load->language('doctype/doctype');
        $this->load->model('doctype/doctype');                
        if (!empty($data['action']['document_source'])) {
            switch ($data['action']['document_source']) {
                case "document_field":
                    if (!empty($data['action']['document_field_id'])) {
                        $field_info = $this->model_doctype_doctype->getField($data['action']['document_field_id'],1);
                        $data['action']['document_field_name'] = $field_info['name'] ?? "";
                    }                    
                    break;
                case "doctype_list":
                    if (!empty($data['action']['doctype_uid'])) {
                        $doctype_info = $this->model_doctype_doctype->getDoctype($data['action']['doctype_uid']);
                        $data['action']['doctype_name'] = $doctype_info['name'] ?? "";
                    }        
                    break;
                case "doctype_field":
                    if (!empty($data['action']['doctype_field_id'])) {
                        $field_info = $this->model_doctype_doctype->getField($data['action']['doctype_field_id'],1);
                        $data['action']['doctype_field_name'] = $field_info['name'] ?? "";
                    }           
                    break;
            }            
            if (!empty($data['action']['conditions'])) {
                foreach ($data['action']['conditions'] as &$condition) {   
                    $field_info_1 = $this->model_doctype_doctype->getField($condition['field_1_id']);
                    $doctype_info_1 = $this->model_doctype_doctype->getDoctype($field_info_1['doctype_uid']);
                    $doctype_name_1 = "";
                    if (isset($doctype_info_1['name'])) {
                        $doctype_name_1 = $doctype_info_1['name'] . " - ";
                    }
                    $field_info_2 = $this->model_doctype_doctype->getField($condition['field_2_id']);
                    if ($data['action']['document_source'] !== "doctype_list") {
                        $condition['field_1_name'] = $doctype_name_1;
                    } else {
                        $condition['field_1_name'] = "";
                    }
                    $condition['field_1_name'] .= isset($field_info_1['name']) ? $field_info_1['name'] : "";
                    $condition['field_2_name'] = isset($field_info_2['name']) ? $field_info_2['name'] : "";
                }
            }

        }
        if (!empty($data['action']['field_result_id'])) {           
            $field_info = $this->model_doctype_doctype->getField($data['action']['field_result_id']);
            $data['action']['field_result_name'] = $field_info['name'];
        }
        return $this->load->view('action/selection/selection_form', $data);        
    }
    
     /**
     * Возвращает неизменяемую информацию о действии
     * @return array()
     */
    public function getActionInfo() {
        return $this::ACTION_INFO;
    }
    

    /**
     * 
     * @param type $data  = array('document_uid', 'button_uid', 'params');
     */
    public function executeButton($data) {
    }
    

    public function executeRoute($data) {
        $this->load->language('action/selection');
        if (!empty($data['params']['document_source']) && !empty($data['params']['field_result_id'])) {
            $this->load->model('document/document');
            $this->load->model('doctype/doctype');
            $document_uid = $data['document_uid'];
            switch ($data['params']['document_source']) {
                case "document_field":
                    $value = $this->model_document_document->getFieldValue($data['params']['document_field_id'], $document_uid);  
                    $data_docs = array(
                        'filter_names'  => array(),
                        'document_uids'  => explode(",", $value)
                    );                    
                    break;
                case "doctype_list":
                    $data_docs = array(
                        'filter_names'  => array(),
                        'doctype_uids'  => array($data['params']['doctype_uid'])
                    );    
                    break;
                case "doctype_field":
                    $value = $this->model_document_document->getFieldValue($data['params']['doctype_field_id'], $document_uid);  
                    $data_docs = array(
                        'filter_names'  => array(),
                        'doctype_uids'  => explode(",", $value)
                    );                               
                    break;
            }            
                  
            if (!empty($data['params']['conditions'])) {
                foreach ($data['params']['conditions'] as $condition) {
                    $field_2_value = $this->model_document_document->getFieldValue($condition['field_2_id'], $document_uid);
                    $data_docs['filter_names'][$condition['field_1_id']][] = array(
                        'value'         => $field_2_value ? $field_2_value : "", //условие нужно, чтобы не передавать NULL
                        'comparison'    => $condition['comparison'],
                        'concat'        => !empty($condition['concat']) ? "or" : "and"
                    );
                }                
            }
            $result = $this->model_document_document->editFieldValue($data['params']['field_result_id'], $document_uid, implode(",",$this->model_document_document->getDocumentIds($data_docs)));
            return $result;                            
                                  
        } else {
            return array(
                'log'   => $this->language->get('error_document_not_found')
            );
        }       
        
    }
 
}