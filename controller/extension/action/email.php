<?php

/**
 * @package		Documentov
 * @author		Andrey V Surov
 * @copyright           Copyright (c) 2018 Andrey V Surov, Roman V Zhukov (https://www.documentov.com/)
 * @license		https://opensource.org/licenses/mit-license.php
 * @link		https://www.documentov.com
 */
class ControllerExtensionActionEmail extends Controller {

    const ACTION_INFO = array(
        'name' => 'email',
        'inRouteContext' => true,
        'inRouteButton' => true,
        'inFolderButton' => true);

    public function index() {
        $this->load->language('extension/action/email');

        $data['cancel'] = $this->url->link('marketplace/extension', 'type=action', true);

        $this->response->setOutput($this->load->view('extension/action/email', $data));
    }

    public function install() {
        
    }

    public function uninstall() {
        
    }

    /**
     * Метод возвращает название действия в соответствии с выбранным языком
     * @return type
     */
    public function getTitle() {

        $this->language->load('extension/action/email');
        return $this->language->get('heading_title');
    }

    /**
     * Метод позволяет изменить сохраняемые в базу параметры действия (при необходимости)
     * @param type $data
     * @return type
     */
    public function setParams($data) {
        $this->load->model('doctype/doctype');
        if (!empty($data['route_action_uid'])) {
            $route_action_info = $this->model_doctype_doctype->getRouteAction($data['route_action_uid']);
            $route_info = $this->model_doctype_doctype->getRoute($route_action_info['route_uid']);
            $doctype_uid = $route_info['doctype_uid'];
        } elseif (!empty($data['route_uid'])) {
            $route_info = $this->model_doctype_doctype->getRoute($data['route_uid']);
            $doctype_uid = $route_info['doctype_uid'];
        } elseif (!empty($data['folder_uid'])) {
            $this->load->model('doctype/folder');
            $folder_info = $this->model_doctype_folder->getFolder($data['folder_uid']);
            $doctype_uid = $folder_info['doctype_uid'];
        } elseif (!empty($data['folder_button_uid'])) {
            $this->load->model('doctype/folder');
            $button_info = $this->model_doctype_folder->getButton($data['folder_button_uid']);
            $folder_info = $this->model_doctype_folder->getFolder($button_info['folder_uid']);
            $doctype_uid = $folder_info['doctype_uid'];
        } elseif (!empty($data['route_button_uid'])) {
            $button_info = $this->model_doctype_doctype->getRouteButton($data['route_button_uid']);
            $route_info = $this->model_doctype_doctype->getRoute($button_info['route_uid']);
            $doctype_uid = $route_info['doctype_uid'];
        }

        foreach ($data['params']['action']['template_message'] as &$template) {
            $template = $this->model_doctype_doctype->getIdsTemplate($template, $doctype_uid, $this->model_doctype_doctype->getTemplateVariables());                        
        }
        foreach ($data['params']['action']['template_subject'] as &$template) {
            $template = $this->model_doctype_doctype->getIdsTemplate($template, $doctype_uid, $this->model_doctype_doctype->getTemplateVariables());            
        }
        return $data['params']['action'];
    }

    /**
     * Метод возвращает описание действия, исходя из параметров
     */
    public function getDescription($params) {
        $this->load->language('action/email');
        $result = $this->language->get('description_none_email');
        if (!empty($params['field_email_id'])) {
            $this->load->model('doctype/doctype');
            $field_info = $this->model_doctype_doctype->getField($params['field_email_id']);
            if (isset($field_info['name'])) {
                $result = sprintf($this->language->get('description_send_email'), $field_info['name']);
                if (!empty($params['template_subject'][$this->config->get('config_language')])) {
                    $result .= " " . sprintf($this->language->get('description_email_subject'), $params['template_subject'][$this->config->get('config_language')]);
                }
            }
        }
        return $result;
    }

    /**
     * Метод возвращает форму действия для типа документа
     * @param type $data - массив, включающий doctype_uid, route_uid
     */
    public function getForm($data) {
        $this->load->language('action/email');
        $this->load->model('doctype/doctype');
        $data['fields'] = $this->model_doctype_doctype->getFields(array('doctype_uid' => $data['doctype_uid']));
        if (!empty($data['action']['field_email_id'])) {
            $field_info = $this->model_doctype_doctype->getField($data['action']['field_email_id']);
            if (isset($field_info['name'])) {
                $data['action']['field_email_name'] = $field_info['name'];
            }
        }
        if (isset($data['action']['template_message'])) {
            if (isset($data['route_uid'])) {
                $route_info = $this->model_doctype_doctype->getRoute($data['route_uid']);
                $doctype_uid = $route_info['doctype_uid'];
            } elseif (isset($data['doctype_uid'])) {
                $doctype_uid = $data['doctype_uid'];
            } elseif (isset($data['folder_uid'])) {
                $this->load->model('doctype/folder');
                $folder_info = $this->model_doctype_folder->getFolder($data['folder_uid']);
                $doctype_uid = $folder_info['doctype_uid'];
            }
       
            foreach ($data['action']['template_message'] as &$template) {
                $template = $this->model_doctype_doctype->getNamesTemplate($template, $doctype_uid, $this->model_doctype_doctype->getTemplateVariables());                
            }
            foreach ($data['action']['template_subject'] as &$template) {
                $template = $this->model_doctype_doctype->getNamesTemplate($template, $doctype_uid, $this->model_doctype_doctype->getTemplateVariables());
            }
        }

        $this->load->model('localisation/language');
        $data['languages'] = $this->model_localisation_language->getLanguages();
        return $this->load->view('action/email/email_form', $data);
    }

    /**
     * Возвращает неизменяемую информацию о действии
     * @return array()
     */
    public function getActionInfo() {
        return $this::ACTION_INFO;
    }

    public function executeButton($data) {
        if (isset($data['document_uid'])) { //есть document_uid - запуск действия из документ
            $result = $this->executeRoute($data);
//            $reload = str_replace('&amp;', '&', $this->url->link('document/document', 'document_uid=' . $data['document_uid'] . '&_=' . rand(100000000, 999999999)));
//            if (is_array($result)) {
//                $result['reload'] = $reload;
//            }
//            else {
//                $result = array(
//                    'reload'=>$reload, 
//                    'log'=>"");
//            }
        } else { //запуск из журнала
            foreach ($data['document_uids'] as $document_uid) {
                $data['document_uid'] = $document_uid;
                $result = $this->executeRoute($data);
            }
//            $result = array(
//                'reload' => 'table',
//                'log' => ""
//            );
        }
        return ($result);
    }

    /**
     * 
     * @param type $data  = array('document_uid', 'button_uid', 'params');
     */
    public function executeRoute($data) {
        $log = array();
        $this->language->load('action/email');
        if (!empty($data['params']['field_email_id'])) {
            $this->load->model('doctype/doctype');
            //кому отправляем почту
            $to = $this->model_document_document->getFieldValue($data['params']['field_email_id'], $data['document_uid']);
            if (!$to) {
                $log[] = $this->language->get('text_error_empty_to');
            } else {
                //определяем языки для писем, для этого ищем адреса почты в справочнике Пользователи
                $addresses = array();
                $templates = array();
                $this->load->model('extension/action/email');
                foreach (explode(",", $to) as $email) {
                    //адресатов может быть несколько
                    $email = trim($email);
                    if (preg_match('/^((([0-9A-Za-z]{1}[-0-9A-z\.]{1,}[0-9A-Za-z]{1})|([0-9А-Яа-я]{1}[-0-9А-я\.]{1,}[0-9А-Яа-я]{1}))@([-A-Za-z]{1,}\.){1,2}[-A-Za-z]{2,})$/u', $email)) {
                        //пытаемся найти емайл в справочнике пользователей, чтобы определить язык
                        $language_id = $this->model_extension_action_email->getLanguageId($email);
                        if (!$language_id) {
                            $language_id = $this->config->get('config_language_id');
                        }
                        $addresses[$email] = $language_id;
                        $templates[$language_id] = array(
                            'subject' => "",
                            'message' => ""
                        );
                    } else {
                        $log[] = sprintf($this->language->get('text_error_wrong_email'), $email);
                    }
                }
                //формируем шаблоны
                $document_info = $this->model_document_document->getDocument($data['document_uid'],false);
                $data_template = array(
                    'document_uid'      => $data['document_uid'],
                    'doctype_uid'       => $document_info['doctype_uid'],
                    'draft'             => FALSE,
                    'mode'              => 'view'
                );

                $header = $this->load->view('action/email/mail_header', array());
                $footer = $this->load->view('action/email/mail_footer', array());
                foreach ($templates as $language_id => &$template) {
                    $data_template['template'] = $data['params']['template_subject'][$language_id];
                    $template['subject'] = strip_tags(htmlspecialchars_decode($this->load->controller('document/document/renderTemplate',$data_template)));
                    $data_template['template'] = $data['params']['template_message'][$language_id];
                    $template['message'] = $header . str_replace(array("<br>","<p>"),array("<br>\r\n","\r\n<p>"),htmlspecialchars_decode($this->load->controller('document/document/renderTemplate',$data_template))) . $footer;
                }


                //добавляем задание в очередь демона
                $this->load->model('daemon/queue');
                $emails = array();
                foreach ($addresses as $email => $language_id) {
                    $data_daemon = array(
                        'to' => $email,
                        'subject' => $templates[$language_id]['subject'],
                        'message' => $templates[$language_id]['message']
                    );
                    $this->model_daemon_queue->addTask('tool/mail/send', $data_daemon);
                    $emails[] = $email;
                }
                $log[] = $this->language->get('text_mail_send') . " " . implode(", ", $emails); 
            }
        }
        return array(
            'log'   => implode(". ", $log)
        );
//        $log = "ALL OK";
//        if(!empty($data['params']['move_route_uid'])) {
//            $this->model_document_document->moveRoute($data['document_uid'], $data['params']['move_route_uid']);
//            $log .= " move to " . $data['params']['move_route_uid'];
//        }
//        return array(
//            'log'   => $log
//        );
    }

}
