<?php

/**
 * @package		Documentov
 * @author		Andrey V Surov
 * @copyright           Copyright (c) 2018 Andrey V Surov, Roman V Zhukov (https://www.documentov.com/)
 * @license		https://opensource.org/licenses/mit-license.php
 * @link		https://www.documentov.com
*/


class ControllerExtensionActionRemove extends Controller {
    const ACTION_INFO = array(
        'name'              => 'remove',
        'inFolderButton'    => true,
        'inRouteButton'     => true,
        'inRouteContext'     => true
        );
    
    public function index() {
        $this->load->language('extension/action/remove');

        $data['cancel'] = $this->url->link('marketplace/extension', 'type=action', true);

        $this->response->setOutput($this->load->view('extension/action/dialog', $data));
    }

    public function install() {

    }
    
    public function uninstall() {

    }   
    
    /**
     * Метод возвращает название действия в соответствии с выбранным языком
     * @return type
     */
    public function getTitle() {
        
        $this->language->load('extension/action/remove');
        return $this->language->get('heading_title');
    }
    
    /**
     * Метод позволяет изменить сохраняемые в базу параметры действия (при необходимости)
     * @param type $data
     * @return type
     */
    public function setParams($data) {
        return $data['params']['action'];
    }
    
    /**
     * Метод возвращает описание действия, исходя из параметров
     */
    public function getDescription($params) {
        $this->load->language('action/remove');
        $this->load->model('doctype/doctype');
        if (empty($params['field_document_uid'])) {
            return $this->language->get('text_description_current_doc');
        } else {
            $field_document_info = $this->model_doctype_doctype->getField($params['field_document_uid']);
            return sprintf($this->language->get('text_description_field_doc'), $field_document_info['name']);
        }
    }
    
    
    /**
     * Метод возвращает форму действия для типа документа
     * @param type $data - массив, включающий doctype_uid, route_uid
     */
    public function getForm($data) {
        $this->load->language('action/remove');
        $this->load->model('localisation/language');
        $data['languages'] = $this->model_localisation_language->getLanguages();
        if (empty($data['folder'])) {
            if (empty($data['action']['field_document_uid'])) {
                $data['action']['field_document_uid'] = 0;
            } else {
                $field_document_info = $this->model_doctype_doctype->getField($data['action']['field_document_uid']);
                $data['field_document_name'] = $this->language->get('text_by_link_in_field') . ' &quot' . $field_document_info['name'] . '&quot';
                $data['field_document_setting'] = $field_document_info['setting'];
            }                    
        }
        return $this->load->view('action/remove/remove_form', $data); 
    }
    
    /**
     * Возвращает неизменяемую информацию о действии
     * @return array()
     */
    public function getActionInfo() {
        return ControllerExtensionActionRemove::ACTION_INFO;
    }
    

    /**
     * 
     * @param type $data  = array('document_uid', 'button_uid', 'params');
     */
    public function executeButton($data) {
        $this->load->model('document/document');
        $this->load->language('action/remove');
        if (isset($data['document_uids']) && !$data['document_uids']) {            
            //запуск из журнала, ни один из документов не выбран
            $result = array(
                'window' => $this->load->view('action/remove/remove_unselect_folder_window', array())                            
            );
        } elseif (!empty($data['params']['confirm'][$this->config->get('config_language_id')]) && $this->request->server['REQUEST_METHOD'] != 'POST') {
            //нужно запрашивать подтверждение
            $data_window = array(
                'button_uid'        => $data['button_uid'],
                'document_uid'      => isset($data['document_uid']) ? $data['document_uid'] : 0,
                'document_uids'     => isset($data['document_uids']) ? implode(",", $data['document_uids']) : 0,
                'text_confirm'      => $data['params']['confirm'][$this->config->get('config_language_id')]
            );            
            $result = array(
                'window' => $this->load->view('action/remove/remove_confirm_window', $data_window)                            
            );            
        } else {
            //выполняем удаление
            if (isset($data['document_uid'])) {               
                //запуск из документа
                if (empty($data['params']['field_document_uid'])) {
                    //удаляется текущий документ
                    $error = $this->model_document_document->removeDocument($data['document_uid']);
                    $result = array();                
                    if ($error) {
                        $result['error'] = $error;
                    } else {
                        $result = array(
                                    'reload'    => $this->url->link('info/success','')
                        );
                    }
                } else {
                    //удаление документа (или несколько документов) из какого-то поля
                    $field_value = $this->model_document_document->getFieldValue($data['params']['field_document_uid'], $data['document_uid']);
                    $result = array(
//                        'reload' => str_replace('&amp;', '&', $this->url->link('document/document','document_uid=' . $data['document_uid'] . '&_=' . rand(100000000, 999999999))),
                        'log'      => $this->language->get('text_log')
                    );                   
                    if ($field_value) {
                        foreach (explode(",", $field_value) as $document_uid) {
                            $error = $this->model_document_document->removeDocument($document_uid);
                            //если был удален текущий документ меняем редирект
                            if ($document_uid == $data['document_uid']) {
                                $result = array(
//                                    'reload'    => $this->url->link('info/success','')
                                );                                
                            }
                            if ($error) {
                                $result = array('error' => $error);
                            }                            
                        }
                    }
                    
                }
            } elseif (isset($data['document_uids'])) {            
                //запуск из журнала
                foreach ($data['document_uids'] as $document_uid) {
                    $error = $this->model_document_document->removeDocument($document_uid);
                    if ($error) {
                        $result = array('error' => $error);
                    }                    
                }   
                if (empty($result['error'])) {
                    $result = array(
                        'reload'    => 'table',
                        'log'       => $this->language->get('text_log')
                    );                                    
                }
            }

        }    
        return $result ?? array();
    }
    
    //у Редактирования не должно быть этого метода, это просто демонстрация
    /**
     * 
     * @param type $data  = array('document_uid', 'button_uid', 'params');
     */
    public function executeRoute($data) {
        $this->load->model('document/document');
        if (empty($data['params']['field_document_uid'])) {
            //текущий документ
            $error = $this->model_document_document->removeDocument($data['document_uid']);
            if ($error) {
                return array('error' => $error);
            } else {
                return array('reload'    => $this->url->link('info/success','')); //при reload в плиточ журнале не показывается страница
            }    
        } else {
            //документы из поля
            $field_value = $this->model_document_document->getFieldValue($data['params']['field_document_uid'], $data['document_uid']);
            $result = array(
                'reload' => str_replace('&amp;', '&', $this->url->link('document/document','document_uid=' . $data['document_uid'] . '&_=' . rand(100000000, 999999999))),
                'log'      => $this->language->get('text_log')
            );                   
            if ($field_value) {
                foreach (explode(",", $field_value) as $document_uid) {
                    $error = $this->model_document_document->removeDocument($document_uid);
                    if ($error) {
                        $result = array('error' => $error);
                    } elseif ($document_uid == $data['document_uid']) {
                        $result = array(
                            'reload'    => $this->url->link('info/success','')
                        );                                
                    }
                }
            }

        }
    }
 
}