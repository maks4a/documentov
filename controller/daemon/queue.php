<?php
/**
 * @package		Documentov
 * @author		Roman V Zhukov
 * @copyright           Copyright (c) 2018 Andrey V Surov, Roman V Zhukov (https://www.documentov.com/)
 * @license		https://opensource.org/licenses/GPL-3.0
 * @link		https://www.documentov.com
*/

class ControllerDaemonQueue extends Controller {

    public function index() {
        
    }

    public function runTask($task_id) {
        $this->load->model('daemon/queue');
        print_r("RUNTASK: " . $task_id . ". See " . DIR_STORAGE . "logs/daemontask.log for more information\r\n");
        ob_start();
        $now = new DateTime('now', new DateTimeZone($this->config->get('date.timezone')));
        if ($task_id != "service") {
            //получили номер задачи
            $sql = "SELECT * FROM " . DB_PREFIX . "daemon_queue WHERE "
                    . "task_id = '" . (int) $task_id . "'";
            $query = $this->db->query($sql);
            if (!$query->num_rows) {
                print("\r\n" . $now->format('Y-m-d H:i:s') . "\tTASK: " . $task_id . " not found");                            
            } else {
                $action = $query->row['action'];

                $params = unserialize($query->row['action_params']);
                $exec_attempt = (int) $query->row['exec_attempt'];
                $exec_attempt++;
                $this->db->query("UPDATE " . DB_PREFIX . "daemon_queue SET "
                        . "start_time=NOW(), "
                        . "status=1, "
                        . "exec_attempt=" . $exec_attempt . ", "
                        . "pid=" . getmypid() . " "
                        . "WHERE task_id=" . (int) $task_id);

                try {
                    $this->load->controller($action, $params);
                } catch (Exception $e) {
                    print("\r\n" . $now->format('Y-m-d H:i:s') . "\tTASK: " . $task_id . " " . $e->getMessage());
                } catch (Error $e) {
                    print("\r\n" . $now->format('Y-m-d H:i:s') . "\tTASK: " . $task_id . " " . $e->getMessage());
                }
                $this->db->query("DELETE FROM " . DB_PREFIX . "daemon_queue WHERE "
                        . "task_id=" . (int) $task_id);                
            }
        } else {
            //запускаем сервисную задачу
            $this->runServiceTask();
        }
        $logfile = DIR_STORAGE . "logs/daemontask.log";
        $fp = fopen($logfile, 'a');
        fwrite($fp, ob_get_contents());
        fclose($fp);
        ob_end_clean();
    }
    
    public function runServiceTask() {
        $this->load->model('setting/variable');        
        $this->load->model('doctype/doctype');        
        date_default_timezone_set($this->config->get('date.timezone'));
        //АКТУАЛИЗАЦИЯ ДАННЫХ по подписке
        //проверям на запущенный процесс актуализации
        if (!empty($this->variable->get('subscription_started'))) {
            return;
        }
        //устанавливаем признак работающей актуализации
        $this->model_setting_variable->setVar("subscription_started", 1);
        try {
            //получаем установленные поля      
            $this->load->model('setting/extension');
            $fields = $this->model_setting_extension->getInstalled('field');
            $priority_fields = array(); //поля, которые будут обрабатываться при каждом запуске
            foreach (array('string','string_plus') as $f) {
                if (array_search($f, $priority_fields) !== FALSE) {
                    $priority_fields[] = $f; //после установлено, добавляем его в приоритетные
                }
            }
            
            
            //получаем тип поля для актуализации        
            $last_field = $this->variable->get('subscription_last_field');
            $key = array_search($last_field, $fields) ?? 0;
            $i = 0;
            do {
                $field = $fields[++$key] ?? $fields[$i++];
            } 
            while ($field == "" || array_search($field,$priority_fields) !== FALSE);           
            
            //получаем время последней проверки
            $last_time = $this->variable->get('subscription_time_' . $field) ?? date('Y-m-d H:i:s'); 
            $now = date('Y-m-d H:i:s');
            //записываем последнее проверенное поле
            $this->model_setting_variable->setVar("subscription_last_field", $field);
            //записываем время проверки поля
            $this->model_setting_variable->setVar("subscription_time_" . $field, $now);
          
            //получаем измененные поля, на которые есть подписка
            $subscriptions = $this->model_daemon_queue->getFieldChangeSubscriptions($field, $last_time, $now);
            foreach ($priority_fields as $pf) {
                $subscriptions = array_merge($subscriptions,$this->model_daemon_queue->getFieldChangeSubscriptions($pf, $this->variable->get('subscription_time_' . $pf) ?? $now, $now));
                $this->model_setting_variable->setVar("subscription_time_" . $pf, $now); 
            }              
            if ($subscriptions) {
                $this->load->model('doctype/doctype');
                
                $fields = array();
                foreach ($subscriptions as $subscription) {
                    $fields[$subscription['subscription_field_uid']][] = $subscription['subscription_document_uid'];
                }
                foreach ($fields as $field_uid => $documents) {
                    $field_info = $this->model_doctype_doctype->getField($field_uid);
                    if (empty($field_info['type'])) {
                        $this->model_doctype_doctype->delSubscription($field_uid);
                        continue;
                    }
                    $documents = array_unique($documents);
                    print_r("\r\n" . date('Y-m-d H:i:s') . "\tSERVICE TASK SUBSCRIPTION ON CHANGE: Actualized field " . $field_info['type'] . " (ID=" . $field_uid . ") by subscription ");
                    $model = "model_extension_field_" . $field_info['type'];
                    $this->load->model('extension/field/' . $field_info['type']);
                    echo $this->$model->subscription($field_uid, $documents);

                }
            } 
        } catch (Exception $e) {
            print_r("\r\n" . date('Y-m-d H:i:s') . "\tSERVICE TASK SUBSCRIPTION ON CHANGE: " . $e->getMessage() . " ");
//            if ($field == "pie_diagram") {
//                $model = "model_extension_field_" . $field;
//                $this->load->model('extension/field/' . $field);
//                echo $this->$model->install();
//            }
        }
        finally {
            //завершаем процесс
            $this->model_setting_variable->setVar("subscription_started", 0);
        }        
        
    }

    
}
