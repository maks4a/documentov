<?php

class ModelExtensionServiceExport extends Model {
    
    public function getDoctype($doctype_uid) {
        $doctype_uid = $this->db->escape($doctype_uid);
        
        $result = array();
        
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "doctype WHERE doctype_uid='" . $doctype_uid . "'");
        $result['doctype'] = $query->rows;
        
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "doctype_description WHERE doctype_uid='" . $doctype_uid . "'");
        $result['doctype_description'] = $query->rows;
        
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "doctype_template WHERE doctype_uid='" . $doctype_uid . "'");
        $result['doctype_template'] = $query->rows;
        
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "field WHERE doctype_uid='" . $doctype_uid . "'");
        $result['field'] = $query->rows;
        
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "route WHERE doctype_uid='" . $doctype_uid . "'");
        $result['route'] = $query->rows;
        
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "route_description WHERE route_uid IN (SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid = '" . $doctype_uid . "') ");
        $result['route_description'] = $query->rows;
        
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "route_action WHERE route_uid IN (SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid = '" . $doctype_uid . "') ");
        $result['route_action'] = $query->rows;
        
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "route_button WHERE route_uid IN (SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid = '" . $doctype_uid . "') ");
        $result['route_button'] = $query->rows;
        
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "route_button_description WHERE route_button_uid IN (SELECT route_button_uid FROM " . DB_PREFIX . "route_button WHERE route_uid IN (SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid = '" . $doctype_uid . "')) ");
        $result['route_button_description'] = $query->rows;
        
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "route_button_field WHERE route_button_uid IN (SELECT route_button_uid FROM " . DB_PREFIX . "route_button WHERE route_uid IN (SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid = '" . $doctype_uid . "')) ");
        $result['route_button_field'] = $query->rows;
                        
        //добавляем значения настроечных полей
        foreach ($result['field'] as $field) {
            if ($field['setting']) {
                $query_field_value = $this->db->query("SELECT * FROM " . DB_PREFIX . "field_value_" . $field['type'] . " WHERE field_uid='" . $field['field_uid'] . "' AND document_uid=0");
                $result["field_value_" . $field['type']][] = $query_field_value->row;
            }
        }
        
        return $result;
        
    }
    
    public function getFolder($folder_uid) {
        $folder_uid = $this->db->escape($folder_uid);
        
        $result = array();
        
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "folder WHERE folder_uid='" . $folder_uid . "'");
        $result['folder'] = $query->rows;

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "folder_description WHERE folder_uid='" . $folder_uid . "'");
        $result['folder_description'] = $query->rows;

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "folder_field WHERE folder_uid='" . $folder_uid . "'");
        $result['folder_field'] = $query->rows;

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "folder_filter WHERE folder_uid='" . $folder_uid . "'");
        $result['folder_filter'] = $query->rows;

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "folder_button WHERE folder_uid='" . $folder_uid . "'");
        $result['folder_button'] = $query->rows;        

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "folder_button_description WHERE folder_button_uid IN (SELECT folder_button_uid FROM folder_button WHERE folder_uid='" . $folder_uid . "') ");
        $result['folder_button_description'] = $query->rows;

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "folder_button_field WHERE folder_button_uid IN (SELECT folder_button_uid FROM folder_button WHERE folder_uid='" . $folder_uid . "') ");
        $result['folder_button_field'] = $query->rows;

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "folder_button_route WHERE folder_button_uid IN (SELECT folder_button_uid FROM folder_button WHERE folder_uid='" . $folder_uid . "') ");
        $result['folder_button_route'] = $query->rows;
        
        if ($result['folder'][0]['type']) {
            //экспортируется нестандарный журнал
            $query_folder_ext_table = $this->db->query("SELECT DISTINCT TABLE_NAME FROM information_schema.columns WHERE TABLE_SCHEMA='" . DB_DATABASE . "' AND TABLE_NAME LIKE '" . DB_PREFIX . $result['folder'][0]['type'] . "%' ");
            if ($query_folder_ext_table->num_rows) {
                foreach ($query_folder_ext_table->rows as $table) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . $table['TABLE_NAME'] . " WHERE folder_uid='" . $folder_uid . "'");
                    $result[$table['TABLE_NAME']] = $query->rows;                    
                }
            }
        }
        
        return $result;
    }
    
    public function addConfiguration($configuration) {
       
        $result = array(
            'doctype'   => array(),
            'folder'    => array(),
        );
        foreach ($configuration as $conf) {
            $not_found_tables = array();
            $not_found_fields= array();
            foreach ($conf as $table => $contents) {
                $query_table = $this->db->query("SELECT * FROM information_schema.columns WHERE TABLE_SCHEMA='" . DB_DATABASE . "' AND TABLE_NAME = '" . DB_PREFIX . $this->db->escape($table) . "'");
                if (!$query_table->num_rows) {
                    $not_found_tables[] = $table;
                }
                if ($table == "field") {
                    //проверяем на наличие необходимые для импорта типы полей
                    foreach ($contents as $field) {
                        $query_field = $this->db->query("SELECT extension_id FROM " . DB_PREFIX . "extension WHERE type='field' AND code='" . $this->db->escape($field['type']) . "' ");
                        if (!$query_field->num_rows) {
                            //поле не установлено
                            if(array_search($field['type'], $not_found_fields) === FALSE) {
                                $not_found_fields[]  = $field['type'];
                            }
                            
                        }
                        
                    }
                }
            }   
            $this->load->language('extension/service/export');
            $error = array();
            if ($not_found_tables) {
                $error[] = $this->language->get('error_no_table') . " " . implode(", ", $not_found_tables);                
            }
            if ($not_found_fields) {
                $error[] = $this->language->get('error_no_field') . " " . implode(", ", $not_found_fields);                
            }
            if ($error) {
                return array(
                    'error' => implode("<br>", $error)
                );
                
            }            
        }    
        foreach ($configuration as $conf) {
            foreach ($conf as $table => $contents) {    
                foreach ($contents as $content) {

                    if ($table == "doctype") {
                        //удаляем все записи с UID = импортируемому
                        $this->db->query("DELETE FROM " . DB_PREFIX . "doctype_description WHERE doctype_uid='" . $this->db->escape($content['doctype_uid']) . "'");
                        $this->db->query("DELETE FROM " . DB_PREFIX . "doctype_template WHERE doctype_uid='" . $this->db->escape($content['doctype_uid']) . "'");
                        $this->db->query("DELETE FROM " . DB_PREFIX . "field WHERE doctype_uid='" . $this->db->escape($content['doctype_uid']) . "'");
                        $this->db->query("DELETE FROM " . DB_PREFIX . "route_description WHERE route_uid IN (SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid = '" . $this->db->escape($content['doctype_uid']) . "') ");
                        $this->db->query("DELETE FROM " . DB_PREFIX . "route_action WHERE route_uid IN (SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid = '" . $this->db->escape($content['doctype_uid']) . "') ");
                        $this->db->query("DELETE FROM " . DB_PREFIX . "route_button_description WHERE route_button_uid IN (SELECT route_button_uid FROM " . DB_PREFIX . "route_button WHERE route_uid IN (SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid = '" . $this->db->escape($content['doctype_uid']) . "')) ");
                        $this->db->query("DELETE FROM " . DB_PREFIX . "route_button_field WHERE route_button_uid IN (SELECT route_button_uid FROM " . DB_PREFIX . "route_button WHERE route_uid IN (SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid = '" . $this->db->escape($content['doctype_uid']) . "')) ");        
                        $this->db->query("DELETE FROM " . DB_PREFIX . "route_button WHERE route_uid IN (SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid = '" . $this->db->escape($content['doctype_uid']) . "') ");
                        $this->db->query("DELETE FROM " . DB_PREFIX . "route WHERE doctype_uid='" . $this->db->escape($content['doctype_uid']) . "'");             
                        $this->db->query("DELETE FROM " . DB_PREFIX . "doctype WHERE doctype_uid='" . $this->db->escape($content['doctype_uid']) . "'");                        
                        $result['doctype'][] = $content['doctype_uid'];
                    }   
                    if ($table == "folder") {
                        //удаляем все записи с UID = импортируемому
                        $this->db->query("DELETE FROM " . DB_PREFIX . "folder WHERE folder_uid='" . $this->db->escape($content['folder_uid']) . "'");
                        $this->db->query("DELETE FROM " . DB_PREFIX . "folder_description WHERE folder_uid='" . $this->db->escape($content['folder_uid']) . "'");
                        $this->db->query("DELETE FROM " . DB_PREFIX . "folder_field WHERE folder_uid='" . $this->db->escape($content['folder_uid']) . "'");
                        $this->db->query("DELETE FROM " . DB_PREFIX . "folder_filter WHERE folder_uid='" . $this->db->escape($content['folder_uid']) . "'");
                        $this->db->query("DELETE FROM " . DB_PREFIX . "folder_button_description WHERE folder_button_uid IN (SELECT folder_button_uid FROM folder_button WHERE folder_uid='" . $this->db->escape($content['folder_uid']) . "') ");
                        $this->db->query("DELETE FROM " . DB_PREFIX . "folder_button_field WHERE folder_button_uid IN (SELECT folder_button_uid FROM folder_button WHERE folder_uid='" . $this->db->escape($content['folder_uid']) . "') ");
                        $this->db->query("DELETE FROM " . DB_PREFIX . "folder_button_route WHERE folder_button_uid IN (SELECT folder_button_uid FROM folder_button WHERE folder_uid='" . $this->db->escape($content['folder_uid']) . "') ");
                        $this->db->query("DELETE FROM " . DB_PREFIX . "folder_button WHERE folder_uid='" . $this->db->escape($content['folder_uid']) . "'");
                        $result['folder'][] = $content['folder_uid'];
                    }   
                    if (strpos($table, "field_value") !== false && $content) {
                        //удаляем настроечные поля, чтобы иметь возможность перенести настроенный конфиг документа
                        $this->db->query("DELETE FROM " . DB_PREFIX . $this->db->escape($table) . " WHERE field_uid='" . $this->db->escape($content['field_uid']) . "' AND document_uid='0'");
                    }                    
                        
                    $sql = "INSERT INTO " . DB_PREFIX . $this->db->escape($table) . " SET ";
                    $sets = array();

                    foreach ($content as $name => $value) {
                        if ($name) {
                            $sets[] = "`" . $this->db->escape($name) . "` = '" . $this->db->escape($value) . "' ";
                        }                        
                    }
                    if ($sets) {
                        $sql .= implode(",", $sets);
                        try {
                            $this->db->query($sql);                                                            
                        } catch (Exception $e) {
                            $this->db->query("DELETE FROM "  . DB_PREFIX . $this->db->escape($table) . " WHERE " . implode(" AND ", $sets));
                            $this->db->query($sql);
                        }                                            
                        
                    }
                    
                }            
            }
            
        }
        return $result ?? "";
//        $this->db->query($sql);
    }
    
}

