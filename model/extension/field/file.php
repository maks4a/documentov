<?php

class ModelExtensionFieldFile extends FieldModel {
    
    const DIR_FILE_UPLOAD = DIR_DOWNLOAD . "field_file/";
    
    public function editValue($field_uid, $document_uid, $value) {
        //очищаем $value
        $value_clear = array();
        if ($value) {
            if (!is_array($value)) {
                $value = explode(",", $value);
            }    
            foreach ($value as $v)  {
                if ($v) {
                    $value_clear[] = trim($this->db->escape($v));
                }
            }
        }
        $value_clear = array_unique($value_clear);
        
        //в поле были файлы следующие
        $field_value = $this->getValue($field_uid, $document_uid);
        
        //удаляем существующие связи для данного поля документа
        $this->db->query("DELETE FROM " . DB_PREFIX . "field_value_file_relation WHERE field_uid = '" . $this->db->escape($field_uid) . "' "
                . "AND document_uid = '" . $this->db->escape($document_uid) . "' ");
        $displays = array();
        if ($value_clear) {
            //формируем новые связи для данного поля документа
            $file_rows = array();
            foreach ($value_clear as $file_uid) {
                if (strlen(trim($file_uid)) == 36) {
                    $file_rows[] = "('" . trim($file_uid) . "','" . $this->db->escape($field_uid) . "','" . $this->db->escape($document_uid) . "')";
                    $file_info = $this->getFile($file_uid);
                    $displays[] = $file_info['file_name'];
                }

            }
            $this->db->query("INSERT INTO " . DB_PREFIX . "field_value_file_relation (file_uid,field_uid,document_uid) VALUES " . implode(", ", array_unique($file_rows)));            
        }

        //выполняем очистку

        if ($field_value) {
            $field_values = explode(",", $field_value);
            //сравниваем массивы
            $removed_file_uids = array_diff($field_values, $value_clear);//файлы, которые были удалены из поля документа
            //если удаленные из поля файлы не используются в других поля, то их можно удалить совсем
            foreach ($removed_file_uids as $removed_file_uid) {
                $query_remove_file = $this->db->query("SELECT file_uid FROM " . DB_PREFIX . "field_value_file_relation WHERE file_uid = '" . $removed_file_uid ."' ");
                if (!$query_remove_file->num_rows) {
                    //файл нигде больше не используется и его можно удалить
                    $this->removeFile($removed_file_uid);
                }
            }
        }
        
        //обновляем value в field_value_file
        $query = $this->db->query("SELECT value FROM " . DB_PREFIX . "field_value_file "
                . "WHERE field_uid='" . $field_uid . "' AND document_uid='" . $document_uid . "' ");
        if ($query->num_rows) {
            $this->db->query("UPDATE " . DB_PREFIX . "field_value_file SET "
                . "value='" . $this->db->escape(implode(", ", $displays)) . "', "
                . "display_value='" . $this->db->escape(implode(", ", $displays)) . "', "
                . "time_changed=NOW() "                    
                . "WHERE field_uid = '" . $this->db->escape($field_uid) . "' AND document_uid = '" . $this->db->escape($document_uid) . "' ");

        } else {
            $this->db->query("INSERT INTO " . DB_PREFIX . "field_value_file SET "
                . "document_uid='" . $this->db->escape($document_uid) . "', "
                . "field_uid='" . $this->db->escape($field_uid) . "', "
                . "value='" . $this->db->escape(implode(", ", $displays)) . "', "
                . "display_value='" . $this->db->escape(implode(", ", $displays)) . "' "
            );            
        }        
    }
    
    /**
     * Возвращает значение поля
     * @param type $field_uid
     * @param type $document_uid
     * @param type $widget_value - значение, получаемое от виджета поля; возвращается value, которое пишется в базу данных
     * @return type
     */
    public function getValue($field_uid, $document_uid, $widget_value='') {
        if (!$widget_value) {
//            $query = $this->db->query("SELECT DISTINCT value FROM " . DB_PREFIX . "field_value_file WHERE "
//                . "document_uid='" . $this->db->escape($document_uid) . "' AND "
//                . "field_uid='" . $this->db->escape($field_uid) . "' ");
            $query = $this->db->query("SELECT fvfr.file_uid FROM " . DB_PREFIX . "field_value_file_relation fvfr "
                    . "LEFT JOIN " . DB_PREFIX . "field_value_file_list fvfl ON (fvfl.file_uid = fvfr.file_uid) "
                    . "WHERE "
                    . "fvfr.document_uid='" . $this->db->escape($document_uid) . "' AND "
                    . "fvfr.field_uid='" . $this->db->escape($field_uid) . "' "
                    . "ORDER BY fvfl.date_added ASC");
            if($query->num_rows) {
                $result = array();
                foreach ($query->rows as $file) {
                    $result[] = $file['file_uid'];
                }
                return implode(",",$result);
            }            
        } else {
            if (is_array($widget_value)) {
                return implode(",", $widget_value);
            }
            return $widget_value;
        }
        
    }

    /**
     * Файл загружен пользователем через форму документа
     * @param type $document_uid
     * @param type $field_uid
     * @param type $file_name
     * @param type $size
     * @param type $token
     * @return type
     */
    public function addFile($field_uid,$file_name, $size, $token) {
        $query_uid = $this->db->query("SELECT UUID() as uid");
        $file_uid = $query_uid->row['uid'];
        $this->db->query("INSERT INTO " . DB_PREFIX . "field_value_file_list SET "
                . "file_uid = '" . $file_uid . "', "
                . "field_uid = '" . $this->db->escape($field_uid) . "', "
                . "file_name = '" . $this->db->escape($file_name) . "', "
                . "size = '" . (int) $size . "', " 
                . "token = '" . $this->db->escape($token) . "', "
                . "date_added = NOW(), "
                . "user_uid = ' " . $this->customer->getStructureId() . "' "
                );
        return $file_uid;
    }
    

    
    /**
     * Удаление файла. 
     * @param type $file_uid
     */    
    public function removeFile($file_uid) {
        $query_relation = $this->db->query("SELECT file_uid FROM " . DB_PREFIX . "field_value_file_relation WHERE file_uid = '" . $this->db->escape($file_uid) . "' ");
        if (!$query_relation->num_rows) {            
            //связей нет, можно удалять  
            $query_file = $this->db->query("SELECT * FROM " . DB_PREFIX . "field_value_file_list WHERE file_uid = '" . $this->db->escape($file_uid) . "' ");
            if ($query_file->num_rows) {
                unlink(DIR_DOWNLOAD . "field_file/" . $query_file->row['field_uid'] . date('/Y/m/',strtotime($query_file->row['date_added'])) . $query_file->row['token'] . $query_file->row['file_name']);            
                $this->db->query("DELETE FROM " . DB_PREFIX . "field_value_file_list WHERE file_uid = '" . $this->db->escape($file_uid) . "' ");
            }
        }
    }
    
    public function getFile($file_uid) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "field_value_file_list WHERE file_uid = '" . $this->db->escape($file_uid) . "'");
        return $query->row;
    }

    public function getFiles($file_uids) {
        if (is_array($file_uids)) {
            $files = $this->db->escape(implode(",", $file_uids));
            $files = str_replace(",", "','", $files);
        } else {
            $files = str_replace(",", "','", $this->db->escape($file_uids));
        }
        if ($files) {
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "field_value_file_list WHERE file_uid IN ('" . $files . "') ");
            return $query->rows;            
        }
    }
    
    public function refreshDisplayValues($data) {
    }    
    
    /**
     * Метод проверки права доступа текущего пользователя к файлу
     * @param type $file_uid
     */
    public function hasAccess($file_uid) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "document_access WHERE "
                . "document_uid IN (SELECT document_uid FROM " . DB_PREFIX . "field_value_file_relation "
                . "                 WHERE file_uid = '" . $this->db->escape($file_uid) . "') AND "
                . "                 subject_uid IN ('" . implode("','", $this->customer->getStructureIds()) . "')");
        return $query->num_rows>0;
    }
    
    public function removeValues($field_uid) {
        $query = $this->db->query("SELECT document_uid FROM " . DB_PREFIX . "field_value_file WHERE field_uid = '" . $this->db->escape($field_uid) . "' ");
        foreach ($query->rows as $document) {
            $this->removeValue($field_uid, $document['document_uid']);
        }
    }     
    
    /**
     * Метод, запускаемый при удалении документа
     * @param type $field_uid
     * @param type $document_uid
     */
    public function removeValue($field_uid, $document_uid) {
        $query_files = $this->db->query("SELECT file_uid FROM " . DB_PREFIX . "field_value_file_relation WHERE field_uid = '" . $this->db->escape($field_uid) . "' AND document_uid = '" . $this->db->escape($document_uid) . "' ");
        if ($query_files->num_rows) {            
            $this->db->query("DELETE FROM " . DB_PREFIX . "field_value_file_relation WHERE field_uid = '" . $this->db->escape($field_uid) . "' AND document_uid = '" . $this->db->escape($document_uid) . "' ");
            foreach ($query_files->rows as $file) {
                $this->removeFile($file['file_uid']);
            }
        }        
        $this->db->query("DELETE FROM " . DB_PREFIX . "field_value_file WHERE field_uid='" . $this->db->escape($field_uid) . "' AND document_uid='" . $this->db->escape($document_uid) . "' ");
    }    
    
    public function install() {
        //создаем таблицу поля
        $this->db->query("CREATE TABLE " . DB_PREFIX . "field_value_file ( `field_uid` VARCHAR(36) , `document_uid` VARCHAR(36) , `value` VARCHAR(255) NOT NULL, `display_value` VARCHAR(255) ) ENGINE = InnoDB CHARSET=utf8 COLLATE utf8_general_ci;");
        $this->db->query("CREATE TABLE " . DB_PREFIX . "field_value_file_list ( `file_uid` VARCHAR(36) , `field_uid` VARCHAR(36) , `document_uid` VARCHAR(36) , `file_name` VARCHAR(256) NOT NULL , `size` INT NOT NULL, `token` VARCHAR(32) NOT NULL , `date_added` DATETIME NOT NULL , `user_uid` INT NOT NULL, `status` TINYINT NOT NULL, PRIMARY KEY (file_uid), `time_changed` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;");
        $this->db->query("ALTER TABLE " . DB_PREFIX . "field_value_file ADD UNIQUE KEY field_uid (field_uid,document_uid)");
        $this->db->query("ALTER TABLE `field_value_file` ADD INDEX( `value`);");
        $this->db->query("ALTER TABLE `field_value_file` ADD INDEX( `display_value`);");
        $this->db->query("ALTER TABLE `field_value_file` ADD INDEX( `time_changed`);");
        $this->db->query("ALTER TABLE `field_value_file_list` ADD INDEX( `field_uid`);");
        $this->db->query("ALTER TABLE `field_value_file_list` ADD INDEX( `document_uid`);");
        $this->db->query("INSERT INTO " . DB_PREFIX . "setting SET code='dv_field_file', `key`='config_file_ext_allowed', value='zip,txt,png,jpe,jpeg,jpg,gif,bmp,ico,tiff,tif,svg,svgz,zip,rar,msi,cab,mp3,qt,mov,pdf,psd,ai,eps,ps,doc,xls,docx,xlsx,odt,dot,htm,html,ppt,pptx,rtf,xlsm', serialized=0");
        $this->db->query("INSERT INTO " . DB_PREFIX . "setting SET code='dv_field_file', `key`='config_file_mime_allowed', value='text/plain,image/png,image/jpeg,image/gif,image/bmp,image/tiff,image/svg+xml,application/zip,application/zip,application/x-zip,application/x-zip,application/x-zip-compressed,application/x-zip-compressed,application/rar,application/rar,application/x-rar,application/x-rar,application/x-rar-compressed,application/x-rar-compressed,application/octet-stream,application/octet-stream,audio/mpeg,video/quicktime,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel.sheet.macroEnabled.12,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation', serialized=0");
        $text = "order deny,allow\r\ndeny from all";
        if (!is_dir($this::DIR_FILE_UPLOAD)) {
            mkdir($this::DIR_FILE_UPLOAD);
        }
        $fp = fopen($this::DIR_FILE_UPLOAD . ".htaccess", "w");
        fwrite($fp, $text);
        fclose($fp);
    }

    public function uninstall() {
        //удаляем таблицу поля//
        $this->db->query("DROP TABLE " . DB_PREFIX . "field_value_file");
        $this->db->query("DROP TABLE " . DB_PREFIX . "field_value_file_list");
        $this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE code='dv_field_file' AND `key`='config_file_ext_allowed' ");
        $this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE code='dv_field_file' AND `key`='config_file_mime_allowed' ");
        
        $this->removeDirectory($this::DIR_FILE_UPLOAD);
    }    
    
    
    ////////////////////то, что ниже, удалить (?) ////////////
    

//    public function getFilesByNameSizeToken($name, $size, $field_uid = 0, $document_uid = 0) {
//        $sql = "SELECT * FROM " . DB_PREFIX . "field_value_file_list WHERE file_name = '" . $this->db->escape($name) . "' AND size='" . (int) $size . "' ";
//        if ($document_uid) {
//            $sql .= "AND document_uid = '" . $this->db->escape($document_uid) . "' ";
//            if ($field_uid) {
//                $query_field = $this->db->query("SELECT value FROM " . DB_PREFIX . "field_value_file WHERE field_uid = '" . $this->db->escape($field_uid) . "' AND document_uid='" . $this->db->escape($document_uid) . "' ");
//                $file_uids = explode(",", $query_field->row['value']);
//                $sql .= "AND file_uid IN ('" . implode("','", $file_uids) . "') ";
//            }        
//
//        }
//        
//        $query = $this->db->query($sql);
//
//        return $query->rows;
//    }
//
//
//    public function getFilesByField($field_uid, $document_uid) {
//        $query_field = $this->db->query("SELECT value FROM " . DB_PREFIX . "field_value_file WHERE field_uid = '" . $this->db->escape($field_uid) . "' AND document_uid='" . $this->db->escape($document_uid) . "' ");
//        $file_uids = explode(",", $query_field->row['value']);
//        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "field_value_file_list WHERE file_uid IN ('" . implode("','", $file_uids) . "')");
//        return $query->rows;
//    }    
//    
//    public function removeValue($field_uid, $document_uid) {
//        foreach ($this->getFilesByField($field_uid, $document_uid) as $file) {
//            $this->removeFile($file['file_uid']);
//        }
//        $this->db->query("DELETE FROM " . DB_PREFIX . "field_value_file WHERE field_uid = '" . $this->db->escape($field_uid) . "' AND document_uid = '" . $this->db->escape($document_uid) . "' ");
//        
//    }
//  
//    //метод для обработки удаления поля
  
    
}