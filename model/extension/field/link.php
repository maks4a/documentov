<?php

class ModelExtensionFieldLink extends FieldModel {

    public function editValue($field_uid, $document_uid, $field_value) {
        $this->load->model('document/document');
        $this->load->model('doctype/doctype');
        $field_info = $this->model_doctype_doctype->getField($field_uid);
        $display_value = array();
        if (!is_array($field_value)) {
            $field_value = explode(",", $field_value);
        }        
        //очищаем записываемое значение, удаляя ссылки на несуществующие документы
        $field_value = array_unique($field_value);
        $check_values = $this->model_document_document->getDocumentIds(array(
                                'document_uids' => array_unique($field_value),
                                'draft_less'    => 4
                        ));        
        $field_values = array_intersect($field_value, $check_values);

        if (empty($field_info['params']['doctype_field_uid'])) {
            //не задано отображаемое поле
            $data_f = array(
                'doctype_uid'   => $field_info['params']['doctype_uid'],
                'setting'       => 0,
                'limit'         => 1
            );            
            $fields = $this->model_doctype_doctype->getFields($data_f);
            $doctype_field_uid = $fields[0]['field_uid'] ?? "";    //берем первое по счету поле         
        } else {
            $doctype_field_uid = $field_info['params']['doctype_field_uid'];
        }        
       
        $field_value_db = implode(",", $field_values);
        if ($field_value_db) {
            $display_value = $this->getDisplay($document_uid, $field_uid, $field_value_db, $field_info);
        } else {
            $display_value = "";
        }        
        
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "field_value_link WHERE field_uid = '" . $this->db->escape($field_uid) . "' AND document_uid = '" . $this->db->escape($document_uid) . "' ");
        if ($query->num_rows) {
            $this->db->query("UPDATE " . DB_PREFIX . "field_value_link SET "
                    . "value='" . $this->db->escape($field_value_db) . "', "
                    . "display_value='" . $this->db->escape(trim(strip_tags($display_value))) . "', "
                    . "time_changed=NOW(), "
                    . "full_display_value='" . $this->db->escape(str_replace(", ",$field_info['params']['delimiter'],$display_value)) . "' "
                    . "WHERE field_uid = '" . $this->db->escape($field_uid) . "' AND document_uid = '" . $this->db->escape($document_uid) . "' ");
        } else {
            $this->db->query("INSERT INTO " . DB_PREFIX . "field_value_link SET "
                    . "document_uid='" . $this->db->escape($document_uid) . "', "
                    . "field_uid='" . $this->db->escape($field_uid) . "', "
                    . "value='" . $this->db->escape($field_value_db) . "', "
                    . "display_value='" . $this->db->escape(trim(strip_tags($display_value))) . "', "
                    . "full_display_value='" . $this->db->escape(str_replace(", ",$field_info['params']['delimiter'],$display_value)) . "' ");
        }
        //подписка на изменение отображаемого поля целевого документа
        $this->model_doctype_doctype->delSubscription($field_uid, $document_uid);
        $this->model_doctype_doctype->addSubscription($field_uid, $document_uid, $doctype_field_uid, $field_values);
    }


    /**
     * Метод возвращает полный дисплей ссылки (включая гиперссылки, если так стоит в настройках); множественное значение разделено запятыми
     * @param type $document_uid
     * @param type $field_uid
     * @param type $field_value - если не передано, получаем из БД
     * @param type $field_info - если не педеано, метод получает сам
     * @return type
     */
    public function getDisplay($document_uid, $field_uid, $field_value="", $field_info="") {
        $this->load->model('document/document');
        $this->load->model('doctype/doctype');
        if (!$field_value && $field_uid && $document_uid) {
            $field_value = $this->getValue($field_uid, $document_uid);
        }
        if (!$field_info) {
            $field_info = $this->model_doctype_doctype->getField($field_uid);
        }  
        if (!$field_value || !$field_info) {
            return "";
        }
        $display_value = array();        
        if (empty($field_info['params']['doctype_field_uid'])) {
            //не задано отображаемое поле
            $data_f = array(
                'doctype_uid'   => $field_info['params']['doctype_uid'],
                'setting'       => 0,
                'limit'         => 1
            );            
            $fields = $this->model_doctype_doctype->getFields($data_f);
            $doctype_field_uid = $fields[0]['field_uid'] ?? "";             
        } else {
            $doctype_field_uid = $field_info['params']['doctype_field_uid'];
        }        
        
        $field_values = explode(",", $field_value);
        if (!$field_info['params']['multi_select'] && !empty($field_values[0])) {
                $field_values = array($field_values[0]);
        }
        
        foreach ($field_values as $value) {
            if (!$value) { continue;}
            $data = array(
                'url'           => $this->url->link('document/document', 'document_uid=' . $value),
                'text'          => strip_tags(html_entity_decode($this->model_document_document->getFieldDisplay($doctype_field_uid, $value))),
                'document_uid'  => $value,
                'href'          => $field_info['params']['href'] ?? 0
            );
            $display_value[] = str_replace(",", "&#44;", $this->load->view('field/link/link_widget_view', $data));
        }
        return implode(", ", $display_value);
    }

    /**
     * Возвращает значение поля
     * @param type $field_uid
     * @param type $document_uid
     * @param type $widget_value - значение, получаемое от виджета поля; возвращается value, которое пишется в базу данных
     * @return type
     */
    public function getValue($field_uid, $document_uid, $widget_value = '') {
        if (!$widget_value) {
            $query = $this->db->query("SELECT DISTINCT value FROM " . DB_PREFIX . "field_value_link WHERE "
                    . "document_uid = '" . $this->db->escape($document_uid) . "' AND "
                    . "field_uid = '" . $this->db->escape($field_uid) . "' ");
            if ($query->num_rows > 0) {
                return $query->row['value'];
            }
        } else {
            if (is_array($widget_value)) {
                return implode(",", $widget_value);
            }
            return $widget_value;
        }
    }
    
    /**
     * Метод возвращает value + все дисплеи
     * @param type $field_uid
     * @param type $document_uid
     * @return type
     */
    public function getFieldValue($field_uid, $document_uid) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "field_value_link WHERE "
                . "document_uid = '" . $this->db->escape($document_uid) . "' AND "
                . "field_uid = '" . $this->db->escape($field_uid) . "' ");
        if ($query->num_rows) {
            return $query->row;
        }
        return array();
    }

    public function removeValue($field_uid, $document_uid) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "field_value_link WHERE field_uid = '" . $this->db->escape($field_uid) . "' AND document_uid = '" . $this->db->escape($document_uid) . "' ");
    }

    public function removeValues($field_uid) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "field_value_link WHERE field_uid = '" . $this->db->escape($field_uid) . "' ");
    }

    /**
     * Обновление отображаемого значения
     * @param type $data = array(
     *              field_uid - идентификаторы поля
     *              new_params - если установлен, значит это новые параметры поля
     *              document_uids - если установлен, обновляются не все документы, а только указанные в этом массиве         
     * )
     * @return type
     */
    public function refreshDisplayValues($data) {
        $this->load->model('doctype/doctype');
        $this->load->model('document/document');
        if (empty($data['document_uids'])) {
            //обновление полей всех документов из-за изменения параметров ссылочного поля, получаем их
            $query = $this->db->query("SELECT value, document_uid FROM " . DB_PREFIX . "field_value_link WHERE "
                    . "field_uid='" . $this->db->escape($data['field_uid']) . "' ");
            if ($query->num_rows) {
                $field_info = array(
                    'params' => $data['new_params']
                );                
                foreach ($query->rows as $field) {
                    $display_value = $this->getDisplay($data['field_uid'], $field['document_uid'], $field['value'], $field_info);
                    $this->db->query("UPDATE " . DB_PREFIX . "field_value_link SET "
                            . "display_value='" . $this->db->escape(trim(strip_tags($display_value))) . "', "
                            . "full_display_value='" . $this->db->escape(str_replace(", ",$field_info['params']['delimiter'],$display_value)) . "' "                                            
                            . "WHERE document_uid = '" . $field['document_uid'] . "' "
                            . "AND field_uid = '" . $this->db->escape($data['field_uid']) . "' ");                      
                }
            }
        } else {
            //обновление дисплея конкретных документов по подписке на изменение отображаемого поля
            $field_info = $this->model_doctype_doctype->getField($data['field_uid']);
            if (!$field_info) {
                //удалить подписку
                $this->model_doctype_doctype->delSubscription($data['field_uid']);
                return "Field not found";
            }

            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "field_value_link WHERE "
                    . "field_uid='" . $this->db->escape($data['field_uid']) . "' AND document_uid IN ('" . $this->db->escape(implode("','", $data['document_uids'])) . "') ");
            if (!$query->row['value']) {
                return;
            }
            
            foreach ($query->rows as $field) {
                $display_value = $this->getDisplay($field['field_uid'], $field['document_uid'], $field['value'], $field_info);
                $this->db->query("UPDATE " . DB_PREFIX . "field_value_link SET "
                        . "display_value='" . $this->db->escape(trim(strip_tags($display_value))) . "', "
                        . "full_display_value='" . $this->db->escape(str_replace(", ",$field_info['params']['delimiter'],$display_value)) . "' "                                            
                        . "WHERE document_uid = '" . $field['document_uid'] . "' "
                        . "AND field_uid = '" . $this->db->escape($field['field_uid']) . "' ");  
            }

        }       
    }
    
    public function install() {
        $this->load->model('tool/utils');
        if (!$this->model_tool_utils->isTable('field_value_link')) {
            $this->db->query("CREATE TABLE field_value_link ( `field_uid` VARCHAR(36) , `document_uid` VARCHAR(36) , `value` TEXT NOT NULL, `display_value` VARCHAR(256), `time_changed` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;");
            $this->db->query("ALTER TABLE field_value_link ADD UNIQUE KEY field_uid (field_uid,document_uid)");
            $this->db->query("ALTER TABLE field_value_link ADD INDEX( `value`(250));");
            $this->db->query("ALTER TABLE field_value_link ADD INDEX( `display_value`);");        
            $this->db->query("ALTER TABLE field_value_link ADD INDEX( `time_changed`);");        
        }
    }
            
    public function uninstall() {
        $this->load->model('tool/utils');
        if ($this->model_tool_utils->isTable('field_value_link')) {
            $this->db->query("DROP TABLE field_value_link");
        }    
        if ($this->model_tool_utils->isTable('field_value_link_subscription')) {
            $this->db->query("DROP TABLE field_value_link_subscription");
        }    
    }   
    
    /**
     * Этот метод вызывается демоном, когда изменится целевое поле по подписке
     * @param type $field_uid - идентификатор ссылочного поля
     * @param type $document_uids - документы, для которых сработала подписка
     */
    public function subscription($field_uid, $document_uids) {
        return $this->refreshDisplayValues(array('field_uid' => $field_uid, 'document_uids' => $document_uids));
    }
    

}
