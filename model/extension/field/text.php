<?php

class ModelExtensionFieldText extends FieldModel {    

    public function editValue($field_uid, $document_uid, $field_value) {
        $row_value = ($field_value)? $this->getValue($field_uid, $document_uid, $field_value): '';
        $field_info = $this->model_doctype_doctype->getField($field_uid);
        if (isset($field_info['params']['editor_enabled']) && $field_info['params']['editor_enabled'] === "true") {
            $tag_spacing_search = array("</p>", "<br>", "<hr>","</div>", "</li>", "</h1>", "</h2>", "</h3>", "</h4>", "</h5>", "</h6>", "</table>", "</blockquote>", "</code>");            
            $tag_spacing_replace = array("</p> ", "<br> ", "<hr> ","</div> ", "</li> ", "</h1> ", "</h2> ", "</h3> ", "</h4> ", "</h5> ", "</h6> ", "</table> ", "</blockquote> ", "</code> ");            
            $display_value = substr(strip_tags(str_replace("  "," ",str_replace($tag_spacing_search, $tag_spacing_replace, htmlspecialchars_decode($row_value)))), 0, 255);
        } else {
            $display_value = substr(str_replace("\n"," ",$row_value), 0, 255);
        }
        
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "field_value_text WHERE field_uid = '" . $this->db->escape($field_uid) . "' AND document_uid = '" . $this->db->escape($document_uid) . "' ");
        if ($query->num_rows) {
            $this->db->query("UPDATE " . DB_PREFIX . "field_value_text SET "
                    . "value='" . $this->db->escape($row_value) . "', "
                    . "display_value='" . $this->db->escape($display_value) . "', "
                    . "time_changed=NOW() "
                    . "WHERE field_uid = '" . $this->db->escape($field_uid) . "' AND document_uid = '" . $this->db->escape($document_uid) . "' ");
        } else {
            $this->db->query("INSERT INTO " . DB_PREFIX . "field_value_text SET "
                    . "document_uid='" . $this->db->escape($document_uid) . "', "
                    . "field_uid='" . $this->db->escape($field_uid) . "', "
                    . "value='" . $this->db->escape($row_value) . "', "
                    . "display_value='" . $this->db->escape($display_value) . "'");
        }
    } 

    /**
     * Возвращает значение поля
     * @param type $field_uid
     * @param type $document_uid
     * @param type $widget_value - значение, получаемое от виджета поля; возвращается value, которое пишется в базу данных
     * @return type
     */
    public function getValue($field_uid, $document_uid, $widget_value = '') {
        if (!$widget_value) {
            $query = $this->db->query("SELECT DISTINCT value FROM " . DB_PREFIX . "field_value_text WHERE "
                    . "document_uid='" . $this->db->escape($document_uid) . "' AND "
                    . "field_uid='" . $this->db->escape($field_uid) . "' ");
            
            if ($query->num_rows > 0) {
                return ($query->row['value']);
            }
        } else {
            $this->load->model('doctype/doctype');
            $field_info = $this->model_doctype_doctype->getField($field_uid);
            if (empty($field_info['params']['editor_enabled'])) {
                return $widget_value;
            } else {            
                $purifier_config = HTMLPurifier_Config::createDefault();
                $purifier_config->set('Cache.SerializerPath',DIR_STORAGE . "HTMLPurifier");
                $purifier_config->set('HTML.AllowedElements', array(
                    'strong' => TRUE,
                    'em' => TRUE,
                    'h1' => TRUE,
                    'h2' => TRUE,
                    'h3' => TRUE,
                    'h4' => TRUE,
                    'h5' => TRUE,
                    'h6' => TRUE,                
                    'ul' => TRUE,
                    'ol' => TRUE,
                    'li' => TRUE,
                    'img' => TRUE,
                    'a' => TRUE,
                    'p' => TRUE,
                    'div' => TRUE,
                    'span' => TRUE,
                    'font' => TRUE,
                    'b' => TRUE,
                    'i' => TRUE,
                    's' => TRUE,
                    'strike' => TRUE,
                    'u' => TRUE,
                    'br' => TRUE,
                    'hr' => TRUE,
                    'table' => TRUE, 
                    'tbody' => TRUE,
                    'th'=> TRUE,
                    'tr'=> TRUE, 
                    'td'=> TRUE,
                    'blockquote'=> TRUE,
                    'code'=> TRUE,
                ));
                $purifier_config->set('HTML.AllowedAttributes', array(
                    '*.style' => TRUE,
                    '*.title' => TRUE,
                    '*.color' => TRUE,
                    'a.href' => TRUE,
                    'a.target' => TRUE,
                    'a.document_uid' => TRUE,
                    'a.data-document_uid' => TRUE,                
                    'img.src' => TRUE,
                    'img.data-filename' => TRUE,
                    '*.class' => TRUE
                ));
                $purifier_config->set('URI.MungeResources', true);
                $purifier_config->set('URI.AllowedSchemes', array('data' => true, 'http' => true, 'https' => true,'mailto' => true,'tel' => true)); //data - для изображений, вставленных клипборд в редактор
                $def = $purifier_config->getHTMLDefinition(true);
                $def->addAttribute('a', 'target', 'Enum#_blank,_self,_target,_top');
                $def->addAttribute('a', 'document_uid', 'Text');
                $def->addAttribute('a', 'data-document_uid', 'Text');
                $def->addAttribute('img', 'data-filename', 'Text');
                $purifier = new HTMLPurifier($purifier_config);
                $val = htmlspecialchars_decode($widget_value);
    //            $val = preg_replace('/<br\/>/', "\n", $val);
    //            $val = preg_replace('/<br \/>/', "\n", $val);
    //            $val = preg_replace('/<br>/', "\n", $val);
                return htmlspecialchars($purifier->purify($val));
            }    
        }
    }

    public function removeValue($field_uid, $document_uid) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "field_value_text WHERE field_uid = '" . $this->db->escape($field_uid) . "' AND document_uid = '" . $this->db->escape($document_uid) . "' ");
    }

    public function removeValues($field_uid) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "field_value_text WHERE field_uid = '" . $this->db->escape($field_uid) . "' ");
    }
    
    public function refreshDisplayValues($data) {}        

    public function appendLogValue($field_uid, $document_uid, $log) {
        $this->load->model('doctype/doctype');
        $this->load->language('document/document');
        $value = implode(" ", $log);
        //$field_info = $this->model_doctype_doctype->getField($field_uid, 0);
        //$cr =  (!empty($field_info['params']['editor_enabled']) && strcmp($field_info['params']['editor_enabled'], "true") === 0) ? "<br/>" : "\n";
        $cr = "<br>\n";
        $row_value = $this->getValue($field_uid, $document_uid, $value);
        $display_value = substr(strip_tags($value), 0, 255);
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "field_value_text WHERE field_uid = '" . $this->db->escape($field_uid) . "' AND document_uid = '" . $this->db->escape($document_uid) . "' ");

        if ($query->num_rows) {
            $this->db->query("UPDATE " . DB_PREFIX . "field_value_text SET "
                    . "value=CONCAT(value, '" . $cr . $this->db->escape($row_value) . "'), "
                    . "display_value=CONCAT(display_value, '" . $cr . $this->db->escape($display_value) . "')"
                    . "WHERE field_uid = '" . $this->db->escape($field_uid) . "' AND document_uid = '" . $this->db->escape($document_uid) . "' ");
        } else {
            $this->db->query("INSERT INTO " . DB_PREFIX . "field_value_text SET "
                    . "document_uid='" . $this->db->escape($document_uid) . "', "
                    . "field_uid='" . $this->db->escape($field_uid) . "', "
                    . "value='" . $this->db->escape($row_value) . "', "
                    . "display_value='" . $this->db->escape($display_value) . "'");
        }
    }
    
    public function install() {
        //создаем таблицу поля
        $this->load->model('tool/utils');
        if (!$this->model_tool_utils->isTable('field_value_text')) {  
            $this->db->query("CREATE TABLE " . DB_PREFIX . "field_value_text ( `field_uid` VARCHAR(36) , `document_uid` VARCHAR(36) , `value` MEDIUMTEXT, `display_value` VARCHAR(255), `time_changed` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;");
            $this->db->query("ALTER TABLE " . DB_PREFIX . "field_value_text ADD UNIQUE KEY field_uid (field_uid,document_uid)");
            $this->db->query("ALTER TABLE `field_value_text` ADD INDEX( `value`(250));");
            $this->db->query("ALTER TABLE `field_value_text` ADD INDEX( `display_value`);");                    
            $this->db->query("ALTER TABLE `field_value_text` ADD INDEX( `time_changed`);");                    
        }
    }

    public function uninstall() {
        $this->load->model('tool/utils');
        if ($this->model_tool_utils->isTable('field_value_text')) {            
            $this->db->query("DROP TABLE field_value_text");
        }        
    }    
}
