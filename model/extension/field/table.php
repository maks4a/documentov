<?php

class ModelExtensionFieldTable extends FieldModel {

    public function editValue($field_uid, $document_uid, $field_value) {
        $this->load->model('doctype/doctype');
        $data = json_decode(htmlspecialchars_decode($field_value), true);

        $display_values = array();
        $field_info = $this->model_doctype_doctype->getField($field_uid);
        foreach ($field_info['params']['inner_fields'] as $field) {
            $this->load->model('extension/field/' . $field['field_type']);
        }    
        if ($data) {            
            foreach ($data as $values) {               
                $rows = array();
                for ($i=0; $i<count($field_info['params']['inner_fields']); $i++) {
                    $field_param = $field_info['params']['inner_fields'][$i]['params'];
                    $inner_field_uid = $field_info['params']['inner_fields'][$i]['inner_field_uid'];
                    if ($values[$inner_field_uid]) {
                        $model = "model_extension_field_" . $field_info['params']['inner_fields'][$i]['field_type'];
                        $field_param['field_value']  = $this->$model->getValue('', 0, $values[$inner_field_uid], $field_info['params']['inner_fields'][$i]['params']);
                    } else {
                        $field_param['field_value']  = "";
                    }
                    $rows[] = $this->load->controller('extension/field/' . $field_info['params']['inner_fields'][$i]['field_type'] . '/getView', $field_param);
                }
                $display_values[] = implode(" - ", $rows);
            }
        }
        $display_value = strip_tags(implode("; ", $display_values));
        if (mb_strlen($display_value) > 255) {
            $display_value = mb_substr($display_value, 0, 252) . "...";
        }
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "field_value_table WHERE field_uid = '" . $this->db->escape($field_uid) . "' AND document_uid = '" . $this->db->escape($document_uid) . "' ");

        if ($query->num_rows) {
            $this->db->query("UPDATE " . DB_PREFIX . "field_value_table SET "
                    . "value='" . $this->db->escape($field_value) . "', "
                    . "display_value='" . $this->db->escape($display_value) . "', "
                    . "time_changed=NOW() "
                    . "WHERE field_uid = '" . $this->db->escape($field_uid) . "' AND document_uid = '" . $this->db->escape($document_uid) . "' ");
        } else {
            $this->db->query("INSERT INTO " . DB_PREFIX . "field_value_table SET "
                    . "document_uid='" . $this->db->escape($document_uid) . "', "
                    . "field_uid='" . $this->db->escape($field_uid) . "', "
                    . "value='" . $this->db->escape($field_value) . "', "
                    . "display_value='" . $this->db->escape($display_value) . "'");
        }
    } 

    /**
     * Возвращает значение поля
     * @param type $field_uid
     * @param type $document_uid
     * @param type $widget_value - значение, получаемое от виджета поля; возвращается value, которое пишется в базу данных
     * @return type
     */
    public function getValue($field_uid, $document_uid, $widget_value = '') {

        if (!$widget_value) {
            $query = $this->db->query("SELECT DISTINCT value FROM " . DB_PREFIX . "field_value_table WHERE "
                    . "document_uid='" . $this->db->escape($document_uid) . "' AND "
                    . "field_uid='" . $this->db->escape($field_uid) . "' ");
            if ($query->num_rows > 0) {
                return ($query->row['value']);
            }
        } else {
            return $widget_value;
        }
    }

    public function removeValue($field_uid, $document_uid) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "field_value_table WHERE field_uid = '" . $this->db->escape($field_uid) . "' AND document_uid = '" . $this->db->escape($document_uid) . "' ");
    }

    public function removeValues($field_uid) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "field_value_table WHERE field_uid = '" . $this->db->escape($field_uid) . "' ");
    }
    
    public function refreshDisplayValues($data) {}        

    public function appendLogValue($field_uid, $document_uid, $log) {
        $this->load->model('doctype/doctype');
        $this->load->language('document/document');
        $log_arr = array();
        $cnt = 0;
        foreach ($log as $log_field) {
            $cnt++;
            $log_arr[$cnt] = $log_field;
        }
        $row_value = json_encode($log_arr);
        $display_value = $cnt;
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "field_value_table WHERE field_uid = '" . $this->db->escape($field_uid) . "' AND document_uid = '" . $this->db->escape($document_uid) . "' ");

        if ($query->num_rows) {
            $this->db->query("UPDATE " . DB_PREFIX . "field_value_table SET "
                    . "value=CONCAT(value, '" . $cr . $this->db->escape($row_value) . "'), "
                    . "display_value=CONCAT(display_value, '" . $cr . $this->db->escape($display_value) . "')"
                    . "WHERE field_uid = '" . $this->db->escape($field_uid) . "' AND document_uid = '" . $this->db->escape($document_uid) . "' ");
        } else {
            $this->db->query("INSERT INTO " . DB_PREFIX . "field_value_table SET "
                    . "document_uid='" . $this->db->escape($document_uid) . "', "
                    . "field_uid='" . $this->db->escape($field_uid) . "', "
                    . "value='" . $this->db->escape($row_value) . "', "
                    . "display_value='" . $this->db->escape($display_value) . "'");
        }
    }


    public function install() {
        //создаем таблицу табличного поля. Поле value хранит значения ключа, по который соттветствует table_id  в таблице ячеек
        $this->db->query("CREATE TABLE " . DB_PREFIX . "field_value_table ( `field_uid` VARCHAR(36) , `document_uid` VARCHAR(36) , `value` MEDIUMTEXT, `display_value` VARCHAR(255), `time_changed` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;");
        $this->db->query("ALTER TABLE " . DB_PREFIX . "field_value_table ADD UNIQUE KEY field_uid (field_uid,document_uid)");
        $this->db->query("ALTER TABLE `field_value_table` ADD INDEX( `value`(250));");
        $this->db->query("ALTER TABLE `field_value_table` ADD INDEX( `display_value`);");        
        $this->db->query("ALTER TABLE `field_value_table` ADD INDEX( `time_changed`);");        
    }

    public function uninstall() {
        //удаляем таблицу поля
        $this->db->query("DROP TABLE field_value_table");
    }
    
}
