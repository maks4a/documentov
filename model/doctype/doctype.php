<?php

class ModelDoctypeDoctype extends Model {

    public function addDoctype() {
        //проверяем наличие созданных, но ни разу не сохраненных доктайпов - у них draft=3
        $query_doctype_draft = $this->db->query("SELECT doctype_uid FROM " . DB_PREFIX . "doctype WHERE draft=3");
        if ($query_doctype_draft->num_rows) {
            $doctype_uid = $query_doctype_draft->row['doctype_uid'];
        } else {
            $query = $this->db->query("SELECT UUID() AS uid");
            $doctype_uid = $query->row['uid'];
            $this->db->query("INSERT INTO " . DB_PREFIX . "doctype SET "
                    . "doctype_uid = '" . $doctype_uid . "', "
                    . "date_added = NOW(), user_uid = '" . $this->customer->getStructureId() . "', "
                    . "draft = 3, draft_params = ''"
            );            
        }
        return $doctype_uid;
    }

    public function deleteDoctype($doctype_uid) {        
        $doctype_uid = $this->db->escape($doctype_uid);
        //проверяем наличие документов
        $query = $this->db->query("SELECT document_uid FROM document WHERE doctype_uid = '" . $doctype_uid . "' LIMIT 0,1");
        //проверяем наличие полей в доктайпе, которые есть в системных настройках
        $query_setting_field = $this->db->query("SELECT setting_id FROM " . DB_PREFIX . "setting WHERE `value` IN (SELECT field_uid FROM field WHERE doctype_uid = '" . $doctype_uid . "')");
        if (!$query->num_rows && !$query_setting_field->num_rows) {
            $this->load->model('document/document');
            //получаем настроечные поля, чтобы удалить их значения
            $query_field = $this->db->query("SELECT * FROM " . DB_PREFIX . "field WHERE doctype_uid = '" . $doctype_uid . "' AND setting=1 ");
            foreach ($query_field->rows as $field) {
                $this->model_document_document->removeFieldValue($field['field_uid'],0);
            }
            
            //документов нет, удаляем доктайп
            $this->db->query("DELETE FROM " . DB_PREFIX . "folder_button_field WHERE field_uid IN (SELECT field_uid FROM " . DB_PREFIX . "field WHERE doctype_uid = '" . $doctype_uid . "') ");
            $this->db->query("DELETE FROM " . DB_PREFIX . "folder_field WHERE field_uid IN (SELECT field_uid FROM " . DB_PREFIX . "field WHERE doctype_uid = '" . $doctype_uid . "') ");
            $this->db->query("DELETE FROM " . DB_PREFIX . "folder_filter WHERE field_uid IN (SELECT field_uid FROM " . DB_PREFIX . "field WHERE doctype_uid = '" . $doctype_uid . "') ");
            $this->db->query("DELETE FROM " . DB_PREFIX . "field WHERE doctype_uid = '" . $doctype_uid . "' ");

            $this->db->query("DELETE FROM " . DB_PREFIX . "route_action WHERE route_uid IN (SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid='" . $doctype_uid . "') ");
            $this->db->query("DELETE FROM " . DB_PREFIX . "route_button_delegate WHERE route_button_uid IN (SELECT route_button_uid FROM route_button WHERE route_uid IN (SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid='" . $doctype_uid . "')) ");
            $this->db->query("DELETE FROM " . DB_PREFIX . "route_button_description WHERE route_button_uid IN (SELECT route_button_uid FROM route_button WHERE route_uid IN (SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid='" . $doctype_uid . "')) ");
            $this->db->query("DELETE FROM " . DB_PREFIX . "route_button_field WHERE route_button_uid IN (SELECT route_button_uid FROM route_button WHERE route_uid IN (SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid='" . $doctype_uid . "')) ");
            $this->db->query("DELETE FROM " . DB_PREFIX . "route_button WHERE route_uid IN (SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid='" . $doctype_uid . "') ");
            $this->db->query("DELETE FROM " . DB_PREFIX . "route_description WHERE route_uid IN (SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid='" . $doctype_uid . "') ");
            $this->db->query("DELETE FROM " . DB_PREFIX . "folder_button_route WHERE route_uid IN (SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid='" . $doctype_uid . "') ");
            $this->db->query("DELETE FROM " . DB_PREFIX . "route WHERE doctype_uid = '" . $doctype_uid . "' ");

            $this->db->query("DELETE FROM " . DB_PREFIX . "doctype_template WHERE doctype_uid = '" . $doctype_uid . "' ");
            $this->db->query("DELETE FROM " . DB_PREFIX . "doctype_description WHERE doctype_uid = '" . $doctype_uid . "' ");
                        
            $this->db->query("DELETE FROM " . DB_PREFIX . "doctype WHERE doctype_uid = '" . $doctype_uid . "' ");

            return true;
        } else {
            //документы есть, удаление невозможно
            return false;
        }
    }

    public function copyDoctype($doctype_uid) {
        $this->load->model('document/document');

        $doctype_uid = $this->db->escape($doctype_uid);
        $query_uid = $this->db->query("SELECT UUID() AS uid");
        $copy_doctype_uid = $query_uid->row['uid'];
        $this->db->query("INSERT INTO " . DB_PREFIX . "doctype (doctype_uid, field_log_uid, date_added, date_edited, user_uid, draft, draft_params) SELECT '" . $copy_doctype_uid . "', field_log_uid, NOW(), NOW(), '" . $this->customer->getStructureId() . "', draft, draft_params FROM " . DB_PREFIX . "doctype WHERE doctype_uid = '" . $doctype_uid . "'");
        $this->db->query("INSERT INTO " . DB_PREFIX . "doctype_description (doctype_uid, name, short_description, language_id) SELECT '" . $copy_doctype_uid . "', CONCAT(name, '_copy'), short_description, language_id FROM " . DB_PREFIX . "doctype_description WHERE doctype_uid = '" . $doctype_uid . "'");
        $this->db->query("INSERT INTO " . DB_PREFIX . "doctype_template (doctype_uid, template, type, language_id) SELECT '" . $copy_doctype_uid . "', template, type, language_id FROM " . DB_PREFIX . "doctype_template WHERE doctype_uid = '" . $doctype_uid . "'");

        $query_field = $this->db->query("SELECT * FROM " . DB_PREFIX . "field WHERE doctype_uid = '" . $doctype_uid . "'");
        $copies_field = array();
        $copies_route = array();
        foreach ($query_field->rows as $field) {
            $query_field_uid = $this->db->query("SELECT UUID() AS uid");
            $copy_field_uid = $query_field_uid->row['uid'];
            $this->db->query("INSERT INTO " . DB_PREFIX . "field (field_uid, name, doctype_uid, type, setting, change_field, access_form, access_view, `required`, `unique`, params, sort, draft, draft_params) SELECT '" . $copy_field_uid . "', name, '" . $copy_doctype_uid . "',  type, setting, change_field, access_form, access_view, `required`, `unique`, params, sort, draft, draft_params FROM " . DB_PREFIX . "field WHERE field_uid = '" . $field['field_uid'] . "'");
            $copies_field[$field['field_uid']] = $copy_field_uid;
            if ($field['setting']) {
                $value = $this->model_document_document->getFieldValue($field['field_uid'], 0);
                $this->model_document_document->editFieldValue($copy_field_uid, 0, $value);
            }
        }

        $query_route = $this->db->query("SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid = '" . $doctype_uid . "'");
        foreach ($query_route->rows as $route) {
            $query_route_uid = $this->db->query("SELECT UUID() AS uid");
            $copy_route_uid = $query_route_uid->row['uid'];
            $copies_route[$route['route_uid']] = $copy_route_uid;
            $this->db->query("INSERT INTO " . DB_PREFIX . "route (route_uid, doctype_uid, sort, draft, draft_params) SELECT '" . $copy_route_uid . "', '" . $copy_doctype_uid . "',  sort, draft, draft_params FROM " . DB_PREFIX . "route WHERE route_uid = '" . $route['route_uid'] . "'");
            $this->db->query("INSERT INTO " . DB_PREFIX . "route_description (route_uid, name, description, language_id) SELECT '" . $copy_route_uid . "', name, description, language_id FROM " . DB_PREFIX . "route_description WHERE route_uid = '" . $route['route_uid'] . "'");
            $query_route_action = $this->db->query("SELECT route_action_uid FROM " . DB_PREFIX . "route_action WHERE route_uid = '" . $route['route_uid'] . "'");
            foreach ($query_route_action->rows as $route_action) {
                $query_route_action_uid = $this->db->query("SELECT UUID() AS uid");
                $copy_route_action_uid = $query_route_action_uid->row['uid'];
                $this->db->query("INSERT INTO " . DB_PREFIX . "route_action (route_action_uid, route_uid, context, action, action_log, params, sort, status, draft, draft_params) SELECT '" . $copy_route_action_uid . "', '" . $copy_route_uid . "', context, action, action_log, params, sort, status, draft, draft_params FROM " . DB_PREFIX . "route_action WHERE route_action_uid = '" . $route_action['route_action_uid'] . "'");
            }
            $query_route_button = $this->db->query("SELECT route_button_uid FROM " . DB_PREFIX . "route_button WHERE route_uid = '" . $route['route_uid'] . "'");
            foreach ($query_route_button->rows as $route_button) {
                $query_route_button_uid = $this->db->query("SELECT UUID() AS uid");
                $copy_route_button_uid = $query_route_button_uid->row['uid'];
                $this->db->query("INSERT INTO " . DB_PREFIX . "route_button (route_button_uid, route_uid, picture, color, background, action, action_log, action_move_route_uid, show_after_execute, hide_button_name, action_params, sort, draft, draft_params) SELECT '" . $copy_route_button_uid . "', '" . $copy_route_uid . "', picture, color, background, action, action_log, action_move_route_uid, show_after_execute, hide_button_name, action_params, sort, draft, draft_params FROM " . DB_PREFIX . "route_button WHERE route_button_uid = '" . $route_button['route_button_uid'] . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "route_button_description (route_button_uid, name, description, language_id) SELECT '" . $copy_route_button_uid . "', name, description, language_id FROM " . DB_PREFIX . "route_button_description WHERE route_button_uid = '" . $route_button['route_button_uid'] . "'");
                $query_route_button_field = $this->db->query("SELECT * FROM " . DB_PREFIX . "route_button_field WHERE route_button_uid = '" . $route_button['route_button_uid'] . "'");
                foreach ($query_route_button_field->rows as $route_button_field) {
                    if (isset($copies_field[$route_button_field['field_uid']])) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "route_button_field SET route_button_uid = '" . $copy_route_button_uid . "', field_uid = '" . $copies_field[$route_button_field['field_uid']] . "'");
                    }
                }
                $this->updateButtonDelegate($copy_route_button_uid);
            }
        }
        foreach ($copies_field as $old_field_uid => $new_field_uid) {
            $this->db->query("UPDATE " . DB_PREFIX . "doctype_template SET template = REPLACE(template, '{{ f_" . str_replace("-", "", $old_field_uid) . " }}', '{{ f_" . str_replace("-", "", $new_field_uid) . " }}') WHERE doctype_uid = '" . $copy_doctype_uid . "'");
            $this->db->query("UPDATE " . DB_PREFIX . "route_button SET action_params = REPLACE(action_params, '{{ f_" . str_replace("-", "", $old_field_uid) . " }}', '{{ f_" . str_replace("-", "", $new_field_uid) . " }}') WHERE route_uid IN (SELECT route_uid FROM route WHERE doctype_uid='" . $copy_doctype_uid . "')");
            $this->db->query("UPDATE " . DB_PREFIX . "route_button SET draft_params = REPLACE(draft_params, '{{ f_" . str_replace("-", "", $old_field_uid) . " }}', '{{ f_" . str_replace("-", "", $new_field_uid) . " }}') WHERE route_uid IN (SELECT route_uid FROM route WHERE doctype_uid='" . $copy_doctype_uid . "')");
            $this->db->query("UPDATE " . DB_PREFIX . "route_button SET action_params = REPLACE(action_params, '" . $old_field_uid . "', '" . $new_field_uid . "') WHERE route_uid IN (SELECT route_uid FROM route WHERE doctype_uid='" . $copy_doctype_uid . "')");
            $this->db->query("UPDATE " . DB_PREFIX . "route_button SET draft_params = REPLACE(draft_params, '" . $old_field_uid . "', '" . $new_field_uid . "') WHERE route_uid IN (SELECT route_uid FROM route WHERE doctype_uid='" . $copy_doctype_uid . "')");
            $this->db->query("UPDATE " . DB_PREFIX . "route_action SET params = REPLACE(params, '{{ f_" . str_replace("-", "", $old_field_uid) . " }}', '{{ f_" . str_replace("-", "", $new_field_uid) . " }}') WHERE route_uid IN (SELECT route_uid FROM route WHERE doctype_uid='" . $copy_doctype_uid . "')");
            $this->db->query("UPDATE " . DB_PREFIX . "route_action SET draft_params = REPLACE(draft_params, '{{ f_" . str_replace("-", "", $old_field_uid) . " }}', '{{ f_" . str_replace("-", "", $new_field_uid) . " }}') WHERE route_uid IN (SELECT route_uid FROM route WHERE doctype_uid='" . $copy_doctype_uid . "')");
            $this->db->query("UPDATE " . DB_PREFIX . "route_action SET params = REPLACE(params, '" . $old_field_uid . "', '" . $new_field_uid . "') WHERE route_uid IN (SELECT route_uid FROM route WHERE doctype_uid='" . $copy_doctype_uid . "')");
            $this->db->query("UPDATE " . DB_PREFIX . "route_action SET draft_params = REPLACE(draft_params, '" . $old_field_uid . "', '" . $new_field_uid . "') WHERE route_uid IN (SELECT route_uid FROM route WHERE doctype_uid='" . $copy_doctype_uid . "')");
            $this->db->query("UPDATE " . DB_PREFIX . "field SET params = REPLACE(params, '{{ f_" . str_replace("-", "", $old_field_uid) . " }}', '{{ f_" . str_replace("-", "", $new_field_uid) . " }}') WHERE doctype_uid='" . $copy_doctype_uid . "'");
            $this->db->query("UPDATE " . DB_PREFIX . "field SET draft_params = REPLACE(draft_params, '{{ f_" . str_replace("-", "", $old_field_uid) . " }}', '{{ f_" . str_replace("-", "", $new_field_uid) . " }}') WHERE doctype_uid='" . $copy_doctype_uid . "'");
            $this->db->query("UPDATE " . DB_PREFIX . "field SET params = REPLACE(params, '" . $old_field_uid . "', '" . $new_field_uid . "') WHERE doctype_uid='" . $copy_doctype_uid . "'");
            $this->db->query("UPDATE " . DB_PREFIX . "field SET draft_params = REPLACE(draft_params, '" . $old_field_uid . "', '" . $new_field_uid . "') WHERE doctype_uid='" . $copy_doctype_uid . "'");
        }
        foreach ($copies_route as $old_route_uid => $new_route_uid) {
            $this->db->query("UPDATE " . DB_PREFIX . "doctype_template SET template = REPLACE(template, '{{ f_" . str_replace("-", "", $old_route_uid) . " }}', '{{ f_" . str_replace("-", "", $new_route_uid) . " }}') WHERE doctype_uid = '" . $copy_doctype_uid . "'");
            $this->db->query("UPDATE " . DB_PREFIX . "route_button SET action_move_route_uid = '" . $new_route_uid . "' WHERE action_move_route_uid = '" . $old_route_uid . "' AND route_uid IN (SELECT route_uid FROM route WHERE doctype_uid='" . $copy_doctype_uid . "')");
            $this->db->query("UPDATE " . DB_PREFIX . "route_button SET action_params = REPLACE(action_params, '{{ f_" . str_replace("-", "", $old_route_uid) . " }}', '{{ f_" . str_replace("-", "", $new_route_uid) . " }}') WHERE route_uid IN (SELECT route_uid FROM route WHERE doctype_uid='" . $copy_doctype_uid . "')");
            $this->db->query("UPDATE " . DB_PREFIX . "route_button SET draft_params = REPLACE(draft_params, '{{ f_" . str_replace("-", "", $old_route_uid) . " }}', '{{ f_" . str_replace("-", "", $new_route_uid) . " }}') WHERE route_uid IN (SELECT route_uid FROM route WHERE doctype_uid='" . $copy_doctype_uid . "')");
            $this->db->query("UPDATE " . DB_PREFIX . "route_button SET action_params = REPLACE(action_params, '" . $old_route_uid . "', '" . $new_route_uid . "') WHERE route_uid IN (SELECT route_uid FROM route WHERE doctype_uid='" . $copy_doctype_uid . "')");
            $this->db->query("UPDATE " . DB_PREFIX . "route_button SET draft_params = REPLACE(draft_params, '" . $old_route_uid . "', '" . $new_route_uid . "') WHERE route_uid IN (SELECT route_uid FROM route WHERE doctype_uid='" . $copy_doctype_uid . "')");
            $this->db->query("UPDATE " . DB_PREFIX . "route_action SET params = REPLACE(params, '{{ f_" . str_replace("-", "", $old_route_uid) . " }}', '{{ f_" . str_replace("-", "", $new_route_uid) . " }}') WHERE route_uid IN (SELECT route_uid FROM route WHERE doctype_uid='" . $copy_doctype_uid . "')");
            $this->db->query("UPDATE " . DB_PREFIX . "route_action SET draft_params = REPLACE(draft_params, '{{ f_" . str_replace("-", "", $old_route_uid) . " }}', '{{ f_" . str_replace("-", "", $new_route_uid) . " }}') WHERE route_uid IN (SELECT route_uid FROM route WHERE doctype_uid='" . $copy_doctype_uid . "')");
            $this->db->query("UPDATE " . DB_PREFIX . "route_action SET params = REPLACE(params, '" . $old_route_uid . "', '" . $new_route_uid . "') WHERE route_uid IN (SELECT route_uid FROM route WHERE doctype_uid='" . $copy_doctype_uid . "')");
            $this->db->query("UPDATE " . DB_PREFIX . "route_action SET draft_params = REPLACE(draft_params, '" . $old_route_uid . "', '" . $new_route_uid . "') WHERE route_uid IN (SELECT route_uid FROM route WHERE doctype_uid='" . $copy_doctype_uid . "')");
            $this->db->query("UPDATE " . DB_PREFIX . "field SET params = REPLACE(params, '{{ f_" . str_replace("-", "", $old_route_uid) . " }}', '{{ f_" . str_replace("-", "", $new_route_uid) . " }}') WHERE doctype_uid='" . $copy_doctype_uid . "'");
            $this->db->query("UPDATE " . DB_PREFIX . "field SET draft_params = REPLACE(draft_params, '{{ f_" . str_replace("-", "", $old_route_uid) . " }}', '{{ f_" . str_replace("-", "", $new_route_uid) . " }}') WHERE doctype_uid='" . $copy_doctype_uid . "'");
            $this->db->query("UPDATE " . DB_PREFIX . "field SET params = REPLACE(params, '" . $old_route_uid . "', '" . $new_route_uid . "') WHERE doctype_uid='" . $copy_doctype_uid . "'");
            $this->db->query("UPDATE " . DB_PREFIX . "field SET draft_params = REPLACE(draft_params, '" . $old_route_uid . "', '" . $new_route_uid . "') WHERE doctype_uid='" . $copy_doctype_uid . "'");
        }
    }

    public function editDoctype($doctype_uid, $data) {
        //СОХРАНЕНИЕ ШАБЛОНА И ОБЩИХ ПАРАМЕТРОВ ТИПА ДОКУМЕНТА     
        $this->load->model('extension/service/daemon');
        $this->load->model('daemon/queue');
        $doctype_info = $this->getDoctype($doctype_uid);
        if ($data || $doctype_info['draft_params']) {
            $draft_params = $data ?? unserialize($doctype_info['draft_params']);
            $this->db->query("DELETE FROM " . DB_PREFIX . "doctype_description "
                    . "WHERE doctype_uid = '" . $this->db->escape($doctype_uid) . "'");
            foreach ($draft_params['doctype_description'] as $language_id => $value) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "doctype_description SET "
                        . "doctype_uid = '" . $this->db->escape($doctype_uid) . "', "
                        . "language_id = '" . (int) $language_id . "', "
                        . "name = '" . $this->db->escape($value['name']) . "', "
                        . "short_description = '" . $this->db->escape($value['short_description']) . "' "
                );
            }
            $this->db->query("UPDATE " . DB_PREFIX . "doctype SET "
                    . "field_log_uid = '" . $this->db->escape($draft_params['field_log_uid'] ?? 0) . "' "
                    . " WHERE doctype_uid = '" . $this->db->escape($doctype_uid) . "' "
            );
            $this->db->query("DELETE FROM " . DB_PREFIX . "doctype_template "
                    . "WHERE doctype_uid = '" . $this->db->escape($doctype_uid) . "'");
            if (!empty(($draft_params['doctype_template']))) {
                $variables = $this->getTemplateVariables();
                foreach ($draft_params['doctype_template'] as $type => $type_templates) {
                    $index = 0;                    
                    ksort($type_templates);
                    foreach ($type_templates as $templates) {                        
                        foreach ($templates as $name => $value) {
                            $template = $this->getIdsTemplate($value, $doctype_uid, $variables);
                            $this->db->query("INSERT INTO " . DB_PREFIX . "doctype_template SET "
                                    . "doctype_uid = '" . $this->db->escape($doctype_uid) . "', "
                                    . "language_id = '" . (int) $name . "', "
                                    . "type = '" . $this->db->escape($type) . "', "
                                    . "template = '" . $this->db->escape(trim($template)) . "', "
                                    . "`sort` = '" . $index ."' "
                            );                         
                        }
                        $index++;
                    }
                }  
                if (!empty($draft_params['params']['doctype_template'])) {
                    $doctype_template = array();                    
                    foreach ($draft_params['params']['doctype_template'] as $template_type => $templates) {
                        $index = 0;
                        ksort($templates);
                        foreach ($templates as $template_data) {
                            $doctype_template[$template_type][++$index] = $template_data;
                        }
                    }
                    $draft_params['params']['doctype_template'] = $doctype_template;
                    $this->db->query("UPDATE " . DB_PREFIX . "doctype SET params = '" . $this->db->escape(serialize($draft_params['params'])) . "' WHERE doctype_uid = '" . $this->db->escape($doctype_uid) . "' ");
                } else {
                    //дополнительных шаблонов нет
                    $this->db->query("UPDATE " . DB_PREFIX . "doctype SET params = '" . $this->db->escape(serialize(array())) . "' WHERE doctype_uid = '" . $this->db->escape($doctype_uid) . "' ");
                    
                }
                
            }
        }
        
        //СОХРАНЯЕМ ПОЛЯ
        //получаем список измененных полей
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "field WHERE doctype_uid = '" . $this->db->escape($doctype_uid) . "' AND draft > 0 AND draft != 2");
        foreach ($query->rows as $field) {
            $params = array();
            $draft_params = array();
            if ($field['draft_params']) {
                $draft_params = unserialize($field['draft_params']);
                $field_name = $draft_params['field_name'];
                $field_uid = $field['field_uid'];
                $field_type = $field['type'];
                foreach ($draft_params as $param => $value) {
                    if ($param != "field_name" || $param != "field_type" || $param != "sort") {
                        $params[$param] = $value;
                    }
                }
                if (!empty($params['sort'])) {
                    $sort = $params['sort'];
                } else {
                    $sort = $field['sort'];
                }
                if (isset($params['change_field'])) {
                    $change_field = $params['change_field'];
                } else {
                    $change_field = $field['change_field'];
                }
                if (isset($params['access_form'])) {
                    $access_form = implode(",",$params['access_form']);
                } else {
                    $access_form = $field['access_form'];
                }
                if (isset($params['access_view'])) {
                    $access_view = implode(",",$params['access_view']);
                } else {
                    $access_view = $field['access_view'];
                }
                if (isset($params['required'])) {
                    $required = $params['required'];
                } else {
                    $required = $field['required'];
                }
                if (isset($params['unique'])) {
                    $unique = $params['unique'];
                } else {
                    $unique = $field['unique'];
                }
                $params = serialize($params);
                $this->db->query("UPDATE " . DB_PREFIX . "field SET "
                        . "name='" . $this->db->escape($field_name) . "', "
                        . "change_field='" . (int) $change_field . "', "
                        . "access_form='" . $this->db->escape($access_form) . "', "
                        . "access_view='" . $this->db->escape($access_view) . "', "
                        . "`required` ='" . (int) $required . "', "
                        . "`unique` ='" . (int) $unique . "', "
                        . "params='" . $this->db->escape($params) . "', "
                        . "draft=0, draft_params='', "
                        . "sort='" . (int) $sort . "' "
                        . "WHERE field_uid='" . $this->db->escape($field_uid) . "'"
                );
                $data_refresh = array();
                $data_refresh['field_uid'] = $field_uid;
                $data_refresh['field_type'] = $field_type;
                $data_refresh['new_params'] = $draft_params;
                $data_refresh['params'] = unserialize($field['params']);  

                if ($this->variable->get('daemon_started') && $this->model_extension_service_daemon->getStatus()) {
                    //если демон доступен - отдаем ему задачу обработки  обновления значений полей                     
                    $this->model_daemon_queue->addTask('doctype/doctype/executeRefreshDisplay', $data_refresh, 1);
                } else {
                    $model = "model_extension_field_" . $field_type;
                    $this->load->model('extension/field/' . $field_type);
                    $value = $this->$model->refreshDisplayValues($data_refresh);
                }
                
                

            }
        }
        //удаляем поля, помеченные на удаление
        //сначала чистим их таблицы   
        $query_field = $this->db->query("SELECT * FROM " . DB_PREFIX . "field WHERE doctype_uid='" . $this->db->escape($doctype_uid) . "' AND draft = 2");
        $this->load->model('document/document');
        foreach ($query_field->rows as $field) {
            $this->model_document_document->removeFieldValues($field['field_uid']);
        }
        $this->db->query("DELETE FROM " . DB_PREFIX . "field WHERE doctype_uid='" . $this->db->escape($doctype_uid) . "' AND draft = 2");
        //проверяем сортировку полей
        $query_field_sort = $this->db->query("SELECT field_uid, sort FROM " . DB_PREFIX . "field WHERE doctype_uid='" . $this->db->escape($doctype_uid) . "' ORDER BY sort ASC");
        $sort = 1;
        foreach ($query_field_sort->rows as $field) {
            $this->db->query("UPDATE " . DB_PREFIX . "field SET sort='" . (int)$sort++ . "' WHERE field_uid = '" . $field['field_uid'] . "'");
        }
        

        //СОХРАНЕНИЕ ТОЧЕК МАРШРУТА
        //получаем измененные точки маршрута
        $query = $this->db->query("SELECT * FROM route WHERE draft >0 AND draft != 2 AND doctype_uid = '" . $this->db->escape($doctype_uid) . "' ");
        foreach ($query->rows as $route) {
            $draft_params = array();
            if ($route['draft_params']) {
                //точка изменена, сохраняем новые параметры
                $draft_params = unserialize($route['draft_params']);
                $route_params = array();
                $route_descriptions = array();
                foreach ($draft_params as $param => $value) {
                    if ($param == "descriptions") {
                        $route_descriptions = $value;
                    } else {
                        $route_params[$param] = $value;
                    }
                }
                if ($route_descriptions) {
                    //удаляем существующее описание точки
                    $this->db->query("DELETE FROM " . DB_PREFIX . "route_description WHERE route_uid = '" . $this->db->escape($route['route_uid']) . "' ");
                    //сохраняем новое описание точки
                    foreach ($route_descriptions as $language_id => $description) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "route_description SET "
                                . "route_uid = '" . $this->db->escape($route['route_uid']) . "', "
                                . "name = '" . $this->db->escape($description['name']) . "', "
                                . "description = '" . $this->db->escape($description['description']) . "',"
                                . "language_id = '" . (int) $language_id . "' ");
                    }
                }
                if ($route_params) {
                    $sql = "UPDATE " . DB_PREFIX . "route SET ";
                    $params = array();
                    foreach ($route_params as $param_name => $param_value) {
                        $params[] .= $param_name . " = '" . $this->db->escape($param_value) . "'";
                    }
                    $sql .= implode(", ", $params);
                    $sql .= " WHERE route_uid = '" . $this->db->escape($route['route_uid']) . "' ";
                    $this->db->query($sql);
                }
            }
            //удаляем признак черновика у точки маршрута
            $this->db->query("UPDATE " . DB_PREFIX . "route SET draft = '0' AND draft_params = '' WHERE route_uid = '" . $this->db->escape($route['route_uid']) . "' ");
        }
        //удаляем помеченные на удаление точки маршрута и связанные с ними кнопки и действия
        $this->db->query("DELETE FROM " . DB_PREFIX . "route_action WHERE route_uid IN "
                . "(SELECT route_uid FROM " . DB_PREFIX . " route WHERE draft = 2 AND doctype_uid = '" . $this->db->escape($doctype_uid) . "')");
        $this->db->query("DELETE FROM " . DB_PREFIX . "route_button_field WHERE route_button_uid IN "
                . "(SELECT route_button_uid FROM " . DB_PREFIX . " route_button WHERE route_uid IN "
                . "(SELECT route_uid FROM " . DB_PREFIX . "route WHERE draft = 2 AND doctype_uid = ' " . $this->db->escape($doctype_uid) . "')"
                . ")");
        $this->db->query("DELETE FROM " . DB_PREFIX . "route_button_delegate WHERE route_button_uid IN "
                . "(SELECT route_button_uid FROM " . DB_PREFIX . " route_button WHERE route_uid IN "
                . "(SELECT route_uid FROM " . DB_PREFIX . "route WHERE draft = 2 AND doctype_uid = ' " . $this->db->escape($doctype_uid) . "')"
                . ")");
        $this->db->query("DELETE FROM " . DB_PREFIX . "route_button_description WHERE route_button_uid IN "
                . "(SELECT route_button_uid FROM " . DB_PREFIX . " route_button WHERE route_uid IN "
                . "(SELECT route_uid FROM " . DB_PREFIX . "route WHERE draft = 2 AND doctype_uid = ' " . $this->db->escape($doctype_uid) . "')"
                . ")");
        $this->db->query("DELETE FROM " . DB_PREFIX . "route_button WHERE route_uid IN "
                . "(SELECT route_uid FROM " . DB_PREFIX . " route WHERE draft = 2 AND doctype_uid = '" . $this->db->escape($doctype_uid) . "')");
        $this->db->query("DELETE FROM " . DB_PREFIX . "route_description WHERE route_uid IN "
                . "(SELECT route_uid FROM " . DB_PREFIX . "route WHERE draft = 2 AND doctype_uid = '" . $this->db->escape($doctype_uid) . "')"
        );
        $this->db->query("DELETE FROM " . DB_PREFIX . "route WHERE draft = 2 AND doctype_uid = '" . $this->db->escape($doctype_uid) . "' ");

        //проверяем наличие документов с неустановленной точкой маршрута
        $query_route = $this->db->query("SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid = '" . $this->db->escape($doctype_uid) . "' ORDER BY sort ASC LIMIT 0,1");
        if (!empty($query_route->row['route_uid'])) {
            $query = $this->db->query("UPDATE " . DB_PREFIX . "document SET route_uid='" . $this->db->escape($query_route->row['route_uid']) . "' WHERE doctype_uid = '" . $this->db->escape($doctype_uid) . "' AND route_uid = '0'");
        }

        //СОХРАНЯЕМ ИЗМЕНЕННЫЕ КНОПКИ МАРШРУТА
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "route_button WHERE draft >0 AND draft !=2 AND route_uid IN "
                . "(SELECT route_uid FROM " . DB_PREFIX . " route WHERE doctype_uid = '" . $this->db->escape($doctype_uid) . "')");
        foreach ($query->rows as $button) {
            $draft_params = array();
            if ($button['draft_params']) {
                $draft_params = unserialize($button['draft_params']);
                if (!empty($draft_params['sort'])) {
                    $sort = $draft_params['sort'];
                } else {
                    $sort = $button['sort'];
                }

                $this->db->query("UPDATE " . DB_PREFIX . "route_button SET "
                        . "picture='" . $this->db->escape($draft_params['picture'] ?? $button['picture']) . "', "
                        . "hide_button_name='" . (int) (($draft_params['picture'] ?? $button['picture']) ? ($draft_params['hide_button_name'] ?? $button['hide_button_name']) : 0) . "', "
                        . "color='" . $this->db->escape($draft_params['color'] ?? $button['color']) . "', "
                        . "background='" . $this->db->escape($draft_params['background'] ?? $button['background']) . "', "
                        . "action='" . $this->db->escape($draft_params['action'] ?? $button['action']) . "', "
                        . "action_log='" . $this->db->escape($draft_params['action_log'] ?? $button['action_log']) . "', "
                        . "action_move_route_uid ='" . $this->db->escape($draft_params['action_move_route_uid'] ?? $button['action_move_route_uid']) . "', "
                        . "show_after_execute ='" . (int) ($draft_params['show_after_execute'] ?? $button['show_after_execute']) . "', "
                        . "action_params='" . $this->db->escape($draft_params['action_params'] ?? $button['action_params']) . "', "
                        . "draft=0, draft_params='', "
                        . "sort = '" . (int) $sort . "' "
                        . "WHERE route_button_uid='" . $this->db->escape($button['route_button_uid']) . "'"
                );


                if (!empty($draft_params['description'])) {
                    $this->db->query("DELETE FROM " . DB_PREFIX . "route_button_description WHERE route_button_uid = '" . $this->db->escape($button['route_button_uid']) . "' ");
                    foreach ($draft_params['description'] as $language_id => $desription) {
                        $this->db->query("INSERT " . DB_PREFIX . "route_button_description SET "
                                . "route_button_uid = '" . $this->db->escape($button['route_button_uid']) . "' , "
                                . "name = '" . $this->db->escape($desription['name']) . "' , "
                                . "description = '" . $this->db->escape($desription['description']) . "' , "
                                . "language_id = '" . (int) $language_id . "' "
                        );
                    }
                }
                $this->db->query("DELETE FROM " . DB_PREFIX . "route_button_field WHERE route_button_uid = '" . $this->db->escape($button['route_button_uid']) . "' ");
                if (!empty($draft_params['field'])) {
                    foreach ($draft_params['field'] as $field_uid) {
                        $this->db->query("INSERT " . DB_PREFIX . "route_button_field SET "
                                . "route_button_uid = '" . $this->db->escape($button['route_button_uid']) . "' , "
                                . "field_uid = '" . $this->db->escape($field_uid) . "' "
                        );
                    }
                    //обновляем делегирование
                    $this->updateButtonDelegate($button['route_button_uid']);
                }
            }
            //удаляем драфт из кнопки
            $this->db->query("UPDATE " . DB_PREFIX . "route_button SET draft = 0 AND draft_params = '' WHERE route_button_uid = '" . $this->db->escape($button['route_button_uid']) . "'");
        }
        //удаляем помеченные на удаление кнопки
        $this->db->query("DELETE FROM " . DB_PREFIX . "route_button_delegate WHERE route_button_uid IN "
                . "(SELECT route_button_uid FROM " . DB_PREFIX . " route_button WHERE draft = 2 AND route_uid IN "
                . "(SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid = '" . $this->db->escape($doctype_uid) . "')"
                . ")");
       
        $this->db->query("DELETE FROM " . DB_PREFIX . "route_button_description WHERE route_button_uid IN "
                . "(SELECT route_button_uid FROM " . DB_PREFIX . "route_button WHERE draft = 2 AND route_uid IN "
                . "(SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid = '" . $this->db->escape($doctype_uid) . "')"
                . ")");
        $this->db->query("DELETE FROM " . DB_PREFIX . "route_button_field WHERE route_button_uid IN "
                . "(SELECT route_button_uid FROM " . DB_PREFIX . "route_button WHERE draft = 2 AND route_uid IN "
                . "(SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid = '" . $this->db->escape($doctype_uid) . "')"
                . ")");
        $this->db->query("DELETE FROM " . DB_PREFIX . "route_button WHERE draft = 2 AND route_uid IN "
                . "(SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid = '" . $this->db->escape($doctype_uid) . "')");

        //СОХРАНЯЕМ ИЗМЕННЫЕ ДЕЙСТВИЯ МАРШРУТА
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "route_action WHERE draft > 0 AND draft !=2 AND route_uid IN "
                . "(SELECT route_uid FROM route WHERE doctype_uid = '" . $this->db->escape($doctype_uid) . "')");
        foreach ($query->rows as $action) {
            $draft_params = array();
            if ($action['draft_params']) {
                $draft_params = unserialize($action['draft_params']);
                $action_name = $draft_params['action'];
                $action_log = $draft_params['action_log'];
                $params = $draft_params['params'];
                if (!empty($params['sort'])) {
                    $sort = $params['sort'];
                } else {
                    $sort = $action['sort'];
                }
                $this->db->query("UPDATE " . DB_PREFIX . "route_action SET "
                        . "action='" . $this->db->escape($action_name) . "', "
                        . "action_log='" . (int) $action_log . "', "
                        . "params='" . $this->db->escape(serialize($params)) . "', "
                        . "draft=0, draft_params='', "
                        . "sort='" . (int) $sort . "'"
                        . "WHERE route_action_uid='" . $this->db->escape($action['route_action_uid']) . "'"
                );
            } else {
                $this->db->query("UPDATE " . DB_PREFIX . "route_action SET "
                        . "draft=0 "
                        . "WHERE route_action_uid='" . $this->db->escape($action['route_action_uid']) . "'"
                );
            }
        }
        //удаляем  помеченные на удаление действия
        $this->db->query("DELETE FROM " . DB_PREFIX . "route_action WHERE draft = 2 AND route_uid IN "
                . "(SELECT route_uid FROM route WHERE doctype_uid = '" . $this->db->escape($doctype_uid) . "')");

        //проверяем сортировку действий
        //получаем все точки маршрута
        $query_doctype_route = $this->db->query("SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid = '" . $doctype_uid . "' ");
        foreach ($query_doctype_route->rows as $route_uid) {
            $query_route_action = $this->db->query("SELECT route_action_uid, sort FROM route_action WHERE route_uid = '" . $route_uid['route_uid'] . "' ORDER BY sort ASC");
            $sort = 1;            
            foreach ($query_route_action->rows as $action) {
                $this->db->query("UPDATE " . DB_PREFIX . "route_action SET sort='" . (int) $sort++ . "' WHERE route_action_uid = '" . $action['route_action_uid'] . "'");
            }
        }


        //сбрасываем драфт, устанавливаем дату изменения
        $this->db->query("UPDATE " . DB_PREFIX . "doctype SET date_edited = NOW(), draft = 0, draft_params = '' WHERE doctype_uid = '" . $this->db->escape($doctype_uid) . "' ");

    }

    public function getDocuments($data) {
        $field_info = $this->getField($data['field_uid']);
        $sql = "SELECT * FROM " . DB_PREFIX . "field_value_" . $field_info['type'] . " WHERE field_uid = '" . $this->db->escape($data['field_uid']) . "' AND document_uid IN "
                . "(SELECT document_uid FROM document WHERE doctype_uid='" . $this->db->escape($data['doctype_uid']) . "') ";
        if (!empty($data['filter_name'])) {
            $sql .= "AND display_value LIKE '%" . $this->db->escape($data['filter_name']) . "%' ";
        }
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getDoctypes($data) {
        $sql = "SELECT d.*, dd.name, dd.short_description, dd.language_id FROM " . DB_PREFIX . "doctype d "
                . "LEFT JOIN " . DB_PREFIX . "doctype_description dd ON (d.doctype_uid = dd.doctype_uid AND dd.language_id='" . $this->config->get('config_language_id') . "') WHERE draft < 3 ";
        if (!empty($data['filter_name'])) {
            $sql .= " AND ";
            $filter_name = explode(" ", $data['filter_name']);
            $filter_names = array();
            foreach ($filter_name as $word) {
                $filter_names[] = " dd.name LIKE '%" . $this->db->escape($word) . "%' ";
            }
            $sql .= implode(" AND ", $filter_names);
        }
        if (!empty($data['sort'])) {
            $sql .= " ORDER BY " . $this->db->escape($data['sort']);
            if (!empty($data['order'])) {
                $sql .= " " . $this->db->escape($data['order']);
            } else {
                $sql .= " ASC ";
            }
        }
        if (isset($data['start']) && isset($data['limit'])) {
            $sql .= " LIMIT " . (int) $data['start'] . "," . $data['limit'];
        }
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getTotalDoctypes($data) {
        $sql = "SELECT COUNT(doctype_uid) as total FROM " . DB_PREFIX . "doctype WHERE draft < 3 ";
        if (!empty($data['filter_name'])) {
            $filter_name = explode(" ", $data['filter_name']);
            $filter_names = array();
            foreach ($filter_name as $word) {
                $filter_names[] = " name LIKE '%" . $this->db->escape($word) . "%' ";
            }
            $sql .= " AND " . implode(" AND ", $filter_names);
        }
        $query = $this->db->query($sql);
        return $query->row['total'];
    }

    /**
     * Возвращает параметры доктайпа и описание с учетом текущего языка, если установлен draft=true, то с учетом сохраненного черновика
     * @param type $doctype_uid
     * @param type $draft
     * @return type
     */
    public function getDoctype($doctype_uid, $draft=false) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "doctype d "
                . "LEFT JOIN doctype_description dd ON (d.doctype_uid = dd.doctype_uid AND dd.language_id = '" . (int) $this->config->get('config_language_id') . "') "
                . "WHERE d.doctype_uid='" . $this->db->escape($doctype_uid) . "'");
        $result = array();
        if ($query->num_rows) {
            $result['date_added'] = $query->row['date_added'];
            $result['date_edited'] = $query->row['date_edited'];
            $result['user_uid'] = $query->row['user_uid'];
            $result['draft'] = $query->row['draft'];
            $result['draft_params'] = $query->row['draft_params'];
            if ($query->row['draft'] && $query->row['draft_params'] && $draft) {
                $params = unserialize($query->row['draft_params']);
            }
            $result['field_log_uid'] = $params['field_log_uid'] ?? $query->row['field_log_uid'];
            $result['name'] = $params['doctype_description'][$this->config->get('config_language_id')]['name'] ?? $query->row['name'];
            $result['short_description'] = $params['doctype_description'][$this->config->get('config_language_id')]['short_description'] ?? $query->row['short_description'];
            $result['params'] = $params['params'] ?? unserialize($query->row['params']);
            if (!empty($result['params']['doctype_template'])) {                
                $doctype_template = array();
                foreach ($result['params']['doctype_template'] as $type => $templates) {
                    $index = 1;
                    ksort($templates);
                    foreach ($templates as $content) {
                        $doctype_template[$type][$index++] = $content;
                    }
                }
                $result['params']['doctype_template'] = $doctype_template;
            }
            
        }
        return $result;
    }

    /*
     * Возвращает непреобразованный шаблон доктайпа (по языку и типу), то есть с идентификаторами полей, а не с именами
     */

//    public function getDoctypeTemplate($doctype_uid, $type_template, $language_id) {
//        //$sort определяет какой именно шаблон нужно вернуть 0-по умолчанию
//        $sort = 0;
//        $query = $this->db->query("SELECT template FROM " . DB_PREFIX . "doctype_template WHERE "
//                . "doctype_uid = '" . $this->db->escape($doctype_uid) . "' AND language_id = '" . (int) $language_id . "' "
//                . "AND type='" . $this->db->escape($type_template) . "' AND sort='" . (int) $sort . "' ");
//        if ($query->num_rows) {
//            return $query->row['template'];
//        } else {
//            return "";
//        }
//    }

    public function getDoctypeDescriptions($doctype_uid, $draft=false) {
        $doctype_description_data = array();
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "doctype_description "
                . "WHERE doctype_uid = '" . $this->db->escape($doctype_uid) . "'");

        if ($draft) {
            $query_doctype = $this->db->query("SELECT draft, draft_params FROM " . DB_PREFIX . "doctype WHERE doctype_uid='" . $this->db->escape($doctype_uid) . "'");            
        }
        if ($draft && !empty($query_doctype->row['draft']) && !empty($query_doctype->row['draft_params'])) {
            $draft_params = unserialize($query_doctype->row['draft_params']);
            $doctype_description_data = $draft_params['doctype_description'];
        } elseif ($query->num_rows) {                
            foreach ($query->rows as $result) {
                $doctype_description_data[$result['language_id']] = array(
                    'name' => $result['name'],
                    'short_description' => $result['short_description'],
                );
            }                
        }            
        return $doctype_description_data;
    }

    public function getDoctypeTemplates($doctype_uid) {

        $query_doc = $this->db->query("SELECT draft, draft_params FROM " . DB_PREFIX . "doctype WHERE doctype_uid='" . $doctype_uid . "'");
        if ($query_doc->row['draft'] && $query_doc->row['draft_params']) {
            $templates = array();
            $draft_params = unserialize($query_doc->row['draft_params']);
            foreach ($draft_params['doctype_template'] as $type=>$doctype_template_data) {
                ksort($doctype_template_data);
                $index = 0;
                foreach ($doctype_template_data as $templates_data) {                        
                    if (is_array($templates_data) && $templates_data) {
                        foreach ($templates_data as $name => $value) {
                            if ($name !== "params") {
                                $templates[] = array(
                                    'type'          => $type,
                                    'language_id'   => $name,
                                    'template'      => $value,
                                    'sort'          => $index
                                );                            
                            }
                        }
                        $index++;                        
                    }
                }
            }
        } else {
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "doctype_template "
                    . "WHERE doctype_uid = '" . $this->db->escape($doctype_uid) . "' ORDER BY `sort` ");
            $templates = $query->rows;            
        }
        
        $doctype_info = $this->getDoctype($doctype_uid,true);
        $doctype_templates = array();
        foreach ($templates as $result) {
            $doctype_templates[$result['type']][$result['sort']][$result['language_id']] = $this->getNamesTemplate($result['template'], $doctype_uid, $this->getTemplateVariables());
            if (!empty($result['sort']) && isset($doctype_info['params']['doctype_template'][$result['type']][$result['sort']])) {
                $doctype_templates[$result['type']][$result['sort']]['params'] = $doctype_info['params']['doctype_template'][$result['type']][$result['sort']];
            }
        }
        return $doctype_templates;
    }

    public function getRoutes($data) {
        $sql = "SELECT r.route_uid, r.doctype_uid, r.draft, r.draft_params";
        $where = array();

        if (isset($data['filter_name']) && empty($data['doctype_uid'])) {
            $sql .= ", dd.name as doctype_name FROM " . DB_PREFIX . "route r "
                    . "LEFT JOIN " . DB_PREFIX . "doctype_description dd ON (r.doctype_uid = dd.doctype_uid AND dd.language_id = '" . (int) $this->config->get('config_language_id') . "') ";                
            $where[] = "(r.doctype_uid IN (SELECT doctype_uid FROM " . DB_PREFIX . "doctype_description WHERE name LIKE '%" . $this->db->escape($data['filter_name'])  . "%') "
                    . "|| r.route_uid IN (SELECT route_uid FROM " . DB_PREFIX . "route_description WHERE name LIKE '%" . $this->db->escape($data['filter_name']) . "%'))";

        } else {
            $sql .= " FROM " . DB_PREFIX . "route r ";
        }
        if (!empty($data['doctype_uid'])) {
            $where[] = "r.doctype_uid = '" . $this->db->escape($data['doctype_uid']) . "'";
            $sort = " ORDER BY sort ASC ";
        } else {
            $sort = " ORDER BY r.doctype_uid ASC ";
        }
        if ($where) {
            $sql .= " WHERE " . implode(" AND ", $where);
        }
        $sql .= $sort;
        $query = $this->db->query($sql);
        $result = array();
        foreach ($query->rows as $route) { 
            $draft_params = array();
            if ($route['draft'] && $route['draft_params']) {
                $draft_params = unserialize($route['draft_params']);
            }
            $description = $draft_params['description']  ?? $this->getRouteDescriptions($route['route_uid']);
//            if (empty($data['filter_name']) || (isset($description[$this->config->get('config_language_id')]['name']) && strpos(mb_strtolower($description[$this->config->get('config_language_id')]['name']), mb_strtolower($data['filter_name'])) !== false)) {
                $result[] = array(
                    'route_uid' => $route['route_uid'],
                    'doctype_uid' => $route['doctype_uid'],
                    'description' => $description,
                    'name' => $description[$this->config->get('config_language_id')]['name'] ?? "",
                    'actions' => $this->getRouteActions($route['route_uid']),
                    'draft' => $route['draft'],
                    'draft_params' => $route['draft_params'],
                    'doctype_name' => $route['doctype_name'] ?? ""
                );
                
//            }
        }
        return $result;
    }

    public function getRoute($route_uid) {
        $sql = "SELECT * FROM " . DB_PREFIX . "route r WHERE r.route_uid = '" . $this->db->escape($route_uid) . "' ";
        $query = $this->db->query($sql);
        $route = $query->row;
        if (!$route) {
            return array();
        }
        $description = $this->getRouteDescriptions($route['route_uid']);
        $result = array(
            'route_uid' => $route['route_uid'],
            'doctype_uid' => $route['doctype_uid'],
            'description' => $description,
            'name' => isset($description[$this->config->get('config_language_id')]['name']) ? $description[$this->config->get('config_language_id')]['name'] : "",
            'actions' => $this->getRouteActions($route['route_uid']),
            'sort' => $route['sort'],
            'draft' => $route['draft'],
            'draft_params' => $route['draft_params']
        );
        return $result;
    }

    public function getRouteDescriptions($route_uid) {
        $result = array();
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "route r WHERE r.route_uid = '" . $this->db->escape($route_uid) . "' ");
        if ($query->num_rows > 0) {
            if ($query->row['draft'] && $query->row['draft_params']) {
                $descriptions = unserialize($query->row['draft_params']);
                $result = $descriptions['descriptions'];
            } else {
                $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "route_description "
                        . "WHERE route_uid = '" . $this->db->escape($route_uid) . "' ");
                foreach ($query->rows as $route) {
                    $result[$route['language_id']] = array(
                        'name' => $route['name'],
                        'description' => $route['description']
                    );
                }
            }
            return $result;
        } else {
            return array();
        }
    }

    public function getFields($data) {
        if ($data['doctype_uid']) {
            $sql = "SELECT * FROM " . DB_PREFIX . "field f "
                    . "WHERE doctype_uid='" . $this->db->escape($data['doctype_uid']) . "' ";
        } else {
            $sql = "SELECT f.field_uid, f.setting, CONCAT(dd.name, ' - ', f.name) AS name FROM " . DB_PREFIX . "field f "
                    . "LEFT JOIN " . DB_PREFIX . "doctype_description dd ON (dd.doctype_uid = f.doctype_uid "
                    . "AND dd.language_id = '" . $this->config->get('config_language_id') . "') "
                    . "WHERE f.draft<3 ";
        }
        if (!empty($data['filter_name'])) {
            $sql .= "AND ";
            if (!$data['doctype_uid']) {
                $sql .= " (";
            }
            $sql .= "f.name LIKE '%" . $this->db->escape($data['filter_name']) . "%' ";
            if (!$data['doctype_uid']) {
                $sql .= " OR dd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%') ";
            }
        }
        if (isset($data['setting'])) {
            $sql .= "AND f.setting = '" . (int) $data['setting'] . "' ";
        }
        if (isset($data['access_view'])) {
            if ($data['access_view']) {
                $sql .= "AND f.access_view LIKE '%" . $this->db->escape($data['access_view']) . "%' ";
            } else {
                $sql .= "AND (f.access_view = '0' OR f.access_view='' OR f.access_view is NULL) ";
            }
            
        }
        if (isset($data['required'])) {
            $sql .= "AND f.required = '" . (int) $data['required'] . "' ";
        }
        if (!$data['doctype_uid']) {
            $sql .= " ORDER BY dd.name ASC";
        } else {
            $sql .= " ORDER BY f.sort ASC";
        }
        if (!empty($data['limit'])) {
            $sql .= " LIMIT 0," . (int) $data['limit'];
        }
        $query = $this->db->query($sql);
        if ($data['doctype_uid']) {
            $result = array();
            foreach ($query->rows as $field) {
                $draft_params = array();
                if ($field['draft'] && $field['draft_params']) { //draft может быть равен 2, а массив draft_params пуст, если пометили на удаление поле, которое не изменялось
                    $draft_params = unserialize($field['draft_params']);
                    $name = $draft_params['field_name'];
                    $params = $draft_params;
                    if (!empty($params['sort'])) {
                        $sort = $params['sort'];
                    } else {
                        $sort = $field['sort'];
                    }
                    if (isset($params['change_field'])) {
                        $change_field = $params['change_field'];
                    } else {
                        $change_field = $field['change_field'];
                    }
                    if (isset($params['access_form'])) {
                        $access_form = implode(",",$params['access_form']);
                    } else {
                        $access_form = $field['access_form'];
                    }
                    if (isset($params['access_view'])) {
                        $access_view = implode(",",$params['access_view']);
                    } else {
                        $access_view = $field['access_view'];
                    }
                    if (isset($params['required'])) {
                        $required = $params['required'];
                    } else {
                        $required = $field['required'];
                    }
                    if (isset($params['unique'])) {
                        $unique = $params['unique'];
                    } else {
                        $unique = $field['unique'];
                    }
                } else {
                    $name = $field['name'];
                    $params = unserialize($field['params']);
                    $sort = $field['sort'];
                    $change_field = $field['change_field'];
                    $access_form = $field['access_form'];
                    $access_view = $field['access_view'];
                    $required = $field['required'];
                    $unique = $field['unique'];

                }
                $attrs = array();
                if ($change_field) { 
                    $attrs[] = $this->language->get("text_attr_change_field");
                }
                if ($access_form) {
                    $attrs[] = $this->language->get("text_attr_access_form");
                }
                if ($access_view) {
                    $attrs[] = $this->language->get("text_attr_access_view");
                }                
                if ($required) {
                    $attrs[] = $this->language->get("text_attr_required");
                }                
                if ($unique) {
                    $attrs[] = $this->language->get("text_attr_unique");
                }            
                if (!empty($data['system'])) {
                    $query = $this->db->query("SELECT setting_id FROM " . DB_PREFIX . "setting WHERE value LIKE '%" . $this->db->escape($field['field_uid']) . "%'");
                    if ($query->num_rows) {
                        $system = 1;
                    } else {
                        $system = 0;
                    }
                }
                $result[] = array(
                    'field_uid' => $field['field_uid'],
                    'name' => $name,
                    'type' => $field['type'],
                    'type_title' => $this->load->controller('extension/field/' . $field['type'] . '/getTitle'),
                    'params' => $params,
                    'params_description' => $this->load->controller('extension/field/' . $field['type'] . '/getDescriptionParams', $params),
                    'attributes'    => $attrs,
                    'setting' => $field['setting'],
                    'change_field' => $change_field,
                    'access_form' => $access_form,
                    'access_view' => $access_view,
                    'required' => $required,
                    'unique' => $unique,
                    'sort' => $sort,
                    'draft' => $field['draft'],
                    'system'    => !empty($data['system']) ? $system : null
                        // 'draft_params'              => unserialize($field['draft_params'])
                );
            }
            //сортируем поля по sort с учетом драфта
            usort($result, function($a, $b) {
                return $a['sort'] <=>$b['sort'];
            });
            return $result;
        } else {
            return $query->rows;
        }
    }

    /**
     * Добавление поля данного типа в тип документа. Все данные формы передаются через POST
     * $doctype_uid - тип документа
     * $data - тип поля и его параметры в массиве
     * $draft - 1 - черновик, 0 - нет, 2 - поле было удалено из доктайпа, но доктайп пока не сохранены
     */
    public function addField($doctype_uid, $data, $draft = 1) {
        $params = array();
        foreach ($data as $key => $value) {
            if ($key != "field_type") {
                $params[$key] = $value;
            }
        }
        $field_params = $this->load->controller('extension/field/' . $data['field_type'] . '/setParams', $params);
        if ($field_params) {
            $params = $field_params;
        }
        $query = $this->db->query("SELECT sort FROM " . DB_PREFIX . "field WHERE doctype_uid = '" . $this->db->escape($doctype_uid) . "' ORDER BY sort DESC LIMIT 0,1");
        if ($query->num_rows) {
            $sort = (int) $query->row['sort'] + 1;
        } else {
            $sort = 1;
        }
        $params['sort'] = $sort;
        $query_uid = $this->db->query("SELECT UUID() AS uid");
        $field_uid = $query_uid->row['uid'];
        $sql = "INSERT INTO " . DB_PREFIX . "field SET "
                . "field_uid='" . $field_uid . "', "
                . "doctype_uid='" . $this->db->escape($doctype_uid) . "', "
                . "name='" . $this->db->escape($data['field_name']) . "', "
                . "type='" . $this->db->escape($data['field_type']) . "', "
                . "setting='" . $this->db->escape($data['setting']) . "', "
                . "change_field='" . $this->db->escape($data['change_field']) . "', "
                . "access_form='" . (!empty($data['access_form']) ? $this->db->escape(implode(",",$data['access_form'])) : ""). "', "
                . "access_view='" . (!empty($data['access_view']) ? $this->db->escape(implode(",",$data['access_view'])) : ""). "', "
                . "`required`='" . (!empty($data['required']) ? 1 : 0). "', "
                . "`unique`='" . (!empty($data['unique']) ? 1 : 0). "', "
                . "params='" . $this->db->escape(serialize($params)) . "', "
                . "draft='" . (int) $draft . "', "
                . "draft_params='" . $this->db->escape(serialize($params)) . "', "
                . "sort = '" . (int) $sort . "'";
        $this->db->query($sql);
        $this->db->query("UPDATE " . DB_PREFIX . "doctype SET draft = (CASE WHEN draft='3' THEN 3 ELSE 1 END) WHERE doctype_uid = '" . $this->db->escape($doctype_uid) . "'");
        return $field_uid;
    }

    /**
     * Добавление кнопки в маршрут. Все данные формы передаются через POST
     * $route_uid - точка маршрута
     * $data - параметры кнопки
     * $draft - 1 - черновик, 0 - нет, 2 - кнопка было удалено из доктайпа, но доктайп пока не сохранен
     */
    public function addRouteButton($route_uid, $data, $draft = 1) {
        if ($data['route_button_action']) {
            $data_params = array(
                'route_uid' => $route_uid,
                'params' => $data
            );
            $action_params = $this->load->controller('extension/action/' . $data['route_button_action'] . '/setParams', $data_params);
        } else {
            $action_params = array();
        }
        $query = $this->db->query("SELECT sort FROM " . DB_PREFIX . "route_button WHERE route_uid = '" . $this->db->escape($route_uid) . "' ORDER BY sort DESC LIMIT 0,1");
        if ($query->num_rows) {
            $sort = (int) $query->row['sort'] + 1;
        } else {
            $sort = 1;
        }
        if ($action_params) {
            $action_params['params']['sort'] = $sort;
        }

        $draft_button = serialize(array(
            'description' => $data['route_button_descriptions'],
            'picture' => $data['route_button_picture'],
            'hide_button_name' => $data['route_button_picture'] ? (int) ($data['hide_button_name'] ?? 0) : 0,
            'color' => $data['route_button_color'],
            'background' => $data['route_button_background'],
            'field' => !empty($data['route_button_field']) ? $data['route_button_field'] : "",
            'action' => !empty($data['route_button_action']) ? $data['route_button_action'] : "",
            'action_log' => !empty($data['action_log']) ? (int) $data['action_log'] : "0",
            'action_move_route_uid' => $this->db->escape($data['action_move_route_uid'] ?? "0"),
            'show_after_execute' => (int) ($data['show_after_execute'] ?? 0),
            'action_params' => serialize($action_params),
            'sort' => $sort
        ));
        $query_uid = $this->db->query("SELECT UUID() AS uid");
        $route_button_uid = $query_uid->row['uid'];
        $sql = "INSERT INTO " . DB_PREFIX . "route_button SET "
                . "route_button_uid = '" . $route_button_uid . "', "
                . "route_uid='" . $this->db->escape($route_uid) . "', "
                . "draft='" . (int) $draft . "', "
                . "draft_params = '" . $this->db->escape($draft_button) . "', "
                . "sort = '" . (int) $sort . "' ";
        $this->db->query($sql);
        $this->db->query("UPDATE " . DB_PREFIX . "doctype SET draft = (CASE WHEN draft='3' THEN 3 ELSE 1 END) WHERE doctype_uid = (SELECT doctype_uid FROM route WHERE route_uid = '" . $this->db->escape($route_uid) . "')");
        return $route_button_uid;
    }

    public function getMaxRouteId($doctype_uid) {
        $query = $this->db->query("SELECT MAX(sort) as max_id FROM " . DB_PREFIX . "route WHERE doctype_uid = '" . $this->db->escape($doctype_uid) . "' ");
        return $query->row['max_id'];
    }

    public function addRoute($doctype_uid, $data, $draft = 1) {
        $route_draft = serialize(array(
            'descriptions' => $data['route_descriptions']
        ));
        if (!empty($data['route_place_id'])) {
            //точку маршрута нужно добавить перед существующей
            $query = $this->db->query("SELECT route_uid, sort FROM " . DB_PREFIX . "route WHERE doctype_uid = '" . $this->db->escape($doctype_uid) . "' ORDER BY sort DESC");
            $result = array();
            $count = count($query->rows) + 1;
            foreach ($query->rows as $route) {
                //5,4,3,2,1
                if ($route['route_uid'] != $data['route_place_id']) {
                    $result[$route['route_uid']] = $count--;
                } else {
                    $result[$route['route_uid']] = $count--;
                    $sort = $count--;
                }
            }
            foreach ($result as $route_uid => $route_sort) {
                $this->db->query("UPDATE " . DB_PREFIX . "route SET sort = '" . (int) $route_sort . "' WHERE route_uid = '" . $this->db->escape($route_uid) . "' ");
            }
        } else {
            $sort = (int) $this->getMaxRouteId($doctype_uid) + 1;
        }
        $query_uid = $this->db->query("SELECT UUID() AS uid");
        $route_uid = $query_uid->row['uid'];
        $sql = "INSERT INTO " . DB_PREFIX . "route SET "
                . "route_uid = '" . $route_uid . "', "
                . "doctype_uid='" . $this->db->escape($doctype_uid) . "', "
                . "sort='" . (int) $sort . "', "
                . "draft='" . (int) $draft . "', "
                . "draft_params='" . $this->db->escape($route_draft) . "' "
        ;
        $this->db->query($sql);
        $this->db->query("UPDATE " . DB_PREFIX . "doctype SET draft = (CASE WHEN draft='3' THEN 3 ELSE 1 END) WHERE doctype_uid = '" . $this->db->escape($doctype_uid) . "'");
        return $route_uid;
    }

    /**
     * Добавление действия в маршрут. Все данные формы передаются через POST
     * $route_uid - точка маршрута
     * $data - параметры действия
     * $draft - 1 - черновик, 0 - нет, 2 - действие было удалено из доктайпа, но доктайп пока не сохранен, 3- действие добавлено и ни разу не сохранялось
     */
    public function addRouteAction($route_uid, $context, $data, $draft = 1) {
        if ($data['route_action']) {
            $data_params = array(
                'route_uid' => $route_uid,
                'params' => $data
            );
            $action_params = $this->load->controller('extension/action/' . $data['route_action'] . '/setParams', $data_params);
        } else {
            $action_params = array();
        }

        //получаем сортировку
        $actions = $this->getRouteActions($route_uid, $context);
        if ($actions) {
            $max_sort_action = $actions[count($actions) - 1];
            $sort = $max_sort_action['sort'];
            $sort++;
        } else {
            $sort = 1;
        }

        if ($action_params) {
            $action_params['params']['sort'] = $sort;
        }
        $params = array();
        $params['params'] = $action_params;
        $params['action'] = $data['route_action'];
        $params['action_log'] = $data['action_log'];
        $query_uid = $this->db->query("SELECT UUID() AS uid");
        $route_action_uid = $query_uid->row['uid'];
        $this->db->query("INSERT INTO " . DB_PREFIX . "route_action SET "
                . "route_action_uid = '" . $route_action_uid . "', "
                . "route_uid='" . $this->db->escape($route_uid) . "', "
                . "context='" . $this->db->escape($context) . "', "
                . "action='" . $this->db->escape($data['route_action']) . "', "
                . "action_log='" . (int) $data['action_log'] . "', "
                . "params='" . $this->db->escape(serialize($action_params)) . "', "
                . "description='" . $this->db->escape($data['action_description']) . "', "
                . "draft='" . (int) $draft . "', "
                . "draft_params='" . $this->db->escape(serialize($params)) . "', "
                . "sort='" . (int) $sort . "', "
                . "status='" . (int) $data['action_status'] . "' "
        );
        $this->db->query("UPDATE " . DB_PREFIX . "doctype SET draft = (CASE WHEN draft='3' THEN 3 ELSE 1 END) WHERE doctype_uid = (SELECT doctype_uid FROM route WHERE route_uid = '" . $this->db->escape($route_uid) . "')");
        return $route_action_uid;
    }

    public function copyRouteAction($route_action_uid, $route_uid, $context) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "route_action WHERE route_action_uid = '" . $this->db->escape($route_action_uid) . "'");
        if ($query->num_rows) {
            //получаем сортировку
            $actions = $this->getRouteActions($route_uid, $context);
            if ($actions) {
                $max_sort_action = $actions[count($actions) - 1];
                $sort = $max_sort_action['sort'];
                $sort++;
            } else {
                $sort = 1;
            }
            $query_uid = $this->db->query("SELECT UUID() AS uid");
            $route_action_uid = $query_uid->row['uid'];
            $this->db->query("INSERT INTO " . DB_PREFIX . "route_action SET "
                    . "route_action_uid = '" . $route_action_uid . "', "
                    . "route_uid = '" . $this->db->escape($route_uid) . "', "
                    . "context = '" . $this->db->escape($context) . "', "
                    . "action = '" . $this->db->escape($query->row['action']) . "', "
                    . "action_log = '" . $this->db->escape($query->row['action_log']) . "', "
                    . "params = '" . $this->db->escape($query->row['params']) . "', "
                    . "description = '" . $this->db->escape($query->row['description']) . "', "
                    . "draft = '3', "
                    . "draft_params = '" . $this->db->escape($query->row['draft_params']) . "', "
                    . "sort = '" . (int) $sort . "', "
                    . "status = 1 "
            );
            return $route_action_uid;
        } else {
            return 0;
        }
    }

    public function copyRouteButton($route_button_uid, $route_uid) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "route_button WHERE route_button_uid = '" . $this->db->escape($route_button_uid) . "'");
        if ($query->num_rows) {
            //получаем сортировку
            $buttons = $this->getRouteButtons(array('route_uid'=>$route_uid));
            if ($buttons) {
                $max_sort_button = $buttons[count($buttons) - 1];
                $sort = $max_sort_button['sort'];
                $sort++;
            } else {
                $sort = 1;
            }
            $query_uid = $this->db->query("SELECT UUID() AS uid");
            $copy_route_button_uid = $query_uid->row['uid'];
            $sql = "INSERT INTO " . DB_PREFIX . "route_button SET " 
                    . "route_button_uid = '" . $copy_route_button_uid . "', "
                    . "route_uid = '" . $this->db->escape($route_uid) . "', "
                    . "sort = '" . (int) $sort . "', "
                    . "draft = 3, ";
            $sql_sets = array();
            foreach ($query->row as $name => $value) {
                if ($name == "route_button_uid" || $name == "route_uid" 
                        || $name == "sort" || $name == "draft") {
                    continue;
                }     
                if ($name == 'draft_params') {
                    $draft_params = unserialize($value);
                    if (!isset($draft_params['field'])) {
                        //копируем делегирование
                        $query_field = $this->db->query("SELECT * FROM " . DB_PREFIX . "route_button_field WHERE route_button_uid='" . $route_button_uid . "' ");
                        if ($query_field->num_rows) {
                            $draft_params['field'] = array();
                            foreach ($query_field->rows as $field) {
                                $draft_params['field'][] = $field['field_uid'];
                            }                        
                            $value = serialize($draft_params);
                        }
                    }
                }
                $sql_sets[] = $name . "='" . $value . "'";
            }
            $this->db->query($sql . implode(", ", $sql_sets));   
            
            //копируем описание
            $query_description = $this->db->query("SELECT * FROM " . DB_PREFIX . "route_button_description WHERE route_button_uid='" . $route_button_uid . "' ");
            $sql = "INSERT INTO " . DB_PREFIX . "route_button_description SET route_button_uid = '" . $copy_route_button_uid . "', ";
            foreach ($query_description->rows as $description) {
                $sql_sets = array();
                foreach ($description as $name => $value) {
                    if ($name == "route_button_uid") {
                        continue;
                    }
                    $sql_sets[] = $name . "='" . $value . "'";
                }
                $this->db->query($sql . implode(", ", $sql_sets));
            }
            return $copy_route_button_uid;
        } else {
            return 0;
        }
    }

    /**
     * Изменение кнопки маршрута. 
     * $draft - 1 - черновик, 0 - нет, 2 - кнопка была удалена из доктайпа, но доктайп пока не сохранен, 3 - кнопка ни разу не сохранялась
     */
    public function editRouteButton($button_uid, $data, $draft = 1) {
        if ($data['route_button_action']) {
            $data_params = array(
                'route_button_uid' => $button_uid,
                'params' => $data
            );
            $action_params = $this->load->controller('extension/action/' . $data['route_button_action'] . '/setParams', $data_params);
        } else {
            $action_params = array();
        }

        //переносим sort из сохраненного драфта (если он есть)
        $action_info = $this->getRouteButton($button_uid);
        $sort = $action_info['sort'];

        $draft_button = serialize(array(
            'description' => $data['route_button_descriptions'],
            'picture' => $data['route_button_picture'],
            'hide_button_name' => $data['route_button_picture'] ? (int) ($data['hide_button_name'] ?? 0) : 0,
            'color' => $data['route_button_color'],
            'background' => $data['route_button_background'],
            'field' => !empty($data['route_button_field']) ? $data['route_button_field'] : "",
            'action' => !empty($data['route_button_action']) ? $data['route_button_action'] : "",
            'action_log' => !empty($data['action_log']) ? (int) $data['action_log'] : "0",
            'action_move_route_uid' => $this->db->escape($data['action_move_route_uid'] ?? "0"),
            'show_after_execute' => (int) ($data['show_after_execute'] ?? 0),
            'action_params' => serialize($action_params),
            'sort' => $sort
        ));
        $sql = "UPDATE " . DB_PREFIX . "route_button SET "
                . "draft= (CASE WHEN draft='3' THEN 3 ELSE " . (int) $draft . " END), "
                . "draft_params = '" . $this->db->escape($draft_button) . "' "
                . "WHERE route_button_uid = '" . $this->db->escape($button_uid) . "' ";
        $this->db->query($sql);
        $button_info = $this->getRouteButton($button_uid);
        $route_info = $this->getRoute($button_info['route_uid']);
        $this->setDraft($route_info['doctype_uid']);
    }

    public function editSortRouteButton($route_button_uid, $sort) {
        $button_info = $this->getRouteButton($route_button_uid);
        $fields = array();
        foreach ($button_info['fields'] as $field) {
            $fields[] = $field['field_uid'];
        }
        $draft_button = serialize(array(
            'description' => $button_info['descriptions'],
            'picture' => $button_info['picture'],
            'hide_button_name' => $button_info['picture'] ? (int) $button_info['hide_button_name'] : 0,
            'color' => $button_info['color'],
            'background' => $button_info['background'],
            'show_after_execute' => $button_info['show_after_execute'],
            'field' => $fields,
            'action' => $button_info['action'],
            'action_log' => $button_info['action_log'],
            'action_move_route_uid' => $button_info['action_move_route_uid'],
            'action_params' => serialize($button_info['action_params']),
            'sort' => $sort
        ));
        $button_info['sort'] = (int) $sort;
        $this->db->query("UPDATE " . DB_PREFIX . "route_button SET "
                . "draft='" . ($button_info['draft'] ? $button_info['draft'] : 1) . "', "
                . "draft_params = '" . $this->db->escape($draft_button) . "' "
                . "WHERE route_button_uid = '" . $this->db->escape($route_button_uid) . "' ");
        $route_info = $this->getRoute($button_info['route_uid']);
        $this->setDraft($route_info['doctype_uid']);
    }

    public function editRoute($route_uid, $data, $draft = 1) {
        $draft_route = serialize(array(
            'descriptions' => $data['route_descriptions']
        ));
        $this->db->query("UPDATE " . DB_PREFIX . "route SET "
                . "draft = (CASE WHEN draft='3' THEN 3 ELSE " . (int) $draft . " END), "
                . "draft_params = '" . $this->db->escape($draft_route) . "' "
                . "WHERE route_uid = '" . $this->db->escape($route_uid) . "' ");
        $route_info = $this->getRoute($route_uid);
        $this->setDraft($route_info['doctype_uid']);
    }

    /**
     * Изменение действия маршрута. 
     * $draft - 1 - черновик, 0 - нет, 2 - кнопка была удалена из доктайпа, но доктайп пока не сохранен, 3 - действие ни разу не сохранялось
     */
    public function editRouteAction($route_action_uid, $data, $draft = 1) {
        if ($data['route_action']) {
            $data_params = array(
                'route_action_uid' => $route_action_uid,
                'params' => $data
            );
            $action_params = $this->load->controller('extension/action/' . $data['route_action'] . '/setParams', $data_params);
        } else {
            $action_params = array();
        }
        if ($action_params) { //переносим sort из сохраненного драфта (если он есть)
            $action_info = $this->getRouteAction($route_action_uid);
            if (!empty($action_info['action_params']['sort'])) {
                $action_params['params']['sort'] = $action_info['action_params']['sort'];
            }
        }
        $draft_action = serialize(array(
            'action'        => $data['route_action'] ?? "",
            'action_log'    => $data['action_log'] ?? "0",
            'params'        => $action_params,
        ));
        $sql = "UPDATE " . DB_PREFIX . "route_action SET "
                . "draft=(CASE WHEN draft='3' THEN 3 ELSE " . (int) $draft . " END), "
                . "draft_params = '" . $this->db->escape($draft_action) . "',"
                . "description = '" . $this->db->escape($data['action_description']) . "',"
                . "status = '" . (int) ($data['action_status'] ?? 1) . "' "
                . "WHERE route_action_uid = '" . $this->db->escape($route_action_uid) . "' ";
        $this->db->query($sql);
        $route_action_info = $this->getRouteAction($route_action_uid);
        $route_info = $this->getRoute($route_action_info['route_uid']);
        $this->setDraft($route_info['doctype_uid']);
    }

    public function editSortRouteAction($route_action_uid, $sort) {
        $action_info = $this->getRouteAction($route_action_uid);
        $params = array();
        $params['params'] = $action_info['action_params'];
        $params['params']['sort'] = (int) $sort;
        $params['action'] = $action_info['action'];
        $params['action_log'] = $action_info['action_log'];
        $this->db->query("UPDATE " . DB_PREFIX . "route_action SET "
                . "draft='" . ($action_info['draft'] ? $action_info['draft'] : 1) . "', "
                . "draft_params = '" . $this->db->escape(serialize($params)) . "' "
                . "WHERE route_action_uid = '" . $this->db->escape($route_action_uid) . "' ");
        $route_info = $this->getRoute($action_info['route_uid']);
        $this->setDraft($route_info['doctype_uid']);
    }
    

    public function getRouteButtons($data) {
        
        $sql = "SELECT * FROM " . DB_PREFIX . "route_button WHERE ";
        $where = array();
        if (!empty($data['filter_name'])) {
            $where[] = "(route_button_uid IN (SELECT DISTINCT route_button_uid FROM " . DB_PREFIX . "route_button_description WHERE name LIKE '%" . $this->db->escape($data['filter_name']) . "%') "
                    . "OR route_uid IN (SELECT DISTINCT route_uid FROM " . DB_PREFIX . "route_description WHERE name LIKE '%" . $this->db->escape($data['filter_name']) . "%'))";
        }
        if (!empty($data['doctype_uid'])) {
            $where[] = "route_uid IN (SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid = '" . $this->db->escape($data['doctype_uid']) . "')";
        }
        if (!empty($data['route_uid'])) {
            $where[] = "route_uid = '" . $this->db->escape($data['route_uid']) . "'";
        }
        $sql .= implode(" AND ", $where);
        $query = $this->db->query($sql);
        $result = array();
        foreach ($query->rows as $route_button) {
            $draft_button = array();
            if ($route_button['draft'] && $route_button['draft_params']) {
                $draft_button = unserialize($route_button['draft_params']);
                $fields_dlg = array();
                if ($draft_button['field']) {
                    foreach ($draft_button['field'] as $field_uid) {
                        $fields_dlg[] = array(
                            'field_uid' => $field_uid,
                            'name' => $this->getField($field_uid)['name']
                        );
                    }
                }
              
            }
            $descriptions = $draft_button['description'] ?? $this->getRouteButtonDescriptions($route_button['route_button_uid']);
            $fields = $fields_dlg ?? $this->getRouteButtonFields($route_button['route_button_uid']);
            $picture = $draft_button['picture'] ?? $route_button['picture'];
            $hide_button_name = $picture ? ($draft_button['hide_button_name'] ?? $route_button['hide_button_name']) : 0;
            $color = $draft_button['color'] ?? $route_button['color'];
            $background = $draft_button['background'] ?? $route_button['background'];
            $show_after_execute = $draft_button['show_after_execute'] ?? $route_button['show_after_execute'];
            $sort = $draft_button['sort'] ?? $route_button['sort'];              

            $this->load->model('tool/image');
            if ($picture) {
                if (!empty($descriptions[(int) $this->config->get('config_language_id')]['name'])) {
                    $picture25 = $this->model_tool_image->resize($picture, 28, 28);
                } else {
                    $picture25 = $this->model_tool_image->resize($picture, 28, 28);
                }
            } else {
                $picture25 = "";
            }
            $result[] = array(
                'route_button_uid'  => $route_button['route_button_uid'],
                'route_uid'         => $route_button['route_uid'],
                'name'              => !empty($descriptions[(int) $this->config->get('config_language_id')]['name']) ? $descriptions[(int) $this->config->get('config_language_id')]['name'] : "",
                'picture'           => $picture,
                'hide_button_name'  => $hide_button_name,
                'color'             => $color,
                'background'        => $background,
                'show_after_execute'=> $show_after_execute,
                'picture25'         => $picture25,
                'draft'             => $route_button['draft'],
                'descriptions'      => $descriptions,
                'fields'            => $fields,
                'sort'              => $sort
            );
        }
        usort($result, function($a, $b) {
            return $a['sort'] <=> $b['sort'];
        });
        return $result;
    }

    /**
     * Возвращает список действия доктайпа
     * @param type $doctype_uid
     */
    public function getListRouteActions($doctype_uid) {
        $this->load->language('doctype/doctype');
        $query = $this->db->query("SELECT ra.route_uid, ra.context, ra.route_action_uid, ra.action, ra.params, ra.draft, ra.description, ra.draft_params, ra.sort, rd.name FROM " . DB_PREFIX . "route_action ra "
                . "LEFT JOIN " . DB_PREFIX . "route r ON (r.route_uid = ra.route_uid) "
                . "LEFT JOIN " . DB_PREFIX . "route_description rd ON (r.route_uid = rd.route_uid AND rd.language_id = " . (int) $this->config->get('config_language_id') . ") "
                . "WHERE doctype_uid = '" . $this->db->escape($doctype_uid) . "' AND ra.draft != 2 ORDER BY r.sort ASC ");
        $result = array();
        foreach ($query->rows as $route_action) {
             $draft_params = array();
            if ($route_action['draft'] && $route_action['draft_params']) {
                $draft_params = unserialize($route_action['draft_params']);
                if (isset($draft_params['sort'])) {
                    $sort = $draft_params['sort'];
                } else {
                    $sort = $route_action['sort'];
                }
                if (isset($draft_params['action'])) {
                    $action = $draft_params['action'];
                } else {
                    $action = $route_action['action'];
                }
                $params = $draft_params['params'];
            } else {
                $sort = $route_action['sort'];
                $action = $route_action['action'];
                $params = unserialize($route_action['params']);
            }
            $result[$route_action['route_uid']][$route_action['context']][] = array(
                'action_name'           => $this->load->controller('extension/action/' . $action . "/getTitle"),
                'action_description'    => $route_action['description'] ? $route_action['description'] : $this->load->controller('extension/action/' . $action . "/getDescription", $params),
                'action_id'             => $route_action['route_action_uid'],
                'sort'                  => $sort,
                'route_name'            => $route_action['name'],
                'context_name'          => $this->language->get("text_route_" . $route_action['context'] . "_name")
            );
        }
        foreach ($result as &$route) {
            foreach ($route as &$context) {
                usort($context, function($a, $b) {
                    if ($a['sort'] == $b['sort']) {
                        return 0;
                    }
                    return $a['sort'] > $b['sort'] ? 1 : 0;
                });
            }
        }
        return $result;
    }
    /**
     * Возвращает список действия доктайпа
     * @param type $doctype_uid
     */
    public function getListRouteButtons($doctype_uid) {
        $this->load->language('doctype/doctype');
        $query = $this->db->query("SELECT rb.route_uid, rb.route_button_uid, rb.action, rb.action_params, rb.draft, rb.draft_params, rb.sort, rbd.name, rd.name AS route_name FROM " . DB_PREFIX . "route_button rb "
                . "LEFT JOIN " . DB_PREFIX . "route r ON (r.route_uid = rb.route_uid) "
                . "LEFT JOIN " . DB_PREFIX . "route_button_description rbd ON (rb.route_button_uid = rbd.route_button_uid AND rbd.language_id = " . (int) $this->config->get('config_language_id') . ") "
                . "LEFT JOIN " . DB_PREFIX . "route_description rd ON (r.route_uid = rd.route_uid AND rd.language_id = " . (int) $this->config->get('config_language_id') . ") "
                . "WHERE doctype_uid = '" . $this->db->escape($doctype_uid) . "' AND rb.draft != 2 ORDER BY r.sort ASC ");
        $result = array();
        foreach ($query->rows as $route_button) {
            $draft_params = array();
            if ($route_button['draft'] && $route_button['draft_params']) {
                $draft_params = unserialize($route_button['draft_params']);
                if (isset($draft_params['sort'])) {
                    $sort = $draft_params['sort'];
                } else {
                    $sort = $route_button['sort'];
                }
                $action = $draft_params['action'] ?? $route_button['action'];
                $name = $draft_params['description'][$this->config->get('config_language_id')]['name'] ?? $route_button['name'];
                $action_params = $draft_params['action_params'];
            } else {
                $sort = $route_button['sort'];
                $action = $route_button['action'];
                $name = $route_button['name'];
                $action_params = unserialize($route_button['action_params']);
            }
            $result[$route_button['route_uid']][] = array(
                'name' => $name,
                'action_name' => $this->load->controller('extension/action/' . $action . "/getTitle"),
                'action_description' => $this->load->controller('extension/action/' . $action . "/getDescription", $action_params),
                'button_id' => $route_button['route_button_uid'],
                'sort' => $sort,
                'route_name' => $route_button['route_name']
            );
        }
        foreach ($result as &$route) {
                usort($route, function($a, $b) {
                    if ($a['sort'] == $b['sort']) {
                        return 0;
                    }
                    return $a['sort'] > $b['sort'] ? 1 : 0;
                });
            
        }
        return $result;
    }

    public function getRouteActions($route_uid, $context = '') {
        $sql = "SELECT * FROM " . DB_PREFIX . "route_action "
                . "WHERE route_uid = '" . $this->db->escape($route_uid) . "'";
        if ($context) {
            $sql .= " AND context = '" . $this->db->escape($context) . "'";
        }
        $query = $this->db->query($sql);
        $result = array();
        foreach ($query->rows as $route_action) {
            $draft_params = array();
            $sort = 0;
            if ($route_action['draft'] && $route_action['draft_params']) { //draft_params нужна на тот случай, если действие было сохранено, а потом помечено на удаление
                $draft_params = unserialize($route_action['draft_params']);
                $action = $draft_params['action'];
                $action_log = $draft_params['action_log'];
                $params = $draft_params['params'];
                $sort = $params['sort'] ?? $route_action['sort'];
            } else {
                $params = unserialize($route_action['params']);
                $action = $route_action['action'];
                $action_log = $route_action['action_log'];
                $sort = $route_action['sort'];
            }
            if ($context) {
                $result[] = array(
                    'route_action_uid'  => $route_action['route_action_uid'],
                    'action'            => $action,
                    'action_log'        => $action_log,
                    'params'            => $params,
                    'description'       => $route_action['description'],
                    'draft'             => $route_action['draft'],
                    'sort'              => $sort,
                    'status'            => $route_action['status']
                );
            } else {
                $result[$route_action['context']][] = array(
                    'route_action_uid'  => $route_action['route_action_uid'],
                    'action'            => $action,
                    'action_log'        => $action_log,
                    'params'            => $params,
                    'description'       => $route_action['description'],
                    'draft'             => $route_action['draft'],
                    'sort'              => $sort,
                    'status'            => $route_action['status']
                );
            }
        }
        //сортируем действия по полю sort
        if ($context) {
            usort($result, function($a, $b) {
                return $a['sort'] <=> $b['sort'];
            });
        } else {
            foreach ($result as &$context) {
                usort($context, function($a, $b) {
                    return $a['sort'] <=> $b['sort'];
                });
            }
        }
        return $result;
    }

    public function getRouteButton($route_button_uid) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "route_button "
                . "WHERE route_button_uid = '" . $this->db->escape($route_button_uid) . "'"
        );
        $route_button = $query->row;
        if ($route_button['draft'] && $route_button['draft_params']) {
            $draft_button = unserialize($route_button['draft_params']);
            $fields = array();
            if (!empty($draft_button['field'])) {
                foreach ($draft_button['field'] as $field_uid) {
                    $field_info = $this->getField($field_uid);
                    $name = "";
                    if ($field_info['setting']) {
                        $doctype_info = $this->getDoctype($field_info['doctype_uid']);
                        $name = $doctype_info['name'] . " - ";
                    } 
                    $name .= $field_info['name'];
                    $fields[] = array(
                        'field_uid' => $field_uid,
                        'name' => $name
                    );
                }
            }
            if (isset($draft_button['action_params'])) {
                if (!is_array($draft_button['action_params'])) {
                    $action_params = unserialize($draft_button['action_params']);
                } else {
                    $action_params = $draft_button['action_params'];
                }               
            }
            
            if (!empty($draft_button['sort'])) {
                $sort = $draft_button['sort'];
            }
        }
        $descriptions = $draft_button['description'] ?? $this->getRouteButtonDescriptions($route_button['route_button_uid']);
        $picture = $draft_button['picture'] ?? $route_button['picture'];
        $hide_button_name = $picture ? ($draft_button['hide_button_name'] ?? $route_button['hide_button_name'] ?? 0) : 0;
        $color = $draft_button['color'] ?? $route_button['color'];
        $background = $draft_button['background'] ?? $route_button['background'];
        $action_log = $draft_button['action_log'] ?? $route_button['action_log'];
        $action_move_route_uid = $draft_button['action_move_route_uid'] ?? $route_button['action_move_route_uid'];
        $show_after_execute = $draft_button['show_after_execute'] ?? $route_button['show_after_execute'] ?? 0;
        $action = $draft_button['action'] ?? $route_button['action'];
        $fields = $fields ?? $this->getRouteButtonFields($route_button['route_button_uid']);
        $action_params = $action_params ?? unserialize($route_button['action_params']);
        $sort = $sort ?? $route_button['sort'];


        $this->load->model('tool/image');
        if ($picture) {
            if (!empty($descriptions[(int) $this->config->get('config_language_id')]['name'])) {
                $picture25 = $this->model_tool_image->resize($picture, 28, 28);
            } else {
                $picture25 = $this->model_tool_image->resize($picture, 28, 28);
            }
        } else {
            $picture25 = "";
        }
        return array(
            'route_button_uid' => $route_button['route_button_uid'],
            'route_uid' => $route_button['route_uid'],
            'name' => !empty($descriptions[(int) $this->config->get('config_language_id')]['name']) ? $descriptions[(int) $this->config->get('config_language_id')]['name'] : "",
            'picture' => $picture,
            'hide_button_name' => $hide_button_name,
            'color' => $color,
            'background' => $background,
            'picture25' => $picture25,
            'action' => $action,
            'action_log' => $action_log,
            'action_move_route_uid' => $action_move_route_uid,
            'show_after_execute' => $show_after_execute,
            'action_params' => $action_params,
            'draft' => $route_button['draft'],
            'descriptions' => $descriptions,
            'fields' => $fields,
            'sort' => $sort
        );
    }

    public function getRouteAction($route_action_uid) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "route_action "
                . "WHERE route_action_uid = '" . $this->db->escape($route_action_uid) . "'"
        );
        $route_action = $query->row;
        if ($route_action['draft'] && $route_action['draft_params']) {
            $draft_action = unserialize($route_action['draft_params']);
            $action = $draft_action['action'];
            $action_log = $draft_action['action_log'];
            if (!is_array($draft_action['params'])) {
                $action_params = unserialize($draft_action['params']);
            } else {
                $action_params = $draft_action['params'];
            }
        } else {
            $action = $route_action['action'];
            $action_log = $route_action['action_log'];
            $action_params = unserialize($route_action['params']);
        }


        $this->load->model('tool/image');
        return array(
            'route_action_uid'  => $route_action['route_action_uid'],
            'route_uid'         => $route_action['route_uid'],
            'context'           => $route_action['context'],
            'action'            => $route_action['action'],
            'action_log'        => $action_log,
            'action_params'     => $action_params,
            'description'       => $route_action['description'],
            'draft'             => $route_action['draft'],
            'status'            => $route_action['status']
        );
    }

    public function getRouteButtonDescriptions($route_button_uid) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "route_button_description WHERE route_button_uid = '" . $this->db->escape($route_button_uid) . "'");
        $result = array();
        foreach ($query->rows as $route_button) {
            $result[$route_button['language_id']] = array(
                'name' => $route_button['name'],
                'description' => $route_button['description']
            );
        }
        return $result;
    }

    public function getRouteButtonFields($route_button_uid) {
        $query = $this->db->query("SELECT field_uid FROM " . DB_PREFIX . "route_button_field WHERE route_button_uid = '" . $this->db->escape($route_button_uid) . "'");
        $result = array();
        foreach ($query->rows as $field) {
            $field_info = $this->getField($field['field_uid']);
            if ($field_info['setting']) {
                $doctype_info = $this->getDoctype($field_info['doctype_uid']);
                $name = $doctype_info['name'] . " - " . $field_info['name'];
            } else {
                $name = $field_info['name'];
            }
            $result[] = array(
                'field_uid' => $field['field_uid'],
                'name' => $name
            );
        }
        return $result;
    }

    /**
     * Сохранение изменного поля данного типа в тип документа. Все данные формы передаются через POST
     * $field_uid - идентификатор поля
     * $data - тип поля и его параметры в массиве
     * $draft - 1 - черновик, 0 - нет
     */
    public function editField($field_uid, $data) {
        $params = array();
        foreach ($data as $key => $value) {
            if ($key != "field_type") {
                $params[$key] = $value;
            }
        }
        $field_params = $this->load->controller('extension/field/' . $data['field_type'] . '/setParams', $params);
        if ($field_params) {
            $params = $field_params;
        }
        //переносим sort из сохраненного драфта (если он есть)
        $field_info = $this->getField($field_uid);
        if (!empty($field_info['params']['sort'])) {
            $params['sort'] = $field_info['params']['sort'];
        }

        $params = serialize($params);

        $sql = "UPDATE " . DB_PREFIX . "field SET "
                . "draft_params='" . $this->db->escape($params) . "',"
                . "draft=1 "
                . "WHERE field_uid='" . $this->db->escape($field_uid) . "'";
        $this->db->query($sql);
        $this->setDraft($field_info['doctype_uid']);
    }

    public function editSortField($field_uid, $sort) {
        $field_info = $this->getField($field_uid,1);
        $params = $field_info['params'];
        $params['sort'] = (int) $sort;
        $this->db->query("UPDATE " . DB_PREFIX . "field SET "
                . "draft='" . ($field_info['draft'] ? $field_info['draft'] : 1) . "', "
                . "draft_params = '" . $this->db->escape(serialize($params)) . "' "
                . "WHERE field_uid = '" . $this->db->escape($field_uid) . "' ");
        $this->setDraft($field_info['doctype_uid']);
    }

    private function setDraft($doctype_uid) {
        $this->db->query("UPDATE " . DB_PREFIX . "doctype SET draft=CASE WHEN draft=3 THEN 3 ELSE 1 END WHERE doctype_uid='" . $this->db->escape($doctype_uid) . "'");
        
    }
    
    /**
     * Удаление черновика доктайпа
     * @param type $doctype_uid
     */
    public function removeDraft($doctype_uid) {
        $doctype_uid = $this->db->escape($doctype_uid);
        $doctype_query = $this->db->query("SELECT draft FROM " . DB_PREFIX . "doctype WHERE doctype_uid='" . $doctype_uid . "' ");
        if ($doctype_query->num_rows && $doctype_query->row['draft'] == 3) {
            $this->deleteDoctype($doctype_uid);
            return "";
        }
        $this->db->query("UPDATE " . DB_PREFIX . "doctype SET draft=0, draft_params='' WHERE doctype_uid='" . $doctype_uid . "' ");
        $this->db->query("DELETE FROM " . DB_PREFIX . "field WHERE draft=3 AND doctype_uid='" . $doctype_uid . "' ");
        $this->db->query("UPDATE " . DB_PREFIX . "field SET draft=0, draft_params='' WHERE doctype_uid='" . $doctype_uid . "' ");
        $this->db->query("DELETE FROM " . DB_PREFIX . "route_action WHERE draft=3 AND route_uid IN (SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid='" . $doctype_uid . "') ");
        $this->db->query("UPDATE " . DB_PREFIX . "route_action SET draft=0, draft_params='' WHERE route_uid IN (SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid='" . $doctype_uid . "') ");
        $this->db->query("DELETE FROM " . DB_PREFIX . "route_button WHERE draft=3 AND route_uid IN (SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid='" . $doctype_uid . "') ");
        $this->db->query("UPDATE " . DB_PREFIX . "route_button SET draft=0, draft_params='' WHERE route_uid IN (SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid='" . $doctype_uid . "') ");
        $this->db->query("DELETE FROM " . DB_PREFIX . "route WHERE draft=3 AND doctype_uid='" . $doctype_uid . "' ");
        $this->db->query("UPDATE " . DB_PREFIX . "route SET draft=0, draft_params='' WHERE doctype_uid='" . $doctype_uid . "' ");
        return $doctype_uid;
    }
    
    /**
     * Пометка поля на удаление
     * @param type $field_uid
     */
    public function removeField($field_uid) {
        $query = $this->db->query("SELECT setting_id FROM " . DB_PREFIX . "setting WHERE value LIKE '%" . $this->db->escape($field_uid) . "%'");
        if (!$query->num_rows) { //удаляемое поле не используется в настройках
            $this->db->query("UPDATE " . DB_PREFIX . "field SET draft=2 WHERE field_uid='" . $this->db->escape($field_uid) . "'");
            $this->db->query("UPDATE " . DB_PREFIX . "doctype SET draft=CASE WHEN draft=3 THEN 3 ELSE 1 END WHERE doctype_uid = (SELECT doctype_uid FROM " . DB_PREFIX . "field WHERE field_uid = '" . $this->db->escape($field_uid) . "')");            
        }
    }

    /**
     * Снятие с поля пометки на удаление
     * @param type $field_uid
     */
    public function undoRemoveField($field_uid) {
        $sql = "UPDATE " . DB_PREFIX . "field SET "
                . "draft=(CASE WHEN draft_params='' THEN 0 ELSE 1 END) "
                . "WHERE field_uid='" . $this->db->escape($field_uid) . "'";
        $this->db->query($sql);
    }

    /**
     * Пометка точки маршрута на удаление
     * @param type $route_uid
     */
    public function removeRoute($route_uid) {
        $this->load->model('document/document');
        $data_docs = array(
            'route_uid'         => $this->request->get['route_uid'],
            'start'             => 0,
            'limit'             => 1
        );
        if (!$this->model_document_document->getDocumentIds($data_docs)) {
            $this->db->query("UPDATE " . DB_PREFIX . "route SET draft=2 WHERE route_uid='" . $this->db->escape($route_uid) . "' ");
            $this->db->query("UPDATE " . DB_PREFIX . "doctype SET draft=CASE WHEN draft=3 THEN 3 ELSE 1 END WHERE doctype_uid = (SELECT doctype_uid FROM " . DB_PREFIX . "route WHERE route_uid = '" . $this->db->escape($route_uid) . "')");            
        }
        
    }

    /**
     * Снятие пометки на удаление с точки маршрута
     * @param type $route_uid
     */
    public function undoRemoveRoute($route_uid) {
        $this->db->query("UPDATE " . DB_PREFIX . "route SET draft=(CASE WHEN draft_params='' THEN 0 ELSE 1 END) WHERE route_uid='" . $this->db->escape($route_uid) . "' ");
    }

    public function removeRouteButton($route_button_uid) {
        $this->db->query("UPDATE " . DB_PREFIX . "route_button SET draft=2 WHERE route_button_uid='" . $this->db->escape($route_button_uid) . "'");
        $this->db->query("UPDATE " . DB_PREFIX . "doctype SET draft=CASE WHEN draft=3 THEN 3 ELSE 1 END WHERE doctype_uid = (SELECT doctype_uid FROM " . DB_PREFIX . "route WHERE route_uid = (SELECT route_uid FROM " . DB_PREFIX . "route_button WHERE route_button_uid = '" . $this->db->escape($route_button_uid) . "'))");                
    }

    public function undoRemoveRouteButton($route_button_uid) {
        $this->db->query("UPDATE " . DB_PREFIX . "route_button SET draft=(CASE WHEN draft_params='' THEN 0 ELSE 1 END) WHERE route_button_uid='" . $this->db->escape($route_button_uid) . "'");
    }

    public function removeRouteAction($route_action_uid) {
        $this->db->query("UPDATE " . DB_PREFIX . "route_action SET draft=2 WHERE route_action_uid='" . $this->db->escape($route_action_uid) . "'");
        $this->db->query("UPDATE " . DB_PREFIX . "doctype SET draft=CASE WHEN draft=3 THEN 3 ELSE 1 END WHERE doctype_uid = (SELECT doctype_uid FROM " . DB_PREFIX . "route WHERE route_uid = (SELECT route_uid FROM " . DB_PREFIX . "route_action WHERE route_action_uid = '" . $this->db->escape($route_action_uid) . "'))");
    }

    public function undoRemoveRouteAction($route_action_uid) {
        $this->db->query("UPDATE " . DB_PREFIX . "route_action SET draft=(CASE WHEN draft_params='' THEN 0 ELSE 1 END) WHERE route_action_uid='" . $this->db->escape($route_action_uid) . "'");
    }

    /**
     * Возвращает параметры поля доктайпа. 
     * @param type $field_uid
     * @param type $draft Если =1, то поле возвращается с учетом черновика, если =0, то без черновика
     * @return type
     */
    public function getField($field_uid, $draft = 0) {
        if ($field_uid) {
            $sql = "SELECT * FROM " . DB_PREFIX . "field WHERE field_uid = '" . $this->db->escape($field_uid) . "'";
            $query = $this->db->query($sql);
            if ($query->num_rows) {
                if ($query->row['draft'] && $draft) {
                    $draft_params = unserialize($query->row['draft_params']);
                    $name = $draft_params['field_name'];
                    $params = $draft_params;
                    if (!empty($params['sort'])) {
                        $sort = $params['sort'];
                    } else {
                        $sort = $query->row['sort'];
                    }
                    if (isset($params['change_field'])) {
                        $change_field = $params['change_field'];
                    } else {
                        $change_field = $query->row['change_field'];
                    }
                    if (isset($params['access_form'])) {
                        $access_form = implode(",",$params['access_form']);
                    } else {
                        $access_form = $query->row['access_form'];
                    }
                    if (isset($params['access_view'])) {
                        $access_view = implode(",",$params['access_view']);
                    } else {
                        $access_view = $query->row['access_view'];
                    }
                    if (isset($params['required'])) {
                        $required = $params['required'];
                    } else {
                        $required = $query->row['required'];
                    }
                    if (isset($params['unique'])) {
                        $unique = $params['unique'];
                    } else {
                        $unique = $query->row['unique'];
                    }

                } else {
                    $name = $query->row['name'];
                    $params = unserialize($query->row['params']);
                    $sort = $query->row['sort'];
                    $change_field = $query->row['change_field'];
                    $access_form = $query->row['access_form'];
                    $access_view = $query->row['access_view'];
                    $required = $query->row['required'];
                    $unique = $query->row['unique'];
                }
                $result = array(
                    'field_uid' => $query->row['field_uid'],
                    'doctype_uid' => $query->row['doctype_uid'],
                    'name' => $name,
                    'type' => $query->row['type'],
                    'type_title' => $this->load->controller('extension/field/' . $query->row['type'] . '/getTitle'),
                    'params' => $params,
                    'params_description' => $this->load->controller('extension/field/' . $query->row['type'] . '/getDescriptionParams', $params),
                    'setting' => $query->row['setting'],
                    'change_field' => $change_field,
                    'access_form' => $access_form,
                    'access_view' => $access_view,
                    'required' => $required,
                    'unique' => $unique,
                    'sort' => $sort,
                    'draft' => $query->row['draft'],
                );
                return $result;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function getFieldName($field_uid) {
        if ($field_uid) {
            $sql = "SELECT * FROM " . DB_PREFIX . "field WHERE field_uid = '" . $this->db->escape($field_uid) . "'";
            $query = $this->db->query($sql);
            if ($query->row['draft'] && $query->row['draft_params']) {
                $draft_params = unserialize($query->row['draft_params']);
                $name = $draft_params['field_name'];
            } else {
                $name = $query->row['name'];
            }
        } else {
            $name = "";
        }
        return $name;
    }

    /**
     * Метод сохранения некоторых полей доктайпа в фоновом режиме (onBlur)
     */
    public function saveDoctype($doctype_uid, $data) {
        $query = $this->db->query("SELECT draft_params FROM " . DB_PREFIX . "doctype WHERE doctype_uid = '" . $this->db->escape($doctype_uid) . "' ");
        if ($query->num_rows) {
            $data_save = unserialize($query->row['draft_params']);
            if ($data_save) {
                $draft = array_merge($data_save, $data);
            } else {
                $draft = $data;
            }            
            $this->db->query("UPDATE " . DB_PREFIX . "doctype SET "
                    . "draft = CASE WHEN draft=3 THEN 3 ELSE 1 END, "
                    . "draft_params = '" . $this->db->escape(serialize($draft)) . "' "
                    . "WHERE doctype_uid = '" . $this->db->escape($doctype_uid) . "' ");            
        }
    }

    public function getFieldValue($field_uid, $document_uid) {
        $field_info = $this->getField($field_uid);
        $query = $this->db->query("SELECT value FROM " . DB_PREFIX . "field_value_" . $this->db->escape($field_info['type']) . " WHERE "
                . "document_uid = '" . $this->db->escape($document_uid) . "' "
                . "AND field_uid = '" . $this->db->escape($field_uid) . "' ");
        return $query->row['value'];
    }

    public function getTemplateVariables() {
        $this->load->language('doctype/doctype');
        return array(
            'var_author_name'                   => $this->language->get('text_var_author_name'),
            'var_customer_name'                 => $this->language->get('text_var_customer_name'),
            'var_current_route_name'            => $this->language->get('text_var_current_route_name'),
            'var_current_route_description'     => $this->language->get('text_var_current_route_description'),
            'var_current_locale_time'           => $this->language->get('text_var_current_time'),
            'var_current_locale_date'           => $this->language->get('text_var_current_date')
        );
    }
    
    public function getVariables() {
        $this->load->language('doctype/doctype');
        return array(
            'var_author_uid'                => $this->language->get('text_var_author_uid'),
            'var_customer_uid'              => $this->language->get('text_var_customer_uid'),
            'var_customer_uids'             => $this->language->get('text_var_customer_uids'),
            'var_customer_user_uid'         => $this->language->get('text_var_customer_user_uid'),
            'var_current_document_uid'      => $this->language->get('text_var_current_document_uid'),
            'var_current_button_uid'        => $this->language->get('text_var_current_button_uid'),
            'var_change_field_uid'          => $this->language->get('text_var_change_field_uid'),
            'var_change_field_value'        => $this->language->get('text_var_change_field_value'),
            'var_current_folder_uid'        =>  $this->language->get('text_var_current_folder_uid'),
            'var_current_route_uid'         => $this->language->get('text_var_current_route_uid'),
            'var_current_route_name'        => $this->language->get('text_var_current_route_name'),
            'var_current_route_description' => $this->language->get('text_var_current_route_description'),            
            'var_author_name'               => $this->language->get('text_var_author_name'),            
            'var_customer_name'             => $this->language->get('text_var_customer_name'),
            'var_current_time'              => $this->language->get('text_var_current_datetime'),
        );
    }

    /**
     * Обновляется таблица с матрицей делегирования кнопок при изменении делегирования кнопки в доктайпе
     * @param type $field_uid
     * @param type $document_uid
     * @param type $value
     */
    public function updateButtonDelegate($route_button_uid) {
        $route_button_uid = $this->db->escape($route_button_uid);
        //удаляем старое делегирование
        $this->db->query("DELETE FROM " . DB_PREFIX . "route_button_delegate WHERE route_button_uid = '" . $route_button_uid . "' ");
        //получаем поля, на которые делегируется кнопка
        $query_fields = $this->db->query("SELECT * FROM " . DB_PREFIX . "field WHERE field_uid IN (SELECT field_uid FROM " . DB_PREFIX . "route_button_field WHERE route_button_uid = '" . $route_button_uid . "')");
        $sqls = array();
        foreach ($query_fields->rows as $field) {
            //получаем все значения поля, которому делегируется кнопка
            $query_field_values = $this->db->query("SELECT * FROM " . DB_PREFIX . "field_value_" . $field['type'] . " WHERE field_uid = '" . $this->db->escape($field['field_uid']) . "'");
            foreach ($query_field_values->rows as $value_row) {
                if (is_array($value_row['value'])) {
                    $values = $value_row['value'];
                } else { //structure_uid могут быть перечислены через запятую
                    $values = explode(",", $value_row['value']);
                }

                foreach ($values as $structure_uid) {
                    $structure_uid = trim($structure_uid);
                    if ($structure_uid && in_array("('" . $route_button_uid . "','" . $value_row['document_uid'] . "','" . $structure_uid . "')", $sqls) === FALSE) { //исключаем дублирование и пустые значения structure_uid
                        $sqls[] = "('" . $route_button_uid . "','" . $value_row['document_uid'] . "','" . $structure_uid . "')";
                    }
                }
            }
        }
        if ($sqls) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "route_button_delegate (route_button_uid, document_uid, structure_uid) VALUES " . implode(",", array_unique($sqls)));
        }
    }
    
    /**
     * Метод заменяет названия полей в шаблоне на их идентификаторы
     * @param data = array($template, $doctype_uid, $variables)
     * @return type
     */
    public function getIdsTemplate($template, $doctype_uid, $variables) {
        $query_field = $this->db->query("SELECT * FROM " . DB_PREFIX . "field WHERE doctype_uid='" . $this->db->escape($doctype_uid) . "' ");
        $replace = array(
            'search'    => array(),
            'replace'   => array()
        );
        foreach ($query_field->rows as $field) {
            $replace['search'][] =  "/{{ ?" . preg_quote($field['name'], '/') . " ?}}/is";
            $replace['replace'][] =  "{{ f_" . str_replace("-", "", $field['field_uid']) . " }}";
        }

        //добавляем в поля список переменных
        foreach ($variables as $var_id => $var_name) {
            $replace['search'][] =  "/{{ ?" . preg_quote($var_name, '/') . " ?}}/is";
            $replace['replace'][] =  "{{ " . str_replace("-", "", $var_id) . " }}";
        }
        return preg_replace($replace['search'], $replace['replace'], $template);        
    }
    /**
     * Метод выполняет обратное преобразование шаблона - заменяет идентификаторы полей и переменных на их названия
     * @param type $template
     * @param type $doctype_uid
     * @param type $variables
     * @return type
     */
    public function getNamesTemplate($template, $doctype_uid, $variables) {
        $query_field = $this->db->query("SELECT * FROM " . DB_PREFIX . "field WHERE doctype_uid='" . $this->db->escape($doctype_uid) . "' ");

        $fields = array();
        
        foreach ($query_field->rows as $field) {
            $fields['name'][] = "{{ " . $field['name'] . " }}";
            $fields['id'][] = "{{ f_" . str_replace("-", "", $field['field_uid']) . " }}";
        }
        //добавляем идентификаторы и языковые наименования переменных
        foreach ($variables as $var_id => $var_name) {
            $fields['name'][] = "{{ " . $var_name . " }}";
            $fields['id'][] = "{{ " . $var_id . " }}";
        }
        return str_replace($fields['id'], $fields['name'], $template);        
    }

    /**
     * Метод для обновления displayValue полей после изменения их параметров через демон
     * @param type $data
     */
    public function executeRefreshDisplay($data) {
        $model = "model_extension_field_" . $data['field_type'];
        $this->load->model('extension/field/' . $data['field_type']);
        $value = $this->$model->refreshDisplayValues($data);       
    }
    
    /**
     * Метод для добавления подписки на изменение поля
     * @param type $subscription_field_uid - подписываемое поле
     * @param type $subscription_document_uid - подписываемый документ
     * @param type $document_uids - изменяемый документ; может быть массивом, если идет подписка на одно поле нескольких документов
     * @param type $field_uid - изменяемое поле
     */
    public function addSubscription($subscription_field_uid, $subscription_document_uid, $field_uid, $document_uids) {
        if (is_array($document_uids)) {
            $sql = "REPLACE INTO " . DB_PREFIX . "field_change_subscription (subscription_document_uid, subscription_field_uid, document_uid, field_uid) VALUES ";
            $values = array();
            foreach ($document_uids as $document_uid ) {
                if (!$document_uid) {
                    continue;
                }
                $values[] = "('" . $subscription_document_uid . "','" . $subscription_field_uid . "','" . $document_uid . "','" . $field_uid  . "')";
            }
            if ($values) {
                $sql .= implode(", ", $values);
                $this->db->query($sql);
            }
        } else {
            $this->db->query("REPLACE INTO " . DB_PREFIX . "field_change_subscription SET subscription_document_uid = '" . $this->db->escape($subscription_document_uid) ."', subscription_field_uid = '" . $this->db->escape($subscription_field_uid) ."', document_uid='" . $this->db->escape($document_uids) ."', field_uid='" . $this->db->escape($field_uid) ."'");
        }
        
    }
    
    public function delSubscription($subscription_field_uid, $subscription_document_uid="") {
        $sql = "DELETE FROM " . DB_PREFIX . "field_change_subscription WHERE subscription_field_uid = '" . $this->db->escape($subscription_field_uid) ."' ";
        if ($subscription_document_uid) {
            $sql .= "AND subscription_document_uid = '" . $this->db->escape($subscription_document_uid) . "' ";
        }
        $this->db->query($sql);
    }
}
