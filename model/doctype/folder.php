<?php

class ModelDoctypeFolder extends Model {
    
    public function addFolder() {

        $sql = "SELECT folder_uid FROM " . DB_PREFIX . "folder WHERE draft=3";
        if (!empty($this->request->post['folder_type'])) {
            $sql .= " AND type='" . $this->db->escape($this->request->post['folder_type']) . "' ";
        } else {
            $sql .= " AND type='' ";
        }
        $query_folder_draft = $this->db->query($sql);
        if ($query_folder_draft->num_rows) {
            $folder_uid = $query_folder_draft->row['folder_uid'];
        } else {
            $query_uid = $this->db->query("SELECT UUID() AS uid");
            $folder_uid = $query_uid->row['uid'];        
            $this->db->query("INSERT INTO " . DB_PREFIX . "folder SET "
                    . "folder_uid = '" . $folder_uid . "', "
                    . "type='" . ($this->request->post['folder_type'] ?? "") . "', "
                    . "date_added = NOW(), user_uid = '" . $this->customer->getStructureId() . "', "
                    . "draft = 3, draft_params = ''"
                    );
        }
        return $folder_uid;
    }    
    
    public function deleteFolder($folder_uid) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "folder_user_filter WHERE folder_uid='" . $this->db->escape($folder_uid) . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "folder_filter WHERE folder_uid='" . $this->db->escape($folder_uid) . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "folder_field WHERE folder_uid='" . $this->db->escape($folder_uid) . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "folder_description WHERE folder_uid='" . $this->db->escape($folder_uid) . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "folder_button_route WHERE folder_button_uid IN (SELECT folder_button_uid FROM " . DB_PREFIX . "folder_button WHERE folder_uid = '" . $this->db->escape($folder_uid) . "') ");
        $this->db->query("DELETE FROM " . DB_PREFIX . "folder_button_field WHERE folder_button_uid IN (SELECT folder_button_uid FROM " . DB_PREFIX . "folder_button WHERE folder_uid = '" . $this->db->escape($folder_uid) . "') ");
        $this->db->query("DELETE FROM " . DB_PREFIX . "folder_button_description WHERE folder_button_uid IN (SELECT folder_button_uid FROM " . DB_PREFIX . "folder_button WHERE folder_uid = '" . $this->db->escape($folder_uid) . "') ");
        $this->db->query("DELETE FROM " . DB_PREFIX . "folder_button_delegate WHERE folder_button_uid IN (SELECT folder_button_uid FROM " . DB_PREFIX . "folder_button WHERE folder_uid = '" . $this->db->escape($folder_uid) . "') ");
        $this->db->query("DELETE FROM " . DB_PREFIX . "folder_button WHERE folder_uid='" . $this->db->escape($folder_uid) . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "folder WHERE folder_uid='" . $this->db->escape($folder_uid) . "'");
    }
    
    public function editFolder($folder_uid) {
        $folder_uid = $this->db->escape($folder_uid);
        $query_folder = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "folder WHERE folder_uid = '" . $folder_uid . "' ");
        if ($query_folder->row['additional_params']) {
            $additional_params = unserialize($query_folder->row['additional_params']);
        } 
        if (empty($additional_params)) {
            $additional_params = array();
        }
        if ($query_folder->row['draft'] && $query_folder->row['draft_params']) {
            $folder_draft = unserialize($query_folder->row['draft_params']);
            if (isset($folder_draft['folder_description'])) {
                $descriptions = $folder_draft['folder_description'];
                foreach ($descriptions as $language_id => $description) {
                    $sets = array();
                    foreach ($description as $name => $value) {
                        $sets[] = $this->db->escape($name) . " = '" . $this->db->escape($value) . "'";
                    }                    
                    $query_descr = $this->db->query("SELECT folder_uid FROM " . DB_PREFIX . "folder_description WHERE folder_uid = '" . $folder_uid . "' AND language_id = '" . (int) $language_id . "'" );
                    if ($query_descr->num_rows) {
                        $this->db->query("UPDATE " . DB_PREFIX . "folder_description SET "
                            . implode(", ", $sets) . " WHERE folder_uid = '" . $folder_uid . "' AND language_id = '" . (int) $language_id . "'" );
                                                    
                    } else {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "folder_description SET "
                            . "folder_uid = '" . $folder_uid . "', "
                            . "language_id = '" . (int) $language_id . "', "
                            . implode(", ", $sets));
                    }
                }                     
            }
            //обновляем сортировку по умолчанию
            if (isset($folder_draft['default_sort']) || isset($folder_draft['default_sort_order'])) {
                if (isset($folder_draft['default_sort'])) {
                    foreach ($folder_draft['default_sort'] as $language_id => $folder_field_uid) {
                        $this->db->query("UPDATE " . DB_PREFIX . " folder_field SET default_sort=0 WHERE folder_uid = '" . $folder_uid . "' AND language_id = '" . (int) $language_id . "' ");
                        $this->db->query("UPDATE " . DB_PREFIX . " folder_field SET default_sort=" . (int) ($folder_draft['default_sort_order'][$language_id] ?? 1) . " WHERE folder_field_uid = '" . $this->db->escape($folder_field_uid) . "' ");
                    }
                } else {
                    foreach ($folder_draft['default_sort_order'] as $language_id => $order) {
                        $sort_field_info = $this->getDefaultSortField($folder_uid, $language_id);
                        $sort_field_uid = $sort_field_info['folder_field_uid'];                        
                        $this->db->query("UPDATE " . DB_PREFIX . " folder_field SET default_sort=" . (int) ($order) . " WHERE folder_field_uid = '" . $this->db->escape($sort_field_uid) . "' ");
                    }                    
                }
            }
            if (!empty($folder_draft['additional_params'])) {
                $additional_params = array_merge($additional_params, $folder_draft['additional_params']);
            }
//            $additional_params['toolbar'] = $folder_draft['toolbar'];
//            $additional_params['navigation'] = $folder_draft['navigation'];
       
        }
        $this->db->query("UPDATE " . DB_PREFIX . "folder SET date_edited = NOW(), user_uid = '" . $this->customer->getStructureId() . "', additional_params = '" . $this->db->escape(serialize($additional_params ?? array())) . "', draft = 0, draft_params = '' WHERE folder_uid = '" . $folder_uid . "' ");
        
        $this->db->query("DELETE FROM " . DB_PREFIX . "folder_field WHERE folder_uid = '" . $folder_uid . "' AND field_uid NOT IN (SELECT field_uid FROM " .  DB_PREFIX . "field WHERE doctype_uid='" . $query_folder->row['doctype_uid'] . "')");
        
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "folder_field WHERE folder_uid = '" . $folder_uid . "' AND (draft = 1 OR draft = 3)");

        foreach ($query->rows as $folder_field) {
            if($folder_field['draft_params']) {
                $draft_params = unserialize($folder_field['draft_params']);
                //проверяем наличие grouping_parent_uid
                if ($draft_params['grouping_parent_uid']) {
                    $grouping_parent_info = $this->getField($draft_params['grouping_parent_uid']);
                    if ($grouping_parent_info['draft'] != 2) {
                        $grouping_parent_uid = $draft_params['grouping_parent_uid'];
                    } else {
                        $grouping_parent_uid = 0;
                    }                                    
                } else {
                    $grouping_parent_uid = 0;
                } 
                $this->db->query("UPDATE " . DB_PREFIX . "folder_field SET "
                        . "field_uid = '" . $this->db->escape($draft_params['field_uid']) . "', "
                        . "grouping = '" . (int) $draft_params['grouping'] . "', "
                        . "grouping_name = '" . $this->db->escape($draft_params['grouping_name']) . "', "
                        . "grouping_parent_uid = '" . $this->db->escape($grouping_parent_uid) . "', "
                        . "grouping_tree_uid = '" . $this->db->escape($draft_params['grouping_tree_uid']) . "', "
                        . "tcolumn = '" . (int) $draft_params['tcolumn'] . "', "
                        . "tcolumn_name = '" . $this->db->escape($draft_params['tcolumn_name']) . "', "
                        . "tcolumn_total = '" . (int) $draft_params['tcolumn_total'] . "', "
                        . "tcolumn_hidden = '" . (int) $draft_params['tcolumn_hidden'] . "', "
                        . (!empty($draft_params['sort_grouping']) ? "sort_grouping = '" . (int) $draft_params['sort_grouping'] . "', " : "")
                        . (!empty($draft_params['sort_tcolumn']) ? "sort_tcolumn = '" . (int) $draft_params['sort_tcolumn'] . "', " : "")
                        . "draft = 0, draft_params = '' WHERE folder_field_uid = '" . $this->db->escape($folder_field['folder_field_uid']) . "' "
                        );

            } else {
                $this->db->query("UPDATE " . DB_PREFIX . "folder_field SET draft = 0, draft_params = '' WHERE folder_field_uid = '" . $this->db->escape($folder_field['folder_field_uid']) . "' ");             
            }           
        }
        //удаляем поля, помеченные на удаление
        $this->db->query("DELETE FROM " . DB_PREFIX . "folder_field WHERE folder_uid='" . $folder_uid . "' AND draft = 2");  
        //обновляем сортировку
        $this->updateSorting($folder_uid);

        
        //СОХРАНЯЕМ ФИЛЬТРЫ
        //удаляем фильтры с лишними полями (если изменяли тип документа)
        $this->db->query("DELETE FROM " . DB_PREFIX . "folder_filter WHERE folder_uid = '" . $folder_uid . "' AND field_uid !='' AND field_uid NOT IN (SELECT field_uid FROM " .  DB_PREFIX . "field WHERE doctype_uid='" . $query_folder->row['doctype_uid'] . "')");
        
        $filter_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "folder_filter WHERE folder_uid = '" . $folder_uid . "' AND (draft = 1 OR draft = 3)");
        foreach ($filter_query->rows as $filter) {
            if ($filter['draft_params']) {
                $draft_params = unserialize($filter['draft_params']);
                $this->db->query("UPDATE " . DB_PREFIX . "folder_filter SET "
                        . "field_uid = '" . $this->db->escape($draft_params['field_uid']) . "', "
                        . "condition_value = '" . $this->db->escape($draft_params['condition_value']) . "', "
                        . "type_value = '" . $this->db->escape($draft_params['type_value']) . "', "
                        . "value = '" . $this->db->escape($draft_params['value']) . "', "
                        . "action = '" . $this->db->escape($draft_params['action']) . "', "
                        . "action_params = '" . $this->db->escape(serialize($draft_params['action_params'])) . "', "
                        . "draft = 0, draft_params = '' WHERE "
                        . "folder_filter_uid = '" . $this->db->escape($filter['folder_filter_uid']) . "' "               
                        );                
            } else {
                $this->db->query("UPDATE " . DB_PREFIX . "folder_filter SET "
                        . "draft = 0 WHERE "
                        . "folder_filter_uid = '" . $this->db->escape($filter['folder_filter_uid']) . "' "               
                        );                 
            }
        }
        //удаляем фильтры, помеченные на удаление
        $this->db->query("DELETE FROM " . DB_PREFIX . "folder_filter WHERE folder_uid = '" . $folder_uid . "' AND draft = 2");
        
        
        //СОХРАНЯЕМ КНОПКИ        
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "folder_button WHERE folder_uid = '" . $folder_uid . "' AND (draft = 1 OR draft = 3)");

        foreach ($query->rows as $folder_button) {
            if($folder_button['draft_params']) {
                $draft_params = unserialize($folder_button['draft_params']);

                $this->db->query("UPDATE " . DB_PREFIX . "folder_button SET "
                        . "picture = '" . $this->db->escape($draft_params['picture']) . "', "
                        . "hide_button_name = '" . (int) ($draft_params['picture'] ? $draft_params['hide_button_name'] : 0) . "', "
                        . "color = '" . $this->db->escape($draft_params['color']) . "', "
                        . "background = '" . $this->db->escape($draft_params['background']) . "', "
                        . "action = '" . $this->db->escape($draft_params['action']) . "', "
                        . "action_log = '" . $this->db->escape($draft_params['action_log']) . "', "
                        . "action_move_route_uid = '" . $this->db->escape($draft_params['action_move_route_uid']) . "', "
                        . "action_params = '" . $this->db->escape($draft_params['action_params']) . "', "
                        . "draft = 0, draft_params = '' "
                        . (!empty($draft_params['sort']) ? ", sort = '" . (int) $draft_params['sort'] . "' " : "")
                        . "WHERE folder_button_uid = '" . $this->db->escape($folder_button['folder_button_uid']) . "' "
                        );
                $this->db->query("DELETE FROM " . DB_PREFIX . "folder_button_description WHERE folder_button_uid = '" . $this->db->escape($folder_button['folder_button_uid']) . "' ");
                foreach ($draft_params['description'] as $language_id => $value) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "folder_button_description SET "
                            . "folder_button_uid = '" . $this->db->escape($folder_button['folder_button_uid']) . "', "
                            . "name = '" . $this->db->escape($value['name']) . "', "
                            . "description = '" . $this->db->escape($value['description']) . "', "
                            . "language_id = '" . (int) $language_id . "' "
                            );
                }
                $this->db->query("DELETE FROM " . DB_PREFIX . "folder_button_field WHERE folder_button_uid = '" . $this->db->escape($folder_button['folder_button_uid']) . "' ");
                $sqls = array();
                if (isset($draft_params['field']) && $draft_params['field']) {
                    foreach ($draft_params['field'] as $value) {
                        $sqls[] = "('" . $this->db->escape($folder_button['folder_button_uid']) . "','" . $this->db->escape($value) . "')";                        
                    }                    
        
                    if ($sqls) {                   
                        $this->db->query("INSERT INTO " . DB_PREFIX . "folder_button_field (folder_button_uid, field_uid) "
                                . "VALUES " . implode(",", array_unique($sqls)));                    
                    } 
                }
                $this->db->query("DELETE FROM " . DB_PREFIX . "folder_button_route WHERE folder_button_uid = '" . $this->db->escape($folder_button['folder_button_uid']) . "' ");
                if (isset($draft_params['route'])) {
                    foreach ($draft_params['route'] as $value) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "folder_button_route SET "
                                . "folder_button_uid = '" . $this->db->escape($folder_button['folder_button_uid']) . "', "
                                . "route_uid = '" . $this->db->escape($value) . "' "
                                );
                    }                    
                }
                $this->updateButtonDelegate($folder_button['folder_button_uid']);
            } else {
                $this->db->query("UPDATE " . DB_PREFIX . "folder_button SET draft = 0, draft_params = '' WHERE folder_button_uid = '" . $this->db->escape($folder_button['folder_button_uid']) . "' ");             
            }           
        }
        //удаляем кнопки, помеченные на удаление
        $this->db->query("DELETE FROM " . DB_PREFIX . "folder_button_description WHERE folder_button_uid IN "
                . "(SELECT folder_button_uid FROM " . DB_PREFIX . "folder_button WHERE folder_uid = '" . $folder_uid . "' AND draft = 2) ");
        $this->db->query("DELETE FROM " . DB_PREFIX . "folder_button_field WHERE folder_button_uid IN "
                . "(SELECT folder_button_uid FROM " . DB_PREFIX . "folder_button WHERE folder_uid = '" . $folder_uid . "' AND draft = 2) ");
        $this->db->query("DELETE FROM " . DB_PREFIX . "folder_button_route WHERE folder_button_uid IN "
                . "(SELECT folder_button_uid FROM " . DB_PREFIX . "folder_button WHERE folder_uid = '" . $folder_uid . "' AND draft = 2) ");
        $this->db->query("DELETE FROM " . DB_PREFIX . "folder_button WHERE folder_uid='" . $folder_uid . "' AND draft = 2");    
        //проверяем сортировку кнопок
        $query_button_sort = $this->db->query("SELECT folder_button_uid, sort FROM " . DB_PREFIX . "folder_button WHERE folder_uid='" . $folder_uid . "' ORDER BY sort ASC");
        $sort = 1;
        foreach ($query_button_sort->rows as $button) {
            $this->db->query("UPDATE " . DB_PREFIX . "folder_button SET sort='" . $sort++ . "' WHERE folder_button_uid = '" . $button['folder_button_uid'] . "'");
        }
    }    

    public function getTotalFolders($data) {
        $query = $this->db->query("SELECT COUNT(folder_uid) AS total FROM " . DB_PREFIX . "folder WHERE draft < 3");
        return $query->row['total'];
    }
    
    public function getFolders($data) {
        $sql = "SELECT f.*, fd.name, fd.short_description, fd.full_description FROM " . DB_PREFIX . "folder f "
                . "LEFT JOIN " . DB_PREFIX . "folder_description fd ON (f.folder_uid = fd.folder_uid AND fd.language_id = '" . (int)$this->config->get('config_language_id') . "') WHERE f.draft < 3 ";
        if(!empty($data['filter_name'])) {
            $filter_name = explode(" ", $data['filter_name']);
            $filter_names = array();
            foreach ($filter_name as $word) {
                $filter_names[] = " AND fd.name LIKE '%" . $this->db->escape($word) . "%' ";
            }
            $sql .= implode(" AND ", $filter_names);
            
        }
        $sql .= " ORDER BY " . $this->db->escape(($data['sort'] ?? " fd.name"));
        $sql .= " " . $this->db->escape(($data['order'] ?? "ASC"));
        if (!empty($data['start']) && !empty($data['limit'])) {
            $sql .= " LIMIT " . $data['start'] . "," . $data['limit'];
        }
        $query = $this->db->query($sql);
        return $query->rows;
    }
    
    public function getFolderDescriptions($folder_uid) {
        $folder_info = $this->getFolder($folder_uid);  
        $folder_description_data = array();
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "folder_description "
            . "WHERE folder_uid = '" . $this->db->escape($folder_uid) . "'");            
        foreach ($query->rows as $result) {
            $folder_description_data[$result['language_id']] = array(
                    'name'                  => $result['name'],
                    'short_description'     => $result['short_description'],
                    'full_description'      => $result['full_description'],
            );
        }
        if (isset($folder_info['draft_params']['folder_description'])) {
            foreach ( $folder_info['draft_params']['folder_description'] as $language_id => $result) {
                $folder_description_data[$language_id] = array(
                        'name'                  => $result['name'] ?? $folder_description_data[$language_id]['name'] ?? "",
                        'short_description'     => $result['short_description'] ?? $folder_description_data[$language_id]['short_description'] ?? "",
                        'full_description'      => $result['full_description'] ?? $folder_description_data[$language_id]['full_description'] ?? "",
                );            
            }            
        }

        return $folder_description_data;

    }
            
    public function getFolder($folder_uid) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "folder f "
                . "LEFT JOIN folder_description fd ON (f.folder_uid = fd.folder_uid AND fd.language_id = '" . (int) $this->config->get('config_language_id') . "') "
                . "WHERE f.folder_uid='" . $this->db->escape($folder_uid) . "'");
        $result = array();
        if ($query->num_rows) {
            if ($query->row['draft'] && $query->row['draft_params']) {
                $draft_params = unserialize($query->row['draft_params']);
            }
            $additional_params = unserialize($query->row['additional_params']);
            if (!empty($draft_params['additional_params'])) {
                $additional_params = array_merge(is_array($additional_params) ? $additional_params : array(), $draft_params['additional_params']);
            }
//            if (isset($draft_params['toolbar'])) {
//                $additional_params['toolbar'] = $draft_params['toolbar'];
//            }
//            if (isset($draft_params['navigation'])) {
//                $additional_params['navigation'] = $draft_params['navigation'];
//            }
            $result = array(
                'doctype_uid'       => $draft_params['doctype_uid'] ?? $query->row['doctype_uid'],
                'type'              => $query->row['type'],
                'date_added'        => $query->row['date_added'],
                'date_edited'       => $query->row['date_edited'],
                'user_uid'          => $query->row['user_uid'],
                'additional_params' => $additional_params,
                'draft'             => $query->row['draft'],
                'draft_params'      => $draft_params ?? array(),
                'name'              => $draft_params['folder_description'][$this->config->get('config_language_id')]['name'] ?? $query->row['name'],
                'short_description' => $draft_params['folder_description'][$this->config->get('config_language_id')]['short_description'] ?? $query->row['short_description'],
                'full_description'  => $draft_params['folder_description'][$this->config->get('config_language_id')]['full_description'] ?? $query->row['full_description']
            );                    
        }
        return $result;        
    }    
    
    public function getFieldParent($field_uid) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "folder_field WHERE folder_field_uid = '" . $field_uid . "' ");
        if ($query->row['draft'] && $query->row['draft_params']) {
            return unserialize($query->row['draft_params'])['grouping_parent_uid'];
        }
        return $query->row['grouping_parent_uid'];
        
    }
    
    /**
     * Возвращает поле журнала, которое является дочерним по отшение к переданному field_uid 
     * @param type $folder_uid
     * @param type $field_uid
     */
    public function getFieldByGroupingField($folder_uid, $field_uid) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "folder_field WHERE folder_uid = '" . $this->db->escape($folder_uid) . "' AND grouping_parent_uid = (SELECT folder_field_uid FROM " . DB_PREFIX . "folder_field WHERE folder_uid = '" . $this->db->escape($folder_uid) . "' AND field_uid = '" . $this->db->escape($field_uid) . "')");
        return $query->row;
    }
    
    /**
     * Возвращает поле журнала по полю доктайпа
     * @param type $folder_uid
     * @param type $field_uid
     */
    public function getFieldByField($folder_uid, $field_uid) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "folder_field WHERE folder_uid = '" . $this->db->escape($folder_uid) . "' AND field_uid = '" . $this->db->escape($field_uid) . "' ");
        return $query->row;        
    }
    
    public function getDefaultSortField($folder_uid, $language_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "folder_field WHERE folder_uid = '" . $this->db->escape($folder_uid) . "' AND language_id='" . (int) $language_id . "' AND default_sort > 0 ");
        return $query->row;
    }
    
    public function getField($folder_field_uid) {
        $query = $this->db->query("SELECT ff.folder_field_uid, ff.field_uid, ff.folder_uid, ff.sort_grouping, ff.sort_tcolumn, ff.tcolumn, f.name AS field_name, f.type, ff.grouping, ff.grouping_name, "
                . "ff.grouping_parent_uid, ff.grouping_tree_uid, ff.tcolumn, ff.tcolumn_name, ff.tcolumn_total, ff.tcolumn_hidden, ff.language_id, ff.draft, ff.draft_params, "
                . "(SELECT name FROM " . DB_PREFIX . "field WHERE field_uid = ff.grouping_tree_uid) AS grouping_tree_name " 
                . "FROM " . DB_PREFIX . "folder_field ff "
                . "LEFT JOIN " . DB_PREFIX . "field f ON (f.field_uid = ff.field_uid) "
                . "WHERE ff.folder_field_uid='" . $this->db->escape($folder_field_uid) . "' ");
        $field = $query->row;
        if (!empty($field['draft']) && !empty($field['draft_params'])) {
            $this->load->model('doctype/doctype');
            $draft_params = unserialize($field['draft_params']);
            $field['draft_params'] = $draft_params;
            if ($field['field_uid'] != $draft_params['field_uid']) {
                $field['field_uid'] = $draft_params['field_uid'];
                $field_info = $this->model_doctype_doctype->getField($draft_params['field_uid']);
                $field['field_name'] = $field_info['name'];
                $field['type'] = $field_info['type'];
            } 
            $field['grouping'] = $draft_params['grouping'];
            $field['grouping_name'] = $draft_params['grouping_name'];
            $field['grouping_parent_uid'] = $draft_params['grouping_parent_uid'];
            if ($field['grouping_tree_uid'] != $draft_params['grouping_tree_uid']) {
                $field['grouping_tree_uid'] = $draft_params['grouping_tree_uid'];
                $field['grouping_tree_name'] = $this->model_doctype_doctype->getFieldName($draft_params['grouping_parent_uid']);
            }
            $field['tcolumn'] = $draft_params['tcolumn'];
            $field['tcolumn_name'] = $draft_params['tcolumn_name'];    
            $field['tcolumn_total'] = $draft_params['tcolumn_total'] ?? "";    
            $field['tcolumn_hidden'] = $draft_params['tcolumn_hidden'];    
            if (!empty($draft_params['sort_grouping'])) {
                $field['sort_grouping'] = $draft_params['sort_grouping'];
            }
            if (!empty($draft_params['sort_tcolumn'])) {
                $field['sort_tolumn'] = $draft_params['sort_tcolumn'];
            }
        }
        return $field;
        
    }
    
    /**
     * Возвращает поля доктайпа, которые еще не относятся к журналу (и могут быть добавлены в админке - Поля)
     * @param type $folder_uid
     */
    public function getDoctypeFieldsWithoutFolder($folder_uid, $language_id) {
        $query = $this->db->query("SELECT field_uid, name as field_name, type  FROM " . DB_PREFIX . "field WHERE doctype_uid=(SELECT DISTINCT doctype_uid FROM " . DB_PREFIX . "folder WHERE folder_uid='" . $this->db->escape($folder_uid) . "') AND field_uid NOT IN (SELECT field_uid FROM " . DB_PREFIX . "folder_field WHERE folder_uid='" . $this->db->escape($folder_uid) . "' AND language_id='" . (int) $language_id . "') AND setting=0 AND draft<2");
        return $query->rows;
    }
        
    public function getFields($data) {
        $sql = "SELECT ff.folder_field_uid, ff.field_uid, ff.sort_grouping, ff.sort_tcolumn, ff.default_sort, ff.tcolumn, f.name, f.type, ff.grouping, ff.grouping_name, "
                . "ff.grouping_parent_uid, ff.grouping_tree_uid, ff.tcolumn, ff.tcolumn_name, ff.tcolumn_total, ff.tcolumn_hidden, ff.language_id, ff.draft, ff.draft_params, "
                . "(SELECT name FROM " . DB_PREFIX . "field WHERE field_uid = ff.grouping_tree_uid) AS grouping_tree_name " 
                . "FROM " . DB_PREFIX . "folder_field ff "
                . "LEFT JOIN " . DB_PREFIX . "field f ON (f.field_uid = ff.field_uid) "
                . "WHERE ff.folder_uid='" . $this->db->escape($data['folder_uid']) . "' ";
        if (isset($data['draft'])) {
            if (is_array($data['draft'])) {
                $sql .= " AND ff.draft IN (" . $this->db->escape(implode(",",$data['draft'])) . ") ";
            } else {
                $sql .= " AND ff.draft = '" . (int) $data['draft'] . "' ";
            }
        }
        if (isset($data['language_id'])) {
            $sql .= "AND ff.language_id = '" . (int) $data['language_id'] . "' ";
        }        
        $query = $this->db->query($sql);
        $result = array();
        $this->load->model('doctype/doctype');
        $query_folder = $this->db->query("SELECT * FROM " . DB_PREFIX . "folder WHERE folder_uid='" . $this->db->escape($data['folder_uid']) . "' ");
        if ($query_folder->row['draft'] && $query_folder->row['draft_params']) {
            $draft_folder = unserialize($query_folder->row['draft_params']);
        }
        foreach ($query->rows as $field) {
            if ($field['draft'] && $field['draft_params']) {
                $draft_params = unserialize($field['draft_params']);
                if ($field['field_uid'] != $draft_params['field_uid']) {
                    $field['field_uid'] = $draft_params['field_uid'];
                    $field_info = $this->model_doctype_doctype->getField($draft_params['field_uid']);
                    $field['name'] = $field_info['name'];
                    $field['type'] = $field_info['type'];
                } 
                $field['grouping'] = $draft_params['grouping'];
                $field['grouping_name'] = $draft_params['grouping_name'];
                $field['grouping_parent_uid'] = $draft_params['grouping_parent_uid'];
                if ($field['grouping_tree_uid'] != $draft_params['grouping_tree_uid']) {
                    $field['grouping_tree_uid'] = $draft_params['grouping_tree_uid'];
                    $field['grouping_tree_name'] = $this->model_doctype_doctype->getFieldName($draft_params['grouping_parent_uid']);
                }
                $field['tcolumn'] = $draft_params['tcolumn'];
                $field['tcolumn_name'] = $draft_params['tcolumn_name'];
                $field['tcolumn_total'] = $draft_params['tcolumn_total'] ?? "";
                $field['tcolumn_hidden'] = $draft_params['tcolumn_hidden'];
                $field['sort_grouping'] = $draft_params['sort_grouping'] ?? $field['sort_grouping'];
                $field['sort_tcolumn'] = $draft_params['sort_tcolumn'] ?? $field['sort_tcolumn'];
            }
            if (
                       (isset($data['grouping']) && $data['grouping'] != $field['grouping']) 
                    || (isset($data['grouping_tree_uid']) && $data['grouping_tree_uid'] != $field['grouping_tree_uid']) 
                    || (isset($data['tcolumn']) && $data['tcolumn'] != $field['tcolumn']) 
                    || (isset($data['grouping_parent_uid']) && $data['grouping_parent_uid'] != $field['grouping_parent_uid']) 
                    || (isset($data['not_grouping_parent_uid']) && $data['not_grouping_parent_uid'] == $field['grouping_parent_uid'])
                    ) {

                continue;
            }
            if (isset($draft_folder['default_sort'][$field['language_id']])) {
                if ($draft_folder['default_sort'][$field['language_id']] == $field['folder_field_uid']) {
                    $default_sort = $draft_folder['default_sort_order'][$field['language_id']] ?? 1;
                } else {
                    $default_sort = 0;
                }
            }
            
            if (isset($data['language_id'])) {
                $result[] = array(
                    'folder_field_uid'      => $field['folder_field_uid'],
                    'field_uid'             => $field['field_uid'],
                    'type'                  => $field['type'],
                    'field_name'            => $field['name'],
                    'grouping'              => $field['grouping'],
                    'grouping_name'         => $field['grouping_name'],
                    'grouping_parent_uid'   => $field['grouping_parent_uid'],
                    'grouping_tree_uid'     => $field['grouping_tree_uid'],
                    'grouping_tree_name'    => $field['grouping_tree_name'],
                    'tcolumn'               => $field['tcolumn'],
                    'tcolumn_name'          => $field['tcolumn_name'],
                    'tcolumn_total'         => $field['tcolumn_total'],
                    'tcolumn_hidden'        => $field['tcolumn_hidden'],
                    'sort_grouping'         => $field['sort_grouping'],
                    'sort_tcolumn'          => $field['sort_tcolumn'],
                    'default_sort'          => $default_sort ?? $field['default_sort'],
                    'draft'                 => $field['draft']
                );                    
            } else {
                $result[$field['language_id']][] = array(
                    'folder_field_uid'      => $field['folder_field_uid'],
                    'field_uid'             => $field['field_uid'],
                    'type'                  => $field['type'],
                    'field_name'            => $field['name'],
                    'grouping'              => $field['grouping'],
                    'grouping_name'         => $field['grouping_name'],
                    'grouping_parent_uid'   => $field['grouping_parent_uid'],
                    'grouping_tree_uid'     => $field['grouping_tree_uid'],
                    'grouping_tree_name'    => $field['grouping_tree_name'],
                    'tcolumn'               => $field['tcolumn'],
                    'tcolumn_name'          => $field['tcolumn_name'],
                    'tcolumn_total'         => $field['tcolumn_total'],
                    'tcolumn_hidden'        => $field['tcolumn_hidden'],
                    'sort_grouping'         => $field['sort_grouping'],
                    'sort_tcolumn'          => $field['sort_tcolumn'],
                    'default_sort'          => $default_sort ?? $field['default_sort'],
                    'draft'                 => $field['draft']
                );

            }
        }    
        if (!empty($data['sort']) && $data['sort']=="grouping") {
            if (isset($data['language_id'])) {
                usort($result, function($a, $b){
                    if ($a['sort_grouping'] == $b['sort_grouping']) {
                        return 0;
                    }
                    return $a['sort_grouping'] > $b['sort_grouping'] ? 1 : 0;
                });
            } else {
                foreach ($result as &$result_lang) {
                    usort($result_lang, function($a, $b){
                        if ($a['sort_grouping'] == $b['sort_grouping']) {
                            return 0;
                        }
                        return $a['sort_grouping'] > $b['sort_grouping'] ? 1 : 0;
                    });
                }
            }            
        }
        if (!empty($data['sort']) && $data['sort']=="tcolumn") {
            if (isset($data['language_id'])) {
                usort($result, function($a, $b){
                    if ($a['sort_tcolumn'] == $b['sort_tcolumn']) {
                        return 0;
                    }
                    return $a['sort_tcolumn'] > $b['sort_tcolumn'] ? 1 : 0;
                });
            } else {
                foreach ($result as &$result_lang) {
                    usort($result_lang, function($a, $b){
                        if ($a['sort_tcolumn'] == $b['sort_tcolumn']) {
                            return 0;
                        }
                        return $a['sort_tcolumn'] > $b['sort_tcolumn'] ? 1 : 0;
                    });
                }
            }            
        }
//        if (!empty($data['sort'])) {
//            $sql .= "ORDER BY " . $this->db->escape($data['sort']) . " ";
//            if (!empty($data['order'])) {
//                $sql .= $data['order'];
//            } else {
//                $sql .= " ASC";
//            }
//        }
        
        return $result;
    }    
    
    public function getButton($button_uid) {
        $this->load->model('doctype/doctype');
        $this->load->language('doctype/folder');
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "folder_button "
                . "WHERE folder_button_uid = '" . $this->db->escape($button_uid) . "'"
                );
        $button = $query->row;
        if ($button['draft'] && $button['draft_params']) {
            $draft_button = unserialize($button['draft_params']);
            $descriptions = $draft_button['description'];
            $fields = array();
            if($draft_button['field']) {
                foreach ($draft_button['field'] as $field_uid) {
                    $field_info = $this->model_doctype_doctype->getField($field_uid);
                    $name = "";
                    if ($field_info['setting']) {
                        $doctype_info = $this->model_doctype_doctype->getDoctype($field_info['doctype_uid']);
                        $name = $doctype_info['name'] . " - ";
                    } 
                    $name .= $field_info['name'];                    
                    $fields[] = array(
                        'field_uid'      => $field_uid,
                        'name'          => $name
                    );
                }
            }    
            $routes = array();
            if($draft_button['route']) {
                foreach ($draft_button['route'] as $route_uid) {
                    $routes[] = array(
                        'route_uid'      => $route_uid,
                        'name'          => $route_uid ? $this->model_doctype_doctype->getRoute($route_uid)['name'] : $this->language->get('text_all_routes')
                    );
                }
            } else {
                    $routes[] = array(
                        'route_uid'      => 0,
                        'name'          => $this->language->get('text_all_routes')
                    );                
            }   
            $picture = $draft_button['picture'];
            $hide_button_name = $picture ? $draft_button['hide_button_name'] : 0;
            $color = $draft_button['color'];
            $background = $draft_button['background'];
            $action = $draft_button['action'];
            $action_params = unserialize($draft_button['action_params']);
            $action_log = $draft_button['action_log'];
            $action_move_route_uid = $draft_button['action_move_route_uid'];
            if (!empty($draft_button['sort'])) {
                $sort = $draft_button['sort'];
            } else {
                $sort = $button['sort'];
            }
        } else {
            $descriptions = $this->getButtonDescriptions($button['folder_button_uid']);
            $fields = array();
            foreach ($this->getButtonFields($button['folder_button_uid']) as $field) {
                $name = "";
                if ($field['setting']) {
                    $doctype_info = $this->model_doctype_doctype->getDoctype($field['doctype_uid']);
                    $name = $doctype_info['name'] . " - ";
                } 
                $name .= $field['name'];                    
                $fields[] = array(
                    'field_uid'     => $field['field_uid'],
                    'name'          => $name
                );
            }
            
            $routes = $this->getButtonRoutes($button['folder_button_uid']);
            $picture = $button['picture'];
            $hide_button_name = $picture ? $button['hide_button_name'] : 0;
            $color = $button['color'];
            $background = $button['background'];
            $action = $button['action'];
            $action_params = unserialize($button['action_params']);
            $action_log = $button['action_log'];
            $action_move_route_uid = $button['action_move_route_uid'];
            $sort = $button['sort'];
        }
        $this->load->model('tool/image');         
        if ($picture) {
            if (empty($descriptions[(int) $this->config->get('config_language_id')]['name'])) {
                $picture_25 = $this->model_tool_image->resize($picture,28,28);
            } else { 
                $picture_25 = $this->model_tool_image->resize($picture,28,28);
            } 
        } else {
            $picture_25 = "";
        }
        $result = array(
            'folder_button_uid'         => $button['folder_button_uid'],
            'folder_uid'                => $button['folder_uid'],
            'name'                      => !empty($descriptions[(int) $this->config->get('config_language_id')]['name']) ? $descriptions[(int) $this->config->get('config_language_id')]['name'] : "",
            'picture'                   => $picture,
            'hide_button_name'          => $hide_button_name,
            'color'                     => $color,
            'background'                => $background,
            'picture25'                 => $picture_25,
            'draft'                     => $button['draft'],
            'draft_params'              => $button['draft_params'] ? unserialize($button['draft_params']) : "",
            'descriptions'              => $descriptions,
            'fields'                    => $fields,
            'routes'                    => $routes,
            'action'                    => $action,
            'action_params'             => $action_params,
            'action_log'                => $action_log,
            'action_move_route_uid'     => $action_move_route_uid,
            'sort'                      => $sort
        );

        return $result;
    }    
    
    public function getFilter($filter_uid) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "folder_filter WHERE folder_filter_uid = '" . $this->db->escape($filter_uid) . "'");
        $result = array();
        if ($query->num_rows) {
            $filter = $query->row;
            if ($filter['draft'] && $filter['draft_params']) {
                $params = unserialize($filter['draft_params']);
            } else {
                $params = $filter;
            }
            $result['folder_uid']  = $filter['folder_uid'];
            $result['field_uid']          = $params['field_uid'];
            $result['condition']         = $params['condition_value'];
            $result['type_value']        = $params['type_value'];
            $result['value']             = $params['value'];
            $result['action']            = $params['action'];
            $result['action_params']     = $filter['draft'] && $filter['draft_params'] ? $params['action_params'] : unserialize($filter['action_params']);
            $result['draft']             = $filter['draft'];
        }
        return $result;
    }  
    
    public function getFilters($folder_uid) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "folder_filter WHERE folder_uid = '" . $this->db->escape($folder_uid) . "'");
        $result = array();
        foreach ($query->rows as $filter) {            
            if ($filter['draft'] && $filter['draft_params']) {
                $params = unserialize($filter['draft_params']);
            } else {
                $params = $filter;
            }
            $result[] = array(
                'folder_filter_uid'  => $filter['folder_filter_uid'],
                'field_uid'          => $params['field_uid'],
                'condition'         => $params['condition_value'],
                'type_value'        => $params['type_value'],
                'value'             => $params['value'],
                'action'            => $params['action'],
                'action_params'     => $filter['draft'] && $filter['draft_params'] ? $params['action_params'] : unserialize($filter['action_params']),
                'draft'             => $filter['draft']
            );
        }
        return $result;
    }  
    
    public function getButtons($folder_uid) {
        $this->load->model('doctype/doctype');
        $this->load->language('doctype/folder');
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "folder_button "
                . "WHERE folder_uid = '" . $this->db->escape($folder_uid) . "'"
                );
        $result = array();
        foreach ($query->rows as $button) {            
            if ($button['draft'] && $button['draft_params']) {
                $draft_button = unserialize($button['draft_params']);
                $descriptions = $draft_button['description'];
                $fields = array();
                if($draft_button['field']) {
                    foreach ($draft_button['field'] as $field_uid) {
                        $fields[] = array(
                            'field_uid'      => $field_uid,
                            'name'          => $this->model_doctype_doctype->getField($field_uid)['name']
                        );
                    }
                }    
                $routes = array();
                if($draft_button['route']) {
                    foreach ($draft_button['route'] as $route_uid) {
                        $routes[] = array(
                            'route_uid'      => $route_uid,
                            'name'          => $route_uid ? $this->model_doctype_doctype->getRoute($route_uid)['name'] : $this->language->get('text_all_routes')
                        );
                    }
                } else {
                    $routes[] = array(
                        'route_uid'      => 0,
                        'name'          => $this->language->get('text_all_routes')
                    );
                }    
                $picture = $draft_button['picture'];
                $hide_button_name = $picture ? $draft_button['hide_button_name'] : 0;
                $color = $draft_button['color'];
                $background = $draft_button['background'];
                $action = $draft_button['action'];
                if (!empty($draft_button['sort'])) { 
                    $sort = $draft_button['sort'];
                } else {
                    $sort = $button['sort'];
                }
            } else {
                $descriptions = $this->getButtonDescriptions($button['folder_button_uid']);
                $fields = $this->getButtonFields($button['folder_button_uid']);
                $routes = $this->getButtonRoutes($button['folder_button_uid']);
                $picture = $button['picture'];
                $hide_button_name = $picture ? $button['hide_button_name'] : 0;
                $color = $button['color'];
                $background = $button['background'];
                $action = $button['action'];
                $sort = $button['sort'];
            }
            
            $this->load->model('tool/image');
            if ($picture) {
                if (empty($descriptions[(int) $this->config->get('config_language_id')]['name'])) {
                    $picture_25 = $this->model_tool_image->resize($picture,28,28);
                } else { 
                    $picture_25 = $this->model_tool_image->resize($picture,28,28);
                } 
            } else {
                $picture_25 = "";
            }
            $result[] = array(
                'folder_button_uid' => $button['folder_button_uid'],
                'name'              => !empty($descriptions[(int) $this->config->get('config_language_id')]['name']) ? $descriptions[(int) $this->config->get('config_language_id')]['name'] : "",
                'picture'           => $picture,
                'hide_button_name'  => $hide_button_name,
                'color'             => $color,
                'background'        => $background,
                'picture25'         => $picture_25,
                'draft'             => $button['draft'],
                'descriptions'      => $descriptions,
                'fields'            => $fields,
                'routes'            => $routes,
                'action_name'       => $this->load->controller('extension/action/' . $action . "/getTitle"),
                'sort'              => $sort
            );
        }
        usort($result, function($a, $b){
            if ($a['sort'] == $b['sort']) {
                return 0;
            }
            return $a['sort'] > $b['sort'] ? 1 : 0;
        });
        return $result;
    }    
    
    public function getButtonDescriptions($button_uid) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "folder_button_description WHERE folder_button_uid = '" . $this->db->escape($button_uid) . "'");
        $result = array();
        foreach ($query->rows as $folder_button) {
            $result[$folder_button['language_id']] = array(
                'name'          => $folder_button['name'],
                'description'   => $folder_button['description']
            );
        }
        return $result;
    }    
    
    public function getButtonFields($button_uid) {
        $this->load->model('doctype/doctype');
        $query = $this->db->query("SELECT fbf.field_uid, f.name, f.setting, f.doctype_uid FROM " . DB_PREFIX . "folder_button_field fbf "
                . "LEFT JOIN " . DB_PREFIX . "field f ON (fbf.field_uid = f.field_uid) "
                . "WHERE fbf.folder_button_uid = '" . $this->db->escape($button_uid) . "'");
        return $query->rows;
    }    
    
    public function getButtonRoutes($button_uid) {
        $this->load->model('doctype/doctype');
        $this->load->language('doctype/folder');
        $query = $this->db->query("SELECT fbr.route_uid, rd.name FROM " . DB_PREFIX . "folder_button_route fbr "
                . "LEFT JOIN " . DB_PREFIX . "route_description rd ON (rd.route_uid = fbr.route_uid AND rd.language_id='" . (int) $this->config->get('config_language_id') . "') "
                . "WHERE fbr.folder_button_uid = '" . $this->db->escape($button_uid) . "'");
        $result = array();
        foreach ($query->rows as $route) {
                $result[] = array(
                    'route_uid'  => $route['route_uid'],
                    'name'      => $route['name'] ? $route['name'] : $this->language->get('text_all_routes')
                );    
            
            }
        
        return $result;
    }    
    
    public function removeButton($button_uid) {
        $this->db->query("UPDATE " . DB_PREFIX . "folder_button SET draft=2 WHERE folder_button_uid='" . $this->db->escape($button_uid) . "'");
        $this->db->query("UPDATE " . DB_PREFIX . "folder SET draft=CASE WHEN draft=3 THEN 3 ELSE 1 END WHERE folder_uid = (SELECT folder_uid FROM " . DB_PREFIX . "folder_button WHERE folder_button_uid = '" . $this->db->escape($button_uid) . "')");        
    }
    
    public function undoRemoveButton($button_uid) {
        $this->db->query("UPDATE " . DB_PREFIX . "folder_button SET draft=(CASE WHEN draft_params='' THEN 0 ELSE 1 END) WHERE folder_button_uid='" . $this->db->escape($button_uid) . "'");
    }
    
    public function getChildrenIds($parent_id) {
        $query = $this->db->query("SELECT folder_field_uid FROM " . DB_PREFIX . "folder_field WHERE grouping_parent_uid = '" . $this->db->escape($parent_id) . "' ");
        $result = array();
        foreach ($query->rows as $field) {
            $result[] = $field['folder_field_uid'];
        }
        foreach ($result as $child_id) {
            $result = array_merge($result, $this->getChildrenIds($child_id));
        }
        return $result;
    }
    
    private function getMaxSortTColumn($folder_uid, $language_id) {
        $query = $this->db->query("SELECT MAX(sort_tcolumn) AS max_sort FROM " . DB_PREFIX . "folder_field WHERE folder_uid = '" . $this->db->escape($folder_uid) . "' AND language_id = '" . (int) $language_id . "' ");
        $max_sort = (int) $query->row['max_sort'];
        return ++$max_sort;
    } 
    
    private function getMaxSortGrouping($folder_uid, $language_id, $parent_id) {
        if ($parent_id) {
            $parent_with_children = $this->getChildrenIds($parent_id);
            $parent_with_children[] = $parent_id;
            $query = $this->db->query("SELECT MAX(sort_grouping) AS max_sort FROM " . DB_PREFIX . "folder_field WHERE grouping_parent_uid IN ('" . $this->db->escape(implode("','", $parent_with_children)) . "') ");
            $max_sort = (int) $query->row['max_sort'];
            if(!$max_sort) {
                //добавляем первого ребенка к родителю
                $query = $this->db->query("SELECT sort_grouping FROM " . DB_PREFIX . "folder_field WHERE folder_field_uid = '" . $this->db->escape($parent_id) . "' ");                
                $max_sort = (int) $query->row['sort_grouping'];
            }
            //обновляем сортировку
            $this->db->query("UPDATE " . DB_PREFIX . "folder_field SET sort_grouping = sort_grouping+1 WHERE grouping = 1 AND sort_grouping > '" . (int) $max_sort . "' AND folder_uid = '" . $this->db->escape($folder_uid) . "' AND language_id = '" . (int) $language_id . "' ");
        } else {
            $query = $this->db->query("SELECT MAX(sort_grouping) AS max_sort FROM " . DB_PREFIX . "folder_field WHERE folder_uid = '" . $this->db->escape($folder_uid) . "' AND language_id = '" . (int) $language_id . "' ");
            $max_sort = (int) $query->row['max_sort'];
        }
        return  ++$max_sort;        
    }
    
    public function addField($folder_uid, $language_id, $data, $draft=1) {
        if ($data['field_grouping'] || $data['field_tcolumn']) {
            $this->load->model('localisation/language');
            $query_uid = $this->db->query("SELECT UUID() AS uid");
            $folder_field_uid = $query_uid->row['uid'];            
            $this->db->query("INSERT INTO " . DB_PREFIX . "folder_field SET "
                    . "folder_field_uid = '" . $folder_field_uid . "', "
                    . "folder_uid = '" . $this->db->escape($folder_uid) . "', "
                    . "field_uid = '" . $this->db->escape($data['field_field_uid']) . "', "
                    . "grouping = '" . (int) $data['field_grouping'] . "', "
                    . "grouping_name = '" . $this->db->escape($data['field_grouping_name']) . "', "
                    . "grouping_parent_uid = '" . $this->db->escape($data['field_grouping_parent_uid']) . "', "
                    . "grouping_tree_uid = '" . ($data['field_content_grouping'] == 'tree' ? $this->db->escape($data['field_grouping_tree_uid']) : 0) . "', "
                    . "tcolumn = '" . (int) $data['field_tcolumn'] . "', "
                    . "tcolumn_name = '" . $this->db->escape($data['field_tcolumn_name']) . "', "
                    . "tcolumn_total = '" . (int) $data['field_tcolumn_total'] . "', "
                    . "tcolumn_hidden = '" . (int) $data['field_tcolumn_hidden'] . "', "
                    . "draft = 3, "
                    . "draft_params = '', "
                    . "default_sort = 0, "
                    . "sort_grouping = '" . ($data['field_grouping'] ? (int) $this->getMaxSortGrouping($folder_uid, $language_id, $data['field_grouping_parent_uid']) : 0) . "', "
                    . "sort_tcolumn = '" . (int) $this->getMaxSortTColumn($folder_uid, $language_id) . "', "
                    . "language_id = '" . (int) $language_id . "' "
                    );
            $this->db->query("UPDATE " . DB_PREFIX . "folder SET draft = (CASE WHEN draft='3' THEN 3 ELSE 1 END) WHERE folder_uid = '" . $this->db->escape($folder_uid) . "'");        
            return $folder_field_uid;            
        } else {
            return 0;
        }
    }
    
    public function editField($folder_field_uid, $data, $draft=1) {
        if ($data['field_grouping'] || $data['field_tcolumn']) {  
            $field_info = $this->getField($folder_field_uid);            
            if ($field_info['grouping_parent_uid'] != $data['field_grouping_parent_uid']) {
                //нужно изменять сортировку по группе
                $sort_grouping = $this->getMaxSortGrouping($field_info['folder_uid'], $field_info['language_id'], $data['field_grouping_parent_uid']);
            } else {
                $sort_grouping = $field_info['sort_grouping'];
            }
            if ($field_info['tcolumn'] != $data['field_tcolumn']) {
                //переключили отображение в таблице, нужно изменять сортировку
                if ($data['field_tcolumn']) { //в таблице показывается
                    $sort_tcolumn = $this->getMaxSortTColumn($field_info['folder_uid'], $field_info['language_id']);
                } else {
                    $sort_tcolumn = 0;
                }                                
            } else {
                $sort_tcolumn = $field_info['sort_tcolumn'];
            }
            $old_draft_params = array();
            if ($field_info['draft'] && $field_info['draft_params']) {
                $old_draft_params = $field_info['draft_params'];
            }
            if ($data['field_grouping_tree_uid'] && $data['field_grouping_tree_uid']== $this->db->escape($data['field_field_uid'])) {
                //совпадение поля для отображения и поля для построение дерева приводи к циклу
                $data['field_grouping_tree_uid'] = "0";
            }
            
            $draft_params = array(
                'grouping'                  => $data['field_grouping'],
                'grouping_name'             => $data['field_grouping_name'],
                'field_uid'                 => $data['field_field_uid'],
                'grouping_parent_uid'       => $data['field_content_grouping'] == 'list' ? $this->db->escape($data['field_grouping_parent_uid']) : 0,
                'grouping_tree_uid'         => $data['field_content_grouping'] == 'tree' ? $this->db->escape($data['field_grouping_tree_uid']) : 0,
                'tcolumn'                   => $data['field_tcolumn'],
                'tcolumn_name'              => $data['field_tcolumn_name'],
                'tcolumn_total'             => $data['field_tcolumn_total'],
                'tcolumn_hidden'            => $data['field_tcolumn_hidden'],
                'sort_grouping'             => $sort_grouping,
                'sort_tcolumn'              => $sort_tcolumn,
            );
            $this->db->query("UPDATE " . DB_PREFIX . "folder_field SET "
                    . "draft = (CASE WHEN draft='3' THEN 3 ELSE " . (int) $draft . " END), "
                    . "draft_params = '" . $this->db->escape(serialize($draft_params)) . "' "
                    . "WHERE folder_field_uid = '" . $this->db->escape($folder_field_uid) . "' "
                    );
            $this->setDraft($field_info['folder_uid']);
        } else {
            //#TODO выдать ошибку, т.к. если сохранить поле без группировки и таблицы, оно пропадет из выдачи
        }
    }
    
    public function editSortField($folder_field_uid, $sort_grouping, $sort_tcolumn) {
        $field_info = $this->getField($folder_field_uid);
        $draft_params = array(
            'grouping'                  => $field_info['grouping'],
            'grouping_name'             => $field_info['grouping_name'],
            'field_uid'                 => $field_info['field_uid'],
            'grouping_parent_uid'       => $field_info['grouping_parent_uid'],
            'grouping_tree_uid'         => $field_info['grouping_tree_uid'],
            'tcolumn'                   => $field_info['tcolumn'],
            'tcolumn_name'              => $field_info['tcolumn_name'],
            'tcolumn_total'             => $field_info['tcolumn_total'],
            'tcolumn_hidden'            => $field_info['tcolumn_hidden'],
        );
        if ($sort_grouping) {
            $draft_params['sort_grouping'] = $sort_grouping;
        } elseif (!empty($field_info['draft_params']['sort_grouping'])) {
            $draft_params['sort_grouping'] = $field_info['draft_params']['sort_grouping'];
        } 
        
        if ($sort_tcolumn) {
            $draft_params['sort_tcolumn'] = $sort_tcolumn;
        } elseif (!empty($field_info['draft_params']['sort_tcolumn'])) {
            $draft_params['sort_tcolumn'] = $field_info['draft_params']['sort_tcolumn'];
        }
        
        $this->db->query("UPDATE " . DB_PREFIX . "folder_field SET "
                . "draft = CASE WHEN draft=3 THEN 3 ELSE 1 END, "
                . "draft_params = '" . $this->db->escape(serialize($draft_params)) . "' "
                . "WHERE folder_field_uid = '" . $this->db->escape($folder_field_uid) . "' "
        );
        $this->setDraft($field_info['folder_uid']);
    }
    
    public function editSortButton($folder_button_uid, $sort) {
        $button_info = $this->getButton($folder_button_uid);
        $fields = array();
        if ($button_info['fields']) {
            foreach ($button_info['fields'] as $field) {
                $fields[] = $field['field_uid'];
            }
        }
        $routes = array();
        if ($button_info['routes']) {
            foreach ($button_info['routes'] as $route) {
                $routes[] = $route['route_uid'];
            }
        }
        $draft_params = array(
            'description'                   => $button_info['descriptions'],
            'picture'                       => $button_info['picture'],
            'hide_button_name'              => $button_info['hide_button_name'],
            'color'                         => $button_info['color'],
            'background'                    => $button_info['background'],
            'field'                         => $fields,
            'route'                         => $routes,
            'action'                        => $button_info['action'],
            'action_log'                    => $button_info['action_log'],
            'action_move_route_uid'          => $button_info['action_move_route_uid'],
            'action_params'                 => serialize($button_info['action_params']),
            'sort'                          => $sort
        );
        
        
        $this->db->query("UPDATE " . DB_PREFIX . "folder_button SET "
                . "draft = CASE WHEN draft=3 THEN 3 ELSE 1 END, "
                . "draft_params = '" . $this->db->escape(serialize($draft_params)) . "' "
                . "WHERE folder_button_uid = '" . $this->db->escape($folder_button_uid) . "' "
        ); 
        $this->setDraft($button_info['folder_uid']);
    }
    
    public function removeField($folder_field_uid) {
        $this->db->query("UPDATE " . DB_PREFIX . "folder_field SET draft=2 WHERE folder_field_uid='" . $this->db->escape($folder_field_uid) . "'");
        $this->db->query("UPDATE " . DB_PREFIX . "folder SET draft=CASE WHEN draft=3 THEN 3 ELSE 1 END WHERE folder_uid = (SELECT folder_uid FROM " . DB_PREFIX . "folder_field WHERE folder_field_uid = '" . $this->db->escape($folder_field_uid) . "')");        
    }
    
    public function undoRemoveField($folder_field_uid) {
         $this->db->query("UPDATE " . DB_PREFIX . "folder_field SET draft=(CASE WHEN draft_params='' THEN 0 ELSE 1 END) WHERE folder_field_uid='" . $this->db->escape($folder_field_uid) . "'");

    }    
    
    public function saveFolder($folder_uid, $data) {
        $query = $this->db->query("SELECT draft_params FROM " . DB_PREFIX . "folder WHERE folder_uid='" . $this->db->escape($folder_uid) . "' ");
        if ($query->row['draft_params']) {
            $draft = unserialize($query->row['draft_params']);
        } else {
            $draft = array();
        }         
        foreach ($data as $k1 => $v1) {
            if (is_array($v1)) {                                  
                foreach ($v1 as $k2 => $v2) {
                    if (is_array($v2)) {
                        foreach ($v2 as $k3 => $v3) {
                            $draft[$k1][$k2][$k3] = $v3;
                        }
                    } else {
                        $draft[$k1][$k2] = $v2;
                    }
                    
                }
            } else {
                $draft[$k1] = $v1;
            }
        }
        
      
        
        $sql = "UPDATE " . DB_PREFIX . "folder SET ";
        if (!empty($data['doctype_uid'])) {
            $sql .= "doctype_uid = '" . $this->db->escape($data['doctype_uid']) . "', ";
        }
        $sql .= "draft = CASE WHEN draft=3 THEN 3 ELSE 1 END, "
            . "draft_params = '" . $this->db->escape(serialize($draft)) . "' "
            . "WHERE folder_uid = '" . $this->db->escape($folder_uid) . "' ";
        $this->db->query($sql);
    }
    
    public function setDraft($folder_uid) {
            $this->db->query("UPDATE " . DB_PREFIX . "folder SET draft=CASE WHEN draft=3 THEN 3 ELSE 1 END WHERE folder_uid='" . $this->db->escape($folder_uid) . "'");
    }
    
    public function removeDraft($folder_uid) {
        $folder_uid = $this->db->escape($folder_uid);
        $folder_query = $this->db->query("SELECT draft FROM folder WHERE folder_uid='" . $folder_uid . "' ");
        if ($folder_query->num_rows && $folder_query->row['draft'] == 3) {
            $this->deleteFolder($folder_uid);
            return "";
        } else {
            $this->db->query("UPDATE " . DB_PREFIX . "folder SET draft=0, draft_params='' WHERE folder_uid='" . $folder_uid . "' ");
            $this->db->query("DELETE FROM " . DB_PREFIX . "folder_button WHERE draft=3 AND folder_uid='" . $folder_uid . "' ");
            $this->db->query("UPDATE " . DB_PREFIX . "folder_button SET draft=0, draft_params='' WHERE folder_uid='" . $folder_uid . "' ");
            $this->db->query("DELETE FROM " . DB_PREFIX . "folder_field WHERE draft=3 AND folder_uid='" . $folder_uid . "' ");
            $this->db->query("UPDATE " . DB_PREFIX . "folder_field SET draft=0, draft_params='' WHERE folder_uid='" . $folder_uid . "' ");
            $this->db->query("DELETE FROM " . DB_PREFIX . "folder_filter WHERE draft=3 AND folder_uid='" . $folder_uid . "' ");
            $this->db->query("UPDATE " . DB_PREFIX . "folder_filter SET draft=0, draft_params='' WHERE folder_uid='" . $folder_uid . "' ");     
            return $folder_uid;
        }
    }
    
    public function addFilter($folder_uid, $data, $draft=1) {         
        $this->load->model('doctype/doctype');       
        $field_info = $this->model_doctype_doctype->getField($data['filter_field_uid']);
        $this->load->model('extension/field/' . $field_info['type']);
        $model = "model_extension_field_" . $field_info['type'];
        $draft_filter = serialize(array(
            'field_uid'                      => $data['filter_field_uid'],
            'condition_value'               => $data['filter_condition_value'],
            'type_value'                    => $data['type_condition_value'],
            'value'                         => $data['type_condition_value'] == 'var' ? $data['field_value_var'] : $this->$model->getValue($data['filter_field_uid'], 0, $data['filter_value']),
            'action'                        => $data['filter_action_value'],
            'action_params'                 => $data['filter_action_params']
        ));
        $query = $this->db->query("SELECT UUID() AS uid");
        $folder_filter_uid = $query->row['uid'];
        $sql = "INSERT INTO " . DB_PREFIX . "folder_filter SET "
                . "folder_filter_uid = '" . $folder_filter_uid . "', "
                . "folder_uid='" . $this->db->escape($folder_uid) . "', "
                . "draft='" . (int) $draft . "', "
                . "draft_params = '" . $this->db->escape($draft_filter) . "' ";
        $this->db->query($sql);
        $this->db->query("UPDATE " . DB_PREFIX . "folder SET draft = (CASE WHEN draft='3' THEN 3 ELSE 1 END) WHERE folder_uid = '" . $this->db->escape($folder_uid) . "'");        
        return $folder_filter_uid;
    }    

    public function editFilter($filter_uid, $data, $draft=1) {    
        $this->load->model('doctype/doctype');       
        $field_info = $this->model_doctype_doctype->getField($data['filter_field_uid']);
        $this->load->model('extension/field/' . $field_info['type']);
        $model = "model_extension_field_" . $field_info['type'];
        $draft_filter = serialize(array(
            'field_uid'                      => $data['filter_field_uid'],
            'condition_value'               => $data['filter_condition_value'],
            'type_value'                    => $data['type_condition_value'],
            'value'                         => $data['type_condition_value'] == 'var' ? $data['field_value_var'] : $this->$model->getValue($data['filter_field_uid'], 0, $data['filter_value']),
            'action'                        => $data['filter_action_value'],
            'action_params'                 => $data['filter_action_params']
        ));
        $sql = "UPDATE " . DB_PREFIX . "folder_filter SET "
                . "draft=CASE WHEN draft='3' THEN 3 ELSE " . (int) $draft . " END, "
                . "draft_params = '" . $this->db->escape($draft_filter) . "' "
                . "WHERE folder_filter_uid = '" . $this->db->escape($filter_uid) . "'";
        $this->db->query($sql);
        $filter_info = $this->getFilter($filter_uid);
        $this->setDraft($filter_info['folder_uid']);
    }    
    
    public function removeFilter($filter_uid) {
        $this->db->query("UPDATE " . DB_PREFIX . "folder_filter SET draft=2 WHERE folder_filter_uid='" . $this->db->escape($filter_uid) . "'");
        $this->db->query("UPDATE " . DB_PREFIX . "folder SET draft=CASE WHEN draft=3 THEN 3 ELSE 1 END WHERE folder_uid = (SELECT folder_uid FROM " . DB_PREFIX . "folder_filter WHERE folder_filter_uid = '" . $this->db->escape($filter_uid) . "')");        
    }
    
    public function undoRemoveFilter($filter_uid) {
        $this->db->query("UPDATE " . DB_PREFIX . "folder_filter SET draft=(CASE WHEN draft_params='' THEN 0 ELSE 1 END) WHERE folder_filter_uid='" . $this->db->escape($filter_uid) . "'");
    }    
    
    public function getConditions() {
        return array(
            array(
                'value'    => 'equal',
                'title'  => $this->language->get('text_condition_equal')
            ),
            array(
                'value'    => 'notequal',
                'title'  => $this->language->get('text_condition_notequal')
            ),
            array(
                'value'    => 'more',
                'title'  => $this->language->get('text_condition_more')
            ),
            array(
                'value'    => 'moreequal',
                'title'  => $this->language->get('text_condition_moreequal')
            ),
            array(
                'value'    => 'less',
                'title'  => $this->language->get('text_condition_less')
            ),
            array(
                'value'    => 'lessequal',
                'title'  => $this->language->get('text_condition_lessequal')
            ),
            array(
                'value'    => 'contains',
                'title'  => $this->language->get('text_condition_contains')
            ),
            array(
                'value'    => 'notcontains',
                'title'  => $this->language->get('text_condition_notcontains')
            ),
        );
    }
    
    public function getFilterActions() {
        return array(
            array(
                'value'     => 'hide',
                'title'     => $this->language->get('text_action_hide')
            ),        
            array(
                'value'     => 'style',
                'title'     => $this->language->get('text_action_style')
            ),           
            array(
                'value'     => 'font',
                'title'     => $this->language->get('text_action_font')
            )            
        );
    }
    
    public function getVariables() {
        return array(
            'var_customer_id'                   => $this->language->get('text_var_customer_uid'), 
            'var_customer_name'                 => $this->language->get('text_var_customer_name'), 
            'var_current_time'                  => $this->language->get('text_var_current_datetime'),
            'var_current_date'                  => $this->language->get('text_var_current_date'),
            
        );
    }

    public function addButton($folder_uid, $data, $draft=1) {   
        if($data['button_action']) {
            $data_params = array(
                'folder_uid' => $folder_uid,
                'params'    => $data
            );
            $action_params = $this->load->controller('extension/action/' . $data['button_action'] . '/setParams', $data_params);
        } else {
            $action_params = array();
        }        
        $query = $this->db->query("SELECT MAX(sort) AS sort FROM " . DB_PREFIX . "folder_button WHERE folder_uid = '" . $this->db->escape($folder_uid) . "' ");
        $sort = (int) $query->row['sort'];
        $draft_button = serialize(array(
            'description'                   => $data['button_descriptions'],
            'picture'                       => $data['button_picture'],
            'hide_button_name'              => $data['hide_button_name'],
            'color'                         => $data['button_color'],
            'background'                    => $data['button_background'],
            'field'                         => !empty($data['button_field']) ? $data['button_field'] : "",
            'route'                         => !empty($data['button_route']) ? $data['button_route'] : array(0),
            'action'                        => !empty($data['button_action']) ? $data['button_action'] : "",
            'action_log'                    => !empty($data['action_log']) ? (int) $data['action_log'] : "0",
            'action_move_route_uid'         => !empty($data['action_move_route_uid']) ? $this->db->escape($data['action_move_route_uid']) : "0",
            'action_params'                 => serialize($action_params),
            'sort'                          => ++$sort
        ));
        $query_uid = $this->db->query("SELECT UUID() AS uid");
        $folder_button_uid = $query_uid->row['uid'];
        $sql = "INSERT INTO " . DB_PREFIX . "folder_button SET "
                . "folder_button_uid = '" . $folder_button_uid . "', "
                . "folder_uid='" . $this->db->escape($folder_uid) . "', "
                . "draft='" . (int) $draft . "', "
                . "draft_params = '" . $this->db->escape($draft_button) . "', "
                . "sort = '" . $sort . "'";
        $this->db->query($sql);
        $this->db->query("UPDATE " . DB_PREFIX . "folder SET draft = (CASE WHEN draft='3' THEN 3 ELSE 1 END) WHERE folder_uid = '" . $this->db->escape($folder_uid) . "'");        
        return $folder_button_uid;
    }    

    public function editButton($button_uid, $data, $draft=1) {  
        if($data['button_action']) {
            $data_params = array(
                'folder_button_uid' => $button_uid,
                'params'    => $data
            );
            $action_params = $this->load->controller('extension/action/' . $data['button_action'] . '/setParams', $data_params);
        } else {
            $action_params = array();
        }

        $draft_button = array(
            'description'                   => $data['button_descriptions'],
            'picture'                       => $data['button_picture'],
            'hide_button_name'              => $data['hide_button_name'],
            'color'                         => $data['button_color'],
            'background'                    => $data['button_background'],
            'field'                         => !empty($data['button_field']) ? $data['button_field'] : "",
            'route'                         => !empty($data['button_route']) ? $data['button_route'] : array(0),
            'action'                        => !empty($data['button_action']) ? $data['button_action'] : "",
            'action_log'                    => !empty($data['action_log']) ? (int) $data['action_log'] : "0",
            'action_move_route_uid'          => $this->db->escape($data['action_move_route_uid'] ?? 0),
            'action_params'                 => serialize($action_params),
        );

        $button_info = $this->getButton($button_uid);
        if (!empty($button_info['draft_params']['sort'])) {
            $draft_button['sort'] = $button_info['draft_params']['sort'];
        }
        
        $this->db->query("UPDATE " . DB_PREFIX . "folder_button SET "
                . "draft= (CASE WHEN draft='3' THEN 3 ELSE " . (int) $draft . " END), "
                . "draft_params = '" . $this->db->escape(serialize($draft_button)) . "' "
                . "WHERE folder_button_uid = '" . $this->db->escape($button_uid) . "' ");
        $this->setDraft($button_info['folder_uid']);

    }    
     
    /**
     * Обновляется таблица с матрицей делегирования кнопок при изменении делегирования кнопки в журнале
     * @param type $field_uid
     * @param type $document_uid
     * @param type $value
     */
    public function updateButtonDelegate($folder_button_uid) {
        //удаляем старое делегирование
        $this->db->query("DELETE FROM " . DB_PREFIX . "folder_button_delegate WHERE folder_button_uid = '" . $this->db->escape($folder_button_uid) . "' ");
        //получаем поля, на которые делегируется кнопка
        $query_fields = $this->db->query("SELECT * FROM " . DB_PREFIX . "field WHERE field_uid IN (SELECT field_uid FROM " . DB_PREFIX . "folder_button_field WHERE folder_button_uid = '" . $this->db->escape($folder_button_uid) . "')");
        $sqls = array();
        foreach ($query_fields->rows as $field) {
            //получаем все значения поля, которому делегируется кнопка
            $query_field_values = $this->db->query("SELECT * FROM " . DB_PREFIX . "field_value_" . $field['type'] . " WHERE field_uid = '" . $this->db->escape($field['field_uid']) . "'");
            foreach ($query_field_values->rows as $value_row) {
                if (is_array($value_row['value'])) {
                    $values = $value_row['value'];
                } else { //structure_uid могут быть перечислены через запятую
                    $values = explode(",", $value_row['value']);
                } 
               
                foreach ($values as $structure_uid) {
                    $structure_uid = trim($structure_uid);
                    if ($structure_uid && in_array("(" . $this->db->escape($folder_button_uid) . "," . $this->db->escape($value_row['document_uid']) . "," . $structure_uid . ")", $sqls) === FALSE) { //исключаем дублирование и пустые значения structure_uid
                        $sqls[] = "('" . $this->db->escape($folder_button_uid) . "','" . $this->db->escape($value_row['document_uid']) . "','" . $structure_uid . "')";
                    }
                }
            }
        }
        if ($sqls) {                   
            $this->db->query("INSERT INTO " . DB_PREFIX . "folder_button_delegate (folder_button_uid, document_uid, structure_uid) "
                    . "VALUES " . implode(",", array_unique($sqls)));                    
        }                
        
    }
    
    public function updateSorting($folder_uid) {
        $query = $this->db->query("SELECT folder_field_uid FROM " . DB_PREFIX . "folder_field WHERE folder_uid = '" . $this->db->escape($folder_uid) . "' AND (grouping_parent_uid = '0' OR grouping_parent_uid = '' OR grouping_parent_uid IS NULL) ORDER BY sort_grouping ASC ");
        $i = 1;
        foreach ($query->rows as $field) {
            $this->db->query("UPDATE " . DB_PREFIX . "folder_field SET sort_grouping = '" . $i++ . "' WHERE folder_field_uid = '" . $this->db->escape($field['folder_field_uid']) . "' ");

            $query_field = $this->db->query("SELECT folder_field_uid FROM " . DB_PREFIX . "folder_field WHERE grouping_parent_uid = '" . $this->db->escape($field['folder_field_uid']) . "' ORDER BY sort_grouping ASC ");
            foreach ($query_field->rows as $child_field) {
                $this->db->query("UPDATE " . DB_PREFIX . "folder_field SET sort_grouping = '" . $i++ . "' WHERE folder_field_uid = '" . $this->db->escape($child_field['folder_field_uid']) . "' ");                
            }
        }            
        $query = $this->db->query("SELECT folder_field_uid FROM " . DB_PREFIX . "folder_field WHERE folder_uid = '" . $this->db->escape($folder_uid) . "' ORDER BY sort_tcolumn ASC ");
        $i = 1;
        foreach ($query->rows as $field) {
            $this->db->query("UPDATE " . DB_PREFIX . "folder_field SET sort_tcolumn = '" . $i++ . "' WHERE folder_field_uid = '" . $this->db->escape($field['folder_field_uid']) . "' ");
        }            
    }
   
}