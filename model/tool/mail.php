<?php

class ModelToolMail extends Model {

    public function addMail($to, $message, $subject = 'Documentov', $from = '', $sender = 'Documentov', $smtp_hostname = '', $smtp_username = '', $smtp_password = '', $smtp_port = '') {
        /*
         * 	$mail = new Mail($this->config->get('config_mail_engine'));
          $mail->parameter = $this->config->get('config_mail_parameter');
          $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
          $mail->smtp_username = $this->config->get('config_mail_smtp_username');
          $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
          $mail->smtp_port = $this->config->get('config_mail_smtp_port');
          $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

          $mail->setTo($order_info['email']);
          $mail->setFrom($from);
          $mail->setSender(html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
          $mail->setSubject(html_entity_decode(sprintf($language->get('text_subject'), $order_info['store_name'], $order_info['order_id']), ENT_QUOTES, 'UTF-8'));
          $mail->setHtml($this->load->view('mail/order_add', $data));
          $mail->send();
         */


        if (!$to) {
            throw new \Exception('Error: E-Mail to required!');
        }


        if (!$message) {
            throw new \Exception('Error: E-Mail message required!');
        }

        $this->db->query("INSERT INTO mail SET "
                . "to = '" . $this->db->escape($to) . "', "
                . "subject = '" . $this->db->escape($subject) . "', "
                . "message = '" . $this->db->escape($message) . "', "
                . "from = '" . $this->db->escape($from) . "', "
                . "sender = '" . $this->db->escape($sender) . "', "
                . "smtp_hostname = '" . $this->db->escape($smtp_hostname) . "', "
                . "smtp_username = '" . $this->db->escape($smtp_username) . "', "
                . "smtp_password = '" . $this->db->escape($smtp_password) . "', "
                . "smtp_port = '" . $this->db->escape($smtp_port) . "' "
        );
    }

}
