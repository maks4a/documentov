<?php

class ModelToolUpdate extends Model {

    const VAR_IDS = array(
        'var_author_name' => 'var_author_name',
        'var_customer_id' => 'var_customer_id',
        'var_customer_name' => 'var_customer_name',
        'var_current_route_uid' => 'var_current_route_uid',
        'var_current_route_name' => 'var_current_route_name',
        'var_current_route_description' => 'var_current_route_description',
        'var_current_time' => 'var_current_time',
        'var_customer_uid' => 'var_customer_uid',
        'var_customer_user_uid' => 'var_customer_user_uid',
        'var_current_document_uid' => 'var_current_document_uid',
        'var_current_button_uid' => 'var_current_button_uid',
        'var_change_field_uid' => 'var_change_field_uid',
        'var_current_folder_uid' => 'var_current_folder_uid',
        'current_user_uid' => 'var_customer_uid',
        'current_document_uid' => 'var_current_document_uid',
        'current_button_uid' => 'var_current_button_uid',
        'author_id' => 'var_author_uid',
        'current_route_name' => 'var_current_route_name',
        'current_user_user_uid' => 'var_customer_user_uid',
        'change_field_uid' => 'var_change_field_uid',
        'current_date' => 'var_current_time'
    );

    public function update() {
        $versions = array();
        for ($i = 0; $i <= 9; $i++) {
            for ($k = 0; $k <= 9; $k++) {
                for ($m = 0; $m <= 9; $m++) {
                    $versions[] = $i . "." . $k . "." . $m;
                }
            }
        }
        $version_db = $this->config->get('version_db'); //версия БД, версия кода в VERSION. Нужно выполнить все обновления, начиная с версии БД
        if (!$version_db) {
            $version_db = '0.5.0';
        }
        if ($version_db != VERSION) {
            $start = false;
            foreach ($versions as $version) {
                if ($version == VERSION) {
                    break;
                }
                if ($version == $version_db) {
                    $start = true;
                }
                if ($start) {
                    $this->updateTo($version);
                }
            }
            $this->load->model('setting/setting');
            $this->model_setting_setting->editSetting('dv_system', array('version_db' => VERSION));
        }
    }

    public function updateTo($version) {
        $this->load->model('doctype/doctype');
        switch ($version) {
            case "0.5.0": //обновляем 0.5.0 до следующей версии
                //добавляем поле для скрытия названий меню
                $this->db->query("ALTER TABLE " . DB_PREFIX . "menu_item ADD hide_name INT AFTER `image`");
                break;
            case "0.5.7": //обновляем 0.5.7 до 0.5.8, в которой нужны доп столбцы в field
                //добавляем поля для определния доступа к полям в режимах редактирования и просмотра
                $qedit = $this->db->query("SELECT * FROM information_schema.columns WHERE TABLE_SCHEMA='" . DB_DATABASE . "' AND TABLE_NAME = '" . DB_PREFIX . "field' AND COLUMN_NAME = 'access_form'");
                if (!$qedit->num_rows) {
                    $this->db->query("ALTER TABLE " . DB_PREFIX . "field ADD access_form TEXT AFTER `change_field`");
                }
                $qview = $this->db->query("SELECT * FROM information_schema.columns WHERE TABLE_SCHEMA='" . DB_DATABASE . "' AND TABLE_NAME = '" . DB_PREFIX . "field' AND COLUMN_NAME = 'access_view'");
                if (!$qview->num_rows) {
                    $this->db->query("ALTER TABLE " . DB_PREFIX . "field ADD access_view TEXT AFTER `access_form`");
                }
                break;
            case "0.5.9": //обновляем 0.5.9 до 0.6.0, в которой нужны доп столбцы в field для проверки обязательности и уникальности
                $qedit = $this->db->query("SELECT * FROM information_schema.columns WHERE TABLE_SCHEMA='" . DB_DATABASE . "' AND TABLE_NAME = '" . DB_PREFIX . "field' AND COLUMN_NAME = 'required'");
                if (!$qedit->num_rows) {
                    $this->db->query("ALTER TABLE " . DB_PREFIX . "field ADD `required` TINYINT AFTER `access_view`");
                }
                $qview = $this->db->query("SELECT * FROM information_schema.columns WHERE TABLE_SCHEMA='" . DB_DATABASE . "' AND TABLE_NAME = '" . DB_PREFIX . "field' AND COLUMN_NAME = 'unique'");
                if (!$qview->num_rows) {
                    $this->db->query("ALTER TABLE " . DB_PREFIX . "field ADD `unique` TINYINT AFTER `required`");
                }
                break;
            case "0.6.1":
                //импортируем доктайп Профиль пользователя
                $conf = unserialize('a:1:{i:0;a:10:{s:7:"doctype";a:1:{i:0;a:7:{s:11:"doctype_uid";s:36:"9f91722e-5823-11e8-841d-201a06f86b88";s:13:"field_log_uid";s:0:"";s:10:"date_added";s:19:"2018-05-15 15:37:45";s:11:"date_edited";s:19:"2018-05-16 14:32:49";s:8:"user_uid";s:36:"5354e0d0-1df9-11e8-a7fb-201a06f86b88";s:5:"draft";s:1:"0";s:12:"draft_params";s:0:"";}}s:19:"doctype_description";a:2:{i:0;a:5:{s:11:"doctype_uid";s:36:"9f91722e-5823-11e8-841d-201a06f86b88";s:4:"name";s:0:"";s:17:"short_description";s:0:"";s:16:"full_description";s:0:"";s:11:"language_id";s:1:"1";}i:1;a:5:{s:11:"doctype_uid";s:36:"9f91722e-5823-11e8-841d-201a06f86b88";s:4:"name";s:39:"Профиль пользователя";s:17:"short_description";s:0:"";s:16:"full_description";s:0:"";s:11:"language_id";s:1:"2";}}s:16:"doctype_template";a:4:{i:0;a:4:{s:11:"doctype_uid";s:36:"9f91722e-5823-11e8-841d-201a06f86b88";s:8:"template";s:0:"";s:4:"type";s:4:"form";s:11:"language_id";s:1:"1";}i:1;a:4:{s:11:"doctype_uid";s:36:"9f91722e-5823-11e8-841d-201a06f86b88";s:8:"template";s:545:"&lt;h2&gt;Пользовательские настройки&lt;/h2&gt; &lt;table class=&quot;table table-bordered&quot;&gt; &lt;tbody&gt; &lt;tr&gt; &lt;td&gt;Пароль&lt;/td&gt; &lt;td&gt;{{ f_ec8a023d582511e8a8d8201a06f86b88 }}&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;td&gt;Стартовая страница&lt;/td&gt; &lt;td&gt;{{ f_1f458fe7582611e8a8d8201a06f86b88 }}&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;td&gt;Язык&lt;/td&gt; &lt;td&gt;{{ f_c4443c3b58db11e8a8d8201a06f86b88 }}&lt;/td&gt; &lt;/tr&gt; &lt;/tbody&gt; &lt;/table&gt;";s:4:"type";s:4:"form";s:11:"language_id";s:1:"2";}i:2;a:4:{s:11:"doctype_uid";s:36:"9f91722e-5823-11e8-841d-201a06f86b88";s:8:"template";s:0:"";s:4:"type";s:4:"view";s:11:"language_id";s:1:"1";}i:3;a:4:{s:11:"doctype_uid";s:36:"9f91722e-5823-11e8-841d-201a06f86b88";s:8:"template";s:29:"&lt;p&gt;&lt;br&gt;&lt;/p&gt;";s:4:"type";s:4:"view";s:11:"language_id";s:1:"2";}}s:5:"field";a:5:{i:0;a:14:{s:9:"field_uid";s:36:"1f458fe7-5826-11e8-a8d8-201a06f86b88";s:4:"name";s:35:"Стартовая страница";s:11:"doctype_uid";s:36:"9f91722e-5823-11e8-841d-201a06f86b88";s:4:"type";s:6:"string";s:7:"setting";s:1:"0";s:12:"change_field";s:1:"0";s:11:"access_form";s:0:"";s:11:"access_view";s:0:"";s:8:"required";s:1:"0";s:6:"unique";s:1:"0";s:6:"params";s:244:"a:9:{s:4:"mask";s:0:"";s:10:"field_name";s:35:"Стартовая страница";s:7:"setting";s:1:"0";s:12:"change_field";s:1:"0";s:8:"required";s:1:"0";s:6:"unique";s:1:"0";s:11:"access_form";a:0:{}s:11:"access_view";a:0:{}s:4:"sort";i:2;}";s:4:"sort";s:1:"2";s:5:"draft";s:1:"0";s:12:"draft_params";s:0:"";}i:1;a:14:{s:9:"field_uid";s:36:"635eb9f2-5826-11e8-a8d8-201a06f86b88";s:4:"name";s:61:"Пользователь: Стартовая страница";s:11:"doctype_uid";s:36:"9f91722e-5823-11e8-841d-201a06f86b88";s:4:"type";s:4:"link";s:7:"setting";s:1:"0";s:12:"change_field";s:1:"0";s:11:"access_form";s:0:"";s:11:"access_view";s:0:"";s:8:"required";s:1:"0";s:6:"unique";s:1:"0";s:6:"params";s:457:"a:13:{s:11:"doctype_uid";s:36:"51f800b5-1df9-11e8-a7fb-201a06f86b88";s:17:"doctype_field_uid";s:36:"50dd5916-37fc-11e8-a971-52540028bc1e";s:4:"list";s:1:"0";s:12:"multi_select";s:1:"0";s:9:"delimiter";s:2:", ";s:10:"field_name";s:61:"Пользователь: Стартовая страница";s:7:"setting";s:1:"0";s:12:"change_field";s:1:"0";s:8:"required";s:1:"0";s:6:"unique";s:1:"0";s:11:"access_form";a:0:{}s:11:"access_view";a:0:{}s:4:"sort";i:3;}";s:4:"sort";s:1:"3";s:5:"draft";s:1:"0";s:12:"draft_params";s:0:"";}i:2;a:14:{s:9:"field_uid";s:36:"c4443c3b-58db-11e8-a8d8-201a06f86b88";s:4:"name";s:8:"Язык";s:11:"doctype_uid";s:36:"9f91722e-5823-11e8-841d-201a06f86b88";s:4:"type";s:4:"list";s:7:"setting";s:1:"0";s:12:"change_field";s:1:"0";s:11:"access_form";s:0:"";s:11:"access_view";s:0:"";s:8:"required";s:1:"0";s:6:"unique";s:1:"0";s:6:"params";s:395:"a:11:{s:6:"values";a:2:{i:1;a:2:{s:5:"value";s:1:"1";s:5:"title";s:7:"English";}i:2;a:2:{s:5:"value";s:1:"2";s:5:"title";s:14:"Русский";}}s:12:"multi_select";s:1:"0";s:13:"visualization";s:1:"0";s:10:"field_name";s:8:"Язык";s:7:"setting";s:1:"0";s:12:"change_field";s:1:"0";s:8:"required";s:1:"0";s:6:"unique";s:1:"0";s:11:"access_form";a:0:{}s:11:"access_view";a:0:{}s:4:"sort";i:4;}";s:4:"sort";s:1:"4";s:5:"draft";s:1:"0";s:12:"draft_params";s:0:"";}i:3;a:14:{s:9:"field_uid";s:36:"d269d126-58db-11e8-a8d8-201a06f86b88";s:4:"name";s:34:"Пользователь: Язык";s:11:"doctype_uid";s:36:"9f91722e-5823-11e8-841d-201a06f86b88";s:4:"type";s:4:"link";s:7:"setting";s:1:"0";s:12:"change_field";s:1:"0";s:11:"access_form";s:0:"";s:11:"access_view";s:0:"";s:8:"required";s:1:"0";s:6:"unique";s:1:"0";s:6:"params";s:430:"a:13:{s:11:"doctype_uid";s:36:"51f800b5-1df9-11e8-a7fb-201a06f86b88";s:17:"doctype_field_uid";s:36:"d0e9bc2a-30b7-11e8-9fbe-201a06f86b88";s:4:"list";s:1:"0";s:12:"multi_select";s:1:"0";s:9:"delimiter";s:2:", ";s:10:"field_name";s:34:"Пользователь: Язык";s:7:"setting";s:1:"0";s:12:"change_field";s:1:"0";s:8:"required";s:1:"0";s:6:"unique";s:1:"0";s:11:"access_form";a:0:{}s:11:"access_view";a:0:{}s:4:"sort";i:5;}";s:4:"sort";s:1:"5";s:5:"draft";s:1:"0";s:12:"draft_params";s:0:"";}i:4;a:14:{s:9:"field_uid";s:36:"ec8a023d-5825-11e8-a8d8-201a06f86b88";s:4:"name";s:12:"Пароль";s:11:"doctype_uid";s:36:"9f91722e-5823-11e8-841d-201a06f86b88";s:4:"type";s:6:"hidden";s:7:"setting";s:1:"0";s:12:"change_field";s:1:"0";s:11:"access_form";s:0:"";s:11:"access_view";s:0:"";s:8:"required";s:1:"0";s:6:"unique";s:1:"0";s:6:"params";s:227:"a:9:{s:9:"type_hash";s:1:"1";s:10:"field_name";s:12:"Пароль";s:7:"setting";s:1:"0";s:12:"change_field";s:1:"0";s:8:"required";s:1:"0";s:6:"unique";s:1:"0";s:11:"access_form";a:0:{}s:11:"access_view";a:0:{}s:4:"sort";i:1;}";s:4:"sort";s:1:"1";s:5:"draft";s:1:"0";s:12:"draft_params";s:0:"";}}s:5:"route";a:1:{i:0;a:5:{s:9:"route_uid";s:36:"392985cd-5826-11e8-a8d8-201a06f86b88";s:11:"doctype_uid";s:36:"9f91722e-5823-11e8-841d-201a06f86b88";s:4:"sort";s:1:"1";s:5:"draft";s:1:"0";s:12:"draft_params";s:0:"";}}s:17:"route_description";a:2:{i:0;a:4:{s:9:"route_uid";s:36:"392985cd-5826-11e8-a8d8-201a06f86b88";s:4:"name";s:0:"";s:11:"description";s:0:"";s:11:"language_id";s:1:"1";}i:1;a:4:{s:9:"route_uid";s:36:"392985cd-5826-11e8-a8d8-201a06f86b88";s:4:"name";s:16:"Создание";s:11:"description";s:0:"";s:11:"language_id";s:1:"2";}}s:12:"route_action";a:8:{i:0;a:9:{s:16:"route_action_uid";s:36:"1a967585-58dc-11e8-a8d8-201a06f86b88";s:9:"route_uid";s:36:"392985cd-5826-11e8-a8d8-201a06f86b88";s:7:"context";s:4:"jump";s:6:"action";s:6:"record";s:10:"action_log";s:1:"0";s:6:"params";s:482:"a:11:{s:17:"target_field_type";s:1:"0";s:24:"target_doclink_field_uid";s:36:"d269d126-58db-11e8-a8d8-201a06f86b88";s:16:"target_field_uid";s:36:"d0e9bc2a-30b7-11e8-9fbe-201a06f86b88";s:19:"target_field_setter";s:0:"";s:11:"source_type";s:8:"document";s:24:"source_doclink_field_uid";s:1:"0";s:15:"source_variable";s:9:"author_id";s:16:"source_field_uid";s:36:"c4443c3b-58db-11e8-a8d8-201a06f86b88";s:19:"source_field_getter";s:0:"";s:6:"params";a:1:{s:4:"sort";i:3;}s:4:"sort";i:2;}";s:5:"draft";s:1:"0";s:12:"draft_params";s:0:"";s:4:"sort";s:1:"2";}i:1;a:9:{s:16:"route_action_uid";s:36:"45bb1192-582f-11e8-a8d8-201a06f86b88";s:9:"route_uid";s:36:"392985cd-5826-11e8-a8d8-201a06f86b88";s:7:"context";s:4:"jump";s:6:"action";s:6:"record";s:10:"action_log";s:1:"0";s:6:"params";s:467:"a:10:{s:17:"target_field_type";s:1:"0";s:24:"target_doclink_field_uid";s:36:"635eb9f2-5826-11e8-a8d8-201a06f86b88";s:16:"target_field_uid";s:36:"50dd5916-37fc-11e8-a971-52540028bc1e";s:19:"target_field_setter";s:0:"";s:11:"source_type";s:8:"document";s:24:"source_doclink_field_uid";s:1:"0";s:15:"source_variable";s:9:"author_id";s:16:"source_field_uid";s:36:"1f458fe7-5826-11e8-a8d8-201a06f86b88";s:19:"source_field_getter";s:0:"";s:6:"params";a:1:{s:4:"sort";i:1;}}";s:5:"draft";s:1:"0";s:12:"draft_params";s:0:"";s:4:"sort";s:1:"1";}i:2;a:9:{s:16:"route_action_uid";s:36:"464e5cb0-58dc-11e8-a8d8-201a06f86b88";s:9:"route_uid";s:36:"392985cd-5826-11e8-a8d8-201a06f86b88";s:7:"context";s:4:"jump";s:6:"action";s:6:"record";s:10:"action_log";s:1:"0";s:6:"params";s:482:"a:11:{s:17:"target_field_type";s:1:"0";s:24:"target_doclink_field_uid";s:36:"d269d126-58db-11e8-a8d8-201a06f86b88";s:16:"target_field_uid";s:36:"54fde735-1df9-11e8-a7fb-201a06f86b88";s:19:"target_field_setter";s:0:"";s:11:"source_type";s:8:"document";s:24:"source_doclink_field_uid";s:1:"0";s:15:"source_variable";s:9:"author_id";s:16:"source_field_uid";s:36:"ec8a023d-5825-11e8-a8d8-201a06f86b88";s:19:"source_field_getter";s:0:"";s:6:"params";a:1:{s:4:"sort";i:4;}s:4:"sort";i:3;}";s:5:"draft";s:1:"0";s:12:"draft_params";s:0:"";s:4:"sort";s:1:"3";}i:3;a:9:{s:16:"route_action_uid";s:36:"605e8c34-58dc-11e8-a8d8-201a06f86b88";s:9:"route_uid";s:36:"392985cd-5826-11e8-a8d8-201a06f86b88";s:7:"context";s:6:"create";s:6:"action";s:6:"record";s:10:"action_log";s:1:"0";s:6:"params";s:406:"a:10:{s:17:"target_field_type";s:1:"0";s:24:"target_doclink_field_uid";s:1:"0";s:16:"target_field_uid";s:36:"d269d126-58db-11e8-a8d8-201a06f86b88";s:19:"target_field_setter";s:0:"";s:11:"source_type";s:8:"variable";s:24:"source_doclink_field_uid";s:0:"";s:15:"source_variable";s:21:"current_user_user_uid";s:16:"source_field_uid";s:0:"";s:19:"source_field_getter";s:0:"";s:6:"params";a:1:{s:4:"sort";i:3;}}";s:5:"draft";s:1:"0";s:12:"draft_params";s:0:"";s:4:"sort";s:1:"3";}i:4;a:9:{s:16:"route_action_uid";s:36:"704028b1-5826-11e8-a8d8-201a06f86b88";s:9:"route_uid";s:36:"392985cd-5826-11e8-a8d8-201a06f86b88";s:7:"context";s:6:"create";s:6:"action";s:6:"record";s:10:"action_log";s:1:"0";s:6:"params";s:371:"a:9:{s:17:"target_field_type";s:1:"0";s:24:"target_doclink_field_uid";s:1:"0";s:16:"target_field_uid";s:36:"635eb9f2-5826-11e8-a8d8-201a06f86b88";s:19:"target_field_setter";s:0:"";s:11:"source_type";s:8:"variable";s:24:"source_doclink_field_uid";s:0:"";s:15:"source_variable";s:21:"current_user_user_uid";s:16:"source_field_uid";s:0:"";s:19:"source_field_getter";s:0:"";}";s:5:"draft";s:1:"0";s:12:"draft_params";s:0:"";s:4:"sort";s:1:"1";}i:5;a:9:{s:16:"route_action_uid";s:36:"8173f05d-5826-11e8-a8d8-201a06f86b88";s:9:"route_uid";s:36:"392985cd-5826-11e8-a8d8-201a06f86b88";s:7:"context";s:6:"create";s:6:"action";s:6:"record";s:10:"action_log";s:1:"0";s:6:"params";s:458:"a:10:{s:17:"target_field_type";s:1:"0";s:24:"target_doclink_field_uid";s:1:"0";s:16:"target_field_uid";s:36:"1f458fe7-5826-11e8-a8d8-201a06f86b88";s:19:"target_field_setter";s:0:"";s:11:"source_type";s:8:"document";s:24:"source_doclink_field_uid";s:1:"0";s:15:"source_variable";s:9:"author_id";s:16:"source_field_uid";s:36:"635eb9f2-5826-11e8-a8d8-201a06f86b88";s:19:"source_field_getter";s:26:"get_display_value_not_link";s:6:"params";a:1:{s:4:"sort";i:2;}}";s:5:"draft";s:1:"0";s:12:"draft_params";s:0:"";s:4:"sort";s:1:"2";}i:6;a:9:{s:16:"route_action_uid";s:36:"8316c679-582f-11e8-a8d8-201a06f86b88";s:9:"route_uid";s:36:"392985cd-5826-11e8-a8d8-201a06f86b88";s:7:"context";s:4:"jump";s:6:"action";s:6:"remove";s:10:"action_log";s:1:"0";s:6:"params";s:89:"a:3:{s:18:"field_document_uid";s:1:"0";s:6:"params";a:1:{s:4:"sort";i:2;}s:4:"sort";i:4;}";s:5:"draft";s:1:"0";s:12:"draft_params";s:0:"";s:4:"sort";s:1:"4";}i:7;a:9:{s:16:"route_action_uid";s:36:"896fa29a-58dc-11e8-a8d8-201a06f86b88";s:9:"route_uid";s:36:"392985cd-5826-11e8-a8d8-201a06f86b88";s:7:"context";s:6:"create";s:6:"action";s:6:"record";s:10:"action_log";s:1:"0";s:6:"params";s:436:"a:9:{s:17:"target_field_type";s:1:"0";s:24:"target_doclink_field_uid";s:1:"0";s:16:"target_field_uid";s:36:"c4443c3b-58db-11e8-a8d8-201a06f86b88";s:19:"target_field_setter";s:12:"set_by_title";s:11:"source_type";s:8:"document";s:24:"source_doclink_field_uid";s:1:"0";s:15:"source_variable";s:9:"author_id";s:16:"source_field_uid";s:36:"d269d126-58db-11e8-a8d8-201a06f86b88";s:19:"source_field_getter";s:26:"get_display_value_not_link";}";s:5:"draft";s:1:"0";s:12:"draft_params";s:0:"";s:4:"sort";s:1:"4";}}s:12:"route_button";a:0:{}s:24:"route_button_description";a:0:{}s:18:"route_button_field";a:0:{}}}');
//                print_r($conf);exit;
                $this->load->model('extension/service/export');
                $this->model_extension_service_export->addConfiguration($conf);
                $this->db->query("UPDATE " . DB_PREFIX . "menu_item SET `action`='link', action_value='/index.php?route=document/document&doctype_uid=9f91722e-5823-11e8-841d-201a06f86b88' WHERE `action_value`='5ea67e03-1df9-11e8-a7fb-201a06f86b88'");
                break;
            case "0.6.2":
                //добавляем поле Последняя открытая страница в справочник Пользователи
                $this->db->query("INSERT INTO " . DB_PREFIX . "field (`field_uid`, `name`, `doctype_uid`, `type`, `setting`, `change_field`, `access_form`, `access_view`, `required`, `unique`, `params`, `sort`, `draft`, `draft_params`) VALUES ('425fd7d7-59be-11e8-958b-201a06f86b88', 'Последняя страница', '51f800b5-1df9-11e8-a7fb-201a06f86b88', 'string', '0', '0', '', '', '0', '0', 'a:9:{s:4:\"mask\";s:0:\"\";s:10:\"field_name\";s:35:\"Последняя страница\";s:7:\"setting\";s:1:\"0\";s:12:\"change_field\";s:1:\"0\";s:8:\"required\";s:1:\"0\";s:6:\"unique\";s:1:\"0\";s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:4:\"sort\";i:12;}', '12', '0', '')");
                //добавляем настройки для определения поля Последняя открытая страница
                $this->db->query("INSERT INTO " . DB_PREFIX . "setting (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES (NULL, '0', 'dv_system', 'user_field_lastpage_id', '425fd7d7-59be-11e8-958b-201a06f86b88', '0')");
                $this->db->query("INSERT INTO " . DB_PREFIX . "setting (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES (NULL, '0', 'dv_system', 'user_field_lastpage_type', 'string', '0')");
                $this->db->query("UPDATE " . DB_PREFIX . "setting SET code='dv_system' WHERE `key` IN ('user_field_login_type','structure_id','structure_field_name_id','structure_field_user_id','user_field_password_id','structure_type','structure_field_parent_id','user_field_password_type','structure_field_user_type','structure_field_name_type','user_field_attempt_login_type','user_field_attempt_login_id','user_field_login_id','user_field_language_type','user_field_language_id','user_field_email_type','user_field_email_id','user_field_admin_type','user_field_admin_id','user_field_startpage_type','user_field_startpage_id')");
                break;
            case "0.6.3":
                //добавляем настройки для определения полей Должность и Замещающий Структуры
                $this->db->query("INSERT INTO " . DB_PREFIX . "setting (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES (NULL, '0', 'dv_zsystem', 'structure_field_position_id', '54fded41-1df9-11e8-a7fb-201a06f86b88', '0')");
                $this->db->query("INSERT INTO " . DB_PREFIX . "setting (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES (NULL, '0', 'dv_zsystem', 'structure_field_position_type', 'string', '0')");
                $this->db->query("INSERT INTO " . DB_PREFIX . "setting (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES (NULL, '0', 'dv_system', 'structure_field_deputy_id', 'b21c5bf2-5cd0-11e8-b108-201a06f86b88', '0')");
                $this->db->query("INSERT INTO " . DB_PREFIX . "setting (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES (NULL, '0', 'dv_system', 'structure_field_deputy_type', 'link', '0')");
                $this->db->query("INSERT INTO " . DB_PREFIX . "field (`field_uid`, `name`, `doctype_uid`, `type`, `setting`, `change_field`, `access_form`, `access_view`, `required`, `unique`, `params`, `sort`, `draft`, `draft_params`) VALUES ('b21c5bf2-5cd0-11e8-b108-201a06f86b88', 'Замещающий', '51f803b2-1df9-11e8-a7fb-201a06f86b88', 'link', '0', '0', '', '', '0', '0', 'a:12:{s:11:\"doctype_uid\";s:36:\"51f803b2-1df9-11e8-a7fb-201a06f86b88\";s:17:\"doctype_field_uid\";s:36:\"54fde8ea-1df9-11e8-a7fb-201a06f86b88\";s:4:\"list\";s:1:\"0\";s:12:\"multi_select\";s:1:\"0\";s:9:\"delimiter\";s:2:\", \";s:10:\"field_name\";s:20:\"Замещающий\";s:12:\"change_field\";i:0;s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:8:\"required\";i:0;s:6:\"unique\";i:0;s:4:\"sort\";i:6;}', '6', '0', '')");
                $this->resortFields('51f803b2-1df9-11e8-a7fb-201a06f86b88');
                break;
            case "0.6.7":
                //для кнопок доктайпа добавляется атрибут, определяющий что показывать после выполнения кнопки - документ или журнал
                $this->db->query("ALTER TABLE " . DB_PREFIX . "route_button ADD `show_after_execute` TINYINT NOT NULL AFTER `action_params`;");
                break;
            case "0.7.1":
                //добавляем сортировку по умолчанию в журнале
                $this->db->query("ALTER TABLE " . DB_PREFIX . "folder_field ADD default_sort TINYINT NOT NULL AFTER `sort_tcolumn`;");
                break;
            case "0.7.2":
                //добавляем настройку для анонимного входа
                $this->db->query("INSERT INTO " . DB_PREFIX . "setting (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES (NULL, '0', 'dv_zsystem', 'anonymous_user_id', '', '0')");
                break;
            case "0.7.3":
                //добавляем настройку для глубины рекурсии цикла маршрута документа
                $this->db->query("INSERT INTO " . DB_PREFIX . "setting (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES (NULL, '0', 'dv_zsystem', 'recursion_depth', '100', '0')");
                $this->db->query("UPDATE " . DB_PREFIX . "setting SET code='dv_system' WHERE `key` IN ('structure_field_deputy_id','structure_field_position_id','structure_field_position_type','structure_field_deputy_type')");
                break;
            case "0.7.4":
                //добавляем дополнительные параметры в журнал
                $this->db->query("ALTER TABLE " . DB_PREFIX . "folder ADD `additional_params` TEXT NOT NULL AFTER `user_uid`;");
                break;
            case "0.7.5":
                //добавляем настройку стартовой страницы по умолчанию
                $this->db->query("INSERT INTO " . DB_PREFIX . "setting (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES (NULL, '0', 'dv_zsystem', 'default_start_page', '', '0')");
                break;
            case "0.7.7":
                //добавляем тип в журнал
                $this->db->query("ALTER TABLE " . DB_PREFIX . "folder ADD `type` VARCHAR(256) NOT NULL AFTER `folder_uid`;");
                break;
            case "0.7.9":
                //добавляем поле статуса в действия
                $this->db->query("ALTER TABLE " . DB_PREFIX . "route_action ADD `status` TINYINT NOT NULL AFTER `sort`;");
                $this->db->query("UPDATE " . DB_PREFIX . "route_action SET status = 1 ");
                break;
            case "0.8.0":
                //удаляем лишние настройки
                $this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE `code` NOT LIKE '%dv_%' AND `key` != 'config_theme' AND `key` != 'theme_default_status' AND `key` != 'theme_default_directory' AND `key` NOT LIKE 'config_error_%' AND `key` != 'config_icon' AND `key` != 'config_compression' AND `key` != 'config_logo' AND `key` != 'config_language' AND `key` != 'config_name' ");
                $this->db->query("UPDATE " . DB_PREFIX . "setting SET `value` = 'Documentov' WHERE `key` = 'config_name' ");
                //добавляем поле настройку поля статуса пользователя
                $this->addSetting('dv_system', 'user_field_status_type', 'list');
                $this->addSetting('dv_system', 'user_field_status_id', '54fde7d4-1df9-11e8-a7fb-201a06f86b88');

                //добавляем поле настройку поля последней активности пользователя
                $this->addSetting('dv_system', 'user_field_lastactivity_type', 'datetime');
                $this->addSetting('dv_system', 'user_field_lastactivity_id', '0a009bb0-99f9-11e8-8da9-485ab6e1c06f');

                //добавляем в справочник Пользователи поле последней активности пользователя
                $query = $this->db->query("SELECT field_uid FROM " . DB_PREFIX . "field WHERE field_uid = '0a009bb0-99f9-11e8-8da9-485ab6e1c06f' ");
                if (!$query->num_rows) {
                    $this->db->query("INSERT INTO `field` (`field_uid`, `name`, `doctype_uid`, `type`, `setting`, `change_field`, `access_form`, `access_view`, `required`, `unique`, `params`, `sort`, `draft`, `draft_params`) VALUES ('0a009bb0-99f9-11e8-8da9-485ab6e1c06f', 'Последняя активность', '51f800b5-1df9-11e8-a7fb-201a06f86b88', 'datetime', '0', '0', '', '', '0', '0', 'a:9:{s:6:\"format\";s:11:\"d.m.Y H:i:s\";s:10:\"field_name\";s:39:\"Последняя активность\";s:7:\"setting\";s:1:\"0\";s:12:\"change_field\";s:1:\"0\";s:8:\"required\";s:1:\"0\";s:6:\"unique\";s:1:\"0\";s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:4:\"sort\";i:14;}', '14', '0', '')");
                }
                //меняем типа поля статуса пользователя и очищаем старые данные
                $this->db->query("UPDATE " . DB_PREFIX . "field SET type='list', setting=0, change_field=0, params='a:12:{s:6:\"values\";a:2:{i:1;a:2:{s:5:\"value\";s:1:\"0\";s:5:\"title\";s:14:\"Активен\";}i:2;a:2:{s:5:\"value\";s:1:\"1\";s:5:\"title\";s:20:\"Блокирован\";}}s:13:\"default_value\";s:0:\"\";s:12:\"multi_select\";s:1:\"0\";s:13:\"visualization\";s:1:\"0\";s:10:\"field_name\";s:10:\"LISTUPDATE\";s:7:\"setting\";s:1:\"0\";s:12:\"change_field\";s:1:\"0\";s:8:\"required\";s:1:\"0\";s:6:\"unique\";s:1:\"0\";s:11:\"access_form\";a:0:{}s:11:\"access_view\";a:0:{}s:4:\"sort\";i:15;}', sort=99, draft=0, draft_params='' WHERE field_uid='54fde7d4-1df9-11e8-a7fb-201a06f86b88' ");
                $this->resortFields("51f800b5-1df9-11e8-a7fb-201a06f86b88");
                //очищаем статус
                $this->db->query("DELETE FROM " . DB_PREFIX . "field_value_string WHERE field_uid='54fde7d4-1df9-11e8-a7fb-201a06f86b88' ");
                //меняем тип поля value таблицы файлов
                $query = $this->db->query("SELECT * FROM information_schema.statistics WHERE table_schema = '" . DB_DATABASE . "' AND table_name = '" . DB_PREFIX . "field_value_file' AND column_name = 'value' ");
                if ($query->num_rows) {
                    $this->db->query("ALTER TABLE " . DB_PREFIX . "field_value_file DROP INDEX `value`");
                }

                $this->db->query("ALTER TABLE " . DB_PREFIX . "field_value_file CHANGE `value` `value` TEXT");
                $this->db->query("ALTER TABLE " . DB_PREFIX . "field_value_file ADD INDEX(`value`(256));");
                break;
            case "0.8.1":
                $this->db->query("ALTER TABLE " . DB_PREFIX . "field_value_link ADD INDEX( `display_value`)");
                $this->db->query("ALTER TABLE " . DB_PREFIX . "field_value_string CHANGE `display_value` `display_value` VARCHAR(256) CHARACTER SET utf8 COLLATE utf8_general_ci");
                break;
            case "0.8.3":
                //поля для дополнительных шаблонов
                if (!$this->isTableColumn("doctype_template", "sort")) {
                    $this->db->query("ALTER TABLE " . DB_PREFIX . "doctype_template ADD `sort` INT NOT NULL AFTER `language_id`;");
                    $this->db->query("ALTER TABLE " . DB_PREFIX . "doctype_template DROP PRIMARY KEY, ADD PRIMARY KEY (`doctype_uid`, `type`, `language_id`, `sort`) USING BTREE;");
                }
                if (!$this->isTableColumn("doctype", "params")) {
                    $this->db->query("ALTER TABLE " . DB_PREFIX . "doctype ADD `params` TEXT NOT NULL AFTER `user_uid`;");
                }
                //настройка поля последнего IP пользователя
                $this->addSetting('dv_system', 'user_field_lastip_type', 'string');
                $this->addSetting('dv_system', 'user_field_lastip_id', '52a0e563-c2c6-11e8-b6aa-485ab6e1c06f');
                //добавляем в справочник Пользователи поле для IP
                $this->addField("52a0e563-c2c6-11e8-b6aa-485ab6e1c06f", "Последний IP", "51f800b5-1df9-11e8-a7fb-201a06f86b88", "string", 'a:9:{s:4:"mask";s:0:"";s:10:"field_name";s:46:"Последний IP пользователя";s:7:"setting";s:1:"0";s:12:"change_field";s:1:"0";s:8:"required";s:1:"0";s:6:"unique";s:1:"0";s:11:"access_form";a:0:{}s:11:"access_view";a:0:{}s:4:"sort";i:5;}', 99);
                $this->resortFields("51f800b5-1df9-11e8-a7fb-201a06f86b88");
                break;
            case "0.8.4":
                //debugger
                if (!$this->isTableColumn("debugger_log")) {
                    $this->db->query("CREATE TABLE " . DB_PREFIX . "debugger_log ( `log_id` BIGINT NOT NULL AUTO_INCREMENT , `date` DATETIME NOT NULL , `user_uid` VARCHAR(36) NOT NULL, `doc_uid` VARCHAR(36) NOT NULL , `type` VARCHAR(16) NOT NULL , `module` VARCHAR(128) NOT NULL , `object_uid` VARCHAR(36) NOT NULL , `value` MEDIUMTEXT NOT NULL , PRIMARY KEY (`log_id`)) ENGINE = InnoDB CHARSET=utf8 COLLATE utf8_general_ci;");
                }
                //рефакторинг файлового поля
                if ($this->isTableColumn("field_value_file") && !$this->isTableColumn("field_value_file_relation")) {
                    $this->db->query("CREATE TABLE " . DB_PREFIX . "field_value_file_relation ( `file_uid` VARCHAR(36) NOT NULL , `field_uid` VARCHAR(36) NOT NULL , `document_uid` VARCHAR(36) NOT NULL , PRIMARY KEY (`file_uid`, `field_uid`, `document_uid`)) ENGINE = InnoDB  CHARSET=utf8 COLLATE utf8_general_ci;");
                    $query_files = $this->db->query("SELECT * FROM " . DB_PREFIX . "field_value_file");
                    foreach ($query_files->rows as $file) {
                        $file_rows = array();
                        foreach (explode(",", $file['value']) as $file_uid) {
                            if (strlen(trim($file_uid)) == 36) {
                                $file_rows[] = "('" . trim($file_uid) . "','" . $file['field_uid'] . "','" . $file['document_uid'] . "')";
                            }
                        }
                        if ($file_rows) {
                            $this->db->query("INSERT INTO " . DB_PREFIX . "field_value_file_relation (file_uid,field_uid,document_uid) VALUES " . implode(", ", array_unique($file_rows)));
                        }
                    }
                }
                if ($this->isTableColumn("field_value_file_list", "document_uid")) {
                    $this->db->query("ALTER TABLE " . DB_PREFIX . "field_value_file_list DROP `document_uid`");
                }
                break;

            case "0.8.5" :
                $this->db->query("ALTER TABLE " . DB_PREFIX . "field_value_file_relation CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;");
                $this->db->query("ALTER TABLE " . DB_PREFIX . "field_value_file_relation DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");                    
                $this->db->query("ALTER TABLE " . DB_PREFIX . "debugger_log CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;");
                $this->db->query("ALTER TABLE " . DB_PREFIX . "debugger_log DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");                    
                
                $this->load->model('doctype/doctype');
                //route_actions
                //обрабатываем действия "условие" в маршруте
                $conditions = $this->db->query("SELECT * FROM " . DB_PREFIX . "route_action WHERE action='condition'");
                foreach ($conditions->rows as $row) {
                    $route_action_uid = $row['route_action_uid'];
                    $action = array('params' => $row['params']);
                    $this->update_action_condition($action);
                    //print_r(unserialize($action['params']));
                    $sql = "UPDATE " . DB_PREFIX . "route_action SET params='" . $action['params'] . "' WHERE route_action_uid='" . $this->db->escape($route_action_uid) . "' ";
                    $this->db->query($sql);
                }

                //обрабатываем действия "запись" в маршруте
                $records = $this->db->query("SELECT * FROM " . DB_PREFIX . "route_action WHERE action='record'");
                foreach ($records->rows as $row) {
                    $route_action_uid = $row['route_action_uid'];
                    $action = array('params' => $row['params']);
                    //print_r(unserialize($action['params']));
                    $this->update_action_record($action, true);
                    $sql = "UPDATE " . DB_PREFIX . "route_action SET params='" . $action['params'] . "' WHERE route_action_uid='" . $this->db->escape($route_action_uid) . "' ";
                    $this->db->query($sql);
                }

                //обрабатываем действия "запись" в кнопках
                $records = $this->db->query("SELECT * FROM " . DB_PREFIX . "route_button WHERE action='record'");
                foreach ($records->rows as $row) {
                    $route_button_uid = $row['route_button_uid'];
                    $action = array('params' => $row['action_params']);
                    $this->update_action_record($action, true);
                    //print_r(unserialize($action['params']));
                    $sql = "UPDATE " . DB_PREFIX . "route_button SET action_params='" . $action['params'] . "' WHERE route_button_uid='" . $this->db->escape($route_button_uid) . "' ";
                    $this->db->query($sql);
                }

                //обрабатываем действия "запись" в журналах
                $records = $this->db->query("SELECT * FROM " . DB_PREFIX . "folder_button WHERE action='record'");
                foreach ($records->rows as $row) {
                    $folder_button_uid = $row['folder_button_uid'];
                    $action = array('params' => $row['action_params']);
                    $this->update_action_record($action, true);
                    //print_r(unserialize($action['params']));
                    $sql = "UPDATE " . DB_PREFIX . "folder_button SET action_params='" . $action['params'] . "' WHERE folder_button_uid='" . $this->db->escape($folder_button_uid) . "' ";
                    $this->db->query($sql);
                }
                //exit;
                break;
            case "0.8.6":
                if ($this->isTableColumn("field_value_currency")) {
                    $this->db->query("ALTER TABLE " . DB_PREFIX . "field_value_currency CHANGE `value` `value` DECIMAL(21,6) NULL DEFAULT NULL;");
                }
                if ($this->isTableColumn("field_value_tabledoc")) {
                    $this->db->query("ALTER TABLE " . DB_PREFIX . "field_value_tabledoc CHANGE `value` `value` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;");
                }
                if (!$this->isTableColumn("folder_field","tcolumn_hidden")) {
                    $this->db->query("ALTER TABLE " . DB_PREFIX . "folder_field ADD `tcolumn_hidden` TINYINT(4) NOT NULL AFTER `tcolumn`, ADD INDEX (`tcolumn_hidden`);");
                }
                $this->addSetting("dv_zsystem", "technical_break", "");
                break;
            case "0.8.8":
                $this->db->query("DELETE FROM event WHERE code = 'mail_forgotten' OR code = 'fild_link_edit_value' ");
                break;
            case "0.8.9":
                if (!$this->isTableColumn("route_action","description")) {
                    $this->db->query("ALTER TABLE `route_action` ADD `description` VARCHAR(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `draft_params`;");
                }
                break;
            case "0.9.1":
                if (!$this->isTableColumn("field_value_datetime","value0")) {
                    $this->db->query("ALTER TABLE `field_value_datetime` ADD `value0` DATETIME NOT NULL AFTER `value`;");
                }
                $query = $this->db->query("SELECT * FROM setting WHERE `code`='dv_zsystem' AND `key` = 'daemon_started' ");
                if ($query->num_rows) {
                    $this->db->query("UPDATE setting SET `code` = 'dv_system' WHERE setting_id = '" . $query->row['setting_id'] . "' ");
                    $this->db->query("DELETE FROM setting WHERE `code`='dv_zsystem' AND `key` = 'daemon_started' ");
                }
                break;
            case "0.9.2":
                if (!$this->isTableColumn("folder_field","tcolumn_total")) {
                    $this->db->query("ALTER TABLE " . DB_PREFIX . "folder_field ADD `tcolumn_total` TINYINT(4) NOT NULL AFTER `tcolumn_name`;");
                }
                $this->db->query("UPDATE`setting` SET `value`='fv.png' WHERE `key`='config_icon'");
                $q = $this->db->query("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '" . DB_DATABASE ."' AND ENGINE = 'InnoDB'");
                foreach ($q->rows as $row) {
                    $this->db->query("ALTER TABLE " . $row['TABLE_NAME'] . " ENGINE=MyISAM");
                }
                break;
            case "0.9.3":
                if (!($this->isTableColumn('route_button','hide_button_name'))) {
                    $this->db->query("ALTER TABLE " . DB_PREFIX . "route_button ADD `hide_button_name` SMALLINT NOT NULL AFTER `picture`;");
                }
                if (!($this->isTableColumn('folder_button','hide_button_name'))) {
                    $this->db->query("ALTER TABLE " . DB_PREFIX . "folder_button ADD `hide_button_name` SMALLINT NOT NULL AFTER `picture`;");
                }
                break;
            case "0.9.4":
                $q = $this->db->query("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '" . DB_DATABASE ."' AND ENGINE = 'InnoDB'");
                foreach ($q->rows as $row) {
                    $this->db->query("ALTER TABLE " . $row['TABLE_NAME'] . " ENGINE=MyISAM");
                }
                break;
            case "0.9.5":
                if ($this->isTableColumn("field_value_link_subscription")) {
                    $this->db->query("DROP TABLE field_value_link_subscription");
                }
                $q = $this->db->query("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '" . DB_DATABASE ."' AND TABLE_NAME LIKE 'field_value_%'");                
                foreach ($q->rows as $table) {
                    if ($table['TABLE_NAME'] == "field_value_file_relation" || 
                            $table['TABLE_NAME'] == "field_value_text_plus_file" || 
                            $table['TABLE_NAME'] == "field_value_file_list") {
                        continue;
                    }
                    if (!$this->isTableColumn($table['TABLE_NAME'],'time_changed')) {
                        $this->db->query("ALTER TABLE " . DB_PREFIX . $table['TABLE_NAME'] . " ADD `time_changed` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `display_value`;");
                        $this->db->query("ALTER TABLE " . DB_PREFIX . $table['TABLE_NAME'] . " ADD INDEX( `time_changed`);");
                    }
                    if ($table['TABLE_NAME'] == 'field_value_link' || $table['TABLE_NAME'] == 'field_value_link_plus') {
                        if (!$this->isTableColumn($table['TABLE_NAME'],'full_display_value')) {
                            $this->db->query("ALTER TABLE " . DB_PREFIX . $table['TABLE_NAME'] . " CHANGE `value` `value` MEDIUMTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;");
                            $this->db->query("ALTER TABLE " . DB_PREFIX . $table['TABLE_NAME'] . " ADD `full_display_value` MEDIUMTEXT NOT NULL AFTER `display_value`;");
                        }
                    }
                }
                if (!$this->isTableColumn("field_change_subscription")) {
                    $this->db->query("CREATE TABLE " . DB_PREFIX . "field_change_subscription ( `subscription_field_uid` VARCHAR(36) NOT NULL , `subscription_document_uid` VARCHAR(36) NOT NULL , `field_uid` VARCHAR(36) NOT NULL , `document_uid` VARCHAR(36) NOT NULL , PRIMARY KEY (`subscription_field_uid`, `subscription_document_uid`,`field_uid`,`document_uid`), INDEX (`field_uid`,`document_uid`)) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;");
                    $query_field = $this->db->query("SELECT field_uid,type FROM " . DB_PREFIX . "field WHERE type='link' OR type='link_plus' ");
                    $load_models = array();
                    foreach ($query_field->rows as $field) {
                        if (empty($load_models[$field['type']])) {
                            $this->load->model('extension/field/' . $field['type']);
                            $load_models[$field['type']] = 1;
                        }

                        $model = "model_extension_field_" . $field['type'];
                        $data_field = array(
                            'field_uid'     => $field['field_uid'],
                            'new_params'    => $this->model_doctype_doctype->getField($field['field_uid'])['params']
                        );
                        $this->$model->refreshDisplayValues($data_field);                     
                    }
                }
                $this->db->query("DELETE FROM " . DB_PREFIX . "extension  WHERE type !='field' AND type != 'action' AND type != 'service' AND type != 'folder'");
                if (!$this->isTableColumn("variable")) {
                    $this->db->query("CREATE TABLE " . DB_PREFIX . "variable ( `name` VARCHAR(256) NOT NULL, `value` TEXT NOT NULL , `serialized` TINYINT NOT NULL DEFAULT '0', PRIMARY KEY (`name`)) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;");
                }
                $query = $this->db->query("SELECT value  FROM " . DB_PREFIX . "setting WHERE `key`='daemon_started'");
                if ($query->num_rows) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "variable SET name='daemon_started', value='" . $query->row['value'] . "' ");
                    $this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE `key`='daemon_started'");
                }
                $query_index = $this->db->query("SHOW INDEX FROM " . DB_PREFIX . "document");
                if ($query_index->num_rows < 3) {
                    $this->db->query("ALTER TABLE " . DB_PREFIX . "document CHANGE `date_added` `date_added` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;");
                    $this->db->query("ALTER TABLE " . DB_PREFIX . "document CHANGE `date_edited` `date_edited` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;");
                    $this->db->query("ALTER TABLE " . DB_PREFIX . "document ADD INDEX(`draft`);");
                }
                $this->addSetting('dv_zsystem', 'disable_main_menu', 0);
                break;               
            default:
                break;
        }
    }

    protected function addSetting($code, $key, $value) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE `key` = '" . $this->db->escape($key) . "' ");
        if ($query->num_rows) {
            $this->db->query("UPDATE " . DB_PREFIX . "setting SET `code` = '" . $this->db->escape($code) . "', `value` = '" . $this->db->escape($value) . "' WHERE `key` = '" . $this->db->escape($key) . "' ");
        } else {
            $this->db->query("INSERT INTO " . DB_PREFIX . "setting (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) "
                    . "VALUES (NULL, '0', '" . $this->db->escape($code) . "', '" . $this->db->escape($key) . "', '" . $this->db->escape($value) . "', '0')");
        }
    }

    protected function addField($field_uid, $name, $doctype_uid, $field_type, $params, $sort) {
        $query = $this->db->query("SELECT field_uid FROM " . DB_PREFIX . "field WHERE field_uid = '" . $this->db->escape($field_uid) . "' ");
        if (!$query->num_rows) {
            $this->db->query("INSERT INTO `field` (`field_uid`, `name`, `doctype_uid`, `type`, `setting`, `change_field`, `access_form`, `access_view`, "
                    . "`required`, `unique`, `params`, `sort`, `draft`, `draft_params`) "
                    . "VALUES ('" . $this->db->escape($field_uid) . "', "
                    . "'" . $this->db->escape($name) . "', "
                    . "'" . $this->db->escape($doctype_uid) . "', "
                    . "'" . $this->db->escape($field_type) . "', '0', '0', '', '', '0', '0', "
                    . "'" . $this->db->escape($params) . "', '" . (int) $sort . "', '0', '')");
        }
    }

    protected function resortFields($doctype_uid) {
        //проверяем сортировку полей
        $query_field_sort = $this->db->query("SELECT field_uid, sort FROM " . DB_PREFIX . "field WHERE doctype_uid='" . $this->db->escape($doctype_uid) . "' ORDER BY sort ASC");
        $sort = 1;
        foreach ($query_field_sort->rows as $field) {
            $this->db->query("UPDATE " . DB_PREFIX . "field SET sort='" . (int) $sort++ . "' WHERE field_uid = '" . $field['field_uid'] . "'");
        }
    }

    protected function isTableColumn($table, $column = "") {
        $sql = "SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA='" . DB_DATABASE . "' AND TABLE_NAME='" . DB_PREFIX . $this->db->escape($table) . "' ";
        if ($column) {
            $sql .= " AND COLUMN_NAME = '" . $this->db->escape($column) . "' ";
        }
        $query = $this->db->query($sql);
        return ($query->num_rows);
    }

    private function update_action_condition(&$action) {
        $condition_params = unserialize($action['params']);

        if ($condition_params['first_value_field_getter'] !== '') {
            $method_name = $condition_params['first_value_field_getter'];
            $first_value_field_info = $this->model_doctype_doctype->getField($condition_params['first_value_field_uid'], 1);
            if ($first_value_field_info) {
                $field_type = $first_value_field_info['type'];
                //$field_type = "string";
                $update_method_name = "update_" . $field_type;
                if (method_exists($this, $update_method_name)) {
                    $new_method_params = $this->$update_method_name($method_name, $condition_params);
                    $condition_params['first_value_method_params'] = $new_method_params;
                }
            }
        } else {
            $condition_params['first_value_field_getter'] = 'standard_getter';
        }
        if (!isset($condition_params['second_type_value'])) {
            $condition_params['second_type_value'] = '0';
        }
        if ($condition_params['second_type_value'] === '0') {
            if ($condition_params['second_value_field_getter']) {
                $method_name = $condition_params['second_value_field_getter'];
                $second_value_field_info = $this->model_doctype_doctype->getField($condition_params['second_value_field_uid'], 1);
                if ($second_value_field_info) {
                    $field_type = $second_value_field_info['type'];
                    //$field_type = "string";
                    $update_method_name = "update_" . $field_type;
                    if (method_exists($this, $update_method_name)) {
                        $new_method_params = $this->$update_method_name($method_name, $condition_params);
                        $condition_params['second_value_method_params'] = $new_method_params;
                    }
                }
            } else {
                $condition_params['second_value_field_getter'] = 'standard_getter';
            }
        }
        if (isset($condition_params['method'])) {
            unset($condition_params['method']);
        }

        if (isset($condition_params['inner_actions_true']) && !empty($condition_params['inner_actions_true'])) {
            //print_r("CONDITION PARAMS\n");

            $inner_actions_true = $condition_params['inner_actions_true'];
            foreach ($inner_actions_true as &$inner_action) {
                if ($inner_action['action'] === 'record') {
                    $this->update_action_record($inner_action, false);
                }
            }
            //print_r("NNER_ACTIONS_TREU\n");
            //print_r($inner_actions_true);
            $condition_params['inner_actions_true'] = $inner_actions_true;
            //print_r($condition_params);exit;
        }
        if (isset($condition_params['inner_actions_false']) && !empty($condition_params['inner_actions_false'])) {
            $inner_actions_false = $condition_params['inner_actions_false'];
            foreach ($inner_actions_false as &$inner_action) {
                if ($inner_action['action'] === 'record') {
                    $this->update_action_record($inner_action, false);
                }
            }
            $condition_params['inner_actions_false'] = $inner_actions_false;
        }
        $action['params'] = serialize($condition_params);
    }

    private function update_action_record(&$action, $serialization) {
        $record_params = $action['params'];
        if ($serialization) {
            $record_params = unserialize($record_params);
        }
        if (!isset($record_params['target_field_setter'])) {
            return;
        }

        $target_field_info = $this->model_doctype_doctype->getField($record_params['target_field_uid'], 1);
        if (empty($target_field_info)) {
            $action['params'] = '';
            return;
        }

        if ($record_params['target_field_setter'] !== '') {
            $method_name = $record_params['target_field_setter'];
            $field_type = $target_field_info['type'];
            $method_updater = "update_" . $field_type;
            $new_method_params = $this->$method_updater($method_name, $record_params);
            $record_params['target_field_method_name'] = $method_name;
        } else {
            $new_method_params = array('standard_setter_param' => $this->get_standard_setter_param($record_params));
            $record_params['target_field_method_name'] = 'standard_setter';
        }
        if (!isset($record_params['target_field_type'])) {
            $record_params['target_field_type'] = '0';
        }
        switch ($record_params['target_field_type']) {
            case '0':
                $record_params['target_type'] = 'document';
                break;
            case '1':
                $record_params['params']['target_type'] = 'doctype';
                break;
            default:
                $record_params['params']['target_type'] = 'document';
        }
        unset($record_params['method']);
        unset($record_params['target_field_type']);
        unset($record_params['source_type']);
        unset($record_params['source_doclink_field_uid']);
        unset($record_params['source_field_uid']);
        unset($record_params['source_variable']);
        unset($record_params['source_field_getter']);
        unset($record_params['target_field_setter']);
        unset($record_params['params']);
        $record_params['method_params'] = $new_method_params;
        if ($serialization) {
            $action['params'] = serialize($record_params);
        } else {
            $action['params'] = $record_params;
        }
    }

    private function update_currency($method_name, $params) {
        $new_params = array();
        switch ($method_name) {
            case 'get_currency_value':
                $new_params['gcv_currency'] = array('type' => 'document', 'method_name' => 'standard_getter');
                $new_params['gcv_currency']['doclink_field_uid'] = isset($params['method'][$method_name]['document_currency_field_uid']) ? $params['method'][$method_name]['document_currency_field_uid'] : '0';
                $new_params['gcv_currency']['field_uid'] = isset($params['method'][$method_name]['document_currency_field_uid']) ? $params['method'][$method_name]['currency_field_uid'] : '';

                break;
            case "add":
            case "subtract":
            case "multiply":
            case "divide":
                $new_params['standard_setter_param'] = $this->get_standard_setter_param($params);
        }
        return $new_params;
    }

    private function update_string($method_name, $params) {
        $new_params = array();
        switch ($method_name) {
            case 'get_first_chars':
                $new_params['char_count'] = array(
                    'type' => 'value',
                    'value' => isset($params['method'][$method_name]['char_count']) ? $params['method'][$method_name]['char_count'] : ''
                );
                break;
            case "append_text":
            case "insert_text":
                $new_params['standard_setter_param'] = $this->get_standard_setter_param($params);
        }
        return $new_params;
    }

    private function update_datetime($method_name, $params) {
        $new_params = array();
        switch ($method_name) {
            case "get_difference":
                $new_params['other_datetime_value'] = array(
                    'type' => 'document',
                    'doclink_field_uid' => '0',
                    'field_uid' => isset($params['method'][$method_name]['date_fld_id']) ? $params['method'][$method_name]['date_fld_id'] : '',
                    'method_name' => 'standard_getter'
                );
                break;
            case 'adjust_year_plus':
            case 'adjust_month_plus':
            case 'adjust_date_plus':
            case 'adjust_hour_plus':
            case 'adjust_minute_plus':
            case 'adjust_year_minus':
            case 'adjust_month_minus':
            case 'adjust_date_minus':
            case 'adjust_hour_minus':
            case 'adjust_minute_minus':
                $new_params['standard_setter_param'] = $this->get_standard_setter_param($params);
                break;
        }
        return $new_params;
    }

    private function update_file($method_name, $params) {
        $new_params = array();
        switch ($method_name) {
            case 'append_file':
            case 'remove_file':
                $new_params['standard_setter_param'] = $this->get_standard_setter_param($params);
                break;
        }
        return $new_params;
    }

    private function update_hidden($method_name, $params) {
        $new_params = array();
        switch ($method_name) {
            case 'get_decrypt_value':
                break;
        }
        return $new_params;
    }

    private function update_int($method_name, $params) {
        $new_params = array();
        switch ($method_name) {
            case 'add_leading_zeros':
                $new_params['char_count'] = array(
                    'type' => 'value',
                    'value' => isset($params['method'][$method_name]['char_count']) ? $params['method'][$method_name]['char_count'] : 0
                );
                break;
            case 'add':
            case 'subtract':
            case 'divide':
            case 'multiply':
            case 'remainder':
                $new_params['standard_setter_param'] = $this->get_standard_setter_param($params);
                break;
        }
        return $new_params;
    }

    private function update_link($method_name, $params) {
        $new_params = array();
        switch ($method_name) {
            case 'get_another_field_value':
            case 'get_another_field_display':
                $new_params['another_field_uid'] = array(
                    'type' => 'value',
                    'value' => isset($params['method'][$method_name]['another_field_uid']) ? $params['method'][$method_name]['another_field_uid'] : ''
                );
                break;
            case 'append_link':
            case 'remove_link':
                $new_params['standard_setter_param'] = $this->get_standard_setter_param($params);
                break;
        }
        return $new_params;
    }

    private function update_list($method_name, $params) {
        $new_params = array();
        switch ($method_name) {
            case 'set_by_title':
                $new_params['standard_setter_param'] = $this->get_standard_setter_param($params);
                break;
        }
        return $new_params;
    }

    private function update_text($method_name, $params) {
        $new_params = array();
        switch ($method_name) {
            case 'get_first_chars':
                $new_params['char_count'] = array(
                    'type' => 'value',
                    'value' => isset($params['method'][$method_name]['char_count']) ? $params['method'][$method_name]['char_count'] : ''
                );
                break;
            case 'append_text':
            case 'append_text_new_line':
            case 'insert_text':
            case 'insert_text_new_line':
                $new_params['standard_setter_param'] = $this->get_standard_setter_param($params);
                break;
            case 'append_text_separator':
                $new_params['separator'] = array(
                    'type' => 'value',
                    'value' => isset($params['method'][$method_name]['separator']) ? $params['method'][$method_name]['separator'] : ''
                );
                $new_params['standard_setter_param'] = $this->get_standard_setter_param($params);
                break;
        }
        return $new_params;
    }

    private function update_text_plus($method_name, $params) {
        $new_params = array();
        switch ($method_name) {
            case 'get_first_chars':
                $new_params['char_count'] = array(
                    'type' => 'value',
                    'value' => isset($params['method'][$method_name]['char_count']) ? $params['method'][$method_name]['char_count'] : ''
                );
                break;
            case 'append_text':
            case 'append_text_new_line':
            case 'insert_text':
            case 'insert_text_new_line':
                $new_params['standard_setter_param'] = $this->get_standard_setter_param($params);
                break;
            case 'append_text_separator':
                $new_params['separator'] = array(
                    'type' => 'value',
                    'value' => isset($params['method'][$method_name]['separator']) ? $params['method'][$method_name]['separator'] : ''
                );
                $new_params['standard_setter_param'] = $this->get_standard_setter_param($params);
                break;
            case 'get_line':
                $new_params['field_line'] = array(
                    'type' => 'document',
                    'doclink_field_uid' => '0',
                    'field_uid' => isset($params['method'][$method_name]['field_line_uid']) ? $params['method'][$method_name]['field_line_uid'] : '',
                    'method_name' => 'standard_getter'
                );
                break;
            case 'get_substr':
                $new_params['field_separator'] = array(
                    'type' => 'document',
                    'doclink_field_uid' => '0',
                    'field_uid' => isset($params['method'][$method_name]['field_separator_uid']) ? $params['method'][$method_name]['field_separator_uid'] : '',
                    'method_name' => 'standard_getter'
                );
                $new_params['field_part'] = array(
                    'type' => 'document',
                    'doclink_field_uid' => '0',
                    'field_uid' => isset($params['method'][$method_name]['field_part_uid']) ? $params['method'][$method_name]['field_part_uid'] : '',
                    'method_name' => 'standard_getter'
                );
                break;
        }

        return $new_params;
    }

    private function update_treedoc($method_name, $params) {
        $new_params = array();
        switch ($method_name) {
            case 'insert_document':
                $new_params['label'] = array(
                    'type' => 'document',
                    'doclink_field_uid' => '0',
                    'field_uid' => isset($params['method'][$method_name]['label_fld_uid']) ? $params['method'][$method_name]['label_fld_uid'] : '',
                    'method_name' => 'standard_getter'
                );
                $new_params['pardoc'] = array(
                    'type' => 'document',
                    'doclink_field_uid' => '0',
                    'field_uid' => isset($params['method'][$method_name]['pardoc_fld_uid']) ? $params['method'][$method_name]['pardoc_fld_uid'] : '',
                    'method_name' => 'standard_getter'
                );
                $new_params['standard_setter_param'] = $this->get_standard_setter_param($params);
                break;
            case 'insert_treeasbranch':
                $new_params['pardoc'] = array(
                    'type' => 'document',
                    'doclink_field_uid' => '0',
                    'field_uid' => isset($params['method'][$method_name]['pardoc_fld_uid']) ? $params['method'][$method_name]['pardoc_fld_uid'] : '',
                    'method_name' => 'standard_getter'
                );
                $new_params['standard_setter_param'] = $this->get_standard_setter_param($params);
                break;
            case 'get_branchastree':
                $new_params['pardoc'] = array(
                    'type' => 'document',
                    'doclink_field_uid' => '0',
                    'field_uid' => isset($params['method'][$method_name]['pardoc_fld_uid']) ? $params['method'][$method_name]['pardoc_fld_uid'] : '',
                    'method_name' => 'standard_getter'
                );
                break;
            case 'remove_document':
            case 'remove_branch':
                $new_params['standard_setter_param'] = $this->get_standard_setter_param($params);
                break;
        }
        return $new_params;
    }

    private function get_standard_setter_param($params) {


        $new_params = array();
        switch ($params['source_type']) {
            case 'doctype':
                $new_params['type'] = 'doctype';
                $new_params['field_uid'] = $params['source_field_uid'];
                if ($params['source_field_getter'] !== '') {
                    $source_field_info = $this->model_doctype_doctype->getField($params['source_field_uid'], 1);
                    if (empty($source_field_info)) {
                        return;
                    }
                    $field_type = $source_field_info['type'];
                    $method_name = $params['source_field_getter'];
                    $new_params['method_name'] = $method_name;

                    $method_updater = "update_" . $field_type;
                    $new_params['method_params'] = $this->$method_updater($method_name, $params);
                } else {
                    $new_params['method_name'] = 'standard_getter';
                }
                break;
            case 'document':
                $new_params['type'] = 'document';
                $new_params['doclink_field_uid'] = $params['source_doclink_field_uid'];
                $new_params['field_uid'] = $params['source_field_uid'];
                if ($params['source_field_getter'] !== '') {
                    $source_field_info = $this->model_doctype_doctype->getField($params['source_field_uid'], 1);
                    if (empty($source_field_info)) {
                        return;
                    }
                    $field_type = $source_field_info['type'];
                    $method_name = $params['source_field_getter'];
                    $new_params['method_name'] = $method_name;
                    $method_updater = "update_" . $field_type;
                    $new_params['method_params'] = $this->$method_updater($method_name, $params);
                } else {
                    $new_params['method_name'] = 'standard_getter';
                }

                break;
            case 'variable':
                $new_params['type'] = 'variable';
                $new_params['var_id'] = $this->convert_var_ids($params['source_variable']);
                break;
            case 'manual':
                $new_params['type'] = 'value';
                $new_params['value'] = '';
                if (isset($params['field_widget_value'])) {
                    $new_params['value'] = $params['field_widget_value'];
                } else if (isset($params['value'])) {
                    $new_params['value'] = $params['value'];
                }
                break;
        }
        return $new_params;
    }

    private function convert_var_ids($var_id) {
        if (isset($this::VAR_IDS[$var_id])) {
            return $this::VAR_IDS[$var_id];
        } else {
            return '';
        }
    }

}
