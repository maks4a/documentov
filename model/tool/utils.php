<?php

class ModelToolUtils extends Model {
    
    public function getUseFieldDisk() {
        $result = 0;
        $query_tables = $this->db->query("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA='" . DB_DATABASE . "' AND COLUMN_NAME = 'size' AND DATA_TYPE = 'int' AND TABLE_NAME LIKE '%file%' ");
        foreach ($query_tables->rows as $table) {
            $query = $this->db->query("SELECT SUM(size) as sum_size FROM " . DB_PREFIX . $table['TABLE_NAME']);
            $result += (int) $query->row['sum_size'];
        }
        return $result;
    }
    
    public function isTable($table) {
        $query = $this->db->query("SELECT * FROM information_schema.columns WHERE TABLE_SCHEMA='" . DB_DATABASE . "' AND TABLE_NAME = '" . DB_PREFIX . $this->db->escape(trim($table)) . "'");
        return $query->num_rows;
    }

    /**
     * Метод изменяет тип поля с 1 на 2
     * @param type $type1
     * @param type $type2
     */
    public function changeFieldType($type1, $type2) {
        $this->db->query("UPDATE " . DB_PREFIX . "field SET type='" . $this->db->escape($type2) . "' WHERE type='" . $this->db->escape($type1) . "' ");
        //проверяем использование поля в настройках
        $query_s = $this->db->query("SELECT setting_id FROM setting WHERE `key` LIKE '%type' AND `value` = '" . $this->db->escape($type1) . "' ");
        if ($query_s->num_rows) {
            $setting_ids = array();
            foreach ($query_s->rows as $row) {
                $setting_ids[] = $row['setting_id'];
            }
            $this->db->query("UPDATE setting SET value='" . $this->db->escape($type2) . "' WHERE setting_id IN (" . implode(",",$setting_ids) . ") ");
        }
        $query1 = $this->db->query("SELECT * FROM information_schema.columns WHERE TABLE_SCHEMA='" . DB_DATABASE . "' AND TABLE_NAME = '" . DB_PREFIX . "field_value_" . $this->db->escape($type1) . "'");  
        $query2 = $this->db->query("SELECT * FROM information_schema.columns WHERE TABLE_SCHEMA='" . DB_DATABASE . "' AND TABLE_NAME = '" . DB_PREFIX . "field_value_" . $this->db->escape($type2) . "'");  
        if ($query1->num_rows && $query2->num_rows) {
            //таблица для второго типа есть, переносим данные
            $this->db->query("INSERT INTO field_value_" . $this->db->escape($type2) . " SELECT * FROM field_value_" . $this->db->escape($type1));
        }
    }
    /**
     * Метод проверки UID
     * @param type $value
     * @return type
     */
    public function validate($value) {
        return preg_match("/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/", $value);
    }
    
    
    public function addLog($doc_uid, $type, $module, $object_uid, $value) {
        if (!empty($this->config->get('debugger_status'))) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "debugger_log SET "
                    . "date = NOW(), "
                    . "user_uid = '" . ($this->customer->getStructureId() ?? "") . "', "
                    . "doc_uid = '" . $this->db->escape($doc_uid) . "', "
                    . "type = '" . $this->db->escape($type)  . "', "
                    . "module = '" . $this->db->escape($module)  . "', "
                    . "object_uid = '" . $this->db->escape($object_uid)  . "', "
                    . "value = '" . $this->db->escape(is_array($value) ? serialize($value) : $value)  . "' "
                    . "");                 
        }
    }
}