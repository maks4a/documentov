<?php

class ModelDocumentDocument extends Model {

    private $step = 0;
    private $recursion = array();

    public function addDocument($doctype_uid, $author_uid = 0, $draft = 3) {
        $query = $this->db->query("SELECT document_uid FROM " . DB_PREFIX . "document WHERE draft = 3 AND author_uid = '" . $this->customer->getStructureId() . "' AND doctype_uid = '" . $this->db->escape($doctype_uid) . "' ");
        if ($query->num_rows) {
            $document_uid = $query->row['document_uid'];
        } else {
            $query = $this->db->query("SELECT UUID() AS uid");
            $document_uid = $query->row['uid'];
            $this->db->query("INSERT INTO " . DB_PREFIX . "document SET "
                    . "document_uid = '" . $document_uid . "', "
                    . "doctype_uid = '" . $this->db->escape($doctype_uid) . "', "
                    . "author_uid = '" . ($author_uid ? $this->db->escape($author_uid) : $this->customer->getStructureId()) . "', "
                    . "route_uid = '" . $this->getFirstRoute($doctype_uid) . "', "
                    . "draft = '" . (int) $draft . "', "
                    . "date_added=NOW()"
            );
            if (!$author_uid) {
                $author_uid = $this->customer->getStructureId();
            }
            $this->db->query("INSERT INTO " . DB_PREFIX . "document_access SET subject_uid='" . $this->db->escape($author_uid) . "', document_uid = '" . $this->db->escape($document_uid) . "', doctype_uid = '" . $this->db->escape($doctype_uid) . "' ");
        }
        return $document_uid;
    }

    /**
     * Сохраняется документ после нажатия пользователем на кнопку Сохранить, поля сохранены уже, сбрасываем драфт, устанавливает дату изменения
     * @param type $document_uid
     */
    public function editDocument($document_uid) {
        $this->db->query("UPDATE " . DB_PREFIX . "document SET date_added = (CASE WHEN draft=3 THEN NOW() ELSE date_added END), draft=0, draft_params='', date_edited=NOW() WHERE document_uid='" . $this->db->escape($document_uid) . "' ");
    }

    /**
     * Сохраняется черновик документа (пользователь правит документ, все изменения записываются через этот метод)
     * @param type $document_uid
     */
    public function saveDraftDocument($document_uid, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "document SET draft=(CASE WHEN draft=3 THEN 3 ELSE 1 END), draft_params='" . $this->db->escape(serialize($data)) . "' WHERE document_uid='" . $this->db->escape($document_uid) . "' ");
    }

    public function removeDraftDocument($document_uid) {
        $this->db->query("UPDATE " . DB_PREFIX . "document SET draft=(CASE WHEN draft=3 THEN 3 ELSE 0 END), draft_params='' WHERE document_uid='" . $this->db->escape($document_uid) . "' ");
        //если документ имел драфт=3, то вместе с драфотом удаляем и его полностью
        //кейс: док создали чрез Создание и инициализировали поля (они пишутся прямо в базе, поскольку драфт работает только с полями из шаблона, а инициализироваться могут любые
        //при нажатии на отмены при создании дока - нужно все зачистить
        $document_info = $this->getDocument($document_uid);
        if ($document_info['draft'] == 3) {
            $this->removeDocument($document_uid);
        }        
    }

    /**
     * Возвращает информацию о документы
     * @param type $document_uid
     * @param type $check_access - проверяется доступ к документы для текущего пользователя
     * @return type
     */
    public function getDocument($document_uid, $check_access = true) {
        if ($check_access) {
            $query = $this->db->query("SELECT DISTINCT d.doctype_uid, d.author_uid, d.route_uid, d.draft, d.draft_params, d.date_added, d.date_edited, "
                    . "dt.field_log_uid FROM " . DB_PREFIX . "document d "
                    . "LEFT JOIN doctype dt ON (dt.doctype_uid = d.doctype_uid) "
                    . "WHERE d.document_uid = '" . $this->db->escape($document_uid) . "' "
                    . "AND (SELECT doctype_uid FROM " . DB_PREFIX . "document_access WHERE document_uid='" . $this->db->escape($document_uid) . "' AND subject_uid IN ('" . implode("','", $this->customer->getStructureIds()) . "') LIMIT 0,1) IS NOT NULL");
        } else {
            $query = $this->db->query("SELECT DISTINCT d.doctype_uid, d.author_uid, d.route_uid, d.draft, d.draft_params, d.date_added, d.date_edited, dt.field_log_uid FROM " . DB_PREFIX . "document d "
                    . "LEFT JOIN doctype dt ON (dt.doctype_uid = d.doctype_uid) "
                    . "WHERE d.document_uid = '" . $this->db->escape($document_uid) . "' ");
        }
        return $query->row;
    }

    /**
     * Метод возвращает идентификаторы документов на основании критерием в $data
     * @param type $data: 
     *      'author_uids'  - массив с идентификаторами авторов документов
     *      'doctype_uids' - массив с идентификаторами типов документов 
     *      'document_uids - выборка из массима с document_uid
     *      'filter_names' - условия по значению полей: filed_id => $condition_value = array($condition,$value) (value может быть заменен на display, чтобы выборка шла по display
     */
    public function getDocumentIds($data) {
        $this->load->model('doctype/doctype');
        $joins = array();
        $where = "";
        $sql = "SELECT ";
        if (!empty($data['function'])) {
            $sql .= $this->db->escape($data['function']) . "(";
            if (!empty($data['function_join'])) {
                $field_info = $this->model_doctype_doctype->getField($data['function_join']);
                if ($field_info) {
                    $sql .= "fv" . $this->db->escape(str_replace("-", "", $data['function_join'])) . ".value";
                    $joins[] = "LEFT JOIN " . DB_PREFIX . "field_value_" . $this->db->escape($field_info['type']) . " fv" . $this->db->escape(str_replace("-", "", $data['function_join'])) . " ON (fv" . $this->db->escape(str_replace("-", "", $data['function_join'])) . ".document_uid=d.document_uid AND fv" . $this->db->escape(str_replace("-", "", $data['function_join'])) . ".field_uid = '" . $this->db->escape($data['function_join']) . "') ";
                }
            } else {
                $sql .= "d.document_uid";
            }
            $sql .= ") AS result ";
        } else {
            $sql .= "d.document_uid ";
        }
        $sql .= "FROM " . DB_PREFIX . "document d ";
        if (!empty($data['filter_names'])) {
            foreach ($data['filter_names'] as $field_uid => $condition_value) {
                if (count($condition_value)) {
                    $field_info = $this->model_doctype_doctype->getField($field_uid);
                    if (!$field_info) {
                        continue;
                    }
                    $joins[] = "LEFT JOIN " . DB_PREFIX . "field_value_" . $this->db->escape($field_info['type']) . " fv" . $this->db->escape(str_replace("-", "", $field_uid)) . " ON (fv" . $this->db->escape(str_replace("-", "", $field_uid)) . ".document_uid=d.document_uid AND fv" . $this->db->escape(str_replace("-", "", $field_uid)) . ".field_uid = '" . $this->db->escape($field_uid) . "') ";
                    foreach ($condition_value as $condition) {
                        if (isset($condition['value'])) {
                            $table_field = "value";
                            $value = $condition['value'];
                        } else {
                            $table_field = "display_value";
                            $value = $condition['display'];
                        }
                        //проверям знаки сравнения
                        $add_cond = "";
                        $link_add_cond = "OR";
                        switch ($condition['comparison']) {
                            case '=':
                                $comparison = '=';
                                if (!$value) {
                                    $add_cond = "IS NULL";
                                }
                                break;
                            case '>':
                                $comparison = '>';
                                break;
                            case '<':
                                $comparison = '<';
                                break;
                            case 'equal':
                                $comparison = '=';
                                if (!$value) {
                                    $add_cond = "IS NULL";
                                }
                                break;
                            case 'notequal':
                                $comparison = '<>';
                                if ($value) {
                                    $add_cond = "IS NULL";
                                } else {
                                    $add_cond = "IS NOT NULL";
                                    $link_add_cond = "AND";
                                } 
                                break;
                            case 'more':
                                $comparison = '>';
                                break;
                            case 'moreequal':
                                $comparison = '>=';
                                break;
                            case 'less':
                                $comparison = '<';
                                $add_cond = "IS NULL";
                                break;
                            case 'lessequal':
                                $comparison = '<=';
                                break;
                            case 'contains':
                                $comparison = 'LIKE';
                                $value = "%" . $value . '%';
                                break;
                            case 'notcontains':
                                $comparison = 'not LIKE';
                                if ($value) {
                                    $add_cond = "IS NULL";
                                } else {
                                    $add_cond = "IS NOT NULL";
                                    $link_add_cond = "AND";
                                }
                                $value = "%" . $value . '%';
                                
                                break;
                            default:
                                $comparison = '=';
                                break;
                        }
                        
                        $wh = "(fv" . $this->db->escape(str_replace("-", "", $field_uid)) . "." . $table_field . " " . $comparison . " '" . $this->db->escape($value) . "'" . 
                            ($add_cond ? " " . $link_add_cond . " fv" . $this->db->escape(str_replace("-", "", $field_uid)) . "." . $table_field . " " . $add_cond : "") .
                        ") ";
                        
                        if ($where) {
                            if (isset($condition['concat']) && strtolower($condition['concat']) == 'or') {
                                $concat = " OR ";
                            } else {
                                $concat = " AND ";

                        }
                            $where .= $concat . $wh;
                        } else {
                            $where = $wh;
                         }                        
                        
                    }
                }
            }
        }
        if (!empty($data['sort'])) {
            $field_sort_info = $this->model_doctype_doctype->getField($data['sort']);
            if ($field_sort_info) {
                $joins[] = "LEFT JOIN " . DB_PREFIX . "field_value_" . $this->db->escape($field_sort_info['type']) . " fv" . $this->db->escape(str_replace("-", "", $data['sort'])) . " ON (fv" . $this->db->escape(str_replace("-", "", $data['sort'])) . ".document_uid=d.document_uid AND fv" . $this->db->escape(str_replace("-", "", $data['sort'])) . ".field_uid = '" . $this->db->escape($data['sort']) . "') ";
            }
        }
        if (!empty($joins)) {
            $joins = array_unique($joins);
            $sql .= implode(" ", $joins);
        }
        if (!empty($data['draft_less'])) {
            $sql .= "WHERE d.draft < '" . (int) $data['draft_less']  ."' ";
        } else {
            $sql .= "WHERE d.draft < 2 ";
        }
        
        if (!empty($where)) {
            $wh = explode(" AND ", $where);
            $sql .= " AND (" . implode(") AND (", $wh) . ") ";
        }
        if (!empty($data['author_uids'])) {
            $sql .= "AND d.author_uid IN ('" . implode("','", $data['author_uids']) . "') ";
        }
        if (!empty($data['doctype_uids'])) {
            $sql .= "AND d.doctype_uid IN ('" . implode("','", $data['doctype_uids']) . "') ";
        }
        if (!empty($data['document_uids'])) {
            $sql .= "AND d.document_uid IN ('" . implode("','", $data['document_uids']) . "') ";
        }
        if (!empty($data['route_uid'])) {
            $sql .= "AND d.route_uid = '" . $this->db->escape($data['route_uid']) . "' ";
        }
        if (!empty($data['sort']) && $field_sort_info) {
            $query = $this->db->query("SELECT DATA_TYPE FROM information_schema.COLUMNS WHERE TABLE_SCHEMA='" . $this->db->escape(DB_DATABASE) . "' AND TABLE_NAME='field_value_" . $this->db->escape($field_sort_info['type']) . "' AND COLUMN_NAME='value'");
            if ($query->row['DATA_TYPE'] == 'datetime' || $query->row['DATA_TYPE'] == 'date' || $query->row['DATA_TYPE'] == 'time' || $query->row['DATA_TYPE'] == 'int' || $query->row['DATA_TYPE'] == 'tinyint' || $query->row['DATA_TYPE'] == 'smallint' || $query->row['DATA_TYPE'] == 'mediumint' || $query->row['DATA_TYPE'] == 'bigint' || $query->row['DATA_TYPE'] == 'decimal' || $query->row['DATA_TYPE'] == 'float' || $query->row['DATA_TYPE'] == 'double' || $query->row['DATA_TYPE'] == 'real') {
                $sql .= " ORDER BY fv" . $this->db->escape(str_replace("-", "", $data['sort'])) . ".value " . $this->db->escape(strtoupper($data['order'] ?? "ASC")) . " ";
            } else {
                $sql .= " ORDER BY fv" . $this->db->escape(str_replace("-", "", $data['sort'])) . ".display_value " . $this->db->escape(strtoupper($data['order'] ?? "ASC")) . " ";
            }
        }
        if (!empty($data['limit']) && isset($data['start'])) {
            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);
        if (!empty($data['function'])) {
            $result = $query->row['result'];
        } else {
            $result = array();
            foreach ($query->rows as $document) {
                $result[] = $document['document_uid'];
            }
        }
        return $result;
    }

    /**
     * Получает дерево как в Структуре. 
     * @param type $field_uid
     * @param type $parent_field_uid
     * @param type $parent_field_value
     * @param type $all_children - если true, то возвращает всех детей (внуков и пр), а если false, то только детей (у которых есть дети, то есть группы)
     * @return type
     */
    public function getFieldValueTree($field_uid, $parent_field_uid, $parent_field_value = "", $all_children = true) {
        $this->load->model('doctype/doctype');
        $field_info = $this->model_doctype_doctype->getField($field_uid);
        $parent_field_info = $this->model_doctype_doctype->getField($parent_field_uid);
        $sql = "SELECT fv.display_value, fv.field_uid, fv.document_uid  FROM " . DB_PREFIX . "field_value_" . $this->db->escape($field_info['type']) . " fv "
                . "LEFT JOIN " . DB_PREFIX . "field_value_" . $this->db->escape($parent_field_info['type']) . " fvp ON (fv.document_uid = fvp.document_uid AND fvp.field_uid = '" . $this->db->escape($parent_field_uid) . "') "
                . "LEFT JOIN document d ON (d.document_uid = fv.document_uid) "
                . "WHERE "
                . "fv.field_uid = '" . $this->db->escape($field_uid) . "' "
//                . "AND d.draft < 2 "
                . "AND fv.document_uid IN (SELECT document_uid FROM document_access WHERE doctype_uid = '" . $this->db->escape($field_info['doctype_uid']) . "' AND subject_uid IN ('" . implode("','", $this->customer->getStructureIds()) . "')) "
                . "AND (fvp.display_value = '" . $this->db->escape($parent_field_value) . "' " . (!$parent_field_value ? "OR fvp.value = fvp.document_uid" : "") . ") "
                . ($parent_field_value ? "AND fvp.value != fvp.document_uid " : "");
                
                ;

        if (!$all_children) {
            //добавляем условие, чтобы вернулись только группы (дети, у которых есть другие дети)
            $sql .= " AND fv.display_value IN (SELECT display_value FROM field_value_" . $this->db->escape($parent_field_info['type']) . " WHERE field_uid = '" . $this->db->escape($parent_field_uid) . "' AND display_value = fv.display_value) ";
        }
        $sql .= "GROUP BY fv.display_value";
        $query = $this->db->query($sql);
        $result = array();
        $i = 1;
        foreach ($query->rows as $field) {
            if ($all_children && ++$this->step < 20) {
                $children = $this->getFieldValueTree($field_uid, $parent_field_uid, $field['display_value']);
            } else {
                $children = array();
            }
            $result[$i] = array(
                'name' => $field['display_value'],
                'field_uid' => $field['field_uid'],
                'document_uid' => $field['document_uid'],
                'children' => $children
            );
            $i++;
        }
        return $result;
    }

    
//    public function geFolderFieldTree($field_uid, $parent_field_uid, $parent_field_value = "") {
//        $this->load->model('doctype/doctype');
//        $field_info = $this->model_doctype_doctype->getField($field_uid);
//        $parent_field_info = $this->model_doctype_doctype->getField($parent_field_uid);
//        $query = $this->db->query("SELECT fv.display_value, fv.field_uid, fv.document_uid  FROM " . DB_PREFIX . "field_value_" . $this->db->escape($field_info['type']) . " fv "
//                . "LEFT JOIN " . DB_PREFIX . "field_value_" . $this->db->escape($parent_field_info['type']) . " fvp ON (fv.document_uid = fvp.document_uid AND fvp.field_uid = '" . $this->db->escape($parent_field_uid) . "') "
//                . "LEFT JOIN document d ON (d.document_uid = fv.document_uid) "
//                . "WHERE "
//                . "fv.field_uid = '" . $this->db->escape($field_uid) . "' "
//                . "AND d.draft < 2 "
//                . "AND fv.document_uid IN (SELECT document_uid FROM document_access WHERE doctype_uid = '" . $this->db->escape($field_info['doctype_uid']) . "' AND subject_uid IN ('" . implode("','",$this->customer->getStructureIds()) . "')) "
//                . "AND (fvp.display_value = '" . $this->db->escape($parent_field_value) . "' " . (!$parent_field_value ? "OR fvp.value = fvp.document_uid" : "") . ") "
//                . "GROUP BY fv.display_value" 
//                );
//        $result = array();
//        $i = 1;
//        foreach ($query->rows as $field) {
//            $children = $this->getFieldValueTree($field_uid, $parent_field_uid, $field['display_value']);    
//            $result[$i] = array(
//                'name'      => $field['display_value'],
//                'field_uid'  => $field['field_uid'],
//                'document_uid'  => $field['document_uid'],
//                'children'  => $children
//            ); 
//            $i++;
//        }
//        return $result;
//    }

    public function getDocuments($data) {
        $this->load->model('doctype/doctype');
        $joins = array();
        $where = array();
        if (!empty($data['field_uid'])) {
            //если есть field_uid, возвращается только это поле документа
            $field_info = $this->model_doctype_doctype->getField($data['field_uid']);
            if (!empty($field_info['type'])) {
                $sql = "SELECT * FROM " . DB_PREFIX . "field_value_" . $this->db->escape($field_info['type']) . " WHERE "
                        . "document_uid IN (SELECT document_uid FROM " . DB_PREFIX . "document" . (!isset($data['access_all']) ? "_access" : "") . " WHERE doctype_uid = '" . $this->db->escape($field_info['doctype_uid']) . "'" . (!isset($data['access_all']) ? " AND subject_uid IN ('" . implode("','", $this->customer->getStructureIds()) . "')" : "") . ") ";
                if (!empty($data['document_uids'])) {
                    $sql .= " AND document_uid IN ('" . implode("','", $data['document_uids']) . "')";
                }                 
                if (!empty($data['filter_name'])) {
                    $sql .= "AND value LIKE '%" . $this->db->escape($data['filter_name']) . "%' ";
                }
                $sql .= " AND field_uid = '" . $this->db->escape($data['field_uid']) . "' ";                
            }
        } elseif (!empty($data['field_uids'])) {
            $values = array('d.document_uid, d.author_uid');
            $where_qsearch = array();


            if (isset($data['sum'])) {
                $sum = 'SUM(1) as sum';
                if (!empty($data['sum']['sum_field_uid'])) {
                    $sum_field_uid = $data['sum']['sum_field_uid'];
                    //$sum_field_info = $this->model_doctype_doctype->getField($sum_field_uid);
                    $sum = 'SUM(fv' . $this->db->escape(str_replace("-", "", $sum_field_uid)) . '.value) as sum';
                    $data['field_uids'][] = $sum_field_uid;
                }
                $values[] = $sum;
            }

            foreach ($data['field_uids'] as $field_uid) {
                if (is_array($field_uid)) { //по умолчанию, возвращаются display_value, однако, можно передать field_uid в элементе массива value И тогда вернется value
                    if (isset($field_uid['value'])) {
                        $return_field = "value";
                        $field_uid = $field_uid['value'];
                    } else {
                        $return_field = "display_value";
                        $field_uid = $field_uid['display_value'];
                    }
                } else {
                    $return_field = "display_value";
                }
                $field_info = $this->model_doctype_doctype->getField($field_uid);
                if (!$field_info) {
                    continue;
                }
                $values[] = "IFNULL(fv" . $this->db->escape(str_replace("-", "", $field_uid)) . "." . $return_field . ",'') AS v" . $this->db->escape(str_replace("-", "", $field_uid));
                $joins[] = "LEFT JOIN " . DB_PREFIX . "field_value_" . $this->db->escape($field_info['type']) . " fv" . $this->db->escape(str_replace("-", "", $field_uid)) . " ON (fv" . $this->db->escape(str_replace("-", "", $field_uid)) . ".document_uid=d.document_uid AND fv" . $this->db->escape(str_replace("-", "", $field_uid)) . ".field_uid = '" . $this->db->escape($field_uid) . "')";
                //быстрый поиск                
                if (!empty($data['filter_qsearch'])) {
                    $where_qsearch[] = " (fv" . $this->db->escape(str_replace("-", "", $field_uid)) . ".display_value LIKE '%" . $this->db->escape($data['filter_qsearch']) . "%') ";
                }
            }
            if (!empty($data['filter_names'])) {
                foreach ($data['filter_names'] as $field_uid => $filters) {
                    $field_info = $this->model_doctype_doctype->getField($field_uid);
                    if (!$field_info) {
                        continue;
                    }
                    $joins[] = "LEFT JOIN " . DB_PREFIX . "field_value_" . $this->db->escape($field_info['type']) . " fv" . $this->db->escape(str_replace("-", "", $field_uid)) . " ON (fv" . $this->db->escape(str_replace("-", "", $field_uid)) . ".document_uid=d.document_uid AND fv" . $this->db->escape(str_replace("-", "", $field_uid)) . ".field_uid = '" . $this->db->escape($field_uid) . "')";
                    foreach ($filters as $filter) {
                        if (isset($filter['value'])) {
                            $table_field = "value";
                            $value = $filter['value'];
                        } else {
                            $table_field = "display_value";
                            $value = $filter['display'];
                        }
                        $add_cond = "";
                        $link_add_cond = "OR";
                        //проверям знаки сравнения (могут передаваться из пользовательских фильтров)
                        switch ($filter['condition']) {
                            case '=':
                                $condition = '=';
                                if (!$value) {
                                    //добавить проверка на null
                                    $add_cond = "IS NULL";
                                }
                                break;
                            case '>':
                                $condition = '>';
                                break;
                            case '<':
                                $condition = '<';
                                break;
                            case 'equal':
                                $condition = '=';
                                if (!$value) {
                                    //добавить проверка на null
                                    $add_cond = "IS NULL";
                                }
                                break;
                            case 'notequal':
                                $condition = '<>';
                                if ($value) {
                                    //добавить проверка на null
                                    $add_cond = "IS NULL";
                                } else {
                                    $add_cond = "IS NOT NULL";
                                    $link_add_cond = "AND";
                                }
                                
                                break;
                            case 'more':
                                $condition = '>';
                                break;
                            case 'moreequal':
                                $condition = '>=';
                                break;
                            case 'less':
                                $condition = '<';
                                $add_cond = "IS NULL";
                                break;
                            case 'lessequal':
                                $condition = '<=';
                                break;
                            case 'contains':
                                $condition = 'LIKE';
                                $value = "%" . $value . '%';
                                break;
                            case 'notcontains':
                                $condition = 'not LIKE';
                                if ($value) {
                                    //добавить проверка на null
                                    //$add_conds_and = array ("IS NULL", "=''");
                                    $add_cond = "IS NULL";
                                } else {
                                    $add_cond = "IS NOT NULL";
                                    $link_add_cond = "AND";
                                }
                                $value = "%" . $value . '%';                                
                                break;
                            default:
                                $condition = '=';
                                break;
                        }
                        $where[] = "AND (fv" . $this->db->escape(str_replace("-", "", $field_uid)) . "." . $table_field . " " . $condition . " '" . $this->db->escape($value) . "'" .
                                (!empty($add_cond) ? " " . $link_add_cond . " fv" . $this->db->escape(str_replace("-", "", $field_uid)) . "." . $table_field . " " . $add_cond : "") .
                                ") "
                        ;
                    }
                }
            }
        } elseif (!empty($data['doctype_uid'])) {
            $sql = "SELECT * FROM " . DB_PREFIX . "document" . (!isset($data['access_all']) ? "_access" : "") . " WHERE (doctype_uid='" . $this->db->escape($data['doctype_uid']) . "'";
            if (!empty($data['document_uids'])) {
                $sql .= " AND document_uid IN ('" . implode("','", $data['document_uids']) . "')";
            } 
            $sql .= ") ";
        } elseif (!empty($data['document_uids'])) {
            //возврат документов из списка  
            $sql = "SELECT * FROM " . DB_PREFIX . "document" . (!isset($data['access_all']) ? "_access" : "") . " WHERE document_uid IN ('" . implode("','", $data['document_uids']) . "') ";
        } 
        if (!empty($data['sort']) && !empty($data['order'])) { //включена сортировка 
            $field_info = $this->model_doctype_doctype->getField($data['sort']);
            $joins[] = "LEFT JOIN " . DB_PREFIX . "field_value_" . $this->db->escape($field_info['type']) . " fv" . $this->db->escape(str_replace("-", "", $data['sort'])) . " ON (fv" . $this->db->escape(str_replace("-", "", $data['sort'])) . ".document_uid=d.document_uid AND fv" . $this->db->escape(str_replace("-", "", $data['sort'])) . ".field_uid = '" . $this->db->escape($data['sort']) . "')";

            //определяем тип поля value, чтобы соответствующим образом фильтровать по value или display_value
            $query = $this->db->query("SELECT DATA_TYPE FROM information_schema.COLUMNS WHERE TABLE_SCHEMA='" . $this->db->escape(DB_DATABASE) . "' AND TABLE_NAME='field_value_" . $this->db->escape($field_info['type']) . "' AND COLUMN_NAME='value'");
            if ($query->row['DATA_TYPE'] == 'datetime' || $query->row['DATA_TYPE'] == 'date' || $query->row['DATA_TYPE'] == 'time' || $query->row['DATA_TYPE'] == 'int' || $query->row['DATA_TYPE'] == 'tinyint' || $query->row['DATA_TYPE'] == 'smallint' || $query->row['DATA_TYPE'] == 'mediumint' || $query->row['DATA_TYPE'] == 'bigint' || $query->row['DATA_TYPE'] == 'decimal' || $query->row['DATA_TYPE'] == 'float' || $query->row['DATA_TYPE'] == 'double' || $query->row['DATA_TYPE'] == 'real') {
                $sort = " ORDER BY fv" . $this->db->escape(str_replace("-", "", $data['sort'])) . ".value " . $this->db->escape(strtoupper($data['order'])) . " ";
            } else {
                $sort = " ORDER BY fv" . $this->db->escape(str_replace("-", "", $data['sort'])) . ".display_value " . $this->db->escape(strtoupper($data['order'])) . " ";
            }
        }
        if (!empty($data['group_by'])) {
            if (isset($data['group_by']['value'])) {
                $group_by = " fv" . str_replace("-", "", $data['group_by']['value']) . ".value ";
                $field_info = $this->model_doctype_doctype->getField($data['group_by']['value']);
                $joins[] = "LEFT JOIN " . DB_PREFIX . "field_value_" . $this->db->escape($field_info['type']) . " fv" . $this->db->escape(str_replace("-", "", $data['group_by']['value'])) . " ON (fv" . $this->db->escape(str_replace("-", "", $data['group_by']['value'])) . ".document_uid=d.document_uid AND fv" . $this->db->escape(str_replace("-", "", $data['group_by']['value'])) . ".field_uid = '" . $this->db->escape($data['group_by']['value']) . "')";
            } else {
                $field_info = $this->model_doctype_doctype->getField($data['group_by']['display']);
                $joins[] = "LEFT JOIN " . DB_PREFIX . "field_value_" . $this->db->escape($field_info['type']) . " fv" . $this->db->escape(str_replace("-", "", $data['group_by']['display'])) . " ON (fv" . $this->db->escape(str_replace("-", "", $data['group_by']['display'])) . ".document_uid=d.document_uid AND fv" . $this->db->escape(str_replace("-", "", $data['group_by']['display'])) . ".field_uid = '" . $this->db->escape($data['group_by']['display']) . "')";
                $group_by = " fv" . str_replace("-", "", $data['group_by']['display']) . ".display_value ";
            }
            if (isset($data['group_by']['date'])) {
                $date_format = '';
                switch ($data['group_by']['date']) {
                    case 'minute':
                        $date_format = '%i';
                    case 'hour':
                        $date_format = '%H' . $date_format;
                    case 'day':
                        $date_format = '%d' . $date_format;
                    case 'month':
                        $date_format = '%c' . $date_format;
                    case 'year':
                        $date_format = '%Y' . $date_format;
                        $group_by = "DATE_FORMAT(" . $group_by . ", '" . $date_format . "')";
                        break;
                    case 'week':
                        $group_by = "DATE_FORMAT(" . $group_by . ", '%Y%u')";
                        break;
                    case 'quarter':
                        $group_by = "CONCAT(YEAR(" . $group_by . "), QUARTER(" . $group_by . "))";
                        break;
                }
            }
            $group_by = " GROUP BY " . $group_by;
        }
        if (!empty($joins)) {
            $joins = array_unique($joins);
            $where = array_unique($where);
            $sql = "SELECT " . implode(", ", $values) . " FROM document d " . implode(" ", $joins);
            $sql .= " WHERE d.draft < " . (int) ($data['draft_less'] ?? 2);
            if (!empty($data['document_uids'])) {
                $sql .= " AND d.document_uid IN ('" . implode("','", $data['document_uids']) . "')";
            } 
            $sql .= " AND d.document_uid IN (SELECT DISTINCT document_uid FROM " . DB_PREFIX . "document" . (!isset($data['access_all']) ? "_access" : "") . " WHERE doctype_uid = '" . $this->db->escape($data['doctype_uid']) . "'" . (!isset($data['access_all']) ? " AND subject_uid IN ('" . implode("','", $this->customer->getStructureIds()) . "')" : "") . ") ";
            if ($where_qsearch) {
                $sql .= " AND (" . implode(" OR ", $where_qsearch) . ") ";
            }
            if (!empty($where)) {
                $sql .= implode(" ", $where);
            }
        }

        if (!empty($sql)) {
            $sql .= $sort ?? "";
            $sql .= $group_by ?? "";

            if (isset($data['start']) && !empty($data['limit'])) {
                $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
            }
            if ($sql) {
                $query = $this->db->query($sql);
                return $query->rows;
            }            
        } else {
            return array();
        }
    }

    public function getTotalDocuments($data) {
        $joins = array();
        $where = array();
        $where_qsearch = array();
        $this->load->model('doctype/doctype');
        if (!empty($data['field_uid'])) {
            //если есть field_uid, возвращается только это поле документа
            $field_info = $this->model_doctype_doctype->getField($data['field_uid']);
            $sql = "SELECT COUNT(field_uid) AS total FROM " . DB_PREFIX . "field_value_" . $this->db->escape($field_info['type']) . " "
                    . "WHERE "
                    . "document_uid IN (SELECT document_uid FROM " . DB_PREFIX . "document" . (!isset($data['access_all']) ? "_access" : "") . " WHERE doctype_uid = '" . $this->db->escape($data['doctype_uid']) . "' " . (!isset($data['access_all']) ? " AND subject_uid IN ('" . implode("','", $this->customer->getStructureIds()) . "')" : "") . ") ";
            if (!empty($data['filter_name'])) {
                $sql .= "AND value LIKE '%" . $this->db->escape($data['filter_name']) . "%' ";
            }
            $sql .= " AND field_uid = '" . $this->db->escape($data['field_uid']) . "' ";
        } elseif (!empty($data['field_uids'])) {
            $values = array('d.document_uid, d.author_uid');

            foreach ($data['field_uids'] as $field_uid) {
                $field_info = $this->model_doctype_doctype->getField($field_uid);
                if (!$field_info) {
                    continue;
                }
                $joins[] = "LEFT JOIN " . DB_PREFIX . "field_value_" . $this->db->escape($field_info['type']) . " fv" . $this->db->escape(str_replace("-", "", $field_uid)) . " ON (fv" . $this->db->escape(str_replace("-", "", $field_uid)) . ".document_uid=d.document_uid AND fv" . $this->db->escape(str_replace("-", "", $field_uid)) . ".field_uid = '" . $this->db->escape($field_uid) . "')";
                if (!empty($data['filter_qsearch'])) {
                    $where_qsearch[] = " (fv" . $this->db->escape(str_replace("-", "", $field_uid)) . ".display_value LIKE '%" . $this->db->escape($data['filter_qsearch']) . "%') ";
                }
            }

            if (!empty($data['filter_names'])) {
                foreach ($data['filter_names'] as $field_uid => $filters) {
                    $field_info = $this->model_doctype_doctype->getField($field_uid);
                    if (!$field_info) {
                        continue;
                    }
                    $joins[] = "LEFT JOIN " . DB_PREFIX . "field_value_" . $this->db->escape($field_info['type']) . " fv" . $this->db->escape(str_replace("-", "", $field_uid)) . " ON (fv" . $this->db->escape(str_replace("-", "", $field_uid)) . ".document_uid=d.document_uid AND fv" . $this->db->escape(str_replace("-", "", $field_uid)) . ".field_uid = '" . $this->db->escape($field_uid) . "')";
                    foreach ($filters as $filter) {
                        if (isset($filter['value'])) {
                            $table_field = "value";
                            $value = $filter['value'];
                        } else {
                            $table_field = "display_value";
                            $value = $filter['display'];
                        }
                        $add_cond = "";

                        switch ($filter['condition']) {
                            case '=':
                                $condition = '=';
                                if (!$value) {
                                    //добавить проверка на null
                                    $add_cond = "IS NULL";
                                }
                                break;
                            case '>':
                                $condition = '>';
                                break;
                            case '<':
                                $condition = '<';
                                break;
                            case 'equal':
                                $condition = '=';
                                if (!$value) {
                                    //добавить проверка на null
                                    $add_cond = "IS NULL";
                                }
                                break;
                            case 'notequal':
                                $condition = '<>';
                                if ($value) {
                                    //добавить проверка на null
                                    //$add_conds_and = array ("IS NULL", "=''");
                                    $add_cond = "IS NULL";
                                } else {
                                    $add_cond = "IS NOT NULL";
                                }
                                break;
                            case 'more':
                                $condition = '>';
                                break;
                            case 'moreequal':
                                $condition = '>=';
                                break;
                            case 'less':
                                $condition = '<';
                                $add_cond = "IS NULL";
                                break;
                            case 'lessequal':
                                $condition = '<=';
                                break;
                            case 'contains':
                                $condition = 'LIKE';
                                $value = "%" . $value . '%';
                                break;
                            case 'notcontains':
                                $condition = 'not LIKE';
                                if ($value) {
                                    //добавить проверка на null
                                    //$add_conds_and = array ("IS NULL", "=''");
                                    $add_cond = "IS NULL";
                                } else {
                                    $add_cond = "IS NOT NULL";
                                }
                                $value = "%" . $value . '%';
                                break;
                            default:
                                $condition = '=';
                                break;
                        }
                        //проверям знаки сравнения (могут передаваться из пользовательских фильтров)
                        $where[] = "AND (fv" . $this->db->escape(str_replace("-", "", $field_uid)) . "." . $table_field . " " . $condition . " '" . $this->db->escape($value) . "'" .
                                ($add_cond ? " OR fv" . $this->db->escape(str_replace("-", "", $field_uid)) . "." . $table_field . " " . $add_cond : "") . ") ";
                    }
                }
            }
            $joins = array_unique($joins);
            $where = array_unique($where);
            $sql = "SELECT COUNT(d.document_uid) AS total FROM document d " . implode(" ", $joins);
            $sql .= " WHERE d.draft < 2 AND d.document_uid IN (SELECT DISTINCT document_uid FROM " . DB_PREFIX . "document" . (!isset($data['access_all']) ? "_access" : "") . " WHERE doctype_uid = '" . $this->db->escape($data['doctype_uid']) . "'" . (!isset($data['access_all']) ? " AND subject_uid IN ('" . implode("','", $this->customer->getStructureIds()) . "')" : "") . ") ";
            $sql .= implode(" ", $where);
            if ($where_qsearch) {
                $sql .= " AND (" . implode(" OR ", $where_qsearch) . ") ";
            }
            if (!empty($where)) {
                $sql .= implode(" ", $where);
            }
        } elseif (!empty($data['doctype_uid'])) {
            $sql = "SELECT COUNT(document_uid) AS total FROM " . DB_PREFIX . "document" . (!isset($data['access_all']) ? "_access" : "") . " WHERE doctype_uid='" . $this->db->escape($data['doctype_uid']) . "'";
        }
        if (!empty($sql)) {
            $query = $this->db->query($sql);
            return $query->row['total'];
        }
    }

    public function getDoctype($doctype_uid) {
        $query = $this->db->query("SELECT DISTINCT * "
                . "FROM " . DB_PREFIX . "doctype d "
                . "LEFT JOIN " . DB_PREFIX . "doctype_description dd ON (d.doctype_uid = dd.doctype_uid) "
                . "WHERE d.doctype_uid = '" . $this->db->escape($doctype_uid) . "' "
                . "AND dd.language_id = '" . $this->db->escape($this->config->get('config_language_id')) . "' ");
        $result = $query->row;
        if (!empty($result['params'])) {
            $result['params'] = unserialize($result['params']);
        }
        return $result;
    }

    /**
     * Метод возвращает шаблон документа (если есть дополнительные шаблоны, то возвращает первый из них, в котором условие отображение будет TRUE
     * @param type $document_uid - идентификатор документа
     * @param type $type_template - тип шаблона: form || view
     * @param type $doctype_uid - если документ еще не создан, то получаем шаблон по доктайпу
     * @param type $values - если документ еще не создан, значит его поля не инициализированы; можно передать значения полей через этот массив
     * @return string
     */
    public function getTemplate($document_uid, $type_template, $doctype_uid="",$values=array()) {
        if (!$doctype_uid) {
            $document_info = $this->getDocument($document_uid);
            if (!$document_info) {
                return "";
            }
            $doctype_uid = $document_info['doctype_uid'];
            
        }
        $query = $this->db->query("SELECT template, sort FROM " . DB_PREFIX . "doctype_template WHERE "
                . "doctype_uid = '" . $this->db->escape($doctype_uid) . "' AND language_id = '" . (int) $this->config->get('config_language_id') . "' "
                . "AND type='" . $this->db->escape($type_template) . "' ORDER BY `sort` ASC ");
        if ($query->num_rows > 1) {
            $doctype_info = $this->getDoctype($doctype_uid);
            if (!empty($doctype_info['params']['doctype_template'][$type_template])) {
                foreach ($doctype_info['params']['doctype_template'][$type_template] as $index => $doctype_template) {
                    
                    $first_value = $values[$doctype_template['condition_field_uid']] ?? $this->getFieldValue($doctype_template['condition_field_uid'], $document_uid);
                    $second_value = $values[$doctype_template['condition_value_uid']] ?? $this->getFieldValue($doctype_template['condition_value_uid'], $document_uid);  
                    $result = false;
                    switch ($doctype_template['condition_comparison']) {
                        case 'equal':
                            if ($first_value == $second_value) {
                                $result = true;
                            }
                            break;
                        case 'notequal':
                            if ($first_value != $second_value) {
                                $result = true;
                            }
                            break;
                        case 'more' :
                            if ($first_value > $second_value) {
                                $result = true;
                            }
                            break;
                        case 'moreequal':
                            if ($first_value >= $second_value) {
                                $result = true;
                            } 
                            break;
                        case 'less':
                            if ($first_value < $second_value) {
                                $result = true;
                            } 
                            break;
                        case 'lessequal':
                            if ($first_value <= $second_value) {
                                $result = true;
                            } 
                            break;
                        case 'contains':
                            if (mb_strpos($first_value, $second_value) !== FALSE) {
                                $result = true;
                            } 
                            break;
                        case 'notcontains':
                            if (mb_strpos($first_value, $second_value) === FALSE) {
                                $result = true;
                            } 
                            break;
                    }                   
                    if ($result) {
                        foreach ($query->rows as $template) {
                            if ($template['sort'] == $index) {
                                return $template['template'];
                            }
                        }
                    }
                }
            }
        } 
        if ($query->num_rows) {            
            return $query->row['template'];
        } else {
            return "";
        }
    }

    public function getFields($doctype_uid) {
        $sql = "SELECT * FROM " . DB_PREFIX . "field WHERE doctype_uid='" . $this->db->escape($doctype_uid) . "' ORDER BY sort ASC";
        $query = $this->db->query($sql);
        $result = array();
        foreach ($query->rows as $field) {
            $result[] = array(
                'field_uid' => $field['field_uid'],
                'name' => $field['name'],
                'type' => $field['type'],
                'setting' => $field['setting'],
                'params' => unserialize($field['params']),
                'sort' => $field['sort'],
            );
        }
        return $result;
    }

    /**
     * Метод возвращает уникальные значения field_uid без привязки к document_uid. 
     * @param type $field_uid
     */
    public function getFieldValues($field_uid) {
        $this->load->model('doctype/doctype');
        $result = array();
        $field_info = $this->model_doctype_doctype->getField($field_uid);
        $query = $this->db->query("SELECT DISTINCT(fv.display_value), fv.value  FROM " . DB_PREFIX . "field_value_" . $this->db->escape($field_info['type']) . " fv "
                . "LEFT JOIN document d ON (d.document_uid = fv.document_uid) "
                . "WHERE fv.field_uid = '" . $this->db->escape($field_uid) . "' "
                . "AND d.draft < 2 "
                . "AND fv.document_uid IN (SELECT document_uid FROM document_access WHERE doctype_uid = '" . $this->db->escape($field_info['doctype_uid']) . "' AND subject_uid IN ('" . implode("','", $this->customer->getStructureIds()) . "')) "
        );
        $check_empty = true;
        foreach ($query->rows as $field_value) {
            $result[] = array(
                'value' => $field_value['value'],
                'display_value' => $field_value['display_value'],
            );
            if (!$field_value['display_value']) {
                $check_empty = false;
            }
        }
        //проверяем наличие пустых значений, чтобы тоже вернуть при их наличии     
        if ($check_empty) {
            $query_empty = $this->db->query("SELECT document_uid FROM " . DB_PREFIX . "document "
                    . "WHERE doctype_uid = '" . $this->db->escape($field_info['doctype_uid']) . "' "
                    . "AND draft < 2 "
                    . "AND document_uid IN (SELECT document_uid FROM " . DB_PREFIX . "document_access WHERE doctype_uid = '" . $field_info['doctype_uid'] . "' AND subject_uid IN ('" . implode("','", $this->customer->getStructureIds()) . "')) "
                    . "AND document_uid NOT IN "
                    . "(SELECT document_uid FROM " . DB_PREFIX . "field_value_" . $this->db->escape($field_info['type']) . " WHERE field_uid = '" . $this->db->escape($field_uid) . "')");
            if ($query_empty->num_rows) { //документы без поля найдены, добавляем пустоту в результат
                $result[] = array(
                    'display_value' => '',
                    'value' => ''
                );
            }
        }

        return $result;
    }

    /**
     * Возвращает документ, в которых поле field_uid содержит value
     * @param type $field_uid
     * @param type $value
     * @return type
     */
    public function getFieldByValue($field_uid, $value) {
        $this->load->model('doctype/doctype');
        $field_info = $this->model_doctype_doctype->getField($field_uid);
        $query = $this->db->query("SELECT document_uid FROM " . DB_PREFIX . "field_value_" . $field_info['type'] . " WHERE field_uid= '" . $this->db->escape($field_uid) . "' AND value = '" . $this->db->escape($value) . "'");
        return $query->rows;
    }

    /**
     * Возвращает значение поля документа
     * @param type $field_uid
     * @param type $document_uid
     * @param type $draft - если TRUE проверить наличие черновика и, при его наличии, вернуть из него значение данного поля
     * @return type
     */
    public function getFieldValue($field_uid, $document_uid, $draft = FALSE) {
        $this->load->model('doctype/doctype');
        $field_info = $this->model_doctype_doctype->getField($field_uid);
        if (!empty($field_info['setting'])) {
            //имеем дело с настроечным полем, document_uid = 0
            $document_uid = 0;
        }
        if ($document_uid && $draft) { //если запрашивается настроечное поле document_uid может быть равен 0
            $document_info = $this->getDocument($document_uid);
            if ($document_info['draft']) {
                $draft_params = unserialize($document_info['draft_params']);
                if (isset($draft_params[$field_uid])) {
                    return $draft_params[$field_uid];
                }
            }
        }
        $this->load->model('doctype/doctype');
        if (!empty($field_info['type'])) {
            $this->load->model('extension/field/' . $field_info['type']);
            $model = "model_extension_field_" . $field_info['type'];
            return $this->$model->getValue($field_uid, $document_uid,"",$field_info);
        }
    }

    /**
     * Удаление значения поля
     * @param type $field_uid
     * @param type $document_uid
     */
    public function removeFieldValue($field_uid, $document_uid) {
        $this->load->model('doctype/doctype');
        $field_info = $this->model_doctype_doctype->getField($field_uid);
        if ($field_info['change_field']) {
            //установлен атрибут запуска контекста изменения; получаем значение у поля (если оно было до удаления - нужно запустить контекст
            $field_value = $this->getFieldValue($field_uid, $document_uid);
        }
        $this->load->model('extension/field/' . $field_info['type']);
        $model = "model_extension_field_" . $field_info['type'];
        $this->$model->removeValue($field_uid, $document_uid);
        //если была подписка на удаленное значение поля field_uid документа document_uid, нужно ее обновить
        $this->updateSubscription($this->getSubscriptions($field_uid, $document_uid));        
        if (!empty($field_value)) {
            return $this->runChangeContext($field_uid, $field_value, $document_uid, $field_info);
        }
    }
    
    /**
     * Удаление всех значений заданного поля
     * @param type $field_uid
     * @return type
     */
    public function removeFieldValues($field_uid) {
        $this->load->model('doctype/doctype');
        $field_info = $this->model_doctype_doctype->getField($field_uid);
        $this->load->model('extension/field/' . $field_info['type']);
        $model = "model_extension_field_" . $field_info['type'];
        $this->$model->removeValues($field_uid);
        $this->updateSubscription($this->getSubscriptions($field_uid));
        //контекст изменения в отличие от метода removeFieldValue не запускаем, поскольку тут однозначно идет удаление
        //самого поля и смысла в таком запуске нет
    }    
    
    /**
     * Метод для запуска контекста маршрута Изменение при изменении значения поля
     * @param type $field_uid
     * @param type $field_value
     * @param type $document_uid
     * @param type $field_info
     * @return type
     */
    private function runChangeContext($field_uid, $field_value, $document_uid, $field_info = "") {
        if (!$field_info['change_field']) {
            return;
        }
        //защита от бесконечного цикла через контекст изменения
        if (empty($this->recursion[$field_uid])) {
            $this->recursion[$field_uid] = 1;
        } else {
            if (defined("ROUTE_RECURSION_DEPTH")) {
                $recursion_depth = ROUTE_RECURSION_DEPTH;
            } else {
                $recursion_depth = $this->config->get('recursion_depth');
            }
            if (++$this->recursion[$field_uid] > $recursion_depth) {
                return array('redirect' => $this->url->link('error/cycle'));
            }
        }        
        $this->request->get['field_uid']    = $field_uid; //используется для переменной ИДЕНТИФИКАТОР ИЗМЕННЕННОГО ПОЛЯ (д. Запись, Условие)
        $this->request->get['field_value']  = $field_value; //используется для переменной ПРЕЖНЕЕ ЗНАЧЕНИЕ ИЗМЕННЕННОГО ПОЛЯ (д. Запись, Условие)
        if (!$field_info) {
            $this->load->model('doctype/doctype');
            $field_info = $this->model_doctype_doctype->getField($field_uid);
        }
        if ($field_info['setting']) {
            //это настроечное поле - запуск контекста "настройки" для всех документов данного типа
            $data = array(
                'doctype_uid' => $field_info['doctype_uid'],
                'field_uid'   => $field_uid,
                'context'     => 'setting'
            );                            
            $this->load->model('extension/service/daemon');

            if ($this->variable->get('daemon_started') && $this->model_extension_service_daemon->getStatus()) {
                //если демон доступен - отдаем ему задачу обработки
                $this->load->model('daemon/queue');
                $this->model_daemon_queue->addTask('document/document/executeDeferred', $data, 1);
            } else {
                $this->load->controller('document/document/executeDeferred', $data); 
            }
        } else {
            $params = array(
                'document_uid' => $document_uid,
                'context' => 'change'
            );
            return $this->load->controller('document/document/route_cli', $params);                            
        }
        
    }



    public function getFieldDisplay($field_uid, $document_uid) {
        if ($field_uid) {
            $this->load->model('doctype/doctype');
            $field_info = $this->model_doctype_doctype->getField($field_uid);
            if ($field_info) {
                if ($field_info['setting']) {
                    $document_uid = 0; //настроечное поле
                }
                $query = $this->db->query("SELECT display_value FROM " . DB_PREFIX . "field_value_" . $field_info['type'] . " "
                        . "WHERE field_uid = '" . $this->db->escape($field_uid) . "' AND document_uid = '" . $this->db->escape($document_uid) . "' ");
                if (isset($query->row['display_value'])) {
                    return $query->row['display_value'];
                }
            }
        }
        return "";
    }

    public function editFieldValue($field_uid, $document_uid, $value) {
        if ($field_uid) {
            $this->load->model('doctype/doctype');
            $this->load->model('document/document');
            $field_info = $this->model_doctype_doctype->getField($field_uid);
            if (empty($field_info['setting']) && !$document_uid) {
                //поле НЕнастроечное, обязан быть document_uid, а его нет
                return;
            }
            if ($document_uid) {
                $document_info = $this->model_document_document->getDocument($document_uid, false);
            }

            if (!empty($field_info['type']) && (!$document_uid || (!empty($document_info) && $document_info['doctype_uid'] == $field_info['doctype_uid']))) {
                $value_db = $this->getFieldValue($field_uid, $document_uid);
                $this->load->model('extension/field/' . $field_info['type']);
                $model = "model_extension_field_" . $field_info['type'];
                $this->$model->editValue($field_uid, $document_uid, $value);
                $this->load->model('tool/utils');
                $this->model_tool_utils->addLog($document_uid, 'field', $field_info['type'], $field_uid, $value);
                $this->updateButtonDelegate($field_uid, $document_uid, $value);
                $this->load->model('extension/field/' . $field_info['type']);
                $model = "model_extension_field_" . $field_info['type'];
                $value = $this->$model->getValue($field_uid, $document_uid, $value);
                if ($value !== $value_db) {
                    return $this->runChangeContext($field_uid, $value, $document_uid, $field_info);
                }
            }
        }
    }
    
    public function appendLogFieldValue($field_uid, $document_uid, $log) {
        if ($field_uid) {
            $this->load->model('doctype/doctype');
            $field_info = $this->model_doctype_doctype->getField($field_uid);
            if (!empty($field_info['type'])) {
                $this->load->model('extension/field/' . $field_info['type']);
                $model = "model_extension_field_" . $field_info['type'];
                $this->$model->appendLogValue($field_uid, $document_uid, $log);
            }
        }
    }

    public function hasAccessButton($route_button_uid, $document_uid) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "route_button_delegate WHERE "
                . "route_button_uid = '" . $this->db->escape($route_button_uid) . "' AND "
                . "route_button_uid IN (SELECT route_button_uid FROM " . DB_PREFIX . "route_button WHERE route_uid = (SELECT route_uid FROM " . DB_PREFIX . "document WHERE document_uid = '" . $this->db->escape($document_uid) . "') ) AND "
                . "(document_uid = '" . $this->db->escape($document_uid) . "' OR document_uid = '0') AND "
                . "structure_uid IN ('" . implode("','", $this->customer->getStructureIds()) . "') ");
        return ($query->num_rows);
    }

    public function updateButtonDelegate($field_uid, $document_uid, $value) {
        $checks = array('route', 'folder'); //проверяем поле на наличие в делегировании кнопок маршрутов доктайпов и журналов
        foreach ($checks as $check) {
            $query = $this->db->query("SELECT " . $check . "_button_uid FROM " . DB_PREFIX . $check . "_button_field WHERE field_uid='" . $this->db->escape($field_uid) . "' ");
            if ($query->num_rows) { //изменяем поле используется в делегировании 
                //structure_uid могут быть перечислены через запятую
                if (is_array($value)) {
                    $values = $value;
                } else {
                    $values = explode(",", $value);
                }
                foreach ($query->rows as $button) {
                    $delegate_structure_uids = $values;
                    //получаем другие поля, которые используются в делегировании данной кнопки
                    $query_delegate = $this->db->query("SELECT field_uid FROM " . $check . "_button_field WHERE " . $check . "_button_uid = '". $button[$check . '_button_uid'] . "' ");
                    if ($query_delegate->num_rows) {
                        foreach ($query_delegate->rows as $delegate_field) {
                            $delegate_field_value = $this->getFieldValue($delegate_field['field_uid'], $document_uid);
                            if($delegate_field_value) {
                                if (is_array($delegate_field_value)) {
                                    $delegate_field_values = $delegate_field_value;
                                } else {
                                    $delegate_field_values = explode(",", $delegate_field_value);
                                }  
                                $delegate_structure_uids = array_merge($values, $delegate_field_values);
                            }
                        }
                    }
                    //удаляем прежнее значение
                    $this->db->query("DELETE FROM " . DB_PREFIX . $check . "_button_delegate "
                            . "WHERE " . $check . "_button_uid = '" . $this->db->escape($button[$check . "_button_uid"]) . "' "
                            . "AND document_uid = '" . $this->db->escape($document_uid)  . "' ");
                    //готовим новые записи
                    $sqls = $structure_uids = array();
                    foreach ($delegate_structure_uids as $structure_uid) {
                        $structure_uid = trim($structure_uid);
                        if ($structure_uid && in_array($structure_uid, $structure_uids) === FALSE) { //исключаем дублирование и пустые значения structure_uid
                            $sqls[] = "('" . $this->db->escape($button[$check . '_button_uid']) . "','" . $this->db->escape($document_uid) . "','" . $this->db->escape($structure_uid) . "')";
                            $structure_uids[] = $structure_uid;
                        }
                    }
                    $sqls = array_unique($sqls);
                    if ($structure_uids) {                   
                        $this->db->query("INSERT INTO " . DB_PREFIX . $check . "_button_delegate "
                                . "(" . $check . "_button_uid, document_uid, structure_uid) "
                                . "VALUES " . implode(",", $sqls));
                    }
                }
            }
        }
    }

    public function getButtons($document_uid) {
        //выдает все кнопки на текущей точке маршрута
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "route_button rb "
                . "LEFT JOIN " . DB_PREFIX . "route_button_description rbd ON (rb.route_button_uid = rbd.route_button_uid AND rbd.language_id = '" . (int) $this->config->get('config_language_id') . "') "
                . "WHERE rb.route_uid = (SELECT DISTINCT route_uid FROM document WHERE document_uid = '" . $this->db->escape($document_uid) . "') "
                . "AND rb.route_button_uid IN (SELECT route_button_uid FROM " . DB_PREFIX . " route_button_delegate WHERE (document_uid = '" . $this->db->escape($document_uid) . "' OR document_uid = '0') AND structure_uid IN ('" . implode("','", $this->customer->getStructureIds()) . "')) "
                . "ORDER BY sort ASC");
        $result = array();
        foreach ($query->rows as $route_button) {
            $result[] = array(
                'route_button_uid' => $route_button['route_button_uid'],
                'route_uid' => $route_button['route_uid'],
                'picture' => $route_button['picture'],
                'hide_button_name' => $route_button['hide_button_name'],
                'color' => $route_button['color'],
                'background' => $route_button['background'],
                'action' => $route_button['action'],
                'action_log' => $route_button['action_log'],
                'action_move_route_uid' => $route_button['action_move_route_uid'],
                'show_after_execute' => $route_button['show_after_execute'],
                'action_params' => unserialize($route_button['action_params']),
                'name' => $route_button['name'],
                'description' => $route_button['description']
            );
        }
        return $result;
    }

    public function getButton($button_uid) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "route_button rb "
                . "LEFT JOIN " . DB_PREFIX . "route_button_description rbd ON (rb.route_button_uid = rbd.route_button_uid AND rbd.language_id = '" . (int) $this->config->get('config_language_id') . "') "
                . "WHERE rb.route_button_uid = '" . $this->db->escape($button_uid) . "' ");
        if ($query->num_rows) {
            return array(
                'route_uid' => $query->row['route_uid'],
                'picture' => $query->row['picture'],
                'hide_button_name' => $query->row['hide_button_name'],
                'color' => $query->row['color'],
                'background' => $query->row['background'],
                'action' => $query->row['action'],
                'action_log' => $query->row['action_log'],
                'action_move_route_uid' => $query->row['action_move_route_uid'],
                'show_after_execute' => $query->row['show_after_execute'],
                'action_params' => unserialize($query->row['action_params']),
                'name' => $query->row['name'],
                'description' => $query->row['description']
            );
        } else {
            return array();
        }
    }

    public function getFirstRoute($doctype_uid) {
        $query = $this->db->query("SELECT route_uid FROM " . DB_PREFIX . "route WHERE doctype_uid = '" . $this->db->escape($doctype_uid) . "' ORDER BY sort ASC LIMIT 0,1");
        if (isset($query->row['route_uid'])) {
            return $query->row['route_uid'];
        } else {
            return 0;
        }
    }

    public function getRouteActions($route_uid) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "route_action WHERE route_uid = '" . $this->db->escape($route_uid) . "' AND draft = 0 AND status = 1 ORDER BY sort ASC");
        $result = array();
        foreach ($query->rows as $route) {
            $result[$route['context']][] = array(
                'action' => $route['action'],
                'action_log' => $route['action_log'],
                'params' => unserialize($route['params'])
            );
        }
        return $result;
    }

    public function moveRoute($document_uid, $route_uid) {
        if ($route_uid && $document_uid) {
            //проверим наличие точки $route_uid
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "route WHERE route_uid = '" . $this->db->escape($route_uid) . "' AND doctype_uid = (SELECT doctype_uid FROM " . DB_PREFIX . "document WHERE document_uid='" . $this->db->escape($document_uid) . "')");
            if ($query->num_rows) {
                $this->db->query("UPDATE " . DB_PREFIX . "document SET route_uid = '" . $this->db->escape($route_uid) . "' WHERE document_uid = '" . $this->db->escape($document_uid) . "' ");
                $this->load->model('doctype/doctype');
                $route_info = $this->model_doctype_doctype->getRoute($route_uid);
                $this->load->model('tool/utils');
                $this->model_tool_utils->addLog($document_uid, 'route', "", "", $route_info['name']);                
                return TRUE;
            } else {
                return FALSE;
            }
        }
        return FALSE;
    }

    public function getChildDocuments($document_uid, $parent_field_uid) {
        $this->load->model('doctype/doctype');
        $field_info = $this->model_doctype_doctype->getField($parent_field_uid);
        $query = $this->db->query("SELECT fvp.document_uid FROM " . DB_PREFIX . "field_value_" . $this->db->escape($field_info['type']) . " fvp "
                . "WHERE fvp.field_uid = '" . $this->db->escape($parent_field_uid) . "' AND fvp.value = '" . $this->db->escape($document_uid) . "' "
        );
        $result = array();
        foreach ($query->rows as $document) {
            $result[] = $document['document_uid'];
            $result = array_merge($result, $this->getChildDocuments($document['document_uid'], $parent_field_uid));
        }
        return $result;
    }

    public function addAccess($subject_uid, $document_uid) {
        
    }

    public function removeAccess($subject_uid, $document_uid) {
        
    }

    public function removeDocument($document_uid) {
        //получаем все поля типа данного документа
        //проверяем, не удаляет ли пользователь свой структурный ID
        
        if ((is_null($this->customer->getStructureIds()) || array_search($document_uid, $this->customer->getStructureIds()) === FALSE) && $this->customer->getId() != $document_uid) {
            $field_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "field WHERE doctype_uid = (SELECT doctype_uid FROM " . DB_PREFIX . "document WHERE document_uid='" . $this->db->escape($document_uid) . "')");
            foreach ($field_query->rows as $field) {
                $model = "model_extension_field_" . $field['type'];
                $this->load->model('extension/field/' . $field['type']);
                $this->$model->removeValue($field['field_uid'], $document_uid);
            }
            $this->db->query("DELETE FROM " . DB_PREFIX . "document WHERE document_uid = '" . $this->db->escape($document_uid) . "' ");
            $this->db->query("DELETE FROM " . DB_PREFIX . "document_access WHERE document_uid = '" . $this->db->escape($document_uid) . "' ");            
        } else {
            $this->load->language('document/document');
            return $this->language->get('text_remove_self_structure');
        }
    }

    public function getVariable($variable, $document_uid = 0) {
        
        if (!$document_uid && !empty($this->request->get['document_uid'])) {
            $document_uid = $this->request->get['document_uid'];
        }
        switch ($variable) {
            case 'var_author_name':
                if (!empty($document_uid)) {
                    $document_info = $this->getDocument($document_uid, false);
                    $this->load->model('account/customer');
                    return $this->model_account_customer->getCustomerName($document_info['author_uid']);
                } else {
                    return 0;
                } 
            case 'var_author_uid':
                if (!empty($document_uid)) {
                    $document_info = $this->getDocument($document_uid, false);
                    return $document_info['author_uid'];
                } else {
                    return 0;
                }
            case 'var_customer_id':
            case 'var_customer_uid': 
                $structureId = $this->customer->getStructureId();
                if (!$structureId) {
                    return 0;
                }
                return $structureId;
            case 'var_customer_uids': 
                $structureIds = $this->customer->getStructureIds();
                if (!$structureIds) {
                    return "";
                }
                return implode(",",$structureIds);
            case 'var_customer_user_uid':
                $id = $this->customer->getId();
                if (!$id) {
                    return 0;
                }
                return $id;
            case 'var_customer_name':
                return $this->customer->getName();
            case 'var_current_route_uid':
                if (!empty($document_uid)) {
                    $document_info = $this->getDocument($document_uid, false);
                    return $document_info['route_uid'];
                } else {
                    return 0;
                }
            case 'var_current_route_name':
                if (!empty($document_uid)) {
                    $document_info = $this->getDocument($document_uid, false);
                    $this->load->model('doctype/doctype');
                    $route_descriptions = $this->model_doctype_doctype->getRouteDescriptions($document_info['route_uid']);
                    if (isset($route_descriptions[$this->config->get('config_language_id')]['name'])) {
                        return $route_descriptions[$this->config->get('config_language_id')]['name'];
                    } else {
                        return 0;
                    }
                } else {
                    return 0;
                }
            case 'var_current_route_description':
                if (!empty($document_uid)) {
                    $document_info = $this->getDocument($document_uid, false);
                    $this->load->model('doctype/doctype');
                    $route_descriptions = $this->model_doctype_doctype->getRouteDescriptions($document_info['route_uid']);
                    if (isset($route_descriptions[$this->config->get('config_language_id')]['description'])) {
                        return $route_descriptions[$this->config->get('config_language_id')]['description'];
                    } else {
                        return 0;
                    }
                } else {
                    return 0;
                } 
            case 'var_current_date':                               
                $timezone = new DateTimeZone($this->config->get('date.timezone'));
                $dbformat = 'Y-m-d';
                $date = new DateTime('now', $timezone);
                $value = $date->format($dbformat);
                return $value . " 00:00:00";
            case 'var_current_time':                               
                $timezone = new DateTimeZone($this->config->get('date.timezone'));
                $dbformat = 'Y-m-d H:i:s';
                $date = new DateTime('now', $timezone);
                $value = $date->format($dbformat);
                return $value;
            case 'var_current_locale_datetime':                               
                $timezone = new DateTimeZone($this->config->get('date.timezone'));
                $dbformat = $this->language->get('datetime_format');
                $date = new DateTime('now', $timezone);
                $value = $date->format($dbformat);
                return $value;
            case 'var_current_locale_time':                               
                $timezone = new DateTimeZone($this->config->get('date.timezone'));
                $dbformat = $this->language->get('time_format');
                $date = new DateTime('now', $timezone);
                $value = $date->format($dbformat);
                return $value;
            case 'var_current_locale_date':                               
                $timezone = new DateTimeZone($this->config->get('date.timezone'));
                $dbformat = $this->language->get('date_format_short');
                $date = new DateTime('now', $timezone);
                $value = $date->format($dbformat);
                return $value;
            case 'var_current_document_uid':
                return $document_uid;
            case 'var_current_button_uid':
                return $this->session->data['current_button_uid'] ?? "";
            case 'var_change_field_uid':
                return !empty($this->request->get['field_uid']) ? $this->request->get['field_uid'] : 0;
            case 'var_change_field_value':
                return !empty($this->request->get['field_value']) ? $this->request->get['field_value'] : "";
            case 'var_current_folder_uid':
                return $this->request->get['folder_uid'] ?? "";
            default:
                break;
        }
    }

    /**
     * Метод возвращает подписки на изменение для field_uid с возможным указанием document_uid
     * @param type $field_uid
     * @param type $document_uid
     */
    public function getSubscriptions($field_uid, $document_uid="") {
        $sql = "SELECT * FROM " . DB_PREFIX . "field_change_subscription WHERE field_uid = '" . $this->db->escape($field_uid) ."' ";
        if ($document_uid) {
            $sql .= "AND document_uid = '" . $this->db->escape($document_uid) . "' ";
        }
        $query = $this->db->query($sql);
        if ($query->num_rows) {
            return $query->rows;
        } else {
            return array();
        }
    }
    
    /**
     * Обновление подписок полей на изменения. 
     * @param type $subscriptions - массив ('subscription_field_uid','subscription_document_uid')
     */
    public function updateSubscription($subscriptions) {
        if ($subscriptions) {
            $this->load->model('doctype/doctype');

            $fields = array();
            $load_models = array();
            foreach ($subscriptions as $subscription) {
                $fields[$subscription['subscription_field_uid']][] = $subscription['subscription_document_uid'];
            }
            foreach ($fields as $field_uid => $documents) {
                $field_info = $this->model_doctype_doctype->getField($field_uid);
                if (empty($field_info['type'])) {
                    $this->model_doctype_doctype->delSubscription($field_uid);
                    continue;
                }
                $documents = array_unique($documents);
                $model = "model_extension_field_" . $field_info['type'];
                if (empty($load_models[$field_info['type']])) {
                    $this->load->model('extension/field/' . $field_info['type']);
                    $load_models[$field_info['type']] = 1;
                }
                $this->$model->subscription($field_uid, $documents);
            }
        }          
        
    }
    
}
