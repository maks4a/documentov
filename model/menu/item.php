<?php

class ModelMenuItem extends Model {
    public function getMenuItems($item_id = 0) {
        $query = $this->db->query("SELECT DISTINCT(mi.menu_item_id), mid.name, mi.type, mi.image, mi.hide_name, mi.action, mi.action_value FROM " . DB_PREFIX . "menu_item mi "
                . "LEFT JOIN menu_item_description mid ON (mid.menu_item_id = mi.menu_item_id AND mid.language_id = '" . $this->db->escape($this->config->get('config_language_id')) . "') "
                . "LEFT JOIN menu_item_delegate midlg ON (midlg.menu_item_id = mi.menu_item_id) "
                . "WHERE "
                . "mi.parent_id = '" . (int) $item_id . "' "
                . "AND mi.status=1 "
                . "AND midlg.structure_uid IN ('" . implode("','", $this->customer->getStructureIds()) . "') "
                . "ORDER BY mi.sort_order ASC");
        $result = array();
        foreach ($query->rows as $item) {
            $result[] = array(
                'name'          => $item['name'],
                'type'          => $item['type'],
                'image'         => $item['image'],
                'hide_name'     => $item['hide_name'],
                'action'        => $item['action'],
                'action_value'  => $item['action_value'],
                'children'      => $this->getMenuItems($item['menu_item_id'])
            );
        }
        return $result;

    }
    public function getItems($data = array()) {
        $sql = "SELECT mip.menu_item_id AS menu_item_id, GROUP_CONCAT(mid1.name ORDER BY mip.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS name, "
                . "mi1.parent_id, mi1.image, mi1.sort_order "
                . "FROM " . DB_PREFIX . "menu_item_path mip "
                . "LEFT JOIN " . DB_PREFIX . "menu_item mi1 ON (mip.menu_item_id = mi1.menu_item_id) "
                . "LEFT JOIN " . DB_PREFIX . "menu_item mi2 ON (mip.path_id = mi2.menu_item_id) "
                . "LEFT JOIN " . DB_PREFIX . "menu_item_description mid1 ON (mip.path_id = mid1.menu_item_id) "
                . "LEFT JOIN " . DB_PREFIX . "menu_item_description mid2 ON (mip.menu_item_id = mid2.menu_item_id) "
                . "WHERE mid1.language_id = '" . (int) $this->config->get('config_language_id') . "' "
                . "AND mid2.language_id = '" . (int) $this->config->get('config_language_id') . "'";
        if (!empty($data['filter_name'])) {
            $sql .= " AND mid2.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " GROUP BY mip.menu_item_id";

        $sort_data = array(
            'name',
            'sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY sort_order";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }
//echo $sql;exit;
        $query = $this->db->query($sql);

        return $query->rows;
    }    
 public function addItem($data) {
//        print_r($data);exit;
        $sql = "INSERT INTO " . DB_PREFIX . "menu_item SET ";
        if ($data['type'] == "text") {
            $sql .= "type = 'text', "
                    . "action = '" . $this->db->escape($data['action']) . "', "
                    . "action_value = '" . ($data['action'] == 'folder' ? $this->db->escape($data['action_id']) : $this->db->escape($data['action_value'])) . "', "
                    . "image = '" . ($this->db->escape($data['image']) ?? "") . "', "
                    . "hide_name = '" . ((int) $data['hide_name'] ?? 0)  . "', "
                    . "parent_id = '" . (int) $data['parent_id'] . "', "
                    . "sort_order = '" . (int) $data['sort_order'] . "', "
                    . "status = '" . (int) $data['status'] . "' ";
                
        } else {
            $sql .= "type = 'divider', action = '', action_value = '', image = '', parent_id = '" . (int) $data['parent_id'] . "', sort_order = '" . (int) $data['sort_order'] . "', status = '" . (int) $data['status'] . "' ";
            $this->load->model('localisation/language');
            foreach ($this->model_localisation_language->getLanguages() as $language) {
                $data['item_description'][$language['language_id']] = "_";
            }
        }
        
        
        $this->db->query($sql);

        $item_id = $this->db->getLastId();

        foreach ($data['item_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "menu_item_description SET "
                    . "menu_item_id = '" . (int) $item_id . "', "
                    . "language_id = '" . (int) $language_id . "', "
                    . "name = '" . $this->db->escape($value['name'] ?? "-") . "' ");
        }
        if (!empty($data['item_structure'])) {
            foreach ($data['item_structure'] as $structure_uid) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "menu_item_delegate SET "
                        . "menu_item_id = '" . (int) $item_id . "', "
                        . "structure_uid = '" . $this->db->escape($structure_uid) . "' "
                        );
            }                    
        }

        // MySQL Hierarchical Data Closure Table Pattern
        $level = 0;

        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "menu_item_path` WHERE menu_item_id = '" . (int) $data['parent_id'] . "' ORDER BY `level` ASC");

        foreach ($query->rows as $result) {
            $this->db->query("INSERT INTO `" . DB_PREFIX . "menu_item_path` SET "
                    . "`menu_item_id` = '" . (int) $item_id . "', "
                    . "`path_id` = '" . (int) $result['path_id'] . "', "
                    . "`level` = '" . (int) $level . "'");
            $level++;
        }

        $this->db->query("INSERT INTO `" . DB_PREFIX . "menu_item_path` SET "
                . "`menu_item_id` = '" . (int) $item_id . "', "
                . "`path_id` = '" . (int) $item_id . "', "
                . "`level` = '" . (int) $level . "'");

        $this->cache->delete('item');
        return $item_id;
    }

    public function editItem($item_id, $data) {
        $sql = "UPDATE " . DB_PREFIX . "menu_item SET ";
        if ($data['type'] == "text") {
            $sql .= "type = 'text', "
                    . "action = '" . $this->db->escape($data['action']) . "', "
                    . "action_value = '" . ($data['action'] == 'folder' ? $this->db->escape($data['action_id']) : $this->db->escape($data['action_value'])) . "', "
                    . "image = '" . ($this->db->escape($data['image']) ?? "") . "', "
                    . "hide_name = '" . ((int) $data['hide_name'] ?? 0) . "', "
                    . "parent_id = '" . (int) $data['parent_id'] . "', "
                    . "sort_order = '" . (int) $data['sort_order'] . "', "
                    . "status = '" . (int) $data['status'] . "' ";
                
        } else {
            $sql .= "type = 'divider', action = '', action_value = '', image = '', parent_id = '" . (int) $data['parent_id'] . "', sort_order = '" . (int) $data['sort_order'] . "', status = '" . (int) $data['status'] . "' ";
            $this->load->model('localisation/language');
            foreach ($this->model_localisation_language->getLanguages() as $language) {
                $data['item_description'][$language['language_id']] = "_";
            }            
        }
        $sql .= "WHERE menu_item_id = '" . (int) $item_id . "'";
        $this->db->query($sql);

        $this->db->query("DELETE FROM " . DB_PREFIX . "menu_item_description WHERE menu_item_id = '" . (int) $item_id . "'");

        foreach ($data['item_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "menu_item_description SET "
                    . "menu_item_id = '" . (int) $item_id . "', "
                    . "language_id = '" . (int) $language_id . "', "
                    . "name = '" . $this->db->escape($value['name'] ?? "-") . "' ");
        }
        
        $this->db->query("DELETE FROM " . DB_PREFIX . "menu_item_delegate WHERE menu_item_id = '" . (int) $item_id . "'");
        if (!empty($data['item_structure'])) {
            foreach ($data['item_structure'] as $structure_uid) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "menu_item_delegate SET "
                        . "menu_item_id = '" . (int) $item_id . "', "
                        . "structure_uid = '" . $this->db->escape($structure_uid) . "' "
                        );
            }            
        }

        // MySQL Hierarchical Data Closure Table Pattern
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "menu_item_path` WHERE path_id = '" . (int) $item_id . "' ORDER BY level ASC");

        if ($query->rows) {
            foreach ($query->rows as $item_path) {
                // Delete the path below the current one
                $this->db->query("DELETE FROM `" . DB_PREFIX . "menu_item_path` WHERE "
                        . "menu_item_id = '" . (int) $item_path['menu_item_id'] . "' AND "
                        . "level < '" . (int) $item_path['level'] . "' ");

                $path = array();

                // Get the nodes new parents
                $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "menu_item_path` WHERE "
                        . "menu_item_id = '" . (int) $data['parent_id'] . "' ORDER BY level ASC");

                foreach ($query->rows as $result) {
                    $path[] = $result['path_id'];
                }

                // Get whats left of the nodes current path
                $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "menu_item_path` WHERE "
                        . "menu_item_id = '" . (int) $item_path['menu_item_id'] . "' ORDER BY level ASC");

                foreach ($query->rows as $result) {
                    $path[] = $result['path_id'];
                }

                // Combine the paths with a new level
                $level = 0;

                foreach ($path as $path_id) {
                    $this->db->query("REPLACE INTO `" . DB_PREFIX . "menu_item_path` SET "
                            . "menu_item_id = '" . (int) $item_path['menu_item_id'] . "', "
                            . "`path_id` = '" . (int) $path_id . "', "
                            . "level = '" . (int) $level . "'");

                    $level++;
                }
            }
        } else {
            // Delete the path below the current one
            $this->db->query("DELETE FROM `" . DB_PREFIX . "menu_item_path` WHERE menu_item_id = '" . (int) $item_id . "'");

            // Fix for records with no paths
            $level = 0;

            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "menu_item_path` WHERE "
                    . "menu_item_id = '" . (int) $data['parent_id'] . "' ORDER BY level ASC");

            foreach ($query->rows as $result) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "menu_item_path` SET "
                        . "menu_item_id = '" . (int) $item_id . "', "
                        . "`path_id` = '" . (int) $result['path_id'] . "', "
                        . "level = '" . (int) $level . "'");

                $level++;
            }

            $this->db->query("REPLACE INTO `" . DB_PREFIX . "menu_item_path` SET "
                    . "menu_item_id = '" . (int) $item_id . "', "
                    . "`path_id` = '" . (int) $item_id . "', "
                    . "level = '" . (int) $level . "'");
        }



        $this->cache->delete('item');
    }

    public function deleteItem($item_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "menu_item_path WHERE menu_item_id = '" . (int) $item_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "menu_item_path WHERE path_id = '" . (int) $item_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "menu_item WHERE menu_item_id = '" . (int) $item_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "menu_item_description WHERE menu_item_id = '" . (int) $item_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "menu_item_delegate WHERE menu_item_id = '" . (int) $item_id . "'");
//        $this->cache->delete('item');
    }

    public function getItem($item_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "menu_item mi WHERE mi.menu_item_id = '" . (int) $item_id . "' ");
        if ($query->num_rows) {
            return array(
                'type'          => $query->row['type'],
                'action'        => $query->row['action'],
                'action_value'  => $query->row['action_value'],
                'parent_id'     => $query->row['parent_id'],
                'image'         => $query->row['image'],
                'hide_name'     => $query->row['hide_name'],
                'sort_order'    => $query->row['sort_order'],
                'status'        => $query->row['status'],
                'description'   => $this->getItemDescriptions($item_id),
                'delegate'      => $this->getItemDelegate($item_id)
            );
        } else {
            return array();
        }
    }



    public function getItemDescriptions($item_id) {
        $item_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "menu_item_description WHERE menu_item_id = '" . (int) $item_id . "'");

        foreach ($query->rows as $result) {
            $item_description_data[$result['language_id']] = array(
                'name' => $result['name']
            );
        }

        return $item_description_data;
    }

    public function getItemDelegate($item_id) {
        $item_delegate_data = array();

        $query = $this->db->query("SELECT mid.structure_uid as structure_uid, fvn.value as name FROM " . DB_PREFIX . "menu_item_delegate mid "
                . "LEFT JOIN " . DB_PREFIX . "field_value_" . $this->config->get('structure_field_name_type') . " fvn ON (fvn.field_uid = '" . $this->config->get('structure_field_name_id') . "' AND fvn.document_uid = mid.structure_uid) "
                . "WHERE mid.menu_item_id = '" . (int) $item_id . "'");

        foreach ($query->rows as $result) {
            $item_delegate_data[] = array(
                'structure_uid'     => $result['structure_uid'],
                'name'              => $result['name']
            );
        }

        return $item_delegate_data;
    }

    public function getTotalItems() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "menu_item");

        return $query->row['total'];
    }

    
}